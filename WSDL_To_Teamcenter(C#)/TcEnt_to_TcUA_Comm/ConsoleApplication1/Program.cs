﻿using System;
using System.Collections.Generic;
using System.Text;
//using TcUAComm.Core_2008_06_Session;
using TcUAComm.Core_2006_03_Session;
using TcUAComm.RS7_Custom;

namespace TcUAComm
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Net.CookieContainer cc = new System.Net.CookieContainer();

            //Core0806SessionService seService = new Core0806SessionService();

            Core0603SessionService seService = new Core0603SessionService();

            seService.CookieContainer = new System.Net.CookieContainer();

            LoginInput lInput = new LoginInput();

            Console.Write("Please enter the User Name:");           
            lInput.username = Console.ReadLine();

            Console.Write("Please enter the Password:");
            lInput.password = Console.ReadLine();

            Console.Write("Please enter the login group:");
            lInput.group = Console.ReadLine(); 

            LoginResponse lResponse = new LoginResponse();
            lResponse = seService.login(lInput);

            Console.Write("###############Login Success############\n");

            cc = seService.CookieContainer;

            //Console.WriteLine(cc.ToString());

            /* Calling Custom Service */
            Custom1507RS7DataManagementService rs_dmService = new Custom1507RS7DataManagementService();
            rs_dmService.ConnectionGroupName = seService.ConnectionGroupName;
            rs_dmService.Credentials = seService.Credentials;
            rs_dmService.UserAgent = seService.UserAgent;
            rs_dmService.UseDefaultCredentials = true;
            rs_dmService.CookieContainer = cc;
                       
            CreateOrUpdateInput createRequest = new CreateOrUpdateInput();
            CreateOrUpdateIn InputRequest = new CreateOrUpdateIn();

            Console.Write("Please enter Client Id:");
            InputRequest.clientId = Console.ReadLine();

            //Console.Write("Please enter Project Desc:");
            //InputRequest.projectName = Console.ReadLine();

            Console.Write("Please enter Nomenclature value :");
            InputRequest.nomenclature = Console.ReadLine();
            
            createRequest.input = InputRequest;

            CreateOrUpdateResponse theResponse = rs_dmService.createOrUpdate(createRequest);

            Console.Write("Response contains a property policy. The properties returned are");

            Console.Write("Do you want to create Dataset & attach with the Object created ? ");
            
            string strKey  = Console.ReadLine();
                        
            if ( strKey.Equals("Y") )
            {
                CreateDatasetInput createDSInputReq = new CreateDatasetInput();
                CreateDatasetIn createDSIn = new CreateDatasetIn();
                
               
                createDSIn.clientId = InputRequest.clientId;
                createDSIn.strName = "TcEntDS";
                createDSIn.strType = "Text";

                foreach (RS7_Custom.ModelObject m in theResponse.ServiceData.dataObjects)
                    {
                        if (m.properties != null)
                        {
                            foreach (RS7_Custom.Property p in m.properties)
                            {
                                Console.WriteLine("Property Name: " + p.name);

                                if( p.name.Equals("item_id") )
                                {
                                    if (p.values != null)
                                    {
                                        foreach (RS7_Custom.PropertyValue v in p.values)
                                        {
                                            Console.WriteLine("Property Value: "+ v.value);

                                            createDSIn.strEnterpriseItemId = v.value;
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }

                  createDSInputReq.createRSDatasetInput = createDSIn;
                  CreateDatsetResponse theDSResponse = rs_dmService.createDataset(createDSInputReq);

                  theDSResponse.ServiceData.partialErrors.Equals("");

                  Console.WriteLine("\nA TcUA Item: {0} is created.Please login to teamcenter & verify\n", createDSIn.strEnterpriseItemId);
            }
            else
            {
                Console.Write("\nDataset not created.\n");
                Console.WriteLine("\nA TcUA Item is created.Please login to teamcenter & verify\n");
            }

            /* Print all properties & values for the created object.
             
            foreach (Custom_15_07_RS7_DataManagement.ModelObject m in theResponse.ServiceData.dataObjects)
            {
                if (m.properties != null)
                {
                    foreach (Custom_15_07_RS7_DataManagement.Property p in m.properties)
                    {
                        Console.WriteLine("Property Name: " + p.name);
                        if (p.values != null)
                        {
                            foreach (Custom_15_07_RS7_DataManagement.PropertyValue v in p.values)
                            {
                                Console.WriteLine("Property Value: "+ v.value);
                            }
                        }
                    }
                }
            }
        */
            /*Logout*/
            LogoutInput lOutput = new LogoutInput();

            seService.logout(lOutput);    
      
            Console.ReadKey();

        }
    }
}
