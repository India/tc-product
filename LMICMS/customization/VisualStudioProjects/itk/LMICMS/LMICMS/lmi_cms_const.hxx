/*======================================================================================================================================================================
                                                    Copyright 2020  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

 File Description:

 Filename:     lmi_cms_const.hxx

 Description:  Header File for constants used during scope of LMICMSUTILS.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-May-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_CONST_HXX
#define LMI_CONST_HXX

/* Arguments */
#define LMI_USERNAME                                   "u"
#define LMI_PASSWORD                                   "p"
#define LMI_GROUP                                      "g"
#define LMI_MODE_QUERY                                 "query"
#define LMI_OPERATION_EXPORT                           "export"
#define LMI_QUERYNAME                                  "queryname"
#define LMI_EXPORTQUERYNAME                            "export_query_name"
#define LMI_FILENAME                                   "filename"
#define LMI_FILEPATH                                   "filepath"
#define LMI_MODE_WORKFLOW                              "workflow"
#define LMI_EXPORTWORKFLOWNAME                         "export_workflow_name"
#define LMI_MODE_TRANSFER_MODE                         "transfer_mode"
#define LMI_EXPORTTRANSFERMODENAME                     "export_transfer_mode_name"
#define LMI_MODE_STYLESHEET                            "stylesheet"
#define LMI_EXPORTSTYLESHEETNAME                       "export_stylesheet_name"

/* Separators */
#define LMI_STRING_COMMA                               ","

/* Query Names  */
#define LMI_ASK_QUERY_NAME                             "__WEB_ask_query"
#define LMI_EXTRACT_DATASET_NAME                       "__lmicms_extract_dataset"

/* String Values  */
#define LMI_NEW_VALUE                                  "*"

/* File Path */
#define LMI_FILE_PATH                                  "\\"
#define LMI_NEW_LINE                                   '\n'

/* Workflow */
constexpr auto LMI_WF_TEMPL_STAGE_READY_TO_USE         = 2;
#define LMI_EXPORTWORKFLOWTEMPLNAME                    "template_name"

#endif

