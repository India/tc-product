/*======================================================================================================================================================================
													Copyright 2020  LMTec India Consulting Pvt Ltd
													Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_itk_mem_free.hxx

Description:  This file contains macros for memory operations.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-May-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_ITK_MEM_FREE_HXX
#define LMI_ITK_MEM_FREE_HXX

#include <lmi_cms_library.hxx>

/* We want to use our macros. If some others are defined under same name, undefine them */

#ifdef LMI_MEM_TCFREE
#undef LMI_MEM_TCFREE
#endif

/* Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc */

#define LMI_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }


#ifdef LMI_UPDATE_TAG_ARRAY
#undef LMI_UPDATE_TAG_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc.
   IMPORTANT: Always initialize the pointer to NULL if memory allocation is done for the very first time
*/
#define LMI_UPDATE_TAG_ARRAY(iCurrentArrayLen, ptArray, tValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ptArray = (tag_t*) MEM_alloc(iCurrentArrayLen * sizeof(tag_t));\
            }\
            else{\
                ptArray = (tag_t*) MEM_realloc(ptArray, iCurrentArrayLen * sizeof(tag_t));\
            }\
            ptArray[iCurrentArrayLen-1]=  tValue;\
}

#ifdef LMI_CLONE_TAG_ARRAY
#undef LMI_CLONE_TAG_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc.
   IMPORTANT: Always initialize the pointer to NULL if memory allocation is done for the very first time
*/
#define LMI_CLONE_TAG_ARRAY(iNewArrayLen, ptNewArray, iSrcArrayLen, ptSrcArray){\
            for(int iDx = 0; iDx < iSrcArrayLen; iDx++){\
                    iNewArrayLen++;\
                    if(iNewArrayLen == 1){\
                        ptNewArray = (tag_t*) MEM_alloc(iNewArrayLen * sizeof(tag_t));\
                    }\
                    else{\
                        ptNewArray = (tag_t*) MEM_realloc(ptNewArray, iNewArrayLen * sizeof(tag_t));\
                    }\
                    ptNewArray[iNewArrayLen-1]=  ptSrcArray[iDx];\
            }\
}

#endif








