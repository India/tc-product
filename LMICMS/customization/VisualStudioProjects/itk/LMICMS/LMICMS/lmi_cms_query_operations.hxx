/*======================================================================================================================================================================
                                                    Copyright 2020  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_query_operations.hxx

Description:  Header File containing declarations of function present in lmi_cms_query_operations.cxx
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-May-20     Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_QUERY_OPERATIONS_HXX
#define LMI_QUERY_OPERATIONS_HXX

int LMI_store_query_name
(
    char*   pszQueryName,
    char*   pszExportQueryName,
    char*   pszSeparator,
    int*    piQueryNameCount,
    char*** pppszQueryNamelist,
    int*    piQueryDispNameCount,
    char*** pppszQueryDispNamelist
);

int LMI_print_query_name_and_uid
(
    int    iQueryCount,
    int    iExpQueryCount,
    char*  pszFilenam,
    char*  pszFileloc,
    char** pszQueryname,
    char** pszExpQueryname
);

int LMI_create_file_for_query_name
(
    char* pszFilename,
    char* pszFilepath
);

int LMI_execute_saved_query
(
    int     iUserEntryCount,
    int*    ptNoOfObjFound,
    char*   pszQueryName,
    char**  ppszUserEntriesName,
    char**  ppszUserEntriesValue,
    tag_t** ptObjTag
);

int LMI_process_query_operation
(
    char* pszOperation,
    char* pszQueryName,
    char* pszExportQueryname,
    char* pszFilename,
    char* pszFilepath
);

#endif