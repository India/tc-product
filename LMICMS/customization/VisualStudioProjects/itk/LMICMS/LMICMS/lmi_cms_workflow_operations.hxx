/*======================================================================================================================================================================
                                                    Copyright 2020  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_workflow_operations.hxx

Description:  Header File containing declarations of function present in lmi_cms_workflow_operations.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-July-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_WORKFLOW_OPERATIONS_HXX
#define LMI_WORKFLOW_OPERATIONS_HXX

int LMI_process_workflow_operation
(
    char* pszOperation,
    char* pszFilename,
    char* pszFilepath
);

int LMI_create_file_for_workflow_name
(
    char* pszFileName, 
    char* pszFilePath
);

#endif