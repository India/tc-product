﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LMICMS.Domain.Settings
{
    [XmlRoot(ElementName = "TC_USERS_DETAILS")]
    public class TCUsersDetails
    {
        [XmlElement(ElementName = "TC_USER")]
        public List<TCUser> TCUsers { get; set; }
    }

    [XmlRoot(ElementName = "TC_USER")]
    public class TCUser
    {
        [XmlAttribute(AttributeName = "USER_GUID")]
        public string UserGuid { get; set; }
        [XmlAttribute(AttributeName = "USER_ID")]
        public string UserId { get; set; }
        [XmlAttribute(AttributeName = "USER_PASSWORD")]
        public string UserPassword { get; set; }
        [XmlAttribute(AttributeName = "USER_GROUP")]
        public string UserGroup { get; set; }
        [XmlAttribute(AttributeName = "USER_ROLE")]
        public string UserRole { get; set; }



    }
}
