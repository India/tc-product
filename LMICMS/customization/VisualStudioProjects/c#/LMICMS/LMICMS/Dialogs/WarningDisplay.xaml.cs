﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LMICMS.Dialogs
{
    /// <summary>
    /// Interaction logic for WarningDisplay.xaml
    /// </summary>
    public partial class WarningDisplay : LMIWindow
    {
        string strOutMessage = "";
        string strOutMessg = "";
        string strOutMsg = "";
        public WarningDisplay(string strMessage, string strOperationMessage)
        {
            strOutMessage = "";
            strOutMessg = "";
            strOutMsg = "";
          //  strOutMessage = strInpMessage;
            strOutMessg = strMessage;
            strOutMsg = strOperationMessage;
           // this.ResizeMode = ResizeMode.CanResizeWithGrip;
            InitializeComponent();
        }
        private void DialogLoaded(object sender, RoutedEventArgs e)
        {
            lblTopBar.Content = strOutMsg;
            tb.Text = strOutMessg;
            //lb.Content = strOutMsg;
            // tb.Text = strOutMessage;      
        }

        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnCloseClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    
    }
}
