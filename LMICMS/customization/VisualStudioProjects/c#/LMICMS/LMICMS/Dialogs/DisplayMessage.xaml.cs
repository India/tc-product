﻿using System.Windows;

namespace LMICMS.Dialogs
{
    /// <summary>
    /// Interaction logic for DisplayMessage.xaml
    /// </summary>
    public partial class DisplayMessage : LMIWindow
    {
        string strOutMsg = "";
        public DisplayMessage( string strMsg)
        {
            strOutMsg = "";
            strOutMsg = strMsg;
            InitializeComponent();
        }

        private void DialogLoaded(object sender, RoutedEventArgs e)
        {
            tb.Text = strOutMsg;
        }
      

        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnCloseClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
