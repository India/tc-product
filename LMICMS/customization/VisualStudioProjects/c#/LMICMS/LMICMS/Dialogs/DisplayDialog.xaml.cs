﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LMICMS.Dialogs
{
    /// <summary>
    /// Interaction logic for DisplayDialog.xaml
    /// </summary>
    public partial class DisplayDialog : Window
    {
        public DisplayDialog()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
