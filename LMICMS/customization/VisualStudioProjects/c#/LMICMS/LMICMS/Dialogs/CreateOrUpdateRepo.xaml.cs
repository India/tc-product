﻿using LMICMS.Common.Utilities;
using LMICMS.Domain.Settings;
using LMICMS.Factories;
using LMICMS.Infrastructure;
using LMICMS.Models;
using LMICMS.Modules;
using LMICMS.UserControls;
using System;
using System.Windows;
using System.Windows.Controls;

namespace LMICMS.Dialogs
{
    /// <summary>
    /// Interaction logic for CreateOrUpdateRepo.xaml
    /// </summary>
    public partial class CreateOrUpdateRepo : LMIWindow
    {
        public String Message { get; set; }

        public CreateOrUpdateRepo(string strRepoGuid = "")
        {
            InitializeComponent();
            if (!String.IsNullOrEmpty(strRepoGuid))
            {
                Models.CreateOrUpdateRepo objRepoModel = SettingsModelFactory.GetRepoDetailsModel(AppConfig.ActiveProjectGuid, strRepoGuid);
                if (objRepoModel != null)
                {
                    txtRepoName.Text = objRepoModel.RepoName;
                    txtRepoPath.Text = objRepoModel.RepoPath;
                    txtWorflowPath.Text = objRepoModel.WorkflowPath;
                    txtAuthorizationPath.Text = objRepoModel.AuthorizationPath;
                    txtOrganizationPath.Text = objRepoModel.OrganizationPath;
                    txtACLPath.Text = objRepoModel.ACLPath;
                    txtPFFPath.Text = objRepoModel.PFFPath;
                    txtStylesheetPath.Text = objRepoModel.StylesheetPath;
                    txtPreferencePath.Text = objRepoModel.PreferencePath;
                    txtQueryPath.Text = objRepoModel.QueryPath;
                    txtTransferModePath.Text = objRepoModel.TransferModePath;
                    btnSave.CommandParameter = strRepoGuid;
                }
            }
                  //  ExportUC obj = new ExportUC();
        }

      
        private void BtnSetPath(object sender, RoutedEventArgs e)
        {
            Button btnSave = (Button)e.OriginalSource;
            String strButtonName = Convert.ToString(btnSave.CommandParameter);

            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.SelectedPath = txtRepoPath.Text.Trim();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (String.Compare("Repo", strButtonName) == 0)
                {
                    string strOldRepoPath = txtRepoPath.Text;
                    txtRepoPath.Text = dialog.SelectedPath;
                    if (String.Compare(strOldRepoPath, dialog.SelectedPath) != 0)
                    {
                        txtWorflowPath.Text = string.Empty;
                        txtAuthorizationPath.Text = string.Empty;
                        txtAuthorizationPath.Text = string.Empty;
                        txtACLPath.Text = string.Empty;
                        txtPFFPath.Text = string.Empty;
                        txtStylesheetPath.Text = string.Empty;
                        txtPreferencePath.Text = string.Empty;
                        txtQueryPath.Text = string.Empty;
                        txtTransferModePath.Text = string.Empty;
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(txtRepoPath.Text))
                    {
                        MessageBox.Show(ResourceHelper.FindResource("COUR.SetRepoPath"), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        if (!dialog.SelectedPath.Contains(txtRepoPath.Text))
                        {
                            MessageBox.Show(ResourceHelper.FindResource("COUR.InvalidPath"), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                        else
                        {
                            switch (strButtonName)
                            {
                                case Apps.Workflow:
                                    txtWorflowPath.Text = dialog.SelectedPath;
                                    break;
                                case Apps.Authorization:
                                    txtAuthorizationPath.Text = dialog.SelectedPath;
                                    break;
                                case Apps.Organization:
                                    txtOrganizationPath.Text = dialog.SelectedPath;
                                    break;
                                case Apps.ACL:
                                    txtACLPath.Text = dialog.SelectedPath;
                                    break;
                                case Apps.PFF:
                                    txtPFFPath.Text = dialog.SelectedPath;
                                    break;
                                case Apps.Stylesheet:
                                    txtStylesheetPath.Text = dialog.SelectedPath;
                                    break;
                                case Apps.Preference:
                                    txtPreferencePath.Text = dialog.SelectedPath;
                                    break;
                                case Apps.Query:
                                    txtQueryPath.Text = dialog.SelectedPath;
                                    break;
                                case Apps.TransferMode:
                                    txtTransferModePath.Text = dialog.SelectedPath;
                                    break;
                            }
                        }
                    }
                }
            }

            e.Handled = true;
        }

        private void BtnSaveClick(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(txtRepoName.Text) && !String.IsNullOrEmpty(txtRepoPath.Text))
            {
                Button btnSave = (Button)e.OriginalSource;
                String strRepoGuid = Convert.ToString(btnSave.CommandParameter);

                Repo repo = new Repo();
                repo.RepoName = txtRepoName.Text;
                repo.RepoPath = txtRepoPath.Text;
                if (!String.IsNullOrEmpty(txtWorflowPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Apps.Workflow, AppDataPath = txtWorflowPath.Text });
                }
                if (!String.IsNullOrEmpty(txtAuthorizationPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Apps.Authorization, AppDataPath = txtAuthorizationPath.Text });
                }
                if (!String.IsNullOrEmpty(txtOrganizationPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Apps.Organization, AppDataPath = txtOrganizationPath.Text });
                }
                if (!String.IsNullOrEmpty(txtACLPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Apps.ACL, AppDataPath = txtACLPath.Text });
                }
                if (!String.IsNullOrEmpty(txtPFFPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Apps.PFF, AppDataPath = txtPFFPath.Text });
                }
                if (!String.IsNullOrEmpty(txtStylesheetPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Apps.Stylesheet, AppDataPath = txtStylesheetPath.Text });
                }
                if (!String.IsNullOrEmpty(txtPreferencePath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Apps.Preference, AppDataPath = txtPreferencePath.Text });
                }
                if (!String.IsNullOrEmpty(txtQueryPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Apps.Query, AppDataPath = txtQueryPath.Text });
                }
                if (!String.IsNullOrEmpty(txtTransferModePath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Apps.TransferMode, AppDataPath = txtTransferModePath.Text });
                }

                if (String.IsNullOrEmpty(strRepoGuid))
                {
                    repo.RepoGuid = Guid.NewGuid().ToString();
                    SettingsModule.SaveRepo(AppConfig.ActiveProjectGuid, repo);
                }
                else
                {
                    repo.RepoGuid = strRepoGuid;
                    SettingsModule.SaveRepo(AppConfig.ActiveProjectGuid, repo);
                }

                MessageBox.Show(ResourceHelper.FindResource("COUR.RepoSaved"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);

                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show(ResourceHelper.FindResource("COUR.PathNameMandate"), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            e.Handled = true;
        }
        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            e.Handled = true;
        }
    }
}
