﻿using LMICMS.Infrastructure;
using System;
using System.Configuration;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace LMICMS.Dialogs
{
    public class LMIWindow:Window
    {
        public LMIWindow()
        {
            this.FontFamily = new FontFamily(ConfigurationManager.AppSettings[AppSettingKeys.DefaultFontFamily]);
            this.MouseDown += new MouseButtonEventHandler(WindowMouseDown);
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.ResizeMode = ResizeMode.NoResize;
            this.WindowStyle = WindowStyle.None;
            this.BorderThickness = new Thickness(1);
        }

        private void WindowMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

    }
}
