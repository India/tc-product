﻿using System.Windows;

namespace LMICMS.Dialogs
{
    /// <summary>
    /// Interaction logic for Display.xaml
    /// </summary>
    
    public partial class Display : LMIWindow
    {
        string strOutMessage = "";
        string strOutMessg   = "";
        string strOutMsg     = "";
        public Display( string strInpMessage, string strInpMessg, string strInpMsg)
        {
           strOutMessage = "";
           strOutMessg = "";
           strOutMsg = "";
           strOutMessage = strInpMessage;
           strOutMessg = strInpMessg;
           strOutMsg = strInpMsg;
         
           InitializeComponent();
        }

        public Display()
        {
        }

        private void DialogLoaded(object sender, RoutedEventArgs e)
        {
            lblTopBar.Content = strOutMessg;
            tb.Text = strOutMsg;
            //lb.Content = strOutMsg;
           // tb.Text = strOutMessage;      
        }

        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnCloseClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Info_Button_Click(object sender, RoutedEventArgs e)
        {
            //tb.Text = strOutMessage;
            DisplayMessage displayMessage = new DisplayMessage(strOutMessage);
            displayMessage.ShowDialog();
        }

        private void Refresh_Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
