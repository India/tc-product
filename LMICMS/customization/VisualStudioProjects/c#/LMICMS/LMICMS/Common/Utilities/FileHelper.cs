﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LMICMS.Common.Utilities
{
    public class FileHelper
    {
        public static bool SaveFile(string strFilePath, string strFileData) 
        {
            bool flag = false;

            try
            {
                File.WriteAllBytes(strFilePath, Encoding.UTF8.GetBytes(strFileData));
                flag = true;
            }
            catch (Exception ex)
            {
                
                //throw;
            }

            return flag;
        }

        public static bool IsTextFileEmpty(string fileName)
        {
            var info = new FileInfo(fileName);
            if (info.Length == 0)
                return true;

            if (info.Length < 6)
            {
                var content = File.ReadAllText(fileName);
                return content.Length == 0;
            }
            return false;
        }

        public  bool Check_File_Exists(string strFileName)
        {
            Boolean bIsFileAvailable = false;

            if (string.IsNullOrEmpty(strFileName) == true)
            {
                bIsFileAvailable =  false;
            }
            else
            {
                bIsFileAvailable = true;
            }
          
            return bIsFileAvailable;
        }
    }
}
