﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace LMICMS.Common.Utilities
{
    public class XmlHelper
    {
        public static T DeserializeXMLFileToObject<T>(string xmlFilePath)
        {
            T returnObject = default(T);
            if (string.IsNullOrEmpty(xmlFilePath)) return default(T);

            try
            {
                StreamReader xmlStream = new StreamReader(xmlFilePath);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
            }
            catch (Exception ex)
            {
                //ExceptionLogger.WriteExceptionToConsole(ex, DateTime.Now);
            }
            return returnObject;
        }

        public static T DeserializeXMLNodeToObject<T>(XmlNode xmlNode)
        {
            T returnObject = default(T);
            if (xmlNode == null) return returnObject;

            try
            {
                XmlSerializer serial = new XmlSerializer(typeof(T));
                using (XmlNodeReader xmlNodeReader = new XmlNodeReader(xmlNode))
                {
                    returnObject = (T)serial.Deserialize(xmlNodeReader);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return returnObject;
        }
    }
}
