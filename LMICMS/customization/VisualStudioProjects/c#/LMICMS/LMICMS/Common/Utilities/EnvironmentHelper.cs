﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Common.Utilities
{
    public class EnvironmentHelper
    {
        public static string GetEnvironmentVariable(string strVarName)
        {
            string strEnvValue = Environment.GetEnvironmentVariable(strVarName, EnvironmentVariableTarget.User);
            if (String.IsNullOrEmpty(strEnvValue))
            {
                strEnvValue = Environment.GetEnvironmentVariable(strVarName, EnvironmentVariableTarget.Machine);
            }
            return strEnvValue;
        }
    }
}
