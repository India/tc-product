﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using LMICMS.Infrastructure;

namespace LMICMS.Common.Utilities
{
    public class LMIConfigHelper
    {
        public static string GetDefaultConfigFilePath()
        {
            return AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings[AppSettingKeys.ConfigFileName];
        }

        public static string GetConfigFilePath()
        {
            string strConfigFilePath = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings[AppSettingKeys.ConfigFileName];

            if (File.Exists(strConfigFilePath))
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strLocElePath = String.Format(@"//{0}/{1}", ConfigConstants.TagConfigurationRoot, ConfigConstants.ElementConfigFileLoc);
                var configFieLoc = xmlDoc.SelectSingleNode(strLocElePath);
                if (configFieLoc != null && !String.IsNullOrEmpty(configFieLoc.InnerText))
                {
                    strConfigFilePath = configFieLoc.InnerText;
                }
            }
            return strConfigFilePath;
        }

        public static string GetValue(string key)
        {
            return Convert.ToString(ConfigurationManager.AppSettings[key]);
        }
    }
}
