﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LMICMS.Common.Utilities
{
    public class ResourceHelper
    {
        public static string FindResource(string key)
        {
            return Convert.ToString(Application.Current.TryFindResource(key));
        }
    }
}
