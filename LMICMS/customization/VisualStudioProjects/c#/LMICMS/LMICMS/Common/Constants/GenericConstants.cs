﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Common.Constants
{
    public static class GenericConstants
    {
        public const string SET_TC_ROOT              = "SET TC_ROOT=";
        public const string SET_TC_DATA              = "SET TC_DATA=";
        public const string TC_DATA_TC_PROFILEVARS   = "%TC_DATA%\\tc_profilevars";
        public const string SET_ECHO_OFF             = "@echo off";
        public const string TEXT_DISP                = "echo Type in password of ";
        public const string SET_DBAPASS              = "set /P DBAPASS=";
        public const string SET_DBAPASS_TMP          = "set DBAPASS_TMP=";
    }
}
