﻿using LMICMS.Dialogs;
using LMICMS.Infrastructure;
using LMICMS.UserControls;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace LMICMS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : LMIWindow
    {
        public static MainWindow _mainWindow;

        Dictionary<string,UserControl> userControls;

        public MainWindow()
        {
            InitializeComponent();
            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;
            _mainWindow = this;
            this.lblActiveProject.Content = AppConfig.ActiveProjectName;

            userControls = new Dictionary<string, UserControl>();
            userControls.Add(typeof(HomeUC).ToString(), new HomeUC());
            userControls.Add(typeof(SettingsUC).ToString(), new SettingsUC());
            userControls.Add(typeof(QueryUC).ToString(), new QueryUC());
            userControls.Add(typeof(WorkflowUC).ToString(), new WorkflowUC());
            userControls.Add(typeof(TransferModeUC).ToString(), new TransferModeUC());
            userControls.Add(typeof(ACLUC).ToString(), new ACLUC());
            userControls.Add(typeof(AuthorizationUC).ToString(), new AuthorizationUC());
            userControls.Add(typeof(StylesheetUC).ToString(), new StylesheetUC());
            userControls.Add(typeof(OrganizationUC).ToString(), new OrganizationUC());
        }

        private void ShutDownClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void FullscreenClick(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Normal)
            {
                this.WindowState = System.Windows.WindowState.Maximized;
            }
            else if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                this.WindowState = System.Windows.WindowState.Normal;
            }
        }

        private void MinimizeClick(object sender, RoutedEventArgs e)
        {
            if (this.WindowState != System.Windows.WindowState.Minimized)
            {
                this.WindowState = System.Windows.WindowState.Minimized;
            }
            else
            {
                this.WindowState = System.Windows.WindowState.Normal;
            }
        }

        private void ChangeUserControl(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;

            switch (button.CommandParameter.ToString())
            {
                case "Settings":
                    ccAppContainer.Content = userControls[typeof(SettingsUC).ToString()];
                    break;
                case "Home":
                    ccAppContainer.Content = userControls[typeof(HomeUC).ToString()];
                    break;
                case "Query":
                    ccAppContainer.Content = userControls[typeof(QueryUC).ToString()];
                    break;
                case "WorkflowViewer":
                    ccAppContainer.Content = userControls[typeof(WorkflowUC).ToString()];
                    break;
                case "TransferMode":
                    ccAppContainer.Content = userControls[typeof(TransferModeUC).ToString()];
                    break;
                case "ACL":
                    ccAppContainer.Content = userControls[typeof(ACLUC).ToString()];
                    break;
                case "Authorization":
                    ccAppContainer.Content = userControls[typeof(AuthorizationUC).ToString()];
                    break;
                case "Stylesheet":
                    ccAppContainer.Content = userControls[typeof(StylesheetUC).ToString()];
                    break;
                case "Organization":
                    ccAppContainer.Content = userControls[typeof(OrganizationUC).ToString()];
                    break;
            }
        }
    }
}
