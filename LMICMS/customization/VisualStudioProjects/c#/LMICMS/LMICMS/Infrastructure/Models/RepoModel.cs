﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Models
{
    public class RepoModel
    {
        public RepoModel()
        {
            //AppDataLocs = new List<string>();
        }
        public string RepoGuid { get; set; }
        public string RepoName { get; set; }
        public string RepoPath { get; set; }
        public bool Active { get; set; }
        //public IList<string> AppDataLocs { get; set; }
        public string AppDataLocs { get; set; }
    }
}
