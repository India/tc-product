﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Models
{
    public class CreateOrUpdateRepo
    {
        public bool Active { get; set; }
        public string RepoGuid { get; set; }
        public string RepoName { get; set; }
        public string RepoPath { get; set; }
        public string WorkflowPath { get; set; }
        public string AuthorizationPath { get; set; }
        public string OrganizationPath { get; set; }
        public string ACLPath { get; set; }
        public string PFFPath { get; set; }
        public string StylesheetPath { get; set; }
        public string PreferencePath { get; set; }
        public string QueryPath { get; set; }
        public string TransferModePath { get; set; }
    }
}
