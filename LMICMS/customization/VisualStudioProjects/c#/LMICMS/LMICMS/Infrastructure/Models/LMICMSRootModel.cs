﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Models
{
    public class CommonSettingsModel
    {
        public string LMITempDownloadPath { get; set; }
        public string LMICMSExeLoc { get; set; }
        public string LMIConfigFileLoc { get; set; }
    }
}
