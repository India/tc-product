﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Models
{
    public class AvailableUserModel
    {
        public string Guid { get; set; }
        public string DisplayValue { get; set; }
    }
}
