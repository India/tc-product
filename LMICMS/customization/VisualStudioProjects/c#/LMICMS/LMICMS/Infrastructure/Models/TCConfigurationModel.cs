﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Models
{
    public class TCConfigurationModel
    {
        public string ConfigGuid { get; set; }
        public string ConfigName { get; set; }
        public string TCRoot { get; set; }
        public string TCData { get; set; }
        public string LMIActiveUserGuid { get; set; }
        public string ActiveUser { get; set; }
        public bool Active { get; set; }
    }
}
