﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Models
{
    public class ProjectModel
    {
        public string ProjectGuid { get; set; }
        public string ProjectName { get; set; }
        public bool Active { get; set; }
    }
}
