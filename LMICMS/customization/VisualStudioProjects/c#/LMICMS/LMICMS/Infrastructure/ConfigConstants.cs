﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Infrastructure
{
    public static class ConfigConstants
    {
        public const string TagConfigurationRoot        = "LMI_CMS";
        public const string ElementActiveProjectGuid    = "ACTIVE_PROJECT_GUID";
        public const string ElementTempDownloadPath     = "LMI_TEMP_DOWNLOAD_PATH";
        public const string ElementCMSExeLoc            = "LMI_CMS_EXE_LOC";
        public const string ElementConfigFileLoc        = "LMI_CONFIG_FILE_LOC";

        public const string TagProjectsDetails          = "PROJECTS_DETAILS";
        public const string TagProject                  = "PROJECT";
        public const string AttributeProjectGuid        = "PROJECT_GUID";
        public const string AttributeProjectName        = "PROJECT_NAME";
        public const string ElementActiveConfigGuid     = "ACTIVE_CONFIG_GUID";
        public const string ElementActiveRepoGuid       = "ACTIVE_REPO_GUID";

        public const string TagTCConfigurations         = "TC_CONFIGURATIONS";
        public const string TagTCConfiguration          = "TC_CONFIGURATION";
        public const string AttributeConfigGuid         = "CONFIG_GUID";
        public const string AttributeConfigName         = "CONFIG_NAME";
        public const string ElementTCRoot               = "TC_ROOT";
        public const string ElementTCData               = "TC_DATA";
        public const string ElementActiveUserGuid       = "LMI_ACTIVE_USER_GUID";

        public const string TagUsersDetails             = "TC_USERS_DETAILS";
        public const string TagUser                     = "TC_USER";
        public const string AttributeUserGuid           = "USER_GUID";
        public const string AttributeUserId             = "USER_ID";
        public const string AttributeUserPassword       = "USER_PASSWORD";
        public const string AttributeUserGroup          = "USER_GROUP";
        public const string AttributeUserRole           = "USER_ROLE";

        public const string TagSrcControlLoc            = "SRC_CONTROL_LOC";     
        public const string ElementRepo                 = "REPO";
        public const string AttributeRepoGuid           = "REPO_GUID";
        public const string AttributeRepoName           = "REPO_NAME";
        public const string AttributeRepoPath           = "REPO_PATH";
        public const string ElementAppData              = "APP_DATA";
        public const string AttributeAppDataName        = "APP_DATA_NAME";
        public const string AttributeAppDataPath        = "APP_DATA_PATH";
    }
}
