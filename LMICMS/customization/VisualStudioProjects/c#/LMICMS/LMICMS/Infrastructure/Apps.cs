﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Infrastructure
{
    public class Apps
    {
        public const string Workflow          = "Workflow";
        public const string Authorization     = "Authorization";
        public const string Organization      = "Organization";
        public const string ACL               = "ACL";
        public const string PFF               = "PFF";
        public const string Stylesheet        = "Stylesheet";
        public const string Preference        = "Preference";
        public const string Query             = "Query";
        public const string TransferMode      = "TransferMode";
    }
}
