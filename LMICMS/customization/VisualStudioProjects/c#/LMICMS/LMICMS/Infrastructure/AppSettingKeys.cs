﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Infrastructure
{
    public static class AppSettingKeys
    {
        public const string ConfigFileName     = "ConfigFileName";
        public const string EnvTCRoot          = "EnvTCRoot";
        public const string EnvTCData          = "EnvTCData";
        public const string DefaultProjectName = "DefaultProjectName"; 
        public const string DefaultConfigName  = "DefaultConfigName";
        public const string DefaultRepoName    = "DefaultRepoName"; 
        public const string DefaultFontFamily  = "DefaultFontFamily";

        #region Messages

        public const string UserUC_UserAdded             = "Message.UserUC.UserAdded";
        public const string UserUC_UserUpdated           = "Message.UserUC.UserUpdated";
        public const string UserUC_AllFieldsMandatory    = "Message.UserUC.AllFieldsMandatory";
        public const string UserUC_UserDeleted           = "Message.UserUC.UserDeleted";

        public const string ProjectUC_ProjectAdded       = "Message.ProjectUC.ProjectAdded";
        public const string ProjectUC_ProjectUpdated     = "Message.ProjectUC.ProjectUpdated";
        public const string ProjectUC_AllFieldsMandatory = "Message.ProjectUC.AllFieldsMandatory";
        public const string ProjectUC_ProjectDeleted     = "Message.ProjectUC.ProjectDeleted";
        public const string ProjectUC_ProjectActivated   = "Message.ProjectUC.ProjectActivated";

        #endregion
    }
}
