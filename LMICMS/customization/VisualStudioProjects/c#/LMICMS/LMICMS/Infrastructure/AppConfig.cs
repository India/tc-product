﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Infrastructure
{
    public static class AppConfig
    {
        public static string ActiveProjectGuid       = string.Empty;
        public static string ActiveProjectName       = string.Empty;
        public static string ActiveConfigGuid        = string.Empty;
        public static string ActiveRepoPathGuid      = string.Empty;
        public static string ActiveRepoPath          = string.Empty;
        public static string TCRoot                  = string.Empty;
        public static string TCData                  = string.Empty;
        public static string LMITempDownloadPath     = string.Empty;
        public static string LMICMSExeLoc            = string.Empty;
        public static string UserGuid                = string.Empty;
        public static string UserName                = string.Empty;
        public static string UserPassword            = string.Empty;
        public static string UserGroup               = string.Empty;
        public static string UserRole                = string.Empty;

        public static string WorkflowPath            = string.Empty;
        public static string AuthorizationPath       = string.Empty;
        public static string OrganizationPath        = string.Empty;
        public static string ACLPath                 = string.Empty;
        public static string PFFPath                 = string.Empty;
        public static string StylesheetPath          = string.Empty;
        public static string PreferencePath          = string.Empty;
        public static string QueryPath               = string.Empty;
        public static string TransferModePath        = string.Empty;
    }
}
