﻿
using LMICMS.Dialogs;
using LMICMS.Infrastructure;
using LMICMS.Services;
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;


namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for OrganizationExportImportUC.xaml
    /// </summary>
    public partial class OrganizationExportImportUC : UserControl
    {
        public static string strMessage = "";

        public string strOperationMessage = "";

        public static OrganizationExportImportUC ststorganizationExportImportUC  = null;

        public OrganizationExportImportUC()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Export_Location.Text = AppConfig.OrganizationPath;
            tb_Import_Location.Text = AppConfig.OrganizationPath;

        }

        private void Exp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Export_Location.Text))
            {
                System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                dialog.SelectedPath = tb_Export_Location.Text.Trim();

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    tb_Export_Location.Text = dialog.SelectedPath;
                }
                e.Handled = true;
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {

            ststorganizationExportImportUC = this;

            LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(tb_Export_Location.Text, Constants.REQ_ORGANISATIONEXPORT, tb_organization_name.Text, 0);

            progressBar.ShowDialog();
        }

        private void Imp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Import_Location.Text))
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();

                openFileDialog.Multiselect = true;

                openFileDialog.InitialDirectory = tb_Import_Location.Text;

                openFileDialog.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";

                DirectoryInfo dir = new DirectoryInfo(tb_Import_Location.Text);

                if (openFileDialog.ShowDialog() == true)

                    foreach (string filename in openFileDialog.FileNames)
                    {
                        String strFileName = System.IO.Path.GetFileName(filename);

                        string directoryPath = System.IO.Path.GetDirectoryName(openFileDialog.FileName);

                        tb_Import_Location.Text = openFileDialog.FileName;
                    }
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void Import_Button_Click(object sender, RoutedEventArgs e)
        {
            ststorganizationExportImportUC = this;

            LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(tb_Import_Location.Text, Constants.REQ_ORGANISATIONIMPORT, 0, 0);

            progressBar.ShowDialog();

            e.Handled = true;

        }
        public void handleResponseEvent(int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_ORGANISATIONEXPORT:
                        handleExportRespEvent();
                        break;
                    case Constants.REQ_ORGANISATIONIMPORT:
                        handleImportRespEvent();
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private void handleImportRespEvent()
        {
            string InpMsg = null;
            string strfinal = Execute_tc_exe.strError;
            if (String.IsNullOrEmpty(strfinal))
            {
                strOperationMessage = Constants.ERROR_MSG_INFO;
                InpMsg = "Operation Completed Successfully";
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_ERROR;
                InpMsg = "Operation failed";
            }
            // strOperationMessage = Constants.MSG_LABEL;

            string disp = OrganisationImportService.strString;

            Display display = new Display(disp, strOperationMessage, InpMsg);

            if (display.ShowDialog() == true)
            {
                display.ShowDialog();
            }
        }

        void handleExportRespEvent()
        {
            string InpMsg = null;
            string strfinal = Execute_tc_exe.strError;
            if (String.IsNullOrEmpty(strfinal))
            {
                strOperationMessage = Constants.ERROR_MSG_INFO;
                InpMsg = "Operation Completed Successfully";
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_ERROR;
                InpMsg = "Operation failed";
            }
            // strOperationMessage = Constants.MSG_LABEL;






            string disp = OrganisationExportService.strStringOut;

            Display display = new Display(disp, strOperationMessage, InpMsg);

            if (display.ShowDialog() == true)
            {
                display.ShowDialog();
            }

        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}
