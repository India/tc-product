﻿using LMICMS.Common.Utilities;
using LMICMS.Dialogs;
using LMICMS.Infrastructure;
using LMICMS.Requests;
using LMICMS.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for WFExportUC.xaml
    /// </summary>
    public partial class WFExportUC : UserControl
    {
        public static string strMessage = "";

        public string strOperationMessage = "";

        List<WorkflowModel> lMatchedStrings = null;

        public static string strQueryName = "";

        public static string strString
        {
            get { return strQueryName; }

            set { strQueryName = value; }
        }
        public static WFExportUC stWFExportUC = null;

        public WFExportUC()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Export_Location.Text = AppConfig.WorkflowPath;
        }

        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Export_Location.Text))
            {
                string strFileName = "";

                stWFExportUC = this;

                WorkflowExportModel workflowExpModel = new WorkflowExportModel();

                foreach (var item in dg.SelectedItems)
                {
                    ((WorkflowModel)item).FileName = System.IO.Path.GetFileNameWithoutExtension(((WorkflowModel)item).FileName);
                    strFileName = strFileName + ((WorkflowModel)item).FileName + System.Environment.NewLine;
                }

                workflowExpModel.ExportWFNames.Add(strFileName);

                LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(workflowExpModel, Constants.REQ_WORKFLOWEXPORT, tb_Export_Location.Text, 0);

                progressBar.ShowDialog();

                e.Handled = true;
            }

            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }
    
        private void SelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            dg.SelectAll();
        }

        private void Refresh_Button_Click(object sender, RoutedEventArgs e)
        {
            stWFExportUC = this;

            FileHelper fileHelper = new FileHelper();

            Boolean bIsFileAvailable = false;

            lMatchedStrings = new List<WorkflowModel>();

            string strDir = ConfigService.strDirFolder;

            string strFileName = (Constants.WORKFLOW_NAME + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

            LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(strFileName, Constants.REQ_WORKFLOWNAME, 0, 0);

            progressBar.ShowDialog();

            bIsFileAvailable = fileHelper.Check_File_Exists(strDir + Constants.FILE_PATH + strFileName);

            //if (bIsFileAvailable == true)
            //{
            //    string[] filelines = System.IO.File.ReadAllLines(strDir + Constants.FILE_PATH + strFileName);

            //    for (int iDx = 0; iDx < filelines.Length; iDx++)
            //    {
            //        StringBuilder sb = new StringBuilder(filelines[iDx]);

            //        sb = Replace_String_Characters.Process_Replace_String_Characters(sb);

            //        lMatchedStrings.Add(new WorkflowModel() { WorkflowName = filelines[iDx], FileName = sb.ToString() });
            //    }
                dg.ItemsSource =WorkflowName.lMatchedStrings;

                dg.Items.Refresh();
            
            //else
            //{
            //    strOperationMessage = Constants.ERROR_MSG_MAIN;

            //    strMessage = Constants.ERROR_MSG_FILE;

            //    Display display = new Display(strMessage, strOperationMessage);

            //    display.ShowDialog();
            //}
        }

        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Export_Location.Text))
            {
                System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                dialog.SelectedPath = tb_Export_Location.Text.Trim();

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    tb_Export_Location.Text = dialog.SelectedPath;
                }
                e.Handled = true;
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strpattern = txtFilter.Text;

            List<WorkflowModel> lMatched = new List<WorkflowModel>();

            strpattern = Constants.SEP_CARET + Regex.Escape(strpattern).Replace(Constants.SEP_ASTERISK, Constants.SEP_DOT_ASTERISK) + Constants.SEP_DOLLAR;

            foreach (var item in WorkflowName.lMatchedStrings)
            {

                if (Regex.IsMatch(item.WorkflowName, strpattern, RegexOptions.IgnoreCase))
                {
                    lMatched.Add(new WorkflowModel() { WorkflowName = item.WorkflowName, FileName = item.FileName });
                }
            }
            dg.ItemsSource = lMatched;

            dg.Items.Refresh();
        }
    
        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        public void handleResponseEvent(int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_WORKFLOWEXPORT:
                        handleExportRespEvent();
                        break;
                    case Constants.REQ_WORKFLOWNAME:
                        handleWorkflowRespEvent();
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

    void handleExportRespEvent()
        {
            string InpMsg = null;
            string strfinal = Execute_tc_exe.strError;
            if (String.IsNullOrEmpty(strfinal))
            {
                strOperationMessage = Constants.ERROR_MSG_INFO;
                InpMsg = "Operation Completed Successfully";
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_ERROR;
                InpMsg = "Operation failed";
            }
           // strOperationMessage = Constants.MSG_LABEL;

            string disp = WorkflowExportService.strStringOut;

            Display display = new Display(disp, strOperationMessage, InpMsg);

            if (display.ShowDialog() == true)
            {
                dg.Items.Refresh();
            }

            display.ShowDialog();
        }

    private void handleWorkflowRespEvent()
        {
            throw new NotImplementedException();
        }
    }
}
