﻿using System.Windows.Controls;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for Organization.xaml
    /// </summary>
    public partial class OrganizationUC : UserControl
    {
        public OrganizationUC()
        {
            InitializeComponent();
        }
        private void TabOrganizationUCSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            {
                string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

                switch (tabItem)
                {
                    case "Export/Import":
                        ccExportImport.Content = new OrganizationExportImportUC();
                        break;
                    case "Clean":
                        ccClean.Content = new OrganizationCleanUC();
                        break;
                }
                e.Handled = true;
            }

        }
    }
}
