﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Dialogs;
using LMICMS.Infrastructure;
using LMICMS.Requests;
using LMICMS.Services;
using System.IO;
using LMICMS.Common.Utilities;


namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for ExportUC.xaml
    /// </summary>

    public partial class ExportUC : UserControl
    {
        public static string strMessage = "";

        public string strOperationMessage = "";
       
        List<QueryModel> lMatchedStrings = null;

        private static string strQueryName = "";

        public static string strString
        {
            get { return strQueryName; }

            set { strQueryName = value; }
        }

        public static ExportUC stExportUC = null;

        public ExportUC()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Export_Location.Text = AppConfig.QueryPath;
        }

        private void Refresh_Button_Click(object sender, RoutedEventArgs e)
        {
            stExportUC = this;

            FileHelper fileHelper = new FileHelper();
            
            Boolean bIsFileAvailable = false;

            lMatchedStrings = new List<QueryModel>();

            string strDir = ConfigService.strDirFolder;

            string strFileName = (Constants.QUERY_NAME + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

            LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(strFileName, Constants.REQ_QUERYNAME, 0, 0);

            progressBar.ShowDialog();

            bIsFileAvailable = fileHelper.Check_File_Exists(strDir + Constants.FILE_PATH + strFileName);

            //if (bIsFileAvailable == true)
            //{
            //    string[] filelines = System.IO.File.ReadAllLines(strDir + Constants.FILE_PATH + strFileName);

            //    for (int iDx = 0; iDx < filelines.Length; iDx++)
            //    {
            //        StringBuilder sb = new StringBuilder(filelines[iDx]);

            //        sb = Replace_String_Characters.Process_Replace_String_Characters(sb);
                    
            //        lMatchedStrings.Add(new QueryModel() { QueryName = filelines[iDx], FileName = sb.ToString() });
            //    }
                dg.ItemsSource = QueryName.lMatchedStrings;

                dg.Items.Refresh();
            
            //else
            //{
            //    strOperationMessage = Constants.ERROR_MSG_MAIN;

            //    strMessage = Constants.ERROR_MSG_FILE;

            //    Display display = new Display(strMessage, strOperationMessage);

            //    display.ShowDialog();
            //}
        }
        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Export_Location.Text))
            {
                System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                dialog.SelectedPath = tb_Export_Location.Text.Trim();
              
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    tb_Export_Location.Text = dialog.SelectedPath;
                }
                e.Handled = true;
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void SelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            dg.SelectAll();
        }

        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Export_Location.Text))
            {
                string strFileName = "";

                stExportUC = this;

                ExportModel exportModel = new ExportModel();

                foreach (var item in dg.SelectedItems)
                {
                    strQueryName = strQueryName + ((QueryModel)item).QueryName + Constants.SEP_COMMA;

                   ((QueryModel)item).FileName = Path.GetFileNameWithoutExtension(((QueryModel)item).FileName);

                    strFileName = strFileName + ((QueryModel)item).FileName + Constants.SEP_COMMA;
                }
                strQueryName = strQueryName.TrimEnd(Constants.COMMA_SEP);

                strQueryName = Constants.DOUBLE_QUOTES + strQueryName + Constants.DOUBLE_QUOTES;

                strFileName = strFileName.TrimEnd(Constants.COMMA_SEP);

                exportModel.queries.Add(strQueryName);

                exportModel.exportedqueries.Add(strFileName);

                if (exportModel.exportedqueries != null)
                {
                    LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(exportModel, Constants.REQ_EXPORT, tb_Export_Location.Text, 0);

                    progressBar.ShowDialog();

                    e.Handled = true;
                }
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }
        private void txtFilter_TextChanged(object sender,TextChangedEventArgs e)
        {
            string strpattern = txtFilter.Text;

            if(strpattern.Length == 0)
            {
                strpattern = "*";

            }

            List<QueryModel> lMatched = new List<QueryModel>();

            strpattern = Constants.SEP_CARET + Regex.Escape(strpattern).Replace(Constants.SEP_ASTERISK, Constants.SEP_DOT_ASTERISK) + Constants.SEP_DOLLAR;
            
            foreach (var item in QueryName.lMatchedStrings)
            {

                if (Regex.IsMatch(item.QueryName, strpattern, RegexOptions.IgnoreCase))
                {
                    lMatched.Add(new QueryModel() { QueryName = item.QueryName, FileName = item.FileName });
                }
            }
            dg.ItemsSource = lMatched;

            dg.Items.Refresh();   
        }
      
        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
        public void handleResponseEvent(int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_EXPORT:
                        handleExportRespEvent();
                        break;
                    case Constants.REQ_QUERYNAME:
                        handleQueryRespEvent();
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private void handleQueryRespEvent()
        {
            throw new NotImplementedException();
        }

        public void handleExportRespEvent()
        {
            string InpMsg = null;
            string strfinal = Execute_tc_exe.strError;
            if (String.IsNullOrEmpty(strfinal))
            {
                strOperationMessage = Constants.ERROR_MSG_INFO;
                InpMsg = "Operation Completed Successfully";
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_ERROR;
                InpMsg = "Operation failed";
            }
         

            string disp = ExportServiceFinal.strString;

            Display display = new Display(disp, strOperationMessage, InpMsg);

            if (display.ShowDialog() == true)
            {
                dg.Items.Refresh();
            }

            display.ShowDialog();
        }
    }
}

        

















