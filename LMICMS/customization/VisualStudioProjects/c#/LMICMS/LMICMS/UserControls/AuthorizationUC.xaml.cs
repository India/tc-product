﻿using LMICMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for AuthorizationUC.xaml
    /// </summary>
    public partial class AuthorizationUC : UserControl
    {
        public AuthorizationUC()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Export_Location.Text = AppConfig.AuthorizationPath;
            tb_Import_Location.Text = AppConfig.AuthorizationPath;

        }

        private void Exp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Imp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Import_Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
