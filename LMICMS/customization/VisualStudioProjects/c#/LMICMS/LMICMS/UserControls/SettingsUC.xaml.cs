﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for SettingsUC.xaml
    /// </summary>
    public partial class SettingsUC : UserControl
    {
        public SettingsUC()
        {
            InitializeComponent();
        }

        private void TabSettingsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

            switch (tabItem)
            {
                case "Common":
                    ccCommon.Content = new CommonSettingsUC();
                    break;
                case "Project":
                    ccProject.Content = new ProjectUC();
                    break;
                case "Users":
                    ccUsers.Content = new UsersUC();
                    break;
                case "TC Configuration":
                    ccTCConfiguration.Content = new ConfigurationUC();
                    break;
                case "SVN Location":
                    ccSVNLocation.Content = new SVNLocationUC();
                    break;
                
            }
            e.Handled = true;
        }
    }
}
