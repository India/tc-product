﻿using LMICMS.Dialogs;
using LMICMS.Factories;
using LMICMS.Infrastructure;
using LMICMS.Modules;
using System;
using System.Configuration;
using System.Windows;
using System.Windows.Controls;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for Configuration.xaml
    /// </summary>
    public partial class ConfigurationUC : UserControl
    {
        public ConfigurationUC()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            GridViewRefresh();
        }

        private void GridViewRefresh()
        {
            dgConfiguration.ItemsSource = SettingsModelFactory.GetConfigurationModels(AppConfig.ActiveProjectGuid);
            dgConfiguration.Items.Refresh();
        }

        private void BtnAddClick(object sender, RoutedEventArgs e)
        {
            CreateOrUpdateConfiguration createOrUpdateDialog = new CreateOrUpdateConfiguration();
            if (createOrUpdateDialog.ShowDialog() == true)
            {
                GridViewRefresh();
            }
            e.Handled = true;
        }

        private void BtnEditClick(object sender, RoutedEventArgs e)
        {
            Button btnEdit = (Button)e.OriginalSource;

            CreateOrUpdateConfiguration createOrUpdateDialog = new CreateOrUpdateConfiguration(Convert.ToString(btnEdit.CommandParameter));
            if (createOrUpdateDialog.ShowDialog() == true)
            {
                GridViewRefresh();
            }

            e.Handled = true;
        }

        private void BtnDeleteClick(object sender, RoutedEventArgs e)
        {
            Button btnDelete = (Button)e.OriginalSource;
            String strConfigGuid = Convert.ToString(btnDelete.CommandParameter);

            if (!String.IsNullOrEmpty(strConfigGuid))
            {
                bool deleted = SettingsModule.DeleteConfig(AppConfig.ActiveProjectGuid, strConfigGuid);
                if (deleted == true)
                {
                    GridViewRefresh();
                }
            }
            e.Handled = true;
        }

        private void BtnSetActiveClick(object sender, RoutedEventArgs e)
        {
            Button btnSetProActive = (Button)e.OriginalSource;
            String strConfigGuid = Convert.ToString(btnSetProActive.CommandParameter);

            if (!String.IsNullOrEmpty(strConfigGuid))
            {
                bool active = SettingsModule.SetConfigActive(AppConfig.ActiveProjectGuid, strConfigGuid);
                if (active == true)
                {
                    GridViewRefresh();
                }
            }
            e.Handled = true;
        }

        private void DataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
