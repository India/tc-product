﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for QueryUC.xaml
    /// </summary>
    public partial class QueryUC : UserControl
    {
        public QueryUC()
        {
            InitializeComponent();
        }

        private void TabQuerySelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            {
                string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

                switch (tabItem)
                {
                    case "Export":
                        ccExport.Content = new ExportUC();
                        break;
                    case "Import":
                        ccImport.Content = new ImportUC();
                        break;
                }
                e.Handled = true;
            }
        }
    }
}

    

