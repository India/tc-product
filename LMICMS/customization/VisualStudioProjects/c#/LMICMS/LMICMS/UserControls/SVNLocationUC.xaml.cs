﻿using LMICMS.Dialogs;
using LMICMS.Factories;
using LMICMS.Infrastructure;
using LMICMS.Modules;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for SVNLocationUC.xaml
    /// </summary>
    public partial class SVNLocationUC : UserControl
    {
        public SVNLocationUC()
        {
            InitializeComponent();
        }

        private void GridViewRefresh()
        {
            dgRepos.ItemsSource = SettingsModelFactory.GetRepoModels(AppConfig.ActiveProjectGuid);
            dgRepos.Items.Refresh();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            GridViewRefresh();
        }

        private void BtnAddClick(object sender, RoutedEventArgs e)
        {
            CreateOrUpdateRepo createOrUpdateDialog = new CreateOrUpdateRepo();
            if (createOrUpdateDialog.ShowDialog() == true)
            {
                GridViewRefresh();
            }
            e.Handled = true;
        }

        private void BtnEditClick(object sender, RoutedEventArgs e)
        {
            Button btnEdit = (Button)e.OriginalSource;

            CreateOrUpdateRepo createOrUpdateDialog = new CreateOrUpdateRepo(Convert.ToString(btnEdit.CommandParameter));
            if (createOrUpdateDialog.ShowDialog() == true)
            {
                GridViewRefresh();
            }

            e.Handled = true;
        }

        private void BtnDeleteClick(object sender, RoutedEventArgs e)
        {
            Button btnDelete = (Button)e.OriginalSource;
            String strRepoGuid = Convert.ToString(btnDelete.CommandParameter);

            if (!String.IsNullOrEmpty(strRepoGuid))
            {
                bool deleted = SettingsModule.DeleteRepo(AppConfig.ActiveProjectGuid, strRepoGuid);
                if (deleted == true)
                {
                    GridViewRefresh();
                }
            }
            e.Handled = true;
        }

        private void BtnSetActiveClick(object sender, RoutedEventArgs e)
        {
            Button btnSetRepoActive = (Button)e.OriginalSource;
            String strRepoGuid = Convert.ToString(btnSetRepoActive.CommandParameter);

            if (!String.IsNullOrEmpty(strRepoGuid))
            {
                bool active = SettingsModule.SetRepoActive(AppConfig.ActiveProjectGuid, strRepoGuid);
                if (active == true)
                {
                    GridViewRefresh();
                }
            }
            e.Handled = true;
        }

        private void DataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
