﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LMICMS.Dialogs;
using LMICMS.Domain.Settings;
using LMICMS.Infrastructure;
using LMICMS.Modules;
using LMICMS.Services;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for Users.xaml
    /// </summary>
    public partial class UsersUC : UserControl
    {

        private bool editEndHandle = true;

        #region ctor

        public UsersUC()
        {
            InitializeComponent();      
        }

        #endregion

        #region methods

        private void GridViewRefresh()
        {
            dataGrid.ItemsSource = SettingsModule.GetAllUsers();
            dataGrid.Items.Refresh();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            GridViewRefresh();
        }

        #endregion

        private void BtnDeleteClick(object sender, RoutedEventArgs e)
        {
            Button btnDelete = (Button)e.OriginalSource;
            String strUserGuid = Convert.ToString(btnDelete.CommandParameter);

            if (!String.IsNullOrEmpty(strUserGuid))
            {
                bool deleted = SettingsModule.DeleteUser(strUserGuid);
                if (deleted == true)
                {
                    //lblMessage.Content = ConfigurationManager.AppSettings[AppSettingKeys.UserUC_UserDeleted];
                    GridViewRefresh();
                }
            }
        }

        #region events
        /*

    private void TextboxTextChanged(object sender, TextChangedEventArgs e)
    {
        TCUser objUser = dgUsers.SelectedItem as TCUser;
        if (objUser == null && !String.IsNullOrEmpty(txtUserId.Text) && 
                               !string.IsNullOrEmpty(txtUserGroup.Text) && 
                               !string.IsNullOrEmpty(txtUserPassword.Text) &&
                               !string.IsNullOrEmpty(txtUserRole.Text))
        {
            btnAdd.IsEnabled = true;
        }
        else
        {
            btnAdd.IsEnabled = false;
        }
    }
    */
        #endregion


        private void DataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        private void DataGridRowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (editEndHandle == true)
            {
                editEndHandle = false;

                var objUser = e.Row.Item as TCUser;
                dataGrid.CommitEdit();

                if (!String.IsNullOrEmpty(objUser.UserId) &&
                    !String.IsNullOrEmpty(objUser.UserPassword) &&
                    !String.IsNullOrEmpty(objUser.UserGroup) &&
                    !String.IsNullOrEmpty(objUser.UserRole))
                {
                    if (String.IsNullOrEmpty(objUser.UserGuid))
                    {
                        objUser.UserGuid = Guid.NewGuid().ToString();
                        SettingsModule.SaveUser(objUser);
                        //lblMessage.Content = ConfigurationManager.AppSettings[AppSettingKeys.UserUC_UserAdded];
                    }
                    else
                    {
                        SettingsModule.SaveUser(objUser);
                        //lblMessage.Content = ConfigurationManager.AppSettings[AppSettingKeys.UserUC_UserUpdated];
                    }
                }

                GridViewRefresh();
                editEndHandle = true;
            }
        }
    }
}

