﻿using LMICMS.Common.Utilities;
using LMICMS.Dialogs;
using LMICMS.Infrastructure;
using LMICMS.Requests;
using LMICMS.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.IO;
using System.Text.RegularExpressions;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for StylesheetExportUC.xaml
    /// </summary>
    public partial class StylesheetExportUC : UserControl
    {
        public static string strMessage = "";

        public string strOperationMessage = "";

        List<StylesheetModel> lMatchedStrings = null;

        public static StylesheetExportUC stStylesheetExportUC = null;

        public StylesheetExportUC()
        {
            InitializeComponent();
        }

        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Export_Location.Text))
            {
                System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                dialog.SelectedPath = tb_Export_Location.Text.Trim();

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    tb_Export_Location.Text = dialog.SelectedPath;
                }
                e.Handled = true;
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Export_Location.Text = AppConfig.StylesheetPath;
        }

        private void Refresh_Button_Click(object sender, RoutedEventArgs e)
        {
            stStylesheetExportUC = this;

            FileHelper fileHelper = new FileHelper();

            Boolean bIsFileAvailable = false;

            lMatchedStrings = new List<StylesheetModel>();

            string strDir = ConfigService.strDirFolder;

            string strFileName = (Constants.STYLESHEET_NAME + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

            LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(strFileName, Constants.REQ_STYLESHEETNAME, 0, tb_UserReq_Location.Text);

            progressBar.ShowDialog();

            bIsFileAvailable = fileHelper.Check_File_Exists(strDir + Constants.FILE_PATH + strFileName);

            //if (bIsFileAvailable == true)
            //{
                //string[] filelines = File.ReadAllLines(strDir + Constants.FILE_PATH + strFileName);

                //for (int iDx = 0; iDx < filelines.Length; iDx++)
                //{
                //    StringBuilder sb = new StringBuilder(filelines[iDx]);

                //    sb = Replace_String_Characters.Process_Replace_String_Characters(sb);

                //    lMatchedStrings.Add(new StylesheetModel() { StylesheetName = filelines[iDx], FileName = sb.ToString() });
                //}
               dg.ItemsSource = StylesheetName.lMatchedStrings;

                dg.Items.Refresh();
           // }
            //else
            //{
            //    strOperationMessage = Constants.ERROR_MSG_MAIN;

            //    strMessage = Constants.ERROR_MSG_FILE;

            //    Display display = new Display(strMessage, strOperationMessage);

            //    display.ShowDialog();
            //}
        }

        private void SelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            dg.SelectAll();
            
        }

        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {
            stStylesheetExportUC = this;

            if (System.IO.Directory.Exists(tb_Export_Location.Text))
            {
                string strFileName = "";

                string strStylesheetName = "";

                StylesheetExportModel stylesheetExportModel = new StylesheetExportModel();

                foreach (var item in dg.SelectedItems)
                {
                    strStylesheetName = strStylesheetName + ((StylesheetModel)item).StylesheetName + Constants.SEP_COMMA;

                    ((StylesheetModel)item).FileName = System.IO.Path.GetFileNameWithoutExtension(((StylesheetModel)item).FileName);

                    strFileName = strFileName + ((StylesheetModel)item).FileName + Constants.SEP_COMMA;
                }
                strStylesheetName = strStylesheetName.TrimEnd(Constants.COMMA_SEP);

                strStylesheetName = Constants.DOUBLE_QUOTES + strStylesheetName + Constants.DOUBLE_QUOTES;

                strFileName = strFileName.TrimEnd(Constants.COMMA_SEP);

                stylesheetExportModel.stylesheet.Add(strStylesheetName);

                stylesheetExportModel.exportedstylesheet.Add(strFileName);

                dg.UnselectAll();

                if (stylesheetExportModel.exportedstylesheet != null)
                {
                    LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(stylesheetExportModel, Constants.REQ_STYLESHEETEXPORT, tb_Export_Location.Text, 0);

                    progressBar.ShowDialog();

                    e.Handled = true;
                }
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
            
        }

        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strpattern = txtFilter.Text;

            List<StylesheetModel> lMatched = new List<StylesheetModel>();

            strpattern = Constants.SEP_CARET + Regex.Escape(strpattern).Replace(Constants.SEP_ASTERISK, Constants.SEP_DOT_ASTERISK) + Constants.SEP_DOLLAR;

            foreach (var item in StylesheetName.lMatchedStrings)
            {

                if (Regex.IsMatch(item.StylesheetName, strpattern, RegexOptions.IgnoreCase))
                {
                    lMatched.Add(new StylesheetModel() { StylesheetName = item.StylesheetName, FileName = item.FileName });
                }
            }
            dg.ItemsSource = lMatched;

            dg.Items.Refresh();
        }

        public void handleResponseEvent(int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_STYLESHEETEXPORT:
                        handleExportRespEvent();
                        break;
                    case Constants.REQ_STYLESHEETNAME:
                        handleStylesheetRespEvent();
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private void handleStylesheetRespEvent()
        {
            throw new NotImplementedException();
        }

        public void handleExportRespEvent()
        {
            string InpMsg = null;
            string strfinal = Execute_tc_exe.strError;
            if (String.IsNullOrEmpty(strfinal))
            {
                strOperationMessage = Constants.ERROR_MSG_INFO;
                InpMsg = "Operation Completed Successfully" + Environment.NewLine + "Name of unused stylesheets :" + Environment.NewLine + StylesheetExportServiceFinal.strStringFile;
              //  InpMsg = StylesheetExportServiceFinal.strStringFile;
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_ERROR;
                InpMsg = "Operation failed" + Environment.NewLine + Execute_tc_exe.strError;
            }
            
            string disp = StylesheetExportServiceFinal.strString ;

            Display display = new Display(disp, strOperationMessage, InpMsg);

            if (display.ShowDialog() == true)
            {
                dg.Items.Refresh();
            }

            display.ShowDialog();
        }
    }
}
