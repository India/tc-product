﻿using LMICMS.Dialogs;
using LMICMS.Infrastructure;
using LMICMS.Requests;
using LMICMS.Services;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for StylesheetImportUC.xaml
    /// </summary>
    public partial class StylesheetImportUC : UserControl
    {
        public static string strMessage = "";

        public string strOperationMessage = "";

        public static StylesheetImportUC stStylesheetImportUC = null;
        public StylesheetImportUC()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Stylesheet_Location.Text = AppConfig.StylesheetPath;
        }

        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Stylesheet_Location.Text))
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();

                openFileDialog.Multiselect = true;

                openFileDialog.InitialDirectory = tb_Stylesheet_Location.Text;

                openFileDialog.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";

                DirectoryInfo dir = new DirectoryInfo(tb_Stylesheet_Location.Text);

                if (openFileDialog.ShowDialog() == true)

                    foreach (string filename in openFileDialog.FileNames)
                    {
                        String strFileName = System.IO.Path.GetFileName(filename);

                        string directoryPath = System.IO.Path.GetDirectoryName(openFileDialog.FileName);

                        if (!dg.Items.Contains(strFileName))
                        {
                            dg.Items.Add(strFileName);
                        }
                        tb_Stylesheet_Location.Text = directoryPath;
                    }
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }
            private void Load_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Stylesheet_Location.Text))
            {
                string[] filePaths = Directory.GetFiles(tb_Stylesheet_Location.Text, Constants.FILE_EXT_ALL_XML);

                foreach (string file in filePaths)
                {
                    if (dg.Items.Contains(System.IO.Path.GetFileName(file)))
                    {

                    }
                    else
                    {
                        dg.Items.Add(System.IO.Path.GetFileName(file));
                    }
                }
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();

            }
        }

    

    private void Import_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Stylesheet_Location.Text))
            {
                stStylesheetImportUC = this;
                StylesheetImportModel stylesheetImportModel = new StylesheetImportModel();

                foreach (var item in dg.Items)
                {
                    stylesheetImportModel.stylesheet.Add(Convert.ToString(item));
                }

                stylesheetImportModel.strFilePath = tb_Stylesheet_Location.Text;

                LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(stylesheetImportModel, Constants.REQ_STYLESHEETIMPORT, 0, 0);

                progressBar.ShowDialog();

                e.Handled = true;
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            for (int counter = dg.Items.Count - 1; counter >= 0; counter--)
            {
                dg.Items.Remove(dg.SelectedItem);
            }
        }

        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        public void handleResponseEvent(int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_STYLESHEETIMPORT:
                        handleImportRespEvent();
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void handleImportRespEvent()
        {
            string InpMsg = null;
            string strfinal = Execute_tc_exe.strError;
            if (String.IsNullOrEmpty(strfinal))
            {
                strOperationMessage = Constants.ERROR_MSG_INFO;
                InpMsg = "Operation Completed Successfully";
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_ERROR;
                InpMsg = "Operation failed";
            }

            string disp = StylesheetImportService.strString;

            Display display = new Display(disp, strOperationMessage, InpMsg);

            if (display.ShowDialog() == true)
            {
                dg.Items.Refresh();
            }
        }
    }
}
