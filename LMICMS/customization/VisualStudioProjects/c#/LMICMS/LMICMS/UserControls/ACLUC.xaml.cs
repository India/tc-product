﻿using LMICMS.Dialogs;
using LMICMS.Infrastructure;
using LMICMS.Services;
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;


namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for ACLUC.xaml
    /// </summary>
    public partial class ACLUC : UserControl
    {
        public static string strMessage = "";

        public string strOperationMessage = "";

        public static ACLUC stACLUC = null;
        public ACLUC()
        {
            InitializeComponent();
        }

        private void Exp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Export_Location.Text))
            {
                System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                dialog.SelectedPath = tb_Export_Location.Text.Trim();

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    tb_Export_Location.Text = dialog.SelectedPath;
                }
                e.Handled = true;
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }

        }

        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {

            stACLUC = this;

            LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(tb_Export_Location.Text, Constants.REQ_ACLEXPORT, tb_acl_name.Text , 0);

            progressBar.ShowDialog();
        }

        private void Imp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Import_Location.Text))
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();

                openFileDialog.Multiselect = true;

                openFileDialog.InitialDirectory = tb_Import_Location.Text;

                openFileDialog.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";

                DirectoryInfo dir = new DirectoryInfo(tb_Import_Location.Text);

                if (openFileDialog.ShowDialog() == true)

                    foreach (string filename in openFileDialog.FileNames)
                    {
                        String strFileName = System.IO.Path.GetFileName(filename);

                        string directoryPath = System.IO.Path.GetDirectoryName(openFileDialog.FileName);

                        tb_Import_Location.Text = openFileDialog.FileName;
                    }
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void Import_Button_Click(object sender, RoutedEventArgs e)
        {
                stACLUC = this;
              
                LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(tb_Import_Location.Text, Constants.REQ_ACLIMPORT, 0, 0);

                progressBar.ShowDialog();

                e.Handled = true;
            }        
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Export_Location.Text = AppConfig.ACLPath;
            tb_Import_Location.Text = AppConfig.ACLPath;
        }

        public void handleResponseEvent(int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_ACLEXPORT:
                        handleExportRespEvent();
                        break;
                    case Constants.REQ_ACLIMPORT:
                        handleImportRespEvent();
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private void handleImportRespEvent()
        {
            strOperationMessage = Constants.MSG_LABEL;

            string disp = ACLImportService.strString;

            Display display = new Display(disp, strOperationMessage, "Wrong Action");

            if (display.ShowDialog() == true)
            {
                display.ShowDialog();
            }   
        }

        void handleExportRespEvent()
        {
            strOperationMessage = Constants.MSG_LABEL;

            string disp = ACLExportService.strStringOut;

            Display display = new Display(disp, strOperationMessage, "Wrong Action");

            if (display.ShowDialog() == true)
            {
                display.ShowDialog();
            }
   
        }
       
    }
}
