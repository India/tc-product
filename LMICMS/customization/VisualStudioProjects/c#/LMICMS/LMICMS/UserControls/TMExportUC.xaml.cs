﻿using LMICMS.Common.Utilities;
using LMICMS.Dialogs;
using LMICMS.Infrastructure;
using LMICMS.Requests;
using LMICMS.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using System.IO;

using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for TMExportUC.xaml
    /// </summary>
    public partial class TMExportUC : UserControl
    {
        public static string strMessage = "";

        public string strOperationMessage = "";

        List<TransferModeModel> lMatchedStrings = null;

        private static string strTransferModeName = "";
        public static string strString
        {
            get { return strTransferModeName; }

            set { strTransferModeName = value; }
        }
        public static TMExportUC stTMExportUC = null;
        public TMExportUC()
        {
            InitializeComponent();
        }

        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Export_Location.Text))
            {
                System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                dialog.SelectedPath = tb_Export_Location.Text.Trim();

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    tb_Export_Location.Text = dialog.SelectedPath;
                }
                e.Handled = true;
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void Refresh_Button_Click(object sender, RoutedEventArgs e)
        {
            stTMExportUC = this;

            FileHelper fileHelper = new FileHelper();

            Boolean bIsFileAvailable = false;

            lMatchedStrings = new List<TransferModeModel>();

            string strDir = ConfigService.strDirFolder;

            string strFileName = (Constants.TRANSFER_MODE_NAME + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

            LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(strFileName, Constants.REQ_TRANSFERMODENAME, 0, 0);

            progressBar.ShowDialog();

            bIsFileAvailable = fileHelper.Check_File_Exists(strDir + Constants.FILE_PATH + strFileName);

            if (bIsFileAvailable == true)
            {
                string strText;
               
                using (var streamReader = new StreamReader(strDir + Constants.FILE_PATH + strFileName, Encoding.UTF8))
                {
                    strText = streamReader.ReadToEnd();
                }

                string[] parts = strText.Split(new[] { Constants.NEW_LINE_ONE, Constants.NEW_LINE_TWO }, StringSplitOptions.RemoveEmptyEntries);

                int i, j;

                for (i = 0, j = 1; i < parts.Length && j < parts.Length; i += 2, j += 2)
                {  
                    {
                        string strValue1 = parts[i];
                        string strValue2 = parts[j];

                        lMatchedStrings.Add(new TransferModeModel() { TransferModeName = strValue1, FileName = strValue1, UID = strValue2 });
                    }
                    dg.ItemsSource = lMatchedStrings;

                    dg.Items.Refresh();
                }
            }

            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void SelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            dg.SelectAll();
        }

        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Export_Location.Text))
            {
                string strFileName = "";

                string strUID = "";

                stTMExportUC = this;

                TransferModeExportModel transferModeExportModel = new TransferModeExportModel(); 

                foreach (var item in dg.SelectedItems)
                {
                    strTransferModeName = strTransferModeName + ((TransferModeModel)item).TransferModeName + Constants.SEP_COMMA;

                    ((TransferModeModel)item).FileName = System.IO.Path.GetFileNameWithoutExtension(((TransferModeModel)item).FileName);

                    strFileName = strFileName + ((TransferModeModel)item).FileName + Constants.SEP_COMMA;

                    strUID = strUID + ((TransferModeModel)item).UID + Constants.SEP_COMMA;
                }
                strFileName = strFileName.TrimEnd(Constants.COMMA_SEP);

                strUID = strUID.TrimEnd(Constants.COMMA_SEP);

                transferModeExportModel.transfermode.Add(strTransferModeName);

                transferModeExportModel.exportedtransfermode.Add(strFileName);

                transferModeExportModel.transfermodeUID.Add(strUID);

                if (transferModeExportModel.exportedtransfermode != null)
                {
                    LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(transferModeExportModel, Constants.REQ_TRANSFERMODEEXPORT, tb_Export_Location.Text, 0);

                    progressBar.ShowDialog();

                    e.Handled = true;
                }
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }


        }

        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Export_Location.Text = AppConfig.TransferModePath;
        }

        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strpattern = txtFilter.Text;

            List<WorkflowModel> lMatched = new List<WorkflowModel>();

            strpattern = Constants.SEP_CARET + Regex.Escape(strpattern).Replace(Constants.SEP_ASTERISK, Constants.SEP_DOT_ASTERISK) + Constants.SEP_DOLLAR;

            foreach (var item in lMatchedStrings)
            {

                if (Regex.IsMatch(item.TransferModeName, strpattern, RegexOptions.IgnoreCase))
                {
                    lMatched.Add(new WorkflowModel() { WorkflowName = item.TransferModeName, FileName = item.FileName });
                }
            }
            dg.ItemsSource = lMatched;

            dg.Items.Refresh();
        }
        public void handleResponseEvent(int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_TRANSFERMODEEXPORT:
                        handleExportRespEvent();
                        break;
                    case Constants.REQ_TRANSFERMODENAME:
                        handleTransferModeRespEvent();
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        void handleExportRespEvent()
        {
            string InpMsg = null;
            string strfinal = Execute_tc_exe.strError;
            if (String.IsNullOrEmpty(strfinal))
            {
                strOperationMessage = Constants.ERROR_MSG_INFO;
                InpMsg = "Operation Completed Successfully";
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_ERROR;
                InpMsg = "Operation failed";
            }
           // strOperationMessage = Constants.MSG_LABEL;

            string disp = TransferModeExportService.strStringOut;

            Display display = new Display(disp, strOperationMessage, InpMsg);

            if (display.ShowDialog() == true)
            {
                dg.Items.Refresh();
            }

            display.ShowDialog();
        }

        private void handleTransferModeRespEvent()
        {
            throw new NotImplementedException();
        }
    }
}
