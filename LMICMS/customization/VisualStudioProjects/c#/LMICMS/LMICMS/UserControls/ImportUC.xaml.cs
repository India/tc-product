﻿using LMICMS.Dialogs;
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Infrastructure;
using LMICMS.Requests;
using LMICMS.Services;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for ImportUC.xaml
    /// </summary>
    ///  
    public partial class ImportUC : UserControl
    {
        public static string strMessage = "";

        public string strOperationMessage = "";
     
        public static ImportUC stImportUC = null;

        public ImportUC()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Query_Location.Text = AppConfig.QueryPath;
        }

        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Query_Location.Text))
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();

                openFileDialog.Multiselect = true;

                openFileDialog.InitialDirectory = tb_Query_Location.Text;

                openFileDialog.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";

                DirectoryInfo dir = new DirectoryInfo(tb_Query_Location.Text);

                if (openFileDialog.ShowDialog() == true)

                    foreach (string filename in openFileDialog.FileNames)
                    {
                        String strFileName = System.IO.Path.GetFileName(filename);

                        string directoryPath = System.IO.Path.GetDirectoryName(openFileDialog.FileName);

                        if (!dg.Items.Contains(strFileName))
                        {
                            dg.Items.Add(strFileName);
                        }
                        tb_Query_Location.Text = directoryPath;
                    }
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }
        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            for (int counter = dg.Items.Count - 1; counter >= 0; counter--)
            {
                dg.Items.Remove(dg.SelectedItem);
            }
        }

        private void Import_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Query_Location.Text))
            {
                stImportUC = this;

                ImportModel importModel = new ImportModel();

                foreach (var item in dg.Items)
                {
                    importModel.queries.Add(Convert.ToString(item));
                }

                importModel.strFilePath = tb_Query_Location.Text;

                LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(importModel, Constants.REQ_IMPORT, 0, 0);

                progressBar.ShowDialog();

                e.Handled = true;
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void Load_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Query_Location.Text))
            {
                string[] filePaths = Directory.GetFiles(tb_Query_Location.Text, Constants.FILE_EXT_ALL_XML);

                foreach (string file in filePaths)
                {
                    if (dg.Items.Contains(System.IO.Path.GetFileName(file))) 
                    {

                    }
                    else
                    {
                        dg.Items.Add(System.IO.Path.GetFileName(file));
                    }
                }
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();

            }
        }

        public void handleResponseEvent(int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_IMPORT:
                        handleImportRespEvent();
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void handleImportRespEvent()
        {
            string InpMsg = null;
            string strfinal = Execute_tc_exe.strError;
            if (String.IsNullOrEmpty(strfinal))
            {
                strOperationMessage = Constants.ERROR_MSG_INFO;
                InpMsg = "Operation Completed Successfully";
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_ERROR;
                InpMsg = "Operation failed";
            }
          //  strOperationMessage = Constants.MSG_LABEL;

            string disp = ImportService.strString;

            Display display = new Display(disp, strOperationMessage, InpMsg);

            if (display.ShowDialog() == true)
            {
                dg.Items.Refresh();
            }
        } 
    }
}
         
           
            
           





