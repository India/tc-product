﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for TransferModeUC.xaml
    /// </summary>
    public partial class TransferModeUC : UserControl
    {
        public TransferModeUC()
        {
            InitializeComponent();
        }

        private void TabTransferModeSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            {
                string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

                switch (tabItem)
                {
                    case "Export":
                        ccExport.Content = new TMExportUC();
                        break;
                    case "Import":
                        ccImport.Content = new TMImportUC();
                        break;
                }
                e.Handled = true;
            }
        }
    }
}
