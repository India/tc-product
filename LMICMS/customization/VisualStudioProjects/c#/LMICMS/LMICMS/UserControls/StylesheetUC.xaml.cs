﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for StylesheetUC.xaml
    /// </summary>
    public partial class StylesheetUC : UserControl
    {
        public StylesheetUC()
        {
            InitializeComponent();
        }
        private void TabStylesheetSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            {
                string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

                switch (tabItem)
                {
                    case "Export":
                        ccExport.Content = new StylesheetExportUC();
                        break;
                    case "Import":
                        ccImport.Content = new StylesheetImportUC();
                        break;
                }
                e.Handled = true;
            }

        }
    }
}
