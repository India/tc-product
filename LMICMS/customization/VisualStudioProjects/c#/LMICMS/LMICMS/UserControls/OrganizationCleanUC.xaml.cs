﻿using LMICMS.Dialogs;
using LMICMS.Infrastructure;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for OrganizationCleanUC.xaml
    /// </summary>
    public partial class OrganizationCleanUC : UserControl
    {
        public static string strMessage = "";

        public string strOperationMessage = "";

        public static OrganizationCleanUC storganizationCleanUC = null;

        public OrganizationCleanUC()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Input_Location.Text = AppConfig.OrganizationPath;
            tb_Output_Location.Text = AppConfig.OrganizationPath;
        }

        private void Input_Location_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Input_Location.Text))
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();

                openFileDialog.Multiselect = true;

                openFileDialog.InitialDirectory = tb_Input_Location.Text;

                openFileDialog.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";

                DirectoryInfo dir = new DirectoryInfo(tb_Input_Location.Text);

                if (openFileDialog.ShowDialog() == true)

                    foreach (string filename in openFileDialog.FileNames)
                    {
                        String strFileName = System.IO.Path.GetFileName(filename);

                        string directoryPath = System.IO.Path.GetDirectoryName(openFileDialog.FileName);

                        tb_Input_Location.Text = openFileDialog.FileName;
                    }
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void Output_Location_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(tb_Output_Location.Text))
            {
                System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                dialog.SelectedPath = tb_Output_Location.Text.Trim();

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    tb_Output_Location.Text = dialog.SelectedPath;
                }
                e.Handled = true;
            }
            else
            {
                strOperationMessage = Constants.ERROR_MSG_MAIN;

                strMessage = Constants.ERROR_MSG;

                WarningDisplay display = new WarningDisplay(strMessage, strOperationMessage);

                display.ShowDialog();
            }
        }

        private void Org_Name_Submit_Button_Click(object sender, RoutedEventArgs e)
        {
            
             storganizationCleanUC = this;

            LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(tb_Output_Location.Text, Constants.REQ_ORGANISATIONCLEAN, tb_Organization_Name.Text, tb_Input_Location.Text);

            progressBar.ShowDialog();

        }
    }
}
