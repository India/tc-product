﻿using LMICMS.Infrastructure;
using LMICMS.Services;
using LMICMS.UserControls;
using System;

namespace LMICMS.Requests
{
    class ReqandResp
    {
        
        public ReqandResp()
        {
        }
        public void processReqEvent(Object objInp, int iEventType, Object val, Object value)
        {

            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_IMPORT:
                        req_import_operation(objInp);
                        break;
                    case Constants.REQ_WORKFLOWIMPORT:
                        req_workflow_import_operation(objInp);
                        break;
                    case Constants.REQ_TRANSFERMODEIMPORT:
                        req_transfer_mode_import_operation(objInp);
                        break;
                    case Constants.REQ_ACLIMPORT:
                        req_acl_import_operation(objInp);
                        break;
                    case Constants.REQ_STYLESHEETIMPORT:
                        req_stylesheet_import_operation(objInp);
                        break;
                    case Constants.REQ_ORGANISATIONIMPORT:
                        req_organisation_import_operation(objInp);
                        break;
                    case Constants.REQ_EXPORT:
                        req_export_operation(objInp, val);
                        break;
                    case Constants.REQ_WORKFLOWEXPORT:
                        req_workflowexport_operation(objInp, val);
                        break;
                    case Constants.REQ_TRANSFERMODEEXPORT:
                        req_transfer_mode_export_operation(objInp, val);
                        break;
                    case Constants.REQ_ACLEXPORT:
                        req_acl_export_operation(objInp, val);
                        break;
                    case Constants.REQ_STYLESHEETEXPORT:
                        req_stylesheet_export_operation(objInp, val);
                        break;
                    case Constants.REQ_ORGANISATIONEXPORT:
                        req_organisation_export_operation(objInp, val);
                        break;
                    case Constants.REQ_QUERYNAME:
                        req_query_operation(objInp);
                        break;
                    case Constants.REQ_WORKFLOWNAME:
                        req_workflow_operation(objInp);
                        break;
                    case Constants.REQ_TRANSFERMODENAME:
                        req_transfer_mode_operation(objInp);
                        break;
                    case Constants.REQ_STYLESHEETNAME:
                        req_stylesheet_operation(objInp, value);
                        break;
                    case Constants.REQ_ORGANISATIONCLEAN:
                        req_clean_operation(objInp,val, value);
                        break;


                }
            }
            catch (Exception exception)
            {
            }

        }

      

        private void req_query_operation(object objInp)
        {
            try
            {

                QueryName obj = new QueryName();

                obj.ProcessQueryName(objInp.ToString());
            }

            catch (Exception exception)
            {
            }
        }
        private void req_workflow_operation(object objInp)
        {
            try
            {

                WorkflowName obj = new WorkflowName();

                obj.ProcessWorkflowName(objInp.ToString());
            }

            catch (Exception exception)
            {
            }
        }
        private void req_transfer_mode_operation(object objInp)
        {
            try
            {

                TransferModeName obj = new TransferModeName();

                obj.ProcessTransferModeName(objInp.ToString());
            }

            catch (Exception exception)
            {
            }
        }
        private void req_stylesheet_operation(object objInp, object value)
        {
            try
            {

                StylesheetName obj = new StylesheetName();

                obj.ProcessStylesheetName(objInp.ToString(), value.ToString());
            }

            catch (Exception exception)
            {
            }
        }
        private void req_clean_operation(object objInp, object val, object value)
        {
            try
            {

                OrganisationCleanService obj = new OrganisationCleanService();

                obj.ProcessOrganisationClean(objInp.ToString(), val.ToString(), value.ToString());
            }

            catch (Exception exception)
            {
            }
        }
        private void req_export_operation(object objInp, object val)
        {
            try
            {
                ExportService obj = new ExportService();

                ExportModel model = (ExportModel)objInp;

                obj.ProcessExport(model, val.ToString());
            }

            catch (Exception exception)
            {
            }

        }
        private void req_workflowexport_operation(object objInp, object val)
        {
            try
            {
                WorkflowExportService obj = new WorkflowExportService();

                WorkflowExportModel workflowExportModel = (WorkflowExportModel)objInp;

                obj.WorkflowProcessExport(workflowExportModel, val.ToString());
            }

            catch (Exception exception)
            {
            }

        }

        private void req_transfer_mode_export_operation(object objInp, object val)
        {
            try
            {
                TransferModeExportService obj = new TransferModeExportService();

                TransferModeExportModel model = (TransferModeExportModel)objInp;

                obj.TransferModeProcessExport(model, val.ToString());
            }

            catch (Exception exception)
            {
            }
        }
        private void req_acl_export_operation(object objInp, object val)
        {
            try
            {
                ACLExportService obj = new ACLExportService();

               // TransferModeExportModel model = (TransferModeExportModel)objInp;

                obj.ACLProcessExport(objInp.ToString(), val.ToString());
            }

            catch (Exception exception)
            {
            }
        }
        private void req_stylesheet_export_operation(object objInp, object val)
        {
            StylesheetExportService obj = new StylesheetExportService();

            StylesheetExportModel stylesheetExportModel = (StylesheetExportModel)objInp;

            obj.StylesheetProcessExport(stylesheetExportModel, val.ToString());
        }

        private void req_organisation_export_operation(object objInp, object val)
        {
            OrganisationExportService obj = new OrganisationExportService();

            obj.OrganisationProcessExport(objInp.ToString(), val.ToString());
       
        }
        private void req_import_operation(object objInp)
        {
            try
            {
                ImportService obj = new ImportService();

                ImportModel model = (ImportModel)objInp;

                obj.ProcessImport(model);
            }
            catch (Exception exception)
            {
            }
        }

        private void req_workflow_import_operation(object objInp)
        {
            try
            {
                WorkflowImportService obj = new WorkflowImportService();

                WorkflowImportModel workflowImportModel = (WorkflowImportModel)objInp;

                obj.WorkflowProcessImport(workflowImportModel);
                
            }
            catch (Exception exception)
            {
            }
        }
        private void req_transfer_mode_import_operation(object objInp)
        {
            TransferModeImportService obj = new TransferModeImportService();

            TransferModeImportModel transferModeImportModel = (TransferModeImportModel)objInp;

            obj.TransferModeProcessImport(transferModeImportModel);
        }
        private void req_acl_import_operation(object objInp)
        {
            ACLImportService obj = new ACLImportService();

            obj.ACLProcessImport(objInp.ToString());
        }

        private void req_stylesheet_import_operation(object objInp)
        {
            StylesheetImportService obj = new StylesheetImportService();
            StylesheetImportModel stylesheetImportModel = (StylesheetImportModel)objInp;


            obj.StylesheetProcessImport(stylesheetImportModel);
        }
        private void req_organisation_import_operation(object objInp)
        {
            OrganisationImportService obj = new OrganisationImportService();
            obj.OrganisationProcessImport(objInp.ToString());
        }
        public void handleResponseEvent(Object outObj, int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_CANCELLED:
                        break;
                    case Constants.REQ_IMPORT:
                        ImportUC.stImportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_WORKFLOWIMPORT:
                        WFImportUC.stWFImportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_TRANSFERMODEIMPORT:
                        TMImportUC.stTMImportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_ACLIMPORT:
                        ACLUC.stACLUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_STYLESHEETIMPORT:
                        StylesheetImportUC.stStylesheetImportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_ORGANISATIONIMPORT:
                        OrganizationExportImportUC.ststorganizationExportImportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_EXPORT:
                        ExportUC.stExportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_WORKFLOWEXPORT:
                        WFExportUC.stWFExportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_TRANSFERMODEEXPORT:
                        TMExportUC.stTMExportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_ACLEXPORT:
                        ACLUC.stACLUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_STYLESHEETEXPORT:
                        StylesheetExportUC.stStylesheetExportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_ORGANISATIONEXPORT:
                        OrganizationExportImportUC.ststorganizationExportImportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_QUERYNAME:
                        ExportUC.stExportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_WORKFLOWNAME:
                        WFExportUC.stWFExportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_TRANSFERMODENAME:
                        TMExportUC.stTMExportUC.handleResponseEvent(iEventType);
                        break;
                    case Constants.REQ_STYLESHEETNAME:
                        StylesheetExportUC.stStylesheetExportUC.handleResponseEvent(iEventType);
                        break;
                }
            }
            catch (Exception exception)
            {
                /*  No need to do anything here, because this has been called by Custom Progress bar form, and upto now that form has already been disposed,
                 *  so that exception will become unhandled, leaving it the way it is and it will remain the thing as it is. 
                 */
            }
        }
    }
}