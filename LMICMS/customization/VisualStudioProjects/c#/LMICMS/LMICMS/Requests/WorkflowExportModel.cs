﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Requests
{
   public class WorkflowExportModel
    {
        public WorkflowExportModel()
        {
            ExportWFNames = new List<string>();
        }
        public String strFilePath { get; set; }
        public List<String> ExportWFNames { get; set; }
    }
}
