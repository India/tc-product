﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Requests
{
   public class StylesheetImportModel
    {
        public StylesheetImportModel()
        {
            stylesheet = new List<string>();
        }
        public String strFilePath { get; set; }
        public List<String> stylesheet { get; set; }
    }
}
