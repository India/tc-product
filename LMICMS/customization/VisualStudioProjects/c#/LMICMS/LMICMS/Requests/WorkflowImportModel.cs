﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Requests
{
    class WorkflowImportModel
    {
        public WorkflowImportModel()
        {
            workflownames = new List<string>();
        }
        public String strFilePath { get; set; }
        public List<String> workflownames { get; set; }
    }

}
