﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Requests
{
    public class WorkflowModel
    {
        public string WorkflowName { get; set; }
        public string FileName { get; set; }
    }
}

