﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Requests
{
   public class TransferModeExportModel
    {
        public TransferModeExportModel()
        {
            transfermode = new List<string>();

            exportedtransfermode = new List<string>();

            transfermodeUID = new List<string>();
        }

        public List<String> transfermode { get; set; }
        public List<String> exportedtransfermode { get; set; }
        public List<String> transfermodeUID { get; set; }
    }
}
