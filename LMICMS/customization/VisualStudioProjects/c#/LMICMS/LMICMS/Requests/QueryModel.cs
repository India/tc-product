﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Requests
{
    public class QueryModel
    {
        public string QueryName { get; set; }

        public string FileName { get; set; }
    }
}
