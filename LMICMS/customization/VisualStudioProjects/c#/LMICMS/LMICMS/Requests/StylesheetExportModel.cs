﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Requests
{
   public  class StylesheetExportModel
    {
        public StylesheetExportModel()
        {
            stylesheet = new List<string>();

            exportedstylesheet = new List<string>();
        }

        public List<String> stylesheet { get; set; }
        public List<String> exportedstylesheet { get; set; }
    }
}
