﻿using System;
using System.Collections.Generic;

namespace LMICMS.Requests
{
    public class ExportModel
    {
        public ExportModel()
        {
            queries = new List<string>();

            exportedqueries = new List<string>();
        }
        
        public List<String> queries { get; set; }
        public List<String> exportedqueries { get; set; }
    }
}


