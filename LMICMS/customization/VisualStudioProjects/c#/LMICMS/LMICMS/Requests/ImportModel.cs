﻿using System;
using System.Collections.Generic;

namespace LMICMS.Requests
{
    public class ImportModel
    {
        public ImportModel()
        {
            queries = new List<string>();
        }
        public String strFilePath { get; set; }
        public List<String> queries { get; set; }
    }
}
