﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Requests
{
   public class TransferModeModel
    {
        public string TransferModeName { get; set; }

        public string FileName { get; set; }

        public string UID { get; set; }
    }
}
