﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Requests
{
  public  class TransferModeImportModel
    {
        public TransferModeImportModel()
        {
            transfermode = new List<string>();
        }
        public String strFilePath { get; set; }
        public List<String> transfermode { get; set; }
    }
}
