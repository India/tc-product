﻿using System;
using LMICMS.Infrastructure;
using LMICMS.Requests;
using System.IO;

namespace LMICMS.Services
{
   public class ExportService
    { 
        public void ProcessExport(ExportModel exportModel, string strPath)
        {
            string strOutput   = null;

            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            try
            {
                string strDir = ConfigService.strDirFolder;

                string strFileName = Path.Combine(strDir, Constants.EXP_QUERY + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                string strLogFile = Path.Combine(strDir, Constants.EXPORT_QUERY_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                foreach (var str in exportModel.queries)
                foreach (var str2 in exportModel.exportedqueries)
                {
                    string strArguments = "LMICMSUTILS -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=" + AppConfig.UserGroup + " -mode=query -operation=export -internalname=" + str + " -exportfilename=" + str2 + " -tcconfigfilename=ExportQuery.txt -filepath=" + strDir;

                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    Execute_File_Write.Process_Execute_File_Write(strFileName);

                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);

                    ExportServiceFinal exportServiceFinal = new ExportServiceFinal();

                    exportServiceFinal.ProcessExportFinal(strFileName, strPath, strLogFile, exportModel);
                }
            }
            catch(Exception ex)
            {

            }
  
        }
         
    }
}

    

