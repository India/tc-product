﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Services
{
    class DateTimeUtilities
    {
        /*  This Function return current epoch time, Epoch time is current time in seconds from 1st January 1970 */
        public int getEpochTime()
        {
            int iEpochTime = -1;

            /*  Generating current epoch time */
            iEpochTime = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;

            return iEpochTime;
        }
    }
}
