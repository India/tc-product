﻿using LMICMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Services
{
    class TransferModeName
    {
        public void ProcessTransferModeName(string strFileName)
        {
            string strOutput = null;

            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            try
            {
                string strDir = ConfigService.strDirFolder;

                string strArguments = "LMICMSUTILS -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=" + AppConfig.UserGroup + " -mode=transfer_mode -operation=export_transfer_mode_name -tcconfigfilename=" + strFileName + " -filepath=" + strDir;

                execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);
            }
            catch (Exception ex)
            {

            }

        }
    }
}
