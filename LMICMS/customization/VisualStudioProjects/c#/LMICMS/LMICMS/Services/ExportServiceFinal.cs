﻿using LMICMS.Infrastructure;
using System;
using System.IO;
using System.Text;
using LMICMS.Requests;


namespace LMICMS.Services
{
    class ExportServiceFinal
    {
        public static string strOutput = "";

        public static string strString
        {
            get { return strOutput; }

            set { strOutput = value; }
        }

        public void ProcessExportFinal(string strFinalFileName, string strQueryPath, string strFinalLog, ExportModel exportModel)
        {
            string strDir = ConfigService.strDirFolder;

            string strFile = strDir + Constants.FILE_PATH + Constants.EXP_FILE_NAME;

            string strText;

            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            var fileStream = new FileStream(strFile, FileMode.Open, FileAccess.Read);

            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                strText = streamReader.ReadToEnd();
            }

            string[] parts = strText.Split(new[] { Constants.NEW_LINE_ONE, Constants.NEW_LINE_TWO }, StringSplitOptions.RemoveEmptyEntries);

            int i, j;

            for (i = 0, j = 1; i < parts.Length && j < parts.Length; i += 2, j += 2)
            {
                string strValue1 = parts[i];

                string strValue3 = Constants.DOUBLE_QUOTES + strQueryPath + Constants.FILE_PATH + strValue1 + Constants.DOUBLE_QUOTES;
                
                string strValue2 = parts[j];

                string strArguments = "plmxml_export -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=dba -xml_file=" + strValue3 + Constants.FILE_EXT_XML + " -uid=" + strValue2;

                strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFinalFileName);
  
                Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strFinalLog);
                
            }
        }
    }
}


   

    
