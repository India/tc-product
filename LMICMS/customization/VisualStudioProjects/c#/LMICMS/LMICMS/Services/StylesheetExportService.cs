﻿using LMICMS.Infrastructure;
using LMICMS.Requests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Services
{
    class StylesheetExportService
    {
        public void StylesheetProcessExport(StylesheetExportModel stylesheetExportModel, string strPath)
        {
            string strOutput = null;

            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            StylesheetWriteFile stylesheetWriteFile = new StylesheetWriteFile();

            try
            {
                string strDir = ConfigService.strDirFolder;

                string strFileName = Path.Combine(strDir, Constants.EXP_STYLESHEET + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                string strLogFile = Path.Combine(strDir, Constants.EXPORT_STYLESHEET_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                foreach (var str in stylesheetExportModel.stylesheet)
                    foreach (var str2 in stylesheetExportModel.exportedstylesheet)
                    {
                        string strArguments = "LMICMSUTILS -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=" + AppConfig.UserGroup + " -mode=stylesheet -operation=export -internalname=" + str + " -exportfilename=" + str2 + " -tcconfigfilename=ExportStylesheet.txt -filepath=" + strDir;

                        strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                        Execute_File_Write.Process_Execute_File_Write(strFileName);

                        Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                        Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);

                        stylesheetWriteFile.Process_Execute_Stylesheet_Write_File(str, str2, strPath);

                        StylesheetExportServiceFinal stylesheetExportServiceFinal = new StylesheetExportServiceFinal();

                        stylesheetExportServiceFinal.StylesheetProcessExportFinal(strFileName, strPath, strLogFile, stylesheetExportModel);
                    }
            }
            catch (Exception ex)
            {

            }

        }

    }
}
