﻿using LMICMS.Infrastructure;
using LMICMS.Requests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Services
{
    class WorkflowImportService
    {
        public static string strOutput = "";

        public static string strString
        {
            get { return strOutput; }

            set { strOutput = value; }
        }

        public void WorkflowProcessImport(WorkflowImportModel WorkflowImportModel)
        {
            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            string strDir = ConfigService.strDirFolder;

            string strFileName = Path.Combine(strDir, Constants.IMP_WORKFLOW + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

            string strLogFile = Path.Combine(strDir, Constants.IMPORT_WORKFLOW_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

            Boolean bIsFirstQuery = true;

            foreach (var item in WorkflowImportModel.workflownames)
            {
                string strArguments = "plmxml_import -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=dba -xml_file=" + Constants.DOUBLE_QUOTES + WorkflowImportModel.strFilePath + Constants.FILE_PATH + item + Constants.DOUBLE_QUOTES + " -transfermode=workflow_template_overwrite";

                strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                if (bIsFirstQuery == true)
                {
                    bIsFirstQuery = false;

                    Execute_File_Write.Process_Execute_File_Write(strFileName);
                }

                Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
            }
        }
    }
}

