﻿using System;
using System.IO;

namespace LMICMS.Services
{
  public class Execute_File_Write_Arguments
    {
        public static void Process_Execute_File_Write_Arguments(string strArguments, string strFileName)
        {
            File.AppendAllText(strFileName, strArguments + Environment.NewLine);
        }
    }
}
