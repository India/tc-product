﻿using LMICMS.Infrastructure;
using LMICMS.Requests;
using System;
using System.IO;

namespace LMICMS.Services
{
    class WorkflowExportService
    {
        public static string strOutput = "";

        public static string strStringOut
        {
            get { return strOutput; }

            set { strOutput = value; }
        }
        public void WorkflowProcessExport(WorkflowExportModel workflowExportModel, string strPath)
        {
            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            try
            {
                string strDir = ConfigService.strDirFolder;

                string strFileName = Path.Combine(strDir, Constants.EXP_WORKFLOW + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                string strLogFile = Path.Combine(strDir, Constants.EXPORT_WORKFLOW_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);
                Boolean bIsFirstQuery = true;
                foreach (var str in workflowExportModel.ExportWFNames)
                {
                    string[] parts = str.Split(new[] { Constants.NEW_LINE_ONE, Constants.NEW_LINE_TWO }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < parts.Length; i++)
                    {
                        string strValue1 = parts[i];

                        string strValue3 = Constants.DOUBLE_QUOTES + strPath + Constants.FILE_PATH + strValue1 + Constants.DOUBLE_QUOTES;

                        string strArguments = "plmxml_export -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=dba -template=" + strValue1 + " -xml_file=" + strValue3 + Constants.FILE_EXT_XML;

                        strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                        if (bIsFirstQuery == true)
                        {
                            bIsFirstQuery = false;

                            Execute_File_Write.Process_Execute_File_Write(strFileName);
                        }

                        Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                        Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
