﻿using LMICMS.Infrastructure;
using System;
using System.IO;

namespace LMICMS.Services
{
    public class Execute_Log_Write_File
    {
        public static void Process_Execute_Log_Write_File(string strOutput, string strLogFile)
        { 
            StreamWriter writer = new StreamWriter(strLogFile, true);

            writer.WriteLine(DateTime.Now.ToString(Constants.DATE_TIME_STAMP));

            writer.Write(strOutput);

            writer.WriteLine(Constants.NEW_LINE_SEP);

            writer.Close();
        }
    }
}
