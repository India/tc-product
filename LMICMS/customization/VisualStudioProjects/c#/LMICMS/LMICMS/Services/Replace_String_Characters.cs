﻿using LMICMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LMICMS.Services
{
  public class Replace_String_Characters
    {
        public static StringBuilder Process_Replace_String_Characters(StringBuilder sb)

        {
            string strSeperatorOne = Constants.SEP_DASH;

            string strSeperatorTwo = Constants.SEP_SPACE;

            string strSeperatorThree = Constants.SEP_DOT;

            string strSeperatorFour = Constants.SEP_SING_QUOTE;

            string strSeperatorFive = Constants.SEP_BRACES;

            string strReplacedSeperatorOne = Constants.SEP_UNDERSCORE;

            string strReplacedSeperatorTwo = Constants.SEP_NULL;

            sb.Replace(strSeperatorOne, strReplacedSeperatorTwo);

           // Regex.Replace(sb.ToString(), @"\s+", " ");

            sb.Replace(strSeperatorTwo, strReplacedSeperatorOne);

            sb.Replace(strSeperatorThree, strReplacedSeperatorTwo);

            sb.Replace(strSeperatorFour, strReplacedSeperatorOne);

            sb.Replace(strSeperatorFive, strReplacedSeperatorTwo);

            return sb;
        }
    }
}
