﻿using LMICMS.Common.Constants;
using LMICMS.Infrastructure;
using System.Diagnostics;

namespace LMICMS.Services
{
  public class Execute_tc_exe
    {
        public static string strError = "";
        public string Process_Execute_tc_exe(string strArguments, string strOutput)
        {
            //string strError = null;

            Process process = new Process();

            process.StartInfo.FileName = Constants.CMD_PROMPT;

            process.StartInfo.CreateNoWindow = true;

            process.StartInfo.RedirectStandardInput = true;

            process.StartInfo.RedirectStandardOutput = true;

            process.StartInfo.RedirectStandardError = true;

            process.StartInfo.UseShellExecute = false;

            process.Start();

            process.StandardInput.WriteLine(GenericConstants.SET_ECHO_OFF);

            process.StandardInput.WriteLine(GenericConstants.SET_TC_ROOT + AppConfig.TCRoot);

            process.StandardInput.WriteLine(GenericConstants.SET_TC_DATA + AppConfig.TCData);

            process.StandardInput.WriteLine(GenericConstants.TC_DATA_TC_PROFILEVARS);

            process.StandardInput.WriteLine(strArguments);

            process.StandardInput.Flush();

            process.StandardInput.Close();

            process.WaitForExit();

            strOutput = process.StandardOutput.ReadToEnd();

            strError = process.StandardError.ReadToEnd();

            return strOutput + strError;
            
        }
    }
}
