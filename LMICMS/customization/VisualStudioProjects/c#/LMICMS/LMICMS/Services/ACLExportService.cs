﻿using LMICMS.Infrastructure;
using System;
using System.IO;


namespace LMICMS.Services
{
    class ACLExportService
    {
        public static string strOutput = "";

        public static string strStringOut
        {
            get { return strOutput; }

            set { strOutput = value; }
        }
        public void ACLProcessExport( string strPath, string straclname)
        {
            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            try
            {
                string strDir = ConfigService.strDirFolder;

                string strFileName = Path.Combine(strDir, Constants.EXP_ACL + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                string strLogFile = Path.Combine(strDir, Constants.EXPORT_ACL_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);
                if (String.IsNullOrEmpty(straclname))
                {
                    string strLoc = strPath + Constants.FILE_PATH + "tc_rule_tree" + Constants.FILE_EXT_XML;
                    string strArguments = "am_install_tree -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=" + AppConfig.UserGroup + " -operation=export -path=" + strLoc + " -format=xml";
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    Execute_File_Write.Process_Execute_File_Write(strFileName);

                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);

                }
                else
                {
                    string strFile = strPath + Constants.FILE_PATH + straclname + Constants.FILE_EXT_XML;

                    string strArguments = "am_install_tree -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=" + AppConfig.UserGroup + " -operation=export -path=" + strFile + " -format=xml";
                

                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    Execute_File_Write.Process_Execute_File_Write(strFileName);

                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
