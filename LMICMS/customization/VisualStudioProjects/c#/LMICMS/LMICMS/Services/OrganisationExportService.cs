﻿using LMICMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Services
{
    class OrganisationExportService
    {
        public static string strOutput = "";

        public static string strStringOut
        {
            get { return strOutput; }

            set { strOutput = value; }
        }
        public void OrganisationProcessExport(string strPath, string strorgname)
        {
            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            try
            {
                string strDir = ConfigService.strDirFolder;

                string strFileName = Path.Combine(strDir, Constants.EXP_ORGANISATION + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                string strLogFile = Path.Combine(strDir, Constants.EXPORT_ORGANISATION_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);
                if (String.IsNullOrEmpty(strorgname))
                {
                    string strLoc = strPath + Constants.FILE_PATH + "tc_organisaton.xml";
                    string strArguments = "dsa_util -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=dba -class=Group -f=export  -filename=" + strLoc;
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    Execute_File_Write.Process_Execute_File_Write(strFileName);

                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);

                }
                else
                {
                    string strFile = strPath + Constants.FILE_PATH + strorgname + Constants.FILE_EXT_XML;

                    string strArguments = "dsa_util -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=dba -class=Group -f=export  -filename=" + strFile;


                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    Execute_File_Write.Process_Execute_File_Write(strFileName);

                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}

        
    
