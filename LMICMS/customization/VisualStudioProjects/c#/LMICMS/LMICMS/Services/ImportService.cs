﻿using LMICMS.Infrastructure;
using LMICMS.Requests;
using System;
using System.IO;

namespace LMICMS.Services
{

    public class ImportService
    {
        public static string strOutput = "";

        public static string strString
        {
            get { return strOutput; }

            set { strOutput = value; }
        }

        public void ProcessImport(ImportModel importModel)
        {
            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            string strDir = ConfigService.strDirFolder;

            string strFileName = Path.Combine(strDir, Constants.IMP_QUERY + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

            string strLogFile = Path.Combine(strDir, Constants.IMPORT_QUERY_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

            Boolean bIsFirstQuery = true;

            foreach (var item in importModel.queries)
            {
               string strArguments = "plmxml_import -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=dba -xml_file=" + Constants.DOUBLE_QUOTES + importModel.strFilePath + Constants.FILE_PATH + item + Constants.DOUBLE_QUOTES + " -import_mode=overwrite";

               strOutput  = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                if (bIsFirstQuery == true)
                {
                    bIsFirstQuery = false;

                    Execute_File_Write.Process_Execute_File_Write(strFileName);
                }

                Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);
                
                Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
                
            }
        }
    }
}





            
        
    

        

    

