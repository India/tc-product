﻿using LMICMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Services
{
    class OrganisationImportService
    {
        public static string strOutput = "";

        public static string strString
        {
            get { return strOutput; }

            set { strOutput = value; }
        }

        public void OrganisationProcessImport(string strLoc)
        {
            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            string strDir = ConfigService.strDirFolder;

            string strFileName = Path.Combine(strDir, Constants.IMP_ORGANISATION + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

            string strLogFile = Path.Combine(strDir, Constants.IMPORT_ORGANISATION_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

            string strArguments = "dsa_util -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=dba -f=import  -filename=" + strLoc;

            strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

            Execute_File_Write.Process_Execute_File_Write(strFileName);

            Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

            Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
        }
    }
}
