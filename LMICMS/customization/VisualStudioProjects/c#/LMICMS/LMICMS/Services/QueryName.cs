﻿using LMICMS.Infrastructure;
using LMICMS.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMICMS.Services
{
    class QueryName
    {
       public static List<QueryModel> lMatchedStrings = null;


        public void ProcessQueryName(string strFileName)
        {
            string strOutput = null;

            lMatchedStrings = new List<QueryModel>();

            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            try
            {
                string strDir = ConfigService.strDirFolder;

                string strArguments = "LMICMSUTILS -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=" + AppConfig.UserGroup + " -mode=query -operation=export_query_name -tcconfigfilename=" + strFileName + " -filepath=" + strDir;

                execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);


                string[] filelines = System.IO.File.ReadAllLines(strDir + Constants.FILE_PATH + strFileName);

                for (int iDx = 0; iDx < filelines.Length; iDx++)
                {
                    StringBuilder sb = new StringBuilder(filelines[iDx]);

                    sb = Replace_String_Characters.Process_Replace_String_Characters(sb);

                    lMatchedStrings.Add(new QueryModel() { QueryName = filelines[iDx], FileName = sb.ToString() });
                }
            }
            catch(Exception ex)
            {

            }
                
        }
    }
}

