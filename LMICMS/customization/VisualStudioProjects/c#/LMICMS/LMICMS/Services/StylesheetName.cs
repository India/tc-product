﻿using LMICMS.Infrastructure;
using LMICMS.Requests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Services
{
    class StylesheetName
    {
      public static   List<StylesheetModel> lMatchedStrings = null;
        public void ProcessStylesheetName(string strFileName, string strUserReq)
        {
            string strOutput = null;
            lMatchedStrings = new List<StylesheetModel>();
            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            try
            {
                string strDir = ConfigService.strDirFolder;

                string strArguments = "LMICMSUTILS -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=" + AppConfig.UserGroup + " -mode=stylesheet -operation=export_stylesheet_name -tcconfigfilename=" + strFileName + " -filepath=" + strDir + " -UserRequest=" + strUserReq;

                execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                string[] filelines = File.ReadAllLines(strDir + Constants.FILE_PATH + strFileName);

                for (int iDx = 0; iDx < filelines.Length; iDx++)
                {
                    StringBuilder sb = new StringBuilder(filelines[iDx]);

                    sb = Replace_String_Characters.Process_Replace_String_Characters(sb);

                    lMatchedStrings.Add(new StylesheetModel() { StylesheetName = filelines[iDx], FileName = sb.ToString() });
                }


            }
            catch (Exception ex)
            {

            }

        }
    }
}
