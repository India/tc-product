﻿using LMICMS.Common.Constants;
using LMICMS.Infrastructure;
using System;
using System.IO;

namespace LMICMS.Services
{
    class Execute_File_Write
    {
        public static void Process_Execute_File_Write(string strFileName)
        {
            File.AppendAllText(strFileName, GenericConstants.SET_TC_ROOT + AppConfig.TCRoot + Environment.NewLine);

            File.AppendAllText(strFileName, GenericConstants.SET_TC_DATA + AppConfig.TCData + Environment.NewLine);

            File.AppendAllText(strFileName, GenericConstants.TC_DATA_TC_PROFILEVARS + Environment.NewLine);
        }
    }
}
