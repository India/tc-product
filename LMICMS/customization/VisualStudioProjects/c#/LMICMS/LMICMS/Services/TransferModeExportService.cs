﻿using LMICMS.Infrastructure;
using LMICMS.Requests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Services
{
    class TransferModeExportService
    {
        public static string strOutput = "";

        public static string strStringOut
        {
            get { return strOutput; }

            set { strOutput = value; }
        }
        public void TransferModeProcessExport(TransferModeExportModel transferModeExportModel, string strPath)
        {
            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            try
            {
                string strDir = ConfigService.strDirFolder;

                string strFileName = Path.Combine(strDir, Constants.EXP_TRANSFER_MODE + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                string strLogFile = Path.Combine(strDir, Constants.EXPORT_TRANSFER_MODE_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);
                Boolean bIsFirstQuery = true;
                
                    foreach (var nw in transferModeExportModel.exportedtransfermode.Zip(transferModeExportModel.transfermodeUID, Tuple.Create)) 
                { 
                    string[] part1 = nw.Item1.Split(new[] { Constants.COMMA_SEP}, StringSplitOptions.RemoveEmptyEntries);
                    string[] part2 = nw.Item2.Split(new[] { Constants.COMMA_SEP }, StringSplitOptions.RemoveEmptyEntries);
                      for (int i = 0; i < part1.Length && i< part2.Length; i++)
                    {
                        string strValue1 = part2[i];

                        string strValue2 = part1[i];

                        string strValue3 = Constants.DOUBLE_QUOTES + strPath + Constants.FILE_PATH + strValue2 + Constants.DOUBLE_QUOTES;

                        string strArguments = "tcxml_export -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=dba -file=" + strValue3 + Constants.FILE_EXT_XML +  " -uid=" + strValue1;

                        strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                        if (bIsFirstQuery == true)
                        {
                            bIsFirstQuery = false;

                            Execute_File_Write.Process_Execute_File_Write(strFileName);
                        }

                        Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                        Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
