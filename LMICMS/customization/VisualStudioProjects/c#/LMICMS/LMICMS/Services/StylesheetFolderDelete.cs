﻿using LMICMS.Infrastructure;
using System;
using System.IO;
namespace LMICMS.Services
{
    class StylesheetFolderDelete
    {

        public static void ProcessFolderDelete(string stylesheetPath, string strValue1)
        {
            string sourceDir = stylesheetPath + Constants.FILE_PATH + strValue1;
            string backupDir = @"E:\svn\repository\configuration\stylesheet";
            
           
            try
            {
                string[] picList = Directory.GetFiles(sourceDir, " *.xml");
                string[] txtList = Directory.GetFiles(sourceDir, "*.xml");

                // Copy picture files.
                foreach (string f in picList)
                {
                    
                    // Remove path from the file name.
                    string fName = f.Substring(sourceDir.Length + 1);
                   
                    // Use the Path.Combine method to safely append the file name to the path.
                    // Will overwrite if the destination file already exists.
                    File.Copy(Path.Combine(sourceDir, fName), Path.Combine(backupDir, fName), true);
                }

                // Copy text files.
                foreach (string f in txtList)
                {

                    // Remove path from the file name.
                    string fName = f.Substring(sourceDir.Length + 1);

                    try
                    {
                        File.Move(Path.Combine(sourceDir, fName), stylesheetPath + Constants.FILE_PATH + strValue1 + ".xml");
                        // Will not overwrite if the destination file already exists.
                       // File.Copy(Path.Combine(sourceDir, fName), Path.Combine(backupDir, fName));
                    }

                    // Catch exception if the file was already copied.
                    catch (IOException copyError)
                    {
                        Console.WriteLine(copyError.Message);
                    }
                }

                // Delete source files that were copied.
                foreach (string f in txtList)
                {
                    File.Delete(f);
                }
                foreach (string f in picList)
                {
                    File.Delete(f);
                }
            }

            catch (DirectoryNotFoundException dirNotFound)
            {
                Console.WriteLine(dirNotFound.Message);
            }
        }
    }
}
    


