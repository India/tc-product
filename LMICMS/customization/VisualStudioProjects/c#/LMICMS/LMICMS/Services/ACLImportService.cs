﻿using LMICMS.Infrastructure;
using System;
using System.IO;


namespace LMICMS.Services
{
    class ACLImportService
    {
        public static string strOutput = "";

        public static string strString
        {
            get { return strOutput; }

            set { strOutput = value; }
        }

        public void ACLProcessImport(string strLoc)
        {
            Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

            string strDir = ConfigService.strDirFolder;

            string strFileName = Path.Combine(strDir, Constants.IMP_ACL + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

            string strLogFile = Path.Combine(strDir, Constants.IMPORT_ACL_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

            string strArguments = "am_install_tree -u=" + AppConfig.UserName + " -p=" + AppConfig.UserPassword + " -g=" + AppConfig.UserGroup + " -operation=import -path=" + strLoc + " -format=xml";

            strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

            Execute_File_Write.Process_Execute_File_Write(strFileName);

            Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

            Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
        }
    }
}
            
        
    

