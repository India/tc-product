﻿#pragma checksum "..\..\..\UserControls\CommonSettingsUC.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "59100CB74AFDDBBE2567AAA7C961866D"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LMICMS.UserControls {
    
    
    /// <summary>
    /// CommonSettingsUC
    /// </summary>
    public partial class CommonSettingsUC : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 20 "..\..\..\UserControls\CommonSettingsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdMain;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\UserControls\CommonSettingsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtConfigFilePath;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\UserControls\CommonSettingsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnConfigFilePath;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\UserControls\CommonSettingsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnConfigFilePathSave;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\UserControls\CommonSettingsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTempDownPath;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\UserControls\CommonSettingsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnTempDownPath;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\UserControls\CommonSettingsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtCMSExeLoc;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\UserControls\CommonSettingsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCMSExeLoc;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\UserControls\CommonSettingsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSave;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LMICMS;component/usercontrols/commonsettingsuc.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControls\CommonSettingsUC.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.grdMain = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.txtConfigFilePath = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.btnConfigFilePath = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\..\UserControls\CommonSettingsUC.xaml"
            this.btnConfigFilePath.Click += new System.Windows.RoutedEventHandler(this.BtnConfigFilePathClick);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnConfigFilePathSave = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\..\UserControls\CommonSettingsUC.xaml"
            this.btnConfigFilePathSave.Click += new System.Windows.RoutedEventHandler(this.BtnSaveConfigFilePathClick);
            
            #line default
            #line hidden
            return;
            case 5:
            this.txtTempDownPath = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.btnTempDownPath = ((System.Windows.Controls.Button)(target));
            
            #line 40 "..\..\..\UserControls\CommonSettingsUC.xaml"
            this.btnTempDownPath.Click += new System.Windows.RoutedEventHandler(this.BtnTempDownPathClick);
            
            #line default
            #line hidden
            return;
            case 7:
            this.txtCMSExeLoc = ((System.Windows.Controls.TextBox)(target));
            
            #line 43 "..\..\..\UserControls\CommonSettingsUC.xaml"
            this.txtCMSExeLoc.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TxtCMSExeLoc_TextChanged);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnCMSExeLoc = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\..\UserControls\CommonSettingsUC.xaml"
            this.btnCMSExeLoc.Click += new System.Windows.RoutedEventHandler(this.BtnCMSExeLocClick);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnSave = ((System.Windows.Controls.Button)(target));
            
            #line 46 "..\..\..\UserControls\CommonSettingsUC.xaml"
            this.btnSave.Click += new System.Windows.RoutedEventHandler(this.BtnSaveClick);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

