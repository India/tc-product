package com.lmtec.rac.wizards.importer;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;

import org.supercsv.cellprocessor.ift.CellProcessor;

import com.lmtec.rac.wizards.importer.spec.IFileColumn;
import com.lmtec.rac.wizards.importer.spec.IImportProvider;


public class FileColumn implements  IFileColumn{
	
	private String label;
	private String propertyName;
	private String extName;
	private String[] mapping;
	private boolean enabled;
	private boolean mandatory;
	private Class<?> dataType;
	private CellProcessor cellProcessor;
	/**
	 * Creates a column object for the {@link IImportProvider}.addColumn({@link IFileColumn});
	 * @param label String
	 * @param propertyName String bean propertyName
	 * @param mapping Array of valid header names
	 * @param enabled Boolean if the column is enabled
	 * @param mandatory Boolean if the column is mandatory for the import
	 */
	public FileColumn(String label, String propertyName, Class dataType, String[] mapping,
			boolean enabled, boolean mandatory) {
		super();
		this.label = label;
		this.propertyName = propertyName;
		this.setDataType(dataType);
		this.mapping = mapping;
		this.enabled = enabled;
		this.mandatory = mandatory;
	}
	
	public FileColumn(String label, String propertyName, Class dataType, String[] mapping, CellProcessor processor,
			boolean enabled, boolean mandatory) {
		this(label, propertyName, dataType, mapping, enabled, mandatory);
		this.cellProcessor = processor;
	}
	
	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.ITest#hasMap(java.lang.String)
	 */
	@Override
	public boolean hasMap(String name){
		if (mapping == null)
			return false; 
		
		for (int i = 0; i < mapping.length; i++) {
			if(mapping[i].equalsIgnoreCase(name))
				return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "FileColumn [label=" + label + ", propertyName=" + propertyName
				+ ", extName=" + extName + ", mapping="
				+ Arrays.toString(mapping) + ", enabled=" + enabled
				+ ", mandatory=" + mandatory + "]";
	}

	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.ITest#getLabel()
	 */
	@Override
	public String getLabel() {
		return label;
	}

	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.ITest#setLabel(java.lang.String)
	 */
	@Override
	public void setLabel(String label) {
		this.label = label;
	}

	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.ITest#getPropertyName()
	 */
	@Override
	public String getPropertyName() {
		return propertyName;
	}

	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.ITest#setPropertyName(java.lang.String)
	 */
	@Override
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.ITest#getMapping()
	 */
	@Override
	public String[] getMapping() {
		return mapping;
	}

	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.ITest#setMapping(java.lang.String[])
	 */
	@Override
	public void setMapping(String[] mapping) {
		this.mapping = mapping;
	}

	public boolean isEnabled() {
		return enabled;
	}

	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.ITest#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.IMandatory#isMandatory()
	 */
	@Override
	public boolean isMandatory() {
		return mandatory;
	}

	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.IMandatory#setMandatory(boolean)
	 */
	@Override
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.ITest#getExtName()
	 */
	@Override
	public String getExtName() {
		return extName;
	}

	/* (non-Javadoc)
	 * @see com.lmtec.rac.wizards.importer.ITest#setExtName(java.lang.String)
	 */
	@Override
	public void setExtName(String extName) {
		this.extName = extName;
	}

	public Class<?> getDataType() {
		return dataType;
	}

	public void setDataType(Class<?> dataType) {
		this.dataType = dataType;
	}

	@Override
	public CellProcessor getCellProcessor() {
		return this.cellProcessor;
	}

	@Override
	public void setCellProcessor(CellProcessor processor) {
		this.cellProcessor = processor;
		
	}
	
	
	

}
