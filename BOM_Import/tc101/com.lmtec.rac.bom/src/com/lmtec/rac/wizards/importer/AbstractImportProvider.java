package com.lmtec.rac.wizards.importer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.widgets.Shell;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.exception.SuperCsvCellProcessorException;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import com.lmtec.rac.PopupWindow;
import com.lmtec.rac.wizards.importer.spec.IFileColumn;
import com.lmtec.rac.wizards.importer.spec.IFileRow;
import com.lmtec.rac.wizards.importer.spec.IFileRowService;
import com.lmtec.rac.wizards.importer.spec.IImportProvider;


public abstract class AbstractImportProvider implements IImportProvider{

	private String filePath = null;
	protected Set<IFileColumn> columns = null;
	protected Set<IFileRow> rows = null;
	protected Class<? extends IFileRow> rowClazz;
	protected IFileRowService rowService;
	CsvBeanReader reader = null;
	private String[] header = null;
	private CsvPreference CSV_PREFERENCE;

	public AbstractImportProvider(Class<? extends IFileRow> rowClazz) {
		super();
		this.columns = new LinkedHashSet<IFileColumn>();
		this.rows = new HashSet<IFileRow>();
		this.rowClazz = rowClazz;
		this.CSV_PREFERENCE = CsvPreferences.getInstance().getDefault();
	}

	@Override
	public String getFilePath() {
		return filePath;
	}

	@Override
	public void setFilePath(String filePath) {
		this.filePath = filePath;
		
	}

	@Override
	public Set<IFileColumn> getColumns() {
		return columns;
	}

	@Override
	public CsvPreference getCsvPreference() {
		return CSV_PREFERENCE;
	}
	
	public void setCsvPreference(CsvPreference preference){
		this.CSV_PREFERENCE = preference;
	}
	
	@Override
	public void setCsvPreference(String preferenceName) {
		this.CSV_PREFERENCE = CsvPreferences.getInstance().get(preferenceName);
		
	}

	@Override
	public String getFileName() {
		return filePath.substring(filePath.lastIndexOf("\\") +1);
	}

	@Override
	public void readFile() throws IOException {
		
		if(getFilePath() == null)
			throw new FileNotFoundException("No file was selected");
		
		try {
			reader = new CsvBeanReader(new FileReader(getFilePath()), this.getCsvPreference());
			header = reader.getHeader(true);
		} catch (IOException e) {
			e.printStackTrace();
			reader = null;
			header = null;
			throw e;
		}
	}

	@Override
	public void closeFile() throws IOException {
		if (reader != null)
			reader.close();
	}

	@Override
	public String[] getFileHeader() throws NullPointerException {
		if(header == null || header.length == 0)
			throw new NullPointerException("The header file is not set");
		
		return header;
	}

	@Override
	public void disableAllColumns() {
		for (IFileColumn p : getColumns()) {
			p.setEnabled(false);
			p.setExtName(null);
		}
	}
	
	@Override
	public void addColumn(IFileColumn column) {
		this.columns.add(column);
	}

	@Override
	public boolean columnsAreComplete() {
		// Check if any mandatory fields are empty
		for (IFileColumn p : getColumns()) {
			if(p.isMandatory() && ( p.getExtName() == null || p.getExtName().isEmpty() || !p.isEnabled() )){
				return false;
			}
		}
		return true;
	}

	@Override
	public String[] getFileHeaderEnabled() {
		if(header == null || header.length == 0)
			return null;
			
		String[] enabledHeader = new String[header.length];
		for (int i = 0; i < header.length; i++) {
			boolean enabled = false;
			for (IFileColumn p : getColumns()) {
				if(p.isEnabled() && p.hasMap(header[i])){
					enabled = true;
					enabledHeader[i] = p.getPropertyName();
				}
			}
			if(!enabled)
				enabledHeader[i] = null;
		}
		return enabledHeader;
	}

	@Override
	public void enableColumnsFromFileHeader() throws NullPointerException {
		if(header == null || header.length == 0)
			throw new NullPointerException("The header file is not set");
		
		disableAllColumns();
		// enable the columns found in the header 
		for (int i = 0; i < header.length; i++) {
			// loop over the columns
			for (IFileColumn p : getColumns()) {
				if(p.hasMap(header[i])){
					p.setEnabled(true);
					p.setExtName(header[i]);
				}
			}
		}
		
	}
	
	public CellProcessor[] getProcessors(){
		String[] header = getFileHeaderEnabled();
		CellProcessor[] processors = new CellProcessor[header.length];
		
		for (int i = 0; i < header.length; i++) {
			if(header[i] == null)
				continue;
			
			Field field = null;
			Class clazz = rowClazz;
			while(clazz.getSuperclass() != null){
				try {
					 field = clazz.getDeclaredField(header[i]);
					 Class type = field.getType();
					 
					 
					 // get processor from Column
					 for (IFileColumn p : getColumns()) {
							if(p.getPropertyName().equals(header[i]) && p.getCellProcessor() != null){
								processors[i] = p.getCellProcessor();
								break;
							}
					 }
					 
					 if(processors[i] != null)
						 break;
					 
					 if(type == Double.class){
						 processors[i] = new ParseDouble();
						 break;
					 }
					 
					 if(type == String.class){
						 processors[i] = new NotNull();
						 break;
					 }
					 
					 if(type == Integer.class || type == int.class){
						 processors[i] = new ParseInt();
						 break;
					 }
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (NoSuchFieldException e) {
					System.err.println("Did not find field in class, supeclass is "+ clazz.getSuperclass().getName());
					//e.printStackTrace();
				}
				clazz = clazz.getSuperclass();
			}	
			
		}
		return processors;
	}

	@Override
	public void readFileRows() throws IOException {
		
		
		if(reader == null)
			throw new FileNotFoundException(String.format("Cannot open file %s", getFileName()));
		
		IFileRow line;
		String[] header = getFileHeaderEnabled();
		CellProcessor[] processors = getProcessors();
	
		try{
			while( (line = reader.read(rowClazz, header, processors ) ) != null ) {
				if(rowService != null)
					rowService.setRowStatus(line);
				rows.add(line);
			}
		} catch (SuperCsvCellProcessorException e) {
			throw new IOException(
					String.format("Parsing error at line: %d, column: %d\n" +
							"The value cannot be '%s'",
							e.getCsvContext().getLineNumber(), e.getCsvContext().getColumnNumber(),
							e.getCsvContext().getRowSource().get(e.getCsvContext().getColumnNumber() -1))
					, e);
		}	
		closeFile();
	}

	@Override
	public void setFileRowService(IFileRowService rowService) {
		this.rowService = rowService;
	}

	@Override
	public IFileRowService getFileRowService() {
		return rowService;
	}

	@Override
	public void setRowStatus(Set<IFileRow> rows, RowStatus status) {
		for (IFileRow row : rows) {
			row.setStatus(status);
		}
		
	}

	@Override
	public Set<IFileRow> getRows() {
		return rows;
	}

}