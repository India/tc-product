package com.lmtec.rac.wizards.importer;

import java.util.LinkedHashMap;
import org.supercsv.prefs.CsvPreference;

public final class CsvPreferences extends LinkedHashMap<String, CsvPreference>{

	private static final long serialVersionUID = 2187039569683032807L;
	
	private String defaultPreference;
	
	public static CsvPreferences INSTANCE;

	public CsvPreferences() {
		this.defaultPreference = "Standard";
		
		//this.put("Tab delimited", CsvPreference.TAB_PREFERENCE);
		//this.put("Excel America", CsvPreference.EXCEL_PREFERENCE);
		//this.put("Excel Europe", CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE);
		//this.put("Standard", CsvPreference.STANDARD_PREFERENCE);
	}
	
	public static CsvPreferences getInstance(){
		if(INSTANCE == null)
			INSTANCE = new CsvPreferences();
		return INSTANCE;
	}
	
	public CsvPreference getDefault(){
		return CsvPreferences.getInstance().get(defaultPreference);
	}
	
	
	
}
