package com.lmtec.rac.wizards.importer.bom;


import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.lmtec.rac.wizards.importer.RowStatus;
import com.lmtec.rac.wizards.importer.spec.IFileRow;
import com.lmtec.rac.wizards.importer.spec.IFileRowService;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCAccessControlService;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentBOMViewRevision;
import com.teamcenter.rac.kernel.TCComponentBOMWindow;
import com.teamcenter.rac.kernel.TCComponentBOMWindowType;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class BomLineService implements IFileRowService{
	
	private TCComponentBOMLine bomTopLine;
	private TCComponentBOMWindow bomWindow = null;
	private AIFComponentContext[] tcBomChildren;
	private Set<BomLine> bomChildren;
	private TCComponentItemRevision itemRevision;
	private TCSession session;
	private Class<? extends IFileRow> rowClazz;
	private Map<String, String> tcProperties;
	
	public BomLineService() throws TCException {
		this(getItemRevision());
	}
	
	public BomLineService(TCComponentItemRevision itemRevision) {
		this.itemRevision = itemRevision;
		this.session = (TCSession) AIFDesktop.getActiveDesktop().getCurrentApplication().getSession();
		this.bomChildren = new LinkedHashSet<BomLine>();
		this.rowClazz = BomLine.class;
		
		this.tcProperties = new HashMap<String, String>();
		this.tcProperties.put("refDes", "bl_ref_designator");
		this.tcProperties.put("findNo", "bl_sequence_no");
		this.tcProperties.put("quantity", "bl_quantity");
		this.tcProperties.put("transformation", "bl_plmxml_occ_xform");
		
	}

	@Override
	public void setRowStatus(IFileRow row) {
		BomLine bomLine = (BomLine) row;
		
		// check if item id missing
		if(bomLine.getItemId() == null || bomLine.getItemId().isEmpty()){
			bomLine.setStatus(RowStatus.ERROR);
			bomLine.setStatusDescription("The Item ID is missing");
			return;
		}
			
		// check if item exists in db
		if(!itemExist(bomLine.getItemId())){
			bomLine.setStatus(RowStatus.ERROR);
			bomLine.setStatusDescription("The Item does not exist");
			return;
		}
		
		// IGNORE :: check if item exists in existing BOM
		if(getChildByObject(bomLine) != null && getChildByObject(bomLine).equalObject(bomLine) && getChildByObject(bomLine).equalQuantity(bomLine)){
			bomLine.setStatus(RowStatus.IGNORE);
			return;
		}
		
		// UPDATE :: check if item exists in existing BOM
		if(getChildById(bomLine)!=null){
			bomLine.setStatus(RowStatus.UPDATE);
			return;
		}
		
		bomLine.setStatus(RowStatus.INSERT);
	}

	@Override
	public void execute(IFileRow row) throws Exception {
		if(!row.isEnabled())
			return;
		
		BomLine newBomLine = (BomLine)row; 
		TCComponentItem item = (TCComponentItem) getItemById(newBomLine.getItemId());
		
		switch (newBomLine.getStatus()) {
		
		case INSERT:
			TCComponentItemRevision itemRevision;
			try {
				itemRevision = item.getLatestItemRevision();
				TCComponentBOMLine inserted = bomTopLine.add(item, itemRevision, new TCComponentBOMViewRevision() , false );
				
				// update string properties
				for (String field : tcProperties.keySet()) {
					String value = newBomLine.getPropertyValue(newBomLine, field);
					// set only if exists
					if(value != null){
						inserted.setProperty(tcProperties.get(field), value);
					}
				}
			} catch (TCException e) {
				e.printStackTrace();
				throw new Exception("TC exception, cannot insert Item: " + item.toString());
			}
			break;
			
		case UPDATE:
			int uIndex = 0;
			for (BomLine child : bomChildren) {
				if(child.equalId(newBomLine)){
					TCComponentBOMLine bomLine = (TCComponentBOMLine) tcBomChildren[uIndex].getComponent();
					
					// replace new item
					if(! child.getItemId().equalsIgnoreCase(newBomLine.getItemId())){
						bomLine.replace(item, item.getLatestItemRevision(), null);
					}
					
					// update string properties
					for (String field : tcProperties.keySet()) {
						// update only if the value differ
						if(!child.equals(newBomLine, field)){
							String value = newBomLine.getPropertyValue(newBomLine, field);
							bomLine.setProperty(tcProperties.get(field), (value == null) ? "" : value);
						}
					}
				}
				uIndex++;
			}
			
			break;
			
		case REMOVE:
			readBomChildren();
			int rIndex = 0;
			for (BomLine child : bomChildren) {
				if(child.equalObject(newBomLine)){
					TCComponentBOMLine bomLine = (TCComponentBOMLine) tcBomChildren[rIndex].getComponent();
					bomLine.cut();
				}
				rIndex++;
			}
			break;

		default:
			break;
		}
		
	}

	@Override
	public void save() throws Exception {
		if ( bomTopLine != null ){ 
			bomTopLine.save();
			bomTopLine.refresh();
		}
		
		if ( bomWindow != null ){ 
			bomWindow.save();
			bomWindow.refresh();
		}
		
	}
	
	public BomLine getChildByObject(BomLine bomLine){
		for (BomLine child : bomChildren) {
			if(bomLine.equalObject(child)){
				return child;
			}
		}
		return null;
	}
	
	public BomLine getChildById(BomLine bomLine){
		for (BomLine child : bomChildren) {
			if(bomLine.equalId(child)){
				return child;
			}
		}
		return null;
	}
	
	public Set<BomLine> getBomChildren() {
		return bomChildren;
	}

	public void setBomChildren(Set<BomLine> bomChildren) {
		this.bomChildren = bomChildren;
	}
	
	public static TCComponentItemRevision getItemRevision() throws TCException{
		InterfaceAIFComponent selectedComp = AIFDesktop.getActiveDesktop().getCurrentApplication().getTargetComponent();
		((TCComponent) selectedComp).refresh();
		if (selectedComp instanceof TCComponentItemRevision) {
			return (TCComponentItemRevision) selectedComp;
		} else if (selectedComp instanceof TCComponentBOMLine) {
			TCComponentBOMLine bomLine = ((TCComponentBOMLine) selectedComp);
			while (bomLine.parent() != null ){
				bomLine = bomLine.parent();
			}
			return bomLine.getItemRevision();
		}
		return null;
	}


	public void readBom() throws TCException {
		TCSession session = (TCSession) AIFDesktop.getActiveDesktop().getCurrentApplication().getSession();
		TCComponentBOMWindowType bowWinType = (TCComponentBOMWindowType) session.getTypeComponent("BOMWindow");
		bomWindow = bowWinType.create(null);
		bomTopLine = bomWindow.setWindowTopLine(itemRevision.getItem(), itemRevision, null, null);
		readBomChildren();
	}

	public void readBomChildren() throws TCException  {
		bomChildren.clear();
		
		if ( bomTopLine.hasChildren()) {
			
			tcBomChildren = bomTopLine.getChildren();
			
			for (int i = 0; i < tcBomChildren.length; i++) {
				TCComponentBOMLine bomLine = (TCComponentBOMLine) tcBomChildren[i].getComponent();
				while(bomLine.isPacked()){
					if(bomLine.isPacked())
						bomLine.unpack();
				}
			}
			
			tcBomChildren = bomTopLine.getChildren();
			
			for (int i = 0; i < tcBomChildren.length; i++) {
				TCComponentBOMLine tcBomChild = (TCComponentBOMLine) tcBomChildren[i].getComponent();
				
				BomLine bomLine = new BomLine();
				try {
					bomLine = (BomLine) rowClazz.getConstructor().newInstance();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
				
				String itemId = tcBomChild.getStringProperty("bl_item_item_id");
				String refDes = null;
				String quantity = null;
				String findNo = null;
				String transformation = null;

				try {
					refDes = tcBomChild.getStringProperty("bl_ref_designator");
				} catch (TCException e) {
					// no property
				}
				
				try {
					findNo = tcBomChild.getStringProperty("bl_sequence_no");
				} catch (TCException e) {
					// no property
				}
				
				try {
					quantity = tcBomChild.getStringProperty("bl_quantity");
				} catch (TCException e) {
					// no property
				}
				
				try {
					transformation = tcBomChild.getStringProperty("bl_plmxml_occ_xform");
				} catch (TCException e) {
					e.printStackTrace();
				}
				
				bomLine.setItemId(itemId);
				
				if(!findNo.isEmpty())
					bomLine.setFindNo(findNo);
				
				if(!refDes.isEmpty())
					bomLine.setRefDes(refDes);
				
				if(!quantity.isEmpty())
					bomLine.setQuantity(quantity);
				
				if(transformation != null && !transformation.isEmpty())
					bomLine.setTransformation(transformation);
				
				bomChildren.add(bomLine);
			}
		}
	}
	
	
	/**
	 * Query the TC server for an item ID
	 * @param item_id
	 * @return true if exists
	 */
	public boolean itemExist(String item_id)  {
		TCComponent[] items = getItemsById(item_id);
		if ( items != null && items.length > 0 )
			return true;
		return false;
	}
	
	public TCComponent getItemById(String item_id)  {
		TCComponent[] items = getItemsById(item_id);
		if ( items != null && items.length > 0 )
			return items[0];
		return null;
	}

	public TCComponent[] getItemsById(String item_id)  {
        TCComponentQuery query = null;
        TCComponentQueryType queryType;
        TCComponent[] result;
        String entry[] = { "Item ID" };
        String value[] = { item_id };
        
		try {
			queryType = (TCComponentQueryType) session.getTypeComponent("ImanQuery");
			query = (TCComponentQuery) queryType.find("Item...");
			result = query.execute(entry, value);
			if (result == null || result.length == 0) {
	            return null;
	        }
			return result;
		} catch (TCException e) {
			e.printStackTrace();
		}
        return null;
	}

	/**
	 * @return the rowClazz
	 */
	public Class<? extends IFileRow> getRowClazz() {
		return rowClazz;
	}

	/**
	 * Set the Object to work with. <br />
	 * Overrrides the default {@link IFileRow}.class
	 * @param rowClazz 
	 */
	public void setRowClazz(Class<? extends IFileRow> rowClazz) {
		this.rowClazz = rowClazz;
	}

	@Override
	public boolean getPermission() {
		TCAccessControlService access =  ((TCSession) AIFDesktop.getActiveDesktop().getCurrentApplication().getSession()).getTCAccessControlService();                                                                                                                                  
		boolean read = false;
		boolean write = false;
		
		try {
			read = access.checkPrivilege(itemRevision, "READ");
			write = access.checkPrivilege(itemRevision, "WRITE");
		} catch (TCException e) {
			e.printStackTrace();
		}
		return (read && write) ? true : false;
	}

}
