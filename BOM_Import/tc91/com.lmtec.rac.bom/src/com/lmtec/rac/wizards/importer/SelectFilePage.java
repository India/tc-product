package com.lmtec.rac.wizards.importer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.lmtec.rac.wizards.AbstractWizardPage;
import com.lmtec.rac.wizards.importer.spec.IImportProvider;

public class SelectFilePage extends AbstractWizardPage {
	
	private String[] dialogExt;
	private String dialogPath;
	private IImportProvider importSerivice;
	
	private FileDialog fileDialog;
	private Text fileText;
	
	/**
	 * Creates a page to browse and select a file.
	 */
	public SelectFilePage(IImportProvider importSerivice) {
		this.dialogPath = "C:\\";
		this.dialogExt = new String[]{"*.*"};
		this.importSerivice = importSerivice;
	}
	
	/**
	 * Allows the page to continue without a user input. <br />
	 * By default, set to false
	 * @param debug boolean
	 */
	public void setDebug(boolean debug){
		pageReady = debug;
	}
	
	/**
	 * Get the allowed file extensions for the browse popup.
	 * @return dialogExt String[]
	 */
	public String[] getDialogExt() {
		return dialogExt;
	}

	/**
	 * Set the allowed file extensions for the browse popup. <br /> 
	 * By default *.*
	 * @param dialogExt the dialogExt to set
	 */
	public void setDialogExt(String[] dialogExt) {
		this.dialogExt = dialogExt;
	}

	/**
	 * Get the dialog path of the browse popup
	 * @return the dialogPath
	 */
	public String getDialogPath() {
		return dialogPath;
	}

	/**
	 * Set the dialogPath for the browse popup. By default this is C:/
	 * @param dialogPath String
	 */
	public void setDialogPath(String dialogPath) {
		this.dialogPath = dialogPath;
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		
		GridLayout layout = new GridLayout(4, false);
		container.setLayout(layout);
		
		Label label = new Label(container, SWT.NONE);
		label.setText("File");
		
		fileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		fileText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// Listen for manual input
		// assign Text changes to filePath
		fileText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				importSerivice.setFilePath(fileText.getText());
				enableNextButton(true);
			}
		});

		fileDialog = new FileDialog(container.getShell(), SWT.BORDER | SWT.SINGLE);
		fileDialog.setText("Open");
		fileDialog.setFilterPath(dialogPath);
        fileDialog.setFilterExtensions(dialogExt);
        
		// create the combo filters
		Combo filePreference = new Combo(container, SWT.READ_ONLY | SWT.BREAK);
		Map<String, ?> preferences = CsvPreferences.getInstance();
		
		List<String> keyList = new ArrayList<String>(preferences.keySet());
		for ( int i = keyList.size()-1 ; i >= 0 ; i-- ) 
			filePreference.add(keyList.get(i));
		filePreference.select(0);
		
		filePreference.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Combo combo = (Combo) e.widget;
				importSerivice.setCsvPreference(combo.getText());
			}
		});
		
		Button openBtn = new Button(container, SWT.PUSH);
        openBtn.setText("Browse..");
        openBtn.addSelectionListener(new SelectionListener() {
			
        	@Override
			public void widgetDefaultSelected(SelectionEvent arg0) { }
        	
        	@Override
			public void widgetSelected(SelectionEvent arg0) {
        		String filePath = fileDialog.open();
        		
				if (filePath != null && !filePath.isEmpty()){
					fileText.setText(filePath);
        			importSerivice.setFilePath(fileText.getText());
					enableNextButton(true);
				}
			}
		});
        setControl(container);
	}
	
	@Override
	public void onEnter() {
		getAWizard().canFinish(false);
	}

	@Override
	public void onLeave() {
		File file = new File(importSerivice.getFilePath());
		if(file.exists() && file.isFile() && file.canRead()){
			// reset errorMessage on success
			setErrorMessage(null);
			canLeave(true);
		}else {
			setErrorMessage("The file does not exist or cannot be read");
			enableNextButton(false);
			canLeave(false);
		}
	}
}
