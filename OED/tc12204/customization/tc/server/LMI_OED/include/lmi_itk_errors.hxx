/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: lmi_itk_errors.hxx

    Description:  This file contains macros for handling ITK errors, macros for general errors, different Trace macros, and a function TextEMHErrors 
                  which encapsulates EMH_store_error, EMH_store_error_s1...

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Oct-18    Anurag kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_ITK_ERRORS_HXX
#define LMI_ITK_ERRORS_HXX

#include <lmi_library.hxx>

/* We want to use our macros. If some others are defined under same name, undefine them */

#ifdef LMI_ITKCALL
#undef LMI_ITKCALL
#endif

/* This macro works for functions which return integer. The function return value is an input argument and return value in the same time. 'f' is either ITK function, or a function which uses only 
   ITK calls and therefore has to return ITK integer return code
*/
#define LMI_ITKCALL(iErrorCode,f){\
           if(iErrorCode == ITK_ok){\
               (iErrorCode) = (f);\
               LMI_err_start_debug(#f, iErrorCode,__FILE__, __LINE__);\
               if(iErrorCode != ITK_ok) {\
                LMI_err_write_to_logfile(#f, iErrorCode,__FILE__, __LINE__);\
                }\
             }\
          }

#endif






