/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_errors.hxx

    Description:  Header File for containing macros for custom errors

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#ifndef LMI_ERRORS_HXX
#define LMI_ERRORS_HXX

#include <common/emh_const.h>

#define ERROR_CODE_NOT_STORED_ON_STACK                     (EMH_USER_error_base + 1)
#define INVALID_INPUT_TO_FUNCTION                          (EMH_USER_error_base + 3)
#define ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND              (EMH_USER_error_base + 10)
#define UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES            (EMH_USER_error_base + 11)
#define INVALID_WORKFLOW_HANDLER_ARGUMENT_VALUE            (EMH_USER_error_base + 20)
#define BOM_VIEW_TYPE_NOT_DEFINED                          (EMH_USER_error_base + 21)

#define PROCESS_OED_NULL_OBJECT                            (EMH_USER_error_base + 501)
#define PROCESS_OED_EMPTY_OBJECTS                          (EMH_USER_error_base + 502)
#define PROCESS_OED_EMPTY_OED_PREF_NAME                    (EMH_USER_error_base + 503)
#define PROCESS_OED_EMPTY_OED_STRINGS                      (EMH_USER_error_base + 504)
#define PROCESS_OED_STRINGS_COUNT_INVALID                  (EMH_USER_error_base + 505)
#define PROCESS_OED_OBJ_COUNT_INVALID                      (EMH_USER_error_base + 506)

#define OED_PREF_VAL_NOT_EXIST                             (EMH_USER_error_base + 507)
#define OED_PREF_VAL_COUNT_INVALID                         (EMH_USER_error_base + 508)

#define OPTR_PREF_VAL_NOT_EXIST                            (EMH_USER_error_base + 509)
#define OPTR_PREF_VAL_INVALID                              (EMH_USER_error_base + 510)
#define OPTR_PREF_VAL_USING_RESERVED_OPTR                  (EMH_USER_error_base + 511)
#define OPTR_PREF_HAS_FIRST_OR_LAST_VALUE_OPTIONAL_OPT     (EMH_USER_error_base + 512)
#define OPTR_SGMNT_SEPTR_USING_RESERVED_OPTR               (EMH_USER_error_base + 513)
#define OPTR_SGMNT_SEPTR_EXIT_IN_OPTRS                     (EMH_USER_error_base + 514)


#define SNTX_VAL_OED_STRING_INVALID                        (EMH_USER_error_base + 515)
#define SNTX_VAL_OED_STR_OBJ_ATT_NOT_EXIST                 (EMH_USER_error_base + 516)
#define SNTX_VAL_OED_STRING_SUB_SEGMENT_INVALID            (EMH_USER_error_base + 517)
#define SNTX_VAL_OED_STR_SUB_SGMNT_TOKEN_INVALID           (EMH_USER_error_base + 518)

#define LOG_VAL_WFL_POS_INVALID                            (EMH_USER_error_base + 519)
#define LOG_VAL_SUBSEGMENT_PREFIX_INVALID                  (EMH_USER_error_base + 520)
#define LOG_VAL_SUBSEGMENT_BOMLEVEL_INVALID                (EMH_USER_error_base + 521)
#define LOG_VAL_SUBSEGMENT_REVISION_RULE_INVALID           (EMH_USER_error_base + 522)
#define LOG_VAL_SUBSEGMENT_OBJECT_TYPE_INVALID             (EMH_USER_error_base + 523)
#define LOG_VAL_SUBSEGMENT_RELATION_TYPE_INVALID           (EMH_USER_error_base + 524)
#define LOG_VAL_SUBSEGMENT_WFL_ATTACHMENT_TYPE_INVALID     (EMH_USER_error_base + 525)
#define LOG_VAL_SUBSEGMENT_DEPTH_INVALID                   (EMH_USER_error_base + 526)

#define OED_PREF_VAL_COUNT_NOT_EQUAL_INPUT_TAGS            (EMH_USER_error_base + 527)

#define PROPAGATION_NULL_OBJECT                            (EMH_USER_error_base + 528)
#define PROPAGATION_EMPTY_OBJECTS                          (EMH_USER_error_base + 529)
#define PROPAGATION_EMPTY_OED_PREF_NAME                    (EMH_USER_error_base + 530)
#define PROPAGATION_EMPTY_OED_STRINGS                      (EMH_USER_error_base + 531)
#define PROPAGATION_STRINGS_COUNT_INVALID                  (EMH_USER_error_base + 532)
#define PROPAGATION_OBJ_COUNT_INVALID                      (EMH_USER_error_base + 533)

#endif


