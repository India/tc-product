/*======================================================================================================================================================================
                                                    Copyright 2019  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_prop_propagation_func_ext.hxx

    Description:  Header File for Property Propagation Function

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-July-19   Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_PROP_PROPAGATION_FUNC_EXT_HXX
#define LMI_PROP_PROPAGATION_FUNC_EXT_HXX
    
#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <string.h>
#include <vector>
#include <lmi_oed_object_info.hxx>

using namespace std;

int LMI_src_value_exist
(
    const LMIPropValue& objSrcPropVal,
    const PROP_value_type_t& objValueType,
    logical& bValueExist
);

int LMI_propagate_attribute_info
(
    const LMIOEDObjectInfo& objDestInfo,
    const LMIPropValue& objPropValues
);


#endif

