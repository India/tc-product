/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_oed_sub_segment.hxx

    Description: Container for a section of "LMIOEDString".

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_OED_SUB_SEGMENT_HXX
#define LMI_OED_SUB_SEGMENT_HXX

#include <string>

using namespace std;

class LMIOEDSubSegment
{
    public:

        LMIOEDSubSegment()
        {
            strPrefix       = "";
            strRefRelName   = "";
            strObjectType   = "";
            strDepth        = "";
            strBOMLineLevel = "";
            bExactBOMLevel  = false;
            strRevisionRule = "";
        }

        string  strPrefix;
        string  strRefRelName;
        string  strObjectType;
        string  strDepth;
        string  strBOMLineLevel;
        bool    bExactBOMLevel;
        string  strRevisionRule;        
};

#endif

