/*======================================================================================================================================================================
                                                    Copyright 2019  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_process_oed_object.hxx

    Description:  Header File for lmi_process_oed_object.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
15-Sep-19     Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#ifndef LMI_PROCESS_OED_OBJECT_HXX
#define LMI_PROCESS_OED_OBJECT_HXX

#include <unidefs.h>
#include <lmi_oed_string.hxx>
#include <lmi_oed_object_info.hxx>

class LMIProcessOEDObject
{
    public:
         int LMI_process_oed_obj( tag_t tObject, const LMIOEDString& objOEDString, const vector<string>& vstrNoteTypes, LMIOEDObjectInfo& refobjOEDInfo );       
};

#endif

