/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: lmi_itk_mem_free.hxx

    Description:  This file contains macros for memory operations.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Oct-18    Anurag Kumar         Initial Release
   
========================================================================================================================================================================*/
#ifndef LMI_ITK_MEM_FREE_HXX
#define LMI_ITK_MEM_FREE_HXX

#include <lmi_library.hxx>

/* We want to use our macros. If some others are defined under same name, undefine them */

#ifdef LMI_MEM_TCFREE
#undef LMI_MEM_TCFREE
#endif

/* Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc */

#define LMI_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }


#ifdef LMI_UPDATE_TAG_ARRAY
#undef LMI_UPDATE_TAG_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation is done for the very first time
*/
#define LMI_UPDATE_TAG_ARRAY(iCurrentArrayLen, ptArray, tValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ptArray = (tag_t*) MEM_alloc(iCurrentArrayLen * sizeof(tag_t));\
            }\
            else{\
                ptArray = (tag_t*) MEM_realloc(ptArray, iCurrentArrayLen * sizeof(tag_t));\
            }\
            ptArray[iCurrentArrayLen-1]=  tValue;\
}


#ifdef LMI_CLONE_TAG_ARRAY
#undef LMI_CLONE_TAG_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation is done for the very first time
*/
#define LMI_CLONE_TAG_ARRAY(iNewArrayLen, ptNewArray, iSrcArrayLen, ptSrcArray){\
            for(int iDx = 0; iDx < iSrcArrayLen; iDx++){\
                    iNewArrayLen++;\
                    if(iNewArrayLen == 1){\
                        ptNewArray = (tag_t*) MEM_alloc(iNewArrayLen * sizeof(tag_t));\
                    }\
                    else{\
                        ptNewArray = (tag_t*) MEM_realloc(ptNewArray, iNewArrayLen * sizeof(tag_t));\
                    }\
                    ptNewArray[iNewArrayLen-1]=  ptSrcArray[iDx];\
            }\
}

#endif

#ifdef LMI_UPDATE_LOGICAL_ARRAY
#undef LMI_UPDATE_LOGICAL_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation is done for the very first time
*/
#define LMI_UPDATE_LOGICAL_ARRAY(iCurrentArrayLen, pbArray, bValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                pbArray = (logical*) MEM_alloc(iCurrentArrayLen * sizeof(logical));\
            }\
            else{\
                pbArray = (logical*) MEM_realloc(pbArray, iCurrentArrayLen * sizeof(logical));\
            }\
            pbArray[iCurrentArrayLen-1]=  bValue;\
}

#ifdef LMI_UPDATE_INT_ARRAY
#undef LMI_UPDATE_INT_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation is done for the very first time
*/
#define LMI_UPDATE_INT_ARRAY(iCurrentArrayLen, piArray, iValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                piArray = (int*) MEM_alloc(iCurrentArrayLen * sizeof(int));\
            }\
            else{\
                piArray = (int*) MEM_realloc(piArray, iCurrentArrayLen * sizeof(int));\
            }\
            piArray[iCurrentArrayLen-1]=  iValue;\
}

#ifdef LMI_UPDATE_DOUBLE_ARRAY
#undef LMI_UPDATE_DOUBLE_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation is done for the very first time
*/
#define LMI_UPDATE_DOUBLE_ARRAY(iCurrentArrayLen, pdArray, dValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
            pdArray = (double*) MEM_alloc(iCurrentArrayLen * sizeof(double));\
            }\
            else{\
                pdArray = (double*) MEM_realloc(pdArray, iCurrentArrayLen * sizeof(double));\
            }\
            pdArray[iCurrentArrayLen-1]=  dValue;\
}

#ifdef LMI_UPDATE_DATE_ARRAY
#undef LMI_UPDATE_DATE_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation is done for the very first time
*/
#define LMI_UPDATE_DATE_ARRAY(iCurrentArrayLen, pdateArray, dateValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
            pdateArray = (date_t*) MEM_alloc(iCurrentArrayLen * sizeof(date_t));\
            }\
            else{\
                pdateArray = (date_t*) MEM_realloc(pdateArray, iCurrentArrayLen * sizeof(date_t));\
            }\
            pdateArray[iCurrentArrayLen-1]=  dateValue;\
}


