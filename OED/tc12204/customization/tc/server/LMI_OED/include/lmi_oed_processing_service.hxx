/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_oed_processing_service.hxx

    Description:  Header File for lmi_oed_processing_service.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-Nov-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_OED_PROCESSING_SERVICE_HXX
#define LMI_OED_PROCESSING_SERVICE_HXX
    
#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <lmi_operator.hxx>
#include <lmi_oed_string.hxx>
#include <lmi_oed_object_info.hxx>
#include <lmi_oed_info.hxx>

class LMIOEDProcessingService
{
    public:
        int LMI_process_oed1( tag_t tObject, const vector<LMIOEDString>& vobjOEDStrings, vector<LMIOEDObjectInfo>& refvObjsInfo );

        int LMI_process_oed2( vector<tag_t> vtObjects, const vector<LMIOEDString>& vobjOEDStrings, vector<LMIOEDObjectInfo>& refvObjsInfo );

        int LMI_process_src_dest_oed1( tag_t tObject, const vector<LMIOEDString>& vobjOEDStrings, LMIOEDInfo& refobjOEDInfo );

        int LMI_process_src_dest_oed2( vector<tag_t> vtObjects, const vector<LMIOEDString>& vobjOEDStrings, LMIOEDInfo& refobjOEDInfo );

        int LMI_process_oed_prop_propogation( tag_t tObject, const vector<LMIOEDString>& vobjOEDStrings );

        int LMI_process_oed_prop_propogation2( const vector<tag_t> tObjects, const vector<LMIOEDString>& vobjOEDStrings );

        int LMI_propogate_src_to_dest(const LMIOEDObjectInfo& objSrcInfo, const LMIOEDObjectInfo& objDestInfo);

        int LMI_ps_get_all_note_type( vector<string>& refvstrNoteTypes);

        int LMI_BOM_close_windows( const LMIOEDObjectInfo& refOEDObjInfo );

        int LMI_src_dest_bom_close_windows( const vector<LMIOEDSrcDestInfo>& refvOEDSrcDestInfo );
};
 
#endif

