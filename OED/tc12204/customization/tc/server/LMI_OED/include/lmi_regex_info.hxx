/*======================================================================================================================================================================
                                                    Copyright 2019  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_regex_info.hxx

    Description: A container class that holds regex pattern and its corresponding replacement string info.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-FEB-19     Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#ifndef LMI_REGEX_INFO_HXX
#define LMI_REGEX_INFO_HXX

#include <string>
using namespace std;

class LMIRegexInfo
{
    public:
        string strRegxPattern;
        string strRepString;      
};

#endif

