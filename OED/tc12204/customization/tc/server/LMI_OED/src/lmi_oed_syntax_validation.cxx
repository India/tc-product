/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_oed_syntax_validation.cxx

    Description: This file contains methods related to syntactical validation of oed string.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-DEC-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_oed_syntax_validation.hxx>
#include <lmi_library.hxx>

/*
 * This function validates OED strings syntax. Valid OED strings are parsed and various information�s are stored in object of class "LMIOEDString".
 * Function will perform following syntax validations:
 * - Function will check occurrence order and count of Open and Close brackets in each OED string. Occurrence count of both Open and Close brackets in OED string must be same.
 * - Function will also reflector OED string by replacing multiple consecutive occurrence of OED operators (e.g. "," "�" ":" etc.) or separator (e.g. ".") with appropriate single ones.
 *   Function will exclude OED grouping operators i.e. Open and Close brackets from refactoring in this step.
 * - Function will remove all grouping operators (Open and Close brackets) except First and last Open and Close bracket from the input string. Function will parse the OED string and store following
 *   information in "LMIOEDString" object: 
 *                 -- Operation Prefix such as "REFBY", "GRMS2P" etc.
 *                 -- Property Name
 *                 -- Object Type and it position such as "ALL", "LAST" & "FIRST"
 *                 -- BOM Line Level and Revision rule
 *
 * @param[in]  strOEDExprSeparator OED string expression separator.
 * @param[in]  vstrOEDStrings      Vector of OED strings.( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                   not change the value being referenced.)
 * @param[in]  vobjLMIOptrs        Vector of OED operators. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                   not change the value being referenced.)
 * @param[out] refvobjOEDStrings   Vector of "LMIOEDString" objects. 
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDSyntaxValidation::LMI_validate_oed_syntax
(
    const string strOEDExprSeparator,
    const vector<string>& vstrOEDStrings,
    const vector<LMIOperator>& vobjLMIOptrs,
    vector<LMIOEDString>& refvobjOEDStrings
)
{
    int iRetCode = ITK_ok;

    /* Initializing out parameter of this function. */
    refvobjOEDStrings.clear();

    vector<string>::const_iterator iterOEDString;

    for(iterOEDString = vstrOEDStrings.begin(); iterOEDString < vstrOEDStrings.end() && iRetCode == ITK_ok; iterOEDString++)
    {
        string strOEDStrTemp;
        string strFirstOptr;
        string strLastOptr;

        /* Object corresponding to each oed string */
        LMIOEDString objOEDString;

        strFirstOptr.assign(vobjLMIOptrs[LMI_OPTR_FIRST_INDEX].strOperatorSign);
        strLastOptr.assign(vobjLMIOptrs[LMI_OPTR_LAST_INDEX].strOperatorSign);

        /* Validating OED grouping operators */
        LMI_ITKCALL(iRetCode, LMI_validate_OED_grouping_optr(*iterOEDString, strFirstOptr, strLastOptr));       

        if(iRetCode == ITK_ok)
        {
            /* OED string needs to be checked using regular expression hence copy of string is created in order to pass it as reference so that if required string can be modified by the calling functions. */
            strOEDStrTemp.assign(*iterOEDString);
        }

        /* Refectoring OED string(Replacing multiple consecutive occurance of operators with single ones). */
        LMI_ITKCALL(iRetCode, LMI_refactor_OED_string(strOEDExprSeparator, vobjLMIOptrs, strOEDStrTemp));

        /* Validating oed string and returning it in object format */
        LMI_ITKCALL(iRetCode, LMI_validate_oed_string(strOEDStrTemp, strOEDExprSeparator, vobjLMIOptrs, objOEDString));

        if (iRetCode == ITK_ok)
        {  
           refvobjOEDStrings.push_back(objOEDString);       
        }    
    }

    return iRetCode;
}

/*
 * This function validates a single OED string syntax and returns information in "LMIOEDString" object corresponding to the string. OED string can be in following format 
 * e.g. "REFBY(items_tag,ItemRevision).GRM(IMAN_manifestation,PDX).object_string".
 * Function will perform following steps:
 * - Function will remove all grouping operators (Open and Close brackets) except First and last Open and Close bracket from the input string.
 * - Function will parse the OED string and store following information in "LMIOEDString" object: 
 *                 -- Operation Prefix such as "REFBY", "GRMS2P" etc.
 *                 -- Property Name
 *                 -- Object Type and it position such as "ALL", "LAST" & "FIRST"
 *                 -- BOM Line Level and Revision rule
 *
 * @param[in]  strOEDString        OED string.
 * @param[in]  strOEDExprSeparator OED string expression separator.
 * @param[in]  vobjLMIOptrs        Vector of operators. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                   not change the value being referenced.)
 * @param[out] refobjOEDString     Object representing one OED string.
 *                                
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDSyntaxValidation::LMI_validate_oed_string
(
    const string strOEDString, 
    const string strOEDExprSeparator,
    const vector<LMIOperator>& vobjLMIOptrs,
    LMIOEDString& refobjOEDString
)
{
    int iRetCode  = ITK_ok;
    int iSepIndex = 0;
    vector<string> vstrOEDSubSegments;

    /* 
     * Spliting oed string by "strOEDExprSeparator". Ex- REFBY(items_tag,ItemRevision).GRM(IMAN_manifestation,PDX).object_string, If "strOEDExprSeparator" is "." then 
     * this oed string will split in three parts.
     */
    LMI_ITKCALL(iRetCode, LMI_str_tokenize((strOEDExprSeparator.c_str()), (strOEDString.c_str()), vstrOEDSubSegments));

    if (iRetCode == ITK_ok)
    {
        if (vstrOEDSubSegments.size() == 0)
        {
            /*  If size of sub segments are zero then throwing error this refers that OED String is empty */
            iRetCode = SNTX_VAL_OED_STRING_INVALID;

            EMH_store_error_s1(EMH_severity_error, iRetCode, strOEDString.c_str());
        }
        else
        {
            bool bFoundProp = false;
            vector<string>::iterator iterOEDSubSegment;

            /* Loop over sub segments of OED string. */
            for(iterOEDSubSegment = vstrOEDSubSegments.begin(); iterOEDSubSegment < vstrOEDSubSegments.end() && iRetCode == ITK_ok; iterOEDSubSegment++)
            {
                /* Check to ensure that nothing can appear once property is encountered. If appears then it is invalid OED string. */
                if (bFoundProp == false)
                {
                    string strUpdatedString;

                    /* Refactoring OED grouping operators. */
                    LMI_ITKCALL(iRetCode, LMI_refactor_sub_sgmnt_brackets(vobjLMIOptrs, (*iterOEDSubSegment), strUpdatedString));

                    /* 
                     * Validates OED string sub segment and if it succeeds then based on segment type( contains operator or not), either sets "strOEDProperty" 
                     * or push object in "vobjOEDSubSegments" property of "refobjOEDString" object.
                     */
                    LMI_ITKCALL(iRetCode, LMI_validate_and_store_oed_sub_segment(strUpdatedString, vobjLMIOptrs, bFoundProp, refobjOEDString));
                
                    strUpdatedString.clear();
                }
                else
                {
                    iRetCode = SNTX_VAL_OED_STRING_INVALID;

                    EMH_store_error_s1(EMH_severity_error, iRetCode, strOEDString.c_str());   
                }
            }

            if (iRetCode == ITK_ok)
            {
                /* For "LMIOEDSubSegment" object property must be set. */
                if (bFoundProp == false)
                {
                    iRetCode = SNTX_VAL_OED_STR_OBJ_ATT_NOT_EXIST;

                    EMH_store_error_s1(EMH_severity_error, iRetCode, strOEDString.c_str());
                }
            }
        }
    }

    return iRetCode;
}

/*
 * This function validates an oed string sub segment and if succeeds then either sets the "strOEDProperty" of "LMIOEDString" object or push an object of type "LMIOEDSubSegment"
 * into "vobjOEDSubSegments" property of "LMIOEDString" object". 
 *
 * @param[in]  strOEDSubSegment  OED string sub segment.
 * @param[in]  vobjLMIOptrs      Vector of operators. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                 not change the value being referenced.)
 * @param[in]  refbIsPropSet     OED string sub segment.
 * @param[out] refobjOEDString   Object representing an OED string sub segment.
 *                                
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDSyntaxValidation::LMI_validate_and_store_oed_sub_segment
(
    string strOEDSubSegment, 
    const vector<LMIOperator>& vobjLMIOptrs,
    bool& refbFoundProp,
    LMIOEDString& refobjOEDString
)
{
    int  iRetCode = ITK_ok;
    bool bHasOptr = false;  

    /* Check weather "strOEDSubSegment" contains operator or not */
    LMI_ITKCALL(iRetCode, LMI_sub_sgmnt_contains_optr(strOEDSubSegment, vobjLMIOptrs, bHasOptr));

    if(iRetCode == ITK_ok)
    {
        /* If "strOEDSubSegment" contains operator then we need to create "LMIOEDSubSegment" corresponding to it */
        if (bHasOptr == true)
        {
            int   iCurOptrIndex = -1;
            int   iOptrCount    = 0;
            char* pszRemSegment = NULL;
            LMIOEDSubSegment objLMISubsgmnt;
            vector<LMIOperator>::const_iterator iterOptr;
            
            iOptrCount = (int)(vobjLMIOptrs.size());
            
            /* "pszRemSegment" stores remaining segment in each iteration  */
            LMI_STRCPY(pszRemSegment, strOEDSubSegment.c_str());

            for (iterOptr = vobjLMIOptrs.begin(); iterOptr < vobjLMIOptrs.end() && iRetCode == ITK_ok; iterOptr++)
            {
                
                int    iSepStringCnt = 0;
                char** ppszSepString = NULL;

                iCurOptrIndex++;
                
                /* Tokenizing remaining sub segment by current operator */
                LMI_ITKCALL(iRetCode, LMI_str_parse_string(pszRemSegment, ((char *)(*iterOptr).strOperatorSign.c_str()), &iSepStringCnt, &ppszSepString));

                if(iRetCode == ITK_ok)
                {
                    /* Free up "pszRemSegment", so that it can be used to store remaining segment for next iteration */
                    LMI_MEM_TCFREE(pszRemSegment);
                    
                    if(iSepStringCnt == 1 && ((*iterOptr).bIsOptional == true))
                    {
                        /* 
                         * If by tokenizing with current operator, we got only one string, it indicates that current operator is not present in remaining sub segment. 
                         * In this case if current operator is optional then we can proceed with same remaining sub segment. 
                         */

                        LMI_STRCPY(pszRemSegment, ppszSepString[0]);
                    }
                    else if(iSepStringCnt == 2)
                    {
                        /* 
                         * If by tokenizing with current operator, we got two strings, it indicates that current operator is present in remaining sub segment.
                         * In this case second string is stored in "pszRemSegment" to use it as remaining sub segment in next iteration 
                         */

                        LMI_STRCPY(pszRemSegment, ppszSepString[1]);
                    
                        /* Setting up appropriate property on "objLMISubsgmnt" object. */
                        LMI_ITKCALL(iRetCode, LMI_set_sub_sgmnt_prop(ppszSepString[0], iCurOptrIndex, objLMISubsgmnt))
                    }
                    else if((iCurOptrIndex == (iOptrCount -1)) && ((*iterOptr).bIsOptional == false))
                    {
                        /* This case handles the encounter of last operator. In this situation we get only one string. */
                        LMI_ITKCALL(iRetCode, LMI_set_sub_sgmnt_prop(ppszSepString[0], iCurOptrIndex, objLMISubsgmnt))
                    }
                    else
                    {
                        iRetCode = SNTX_VAL_OED_STRING_SUB_SEGMENT_INVALID;

                        EMH_store_error_s1(EMH_severity_error, iRetCode, strOEDSubSegment.c_str());
                    }
                }

                LMI_MEM_TCFREE(ppszSepString);
            }

            if(iRetCode == ITK_ok)
            {
                 refobjOEDString.vobjOEDSubSegments.push_back(objLMISubsgmnt);
            }

            LMI_MEM_TCFREE(pszRemSegment);
        }
        else
        {
            /* If "strOEDSubSegment" does not contain operator then it is considered as "strOEDPropperty" */
            refobjOEDString.strOEDProperty.assign(strOEDSubSegment);

            refbFoundProp = true;
        }
    }
    return iRetCode;
}
                
/*
 * This function sets appropriate property of "refobjOEDSubSegment" object on the base of its current state and current operator index.
 *
 * @param[in]  strPropVal        String value for one of "objOEDSubSegment" property.
 * @param[in]  iCurOptrIndex     Current operator index in vector of operators.
 * @param[out] objOEDSubSegment  Object of class "LMIOEDSubSegment" corresponding to "strOEDSubSegment".                            
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDSyntaxValidation::LMI_set_sub_sgmnt_prop
(
    const string strPropVal,
    const int    iCurOptrIndex,
    LMIOEDSubSegment& refobjOEDSubSegment    
)
{
    int iRetCode    = ITK_ok;
    
    /* Setting properties based on current operator */
    switch (iCurOptrIndex)
    {
        case LMI_OPTR_FIRST_INDEX:
            {
                /* LMI first operator comes right after prefix. */
                refobjOEDSubSegment.strPrefix.assign(strPropVal);
            }
            break;
        case LMI_OPTR_NORMAL_SEP_INDEX:
            {
                /* LMI operator normal separator comes right after reference property or relation name. */
                refobjOEDSubSegment.strRefRelName.assign(strPropVal);
            }
            break;
        case LMI_OPTR_OP1_INDEX:
            {
                /* LMI operator optional operator 1 comes right after object type. */
                refobjOEDSubSegment.strObjectType.assign(strPropVal);            
            }
            break;
        case LMI_OPTR_OP2_INDEX:
            {
                /* 
                 * Optional operator 2 comes just right after revision rule. Because we are dealing with revision rule, we need to make sure that in strRefRelName 
                 * property of "refobjOEDSubSegment" we already have "structure_revisions". 
                 */
                if (tc_strcmp(refobjOEDSubSegment.strRefRelName.c_str(), LMI_ATTR_STRUCTURE_REVISIONS) == 0)
                {
                    refobjOEDSubSegment.strRevisionRule.assign(strPropVal);
                }
                else
                {
                    iRetCode = SNTX_VAL_OED_STRING_SUB_SEGMENT_INVALID;

                    EMH_store_error_s1(EMH_severity_error, iRetCode, strPropVal.c_str());
                }
            }
            break;
        case LMI_OPTR_LAST_INDEX:
        {
            /* If last operator is encountered then there can be following scenarios: */

                if(refobjOEDSubSegment.strObjectType.length() == 0)
                {
                    /* If last operator is encountered and object type is not set yet then current prop value is definitely object type. */
                    refobjOEDSubSegment.strObjectType.assign(strPropVal);
                }
                else if (tc_strcmp(refobjOEDSubSegment.strRefRelName.c_str(), LMI_ATTR_STRUCTURE_REVISIONS) == 0)
                {
                    /* check to ensure whether it is BOM case or not */

                    if (refobjOEDSubSegment.strRevisionRule.length() > 0)
                    {
                        /* check to ensure that revision rule is already set if yes then current prop value is considered as BOM level. */
                        int iOptrExactBomLevLen = (int)tc_strlen(LMI_OPTR_EXACT_BOM);

                        if (tc_strncasecmp(strPropVal.c_str(), LMI_OPTR_EXACT_BOM, iOptrExactBomLevLen) == 0)
                        {
                            /* Check to confirm it is exact BOM level or not. */
                            refobjOEDSubSegment.bExactBOMLevel = true;

                            refobjOEDSubSegment.strBOMLineLevel.assign(strPropVal.substr(iOptrExactBomLevLen));
                        }
                        else
                        {
                            refobjOEDSubSegment.bExactBOMLevel = false;

                            refobjOEDSubSegment.strBOMLineLevel.assign(strPropVal);       
                        }
                    }
                    else
                    {
                        /* 
                         * Throw error because last operator is encountered while it is BOM case and revision rule is not set yet(In BOM case prop value that appear just before
                         * last operator is considered BOM level). 
                         */
                        iRetCode = SNTX_VAL_OED_STRING_SUB_SEGMENT_INVALID;

                        EMH_store_error_s1(EMH_severity_error, iRetCode, strPropVal.c_str());   
                    }
                }
                else
                {
                    refobjOEDSubSegment.strDepth.assign(strPropVal);
                }
          }
    }
          
    return iRetCode;     
 }

/*
 * This function takes OED expression separator and OED operators as input parameters. If any of operators or separator have multiple consecutive occurrence in OED 
 * string then function will replace these operators or separator with appropriate single operators or separator and refactored OED string will be returned. 
 * @note:
 *         Function will exclude OED grouping operators from refactoring.
 *
 * @param[in]  strOEDExprSeparator OED string expression separator 
 * @param[in]  vobjLMIOptrs        Vector of OED operators
 * @param[out] refstrOEDSubSgmnt   Refactored OED string                         
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDSyntaxValidation::LMI_refactor_OED_string
(
    const string strOEDExprSeparator,
    const vector<LMIOperator>& vobjLMIOptrs,
    string& refstrOEDString
)
{
    int iRetCode = ITK_ok;
    int iCount   = 0;
    int iVSize   = 0;
    vector<LMIRegexInfo> vregxInfo;
    
    vector<LMIOperator>::const_iterator iterOptr;

    iVSize = (int)(vobjLMIOptrs.size());

    /* Creating regex info for segment separator */
    LMI_ITKCALL(iRetCode, LMI_add_regex_info(strOEDExprSeparator, vregxInfo));

    /* Creating regex info for operators */
    for (iterOptr = vobjLMIOptrs.begin(); iterOptr < vobjLMIOptrs.end() && iRetCode == ITK_ok; iterOptr++)
    {
        iCount ++;

        /* Check to exclude OED grouping operators from refactoring. */
        if(iCount > 1 && iCount < iVSize)
        {
            LMI_ITKCALL(iRetCode, LMI_add_regex_info(iterOptr->strOperatorSign, vregxInfo));
        }
    }

    /* Replacing patterns */
    LMI_ITKCALL(iRetCode, LMI_rep_regx_ptrns_with_strings(vregxInfo, refstrOEDString));

    return iRetCode;
}

/*
 * This function takes a single character as input parameter. It creates a "LMIRegexInfo" object that have pattern and replacement string. Pattern is defined to replace 
 * multiple consecutive occurance of character with single character.
 *
 * @param[in]  strCharacter   Character whose multiple consecutive occurances needs to be replaced with single character.
 * @param[out] refvregxInfo   Vector holding the regex infos.                            
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDSyntaxValidation::LMI_add_regex_info
(
    const string strCharacter, 
    vector<LMIRegexInfo>& refvregxInfo
)
{
    int iRetCode = ITK_ok;

    LMIRegexInfo objRegxInfo;

    /* Adding pattern to replace two or more consecutive occurance of a character with single one. */
    objRegxInfo.strRegxPattern.append(LMI_REGX_PATTERN_START);

    objRegxInfo.strRegxPattern.append(strCharacter);

    objRegxInfo.strRegxPattern.append(LMI_REGX_PATTERN_END);

    /* Adding character itself as a replacement string */
    objRegxInfo.strRepString.assign(strCharacter);

    refvregxInfo.push_back(objRegxInfo);

    return iRetCode;
}

/*
 * This function takes OED string and two operators as input parameters. Function validates operators occurance order and count. Occurance count of both operators in OED string
 * must be same. At any index in OED string, count of second operator occurance can not be more then count of first operator occurance. 
 *
 * @param[in] strOEDString  OED string to validate.
 * @param[in] strOptrFst    First operator.
 * @param[in] strOptrSec    Second operator.                            
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDSyntaxValidation::LMI_validate_OED_grouping_optr
(
    const string strOEDString, 
    const string strOptrFst, 
    const string strOptrSec
)
{
    int iRetCode   = ITK_ok;
    int iOptrCount = 0;
    int iStrLen    = 0;

    iStrLen = (int)(strOEDString.length());

    for (int iDx = 0; iDx < iStrLen && iRetCode == ITK_ok; iDx++)
    {
        string strCurrentChar = strOEDString.substr(iDx, 1);

        if (tc_strcasecmp(strCurrentChar.c_str(), strOptrFst.c_str()) == 0)
        {
            /* Increasing iOptrCount on first operator occurance. */
            iOptrCount = iOptrCount + 1;
        }
        else if(tc_strcasecmp(strCurrentChar.c_str(), strOptrSec.c_str()) == 0)
        {
            /* Decreasing iOptrCount on second operator occurance. */
            iOptrCount = iOptrCount -1;
        }

        /* Emptying the string to avoid junk value initialization for next time */
        strCurrentChar.clear();

        if ((iOptrCount < 0) || (iDx == (iStrLen -1) && iOptrCount != 0))
        {
             /*
             * If iOptrCount is less then 0 that means second operator has occured more then first which is wrong.
             * If it is last iteration and iOptrCount count is not 0 that means first and second operator occurance count is not same which is wrong.
             */

            iRetCode = SNTX_VAL_OED_STRING_INVALID;

            EMH_store_error_s1(EMH_severity_error, iRetCode, strOEDString.c_str());
        }
    }

    return iRetCode;
}

/*
 * This function takes OED operators and OED string sub segments as input parameters and returns updated OED string sub segment. This function removes extra unnecessary 
 * grouping operators.
 *
 * Ex: REF(structure_revisions,(view):(Latest Revision)�2) it will be updated as REF(structure_revisions,view:Latest Revision�2)
 *
 * @param[in] vobjLMIOptrs           Vector of OED operators.
 * @param[in] strSubSgmnt            OED string sub segment.
 * @param[in] refstrUpdatedSubSgmnt  Updated OED string sub segment.                            
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDSyntaxValidation::LMI_refactor_sub_sgmnt_brackets
(
    const vector<LMIOperator>& vobjLMIOptrs, 
    string  strSubSgmnt,
    string& refstrUpdatedSubSgmnt 
)
{
    int    iRetCode      = ITK_ok;
    int    iOprSize      = 0;
    bool   bFoundFirstOp = false;
    string strGroupStartOpSign;
    string strGroupEndOpSign;
    
 
    /* Initialize out parameter */
    refstrUpdatedSubSgmnt.clear();

    iOprSize = (int)(vobjLMIOptrs.size());

    strGroupStartOpSign.assign(vobjLMIOptrs[LMI_OPTR_FIRST_INDEX].strOperatorSign);

    strGroupEndOpSign.assign(vobjLMIOptrs[iOprSize - 1].strOperatorSign);

    /* Tokenizing OED string subsegment using grouping start operator. */
    char* pszToken = strtok((char*)(strSubSgmnt.c_str()), strGroupStartOpSign.c_str());
    
    /* "bFoundFirstOp" needs to set to decide whether we need to append grouping start operator after prefix or current OED sub segment is just property name. */
    if((strSubSgmnt.length()) != strlen(pszToken))
    {
        bFoundFirstOp = true;
    }
    
    /* Iterating till tokenized  sub segment is not null. */
    while(pszToken != NULL && iRetCode == ITK_ok)
    {           
        string strSubString;

        strSubString.clear();

        /* Validating and removing unnecessary grouping end operator. */
        LMI_ITKCALL(iRetCode, LMI_validate_and_remove_oed_closing_optr(vobjLMIOptrs, pszToken, strGroupEndOpSign, strSubString));

        if (iRetCode == ITK_ok)
        {
            if(strSubString.length() > 0)
            {
                if (refstrUpdatedSubSgmnt.length() <= 0)
                {
                    refstrUpdatedSubSgmnt.append(strSubString);

                    if(bFoundFirstOp == true)
                    {
                        /* If "refstrUpdatedSubSgmnt" is empty yet and grouping start operator is encountered then we needs to append grouping start operator to updated subsegment. */
                        refstrUpdatedSubSgmnt.append(strGroupStartOpSign);
                    }
                }
                else
                {
                    refstrUpdatedSubSgmnt.append(strSubString);
                }
            }
            /* Tokenizing remaining OED string subsegment using grouping start operator. */
            pszToken = strtok(NULL, strGroupStartOpSign.c_str());
        }         
    }

    if(bFoundFirstOp == true && iRetCode == ITK_ok)
    {
        /* In the end we need to append grouping close operator. */
        refstrUpdatedSubSgmnt.append(strGroupEndOpSign);
    }

    return iRetCode;
}

/*
 * This function takes OED operators and OED string sub segments as input parameters and returns updated OED string sub segment. This function removes extra unnecessary 
 * grouping operators.
 *
 * Ex: REF(structure_revisions,(view):(Latest Revision)�2) it will be updated as REF(structure_revisions,view:Latest Revision�2)
 *
 * @param[in] vobjLMIOptrs      Vector of OED operators.
 * @param[in] pszSubSgmntToken  OED string sub segment.
 * @param[in] strGroupEndOpSign Updated OED string sub segment. 
 * @param[in] refstrSubString   Updated OED string sub segment. 
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDSyntaxValidation::LMI_validate_and_remove_oed_closing_optr
(
    const vector<LMIOperator>& vobjLMIOptrs,
    char*   pszSubSgmntToken,
    string  strGroupEndOpSign,
    string& refstrSubString
)
{
    int    iRetCode = ITK_ok;
    string strInputStr;
    vector<string> vStrList;
    vector<string>::const_iterator iterSubStr;

    strInputStr.assign(pszSubSgmntToken);

    /* Parsing remaining sub segment token by grouping end operator. */
    LMI_ITKCALL(iRetCode, LMI_str_parse_string(strInputStr, strGroupEndOpSign, vStrList));

    /* Removing empty strings from parsed vector of strings(Get multiple empty strings when multiple consecutive grouping end operator occures.). */
    LMI_ITKCALL(iRetCode, LMI_vector_remove_empty_string(vStrList));

    if(iRetCode == ITK_ok)
    {
        logical bStart = false;
        
        for(iterSubStr = vStrList.begin(); iterSubStr < vStrList.end() && iRetCode == ITK_ok; iterSubStr++)
        {
            if(bStart == false)
            {
                bStart = true;

                refstrSubString.assign(*iterSubStr);
            }
            else
            {
                logical bFoundOptr = false;
                vector<LMIOperator>::const_iterator iterOptr;

                for(iterOptr = vobjLMIOptrs.begin(); iterOptr < vobjLMIOptrs.end() && iRetCode == ITK_ok; iterOptr++)
                {
                    if((iterSubStr->compare(0, (iterOptr->strOperatorSign.length()), (iterOptr->strOperatorSign))) == 0)
                    {
                        bFoundOptr = true;

                        break;
                    }
                }
                
                if(iRetCode == ITK_ok)
                {
                    if(bFoundOptr == false)
                    {
                        /* Report Error because closing operator for OED occurs in the middle of the string */
                        iRetCode = SNTX_VAL_OED_STRING_INVALID;

                        EMH_store_error_s1(EMH_severity_error, iRetCode, pszSubSgmntToken);
                    }
                    else
                    {
                        refstrSubString.append(*iterSubStr);
                    }
                }
            }
        }
    }

    return iRetCode;
}

/*
 * This function takes an OED string sub segment and OED operators as input parameters and return weather given sub segment contains any operator or not. 
 *
 * @param[in]  strSubSgmnt   OED string sub segment
 * @param[in]  vobjLMIOptrs  Vector of OED operators
 * @param[out] refbHasOptr   Variable to indicate weather given sub segment contains any operator or not                            
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDSyntaxValidation::LMI_sub_sgmnt_contains_optr
(
    const string strSubSgmnt, 
    const vector<LMIOperator>& vobjLMIOptrs, 
    bool& refbHasOptr
)
{
    int iRetCode = ITK_ok;

    /* Initialize out parameter */
    refbHasOptr = false;

    vector<LMIOperator>::const_iterator iterOptr;

    for(iterOptr = vobjLMIOptrs.begin(); iterOptr < vobjLMIOptrs.end() && iRetCode == ITK_ok; iterOptr++)
    {
        int iOptrIdxInSgmnt = (int)(strSubSgmnt.find(iterOptr->strOperatorSign));

        if (iOptrIdxInSgmnt < 0)
        {
            refbHasOptr = false;
        }
        else
        {
            refbHasOptr = true;
            break;
        }
    }

    return iRetCode;
}


