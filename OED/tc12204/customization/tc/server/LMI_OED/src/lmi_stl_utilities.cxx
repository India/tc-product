/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_stl_utilities.cxx

    Description:  STL enhancements.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>

int LMI_stl_copy_content_to_vector
(
    vector<tag_t>  vtSrcInfo,
    vector<tag_t>& refvtDestInfo,
    logical         bUnique
)
{
    int iRetCode = ITK_ok;
    vector<tag_t>::iterator iterSrcInfo;

    for(iterSrcInfo = vtSrcInfo.begin(); iterSrcInfo !=  vtSrcInfo.end(); iterSrcInfo++)
    {
        if (bUnique == true)
        {
            vector<tag_t>::iterator iterDestInfo;

            iterDestInfo = find(refvtDestInfo.begin(), refvtDestInfo.end(), (*iterSrcInfo));
        
            if(iterDestInfo == refvtDestInfo.end())
            {
                refvtDestInfo.push_back(*iterSrcInfo);
            }
        }
        else
        {
             refvtDestInfo.push_back(*iterSrcInfo);   
        }
    }

    return iRetCode;
}

/**
 * This function will copy the content of input tag of vector "vtSrcInfo" to destination vector of tags "pvtDestInfo". Function will only push unique tags in vector which are already not present.
 *
 * @param[in]  vtSrcInfo   Source vector containing list of tags
 * @param[out] pvtDestInfo Destination vector containing unique list of tags
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI_stl_copy_unique_content_to_vector
(
    vector<tag_t>  vtSrcInfo,
    vector<tag_t>& refvtDestInfo
)
{
    int iRetCode = ITK_ok;
    vector<tag_t>::iterator iterSrcInfo;

    for(iterSrcInfo = vtSrcInfo.begin(); iterSrcInfo !=  vtSrcInfo.end(); iterSrcInfo++)
    {
        vector<tag_t>::iterator iterDestInfo;

        iterDestInfo = find(refvtDestInfo.begin(), refvtDestInfo.end(), (*iterSrcInfo));
        
        if(iterDestInfo == refvtDestInfo.end())
        {
            refvtDestInfo.push_back(*iterSrcInfo);
        }
    }

    return iRetCode;
}

/**
 * This function loops input array of strings "ppszValue" and add string from input string array to vector "refvFinal". If input argument "bUnique" is 'true' than string from input array is directly stored 
 * in vector "refvFinal" otherwise uniqueness check is performed and only if the value is not found in vector "refvFinal" than it is stored in the vector. 
 * 
 * @param[in]  iCount    Size of the string array "ppszValue"
 * @param[in]  ppszValue Array containing strings
 * @param[in]  bUnique   If "bUnique" is 'true' than value is checked for uniqueness and if found unique than value is pushed into vector.
 * @param[out] refvFinal List of values.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI_vector_store_string_array
(
    int             iCount,
    char**          ppszValue,
    logical         bUnique,    
    vector<string>& refvFinal
)
{
    int iRetCode = ITK_ok;

    for(int iDx = 0; iDx < iCount; iDx++)
    {
        if(ppszValue[iDx] != NULL && tc_strlen(ppszValue[iDx]) > 0)
        {
            if(bUnique == false)
            {
                refvFinal.push_back(ppszValue[iDx]);
            }
            else
            {
                vector<string>::iterator itervFinal;

                itervFinal = find(refvFinal.begin(), refvFinal.end(), ppszValue[iDx]);

                if(itervFinal == refvFinal.end())
                {
                    refvFinal.push_back(ppszValue[iDx]);
                }
            }
        }
    }    

    return iRetCode;
}

/**
 * This function takes string "strInput" and separator "strSeparator" as input arguments. It parses string 'strInput' by separator 'strSeparator' and returns a vector containing parsed values. 
 *
 * @param[in]  strInput      String to be parsed
 * @param[in]  strSeparator  Separator 
 * @param[out] refvValuelist List of parsed values
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI_str_parse_string
(
    string          strInput,
    string          strSeparator,
    vector<string>& refvValueList
)
{
    int iRetCode = ITK_ok;
    
     /* Validate input */
    if(strInput.size() == 0 || strSeparator.size() == 0)
    {
        TC_write_syslog( "ERROR: Missing input data: List passed or separator is null\n");
    }
    else
    {
        string strToken;
        size_t pos = 0;
    
        while((pos = strInput.find(strSeparator)) != string::npos) 
        {
            strToken = strInput.substr(0, pos);
        
            LMI_ITKCALL(iRetCode, LMI_vector_store_string(strToken, false, refvValueList));

            strInput.erase(0, pos + strSeparator.length());
        }

        LMI_ITKCALL(iRetCode, LMI_vector_store_string(strInput, false, refvValueList));
    }
        
    return iRetCode;
}

/**
 * This function stores input string to vector "refvFinal". If input argument "bUnique" is 'true' than value "strValue" is directly stored in vector "refvFinal" otherwise uniqueness check is
 * performed and only if the value is not found in vector "refvFinal" than it is stored in the vector.  
 * 
 * @param[in]  strValue  Value to be added to vector.
 * @param[in]  bUnique   If 'true' than value is checked and if found unique than added to vector
 * @param[out] refvFinal List of values.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI_vector_store_string
(
    string          strValue,
    logical         bUnique,
    vector<string>& refvFinal
)
{
    int iRetCode = ITK_ok;

    if(bUnique == false)
    {
        refvFinal.push_back(strValue);
    }
    else
    {
        vector<string>::iterator itervFinal;

        itervFinal = find(refvFinal.begin(), refvFinal.end(), strValue);

        if(itervFinal == refvFinal.end())
        {
            refvFinal.push_back(strValue);
        }
    }    

    return iRetCode;
}


/**
 * This function tokenize string based on given tokenizer and returns the vector of tokenized parts. 
 * 
 * @param[in]  pszDelim     Delimeter.
 * @param[in]  pszInpString Input string to be tokenize
 * @param[out] vOutStrings  Vector of tokenized parts .
 * 
 * @retval ITK_ok is always returned.
 */
int LMI_str_tokenize
(
    const char*   pszDelim, 
    const char*   pszInpString,
    vector<string>& vOutStrings
)
{
    int    iRetCode         = ITK_ok;
    int    iLen             = 0;
    int    iTokensCnt       = 0;
    char*  pszToken         = NULL;
    char*  pszInpTempString = NULL;    
    char** ppszOutList      = NULL;

    if(pszInpString != NULL && strlen(pszInpString) > 0)
    {
        /* Creating copy of input string as tc_strtok will modify the input source string */        
        LMI_STRCPY(pszInpTempString, pszInpString);
        
        pszToken = tc_strtok(pszInpTempString, pszDelim);

        iLen = (int)tc_strlen(pszToken);                 
       
        while(pszToken!= NULL)
        {                              
            LMI_UPDATE_STRING_ARRAY(iTokensCnt, ppszOutList, pszToken);        
                        
            pszToken = tc_strtok(NULL, pszDelim);
        }

        if (iTokensCnt > 0)
        {
            LMI_vector_store_string_array(iTokensCnt, ppszOutList, false, vOutStrings);
        }
    }
 
    LMI_MEM_TCFREE(pszToken);
    LMI_MEM_TCFREE(pszInpTempString);
    LMI_MEM_TCFREE(ppszOutList);

    return iRetCode;
}

/**
 * This function copy the content of input "LMIPropValue" vector "vtSrcPropValues" to destination vector of "LMIPropValue" "refvtDestPropValues".
 *
 * @param[in]  vtSrcPropValues     Source vector containing list of tags
 * @param[out] refvtDestPropValues Destination vector containing unique list of tags
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI_stl_copy_propvalue_to_vector
(
    vector<LMIPropValue>  vtSrcPropValues,
    vector<LMIPropValue>& refvtDestPropValues
)
{
    int iRetCode = ITK_ok;
    vector<LMIPropValue>::iterator iterSrcProp;

    for(iterSrcProp = vtSrcPropValues.begin(); iterSrcProp !=  vtSrcPropValues.end(); iterSrcProp++)
    {
        refvtDestPropValues.push_back(*iterSrcProp);   
    }

    return iRetCode;
}

/*
 * This function removes empty strings from given vector of strings.
 *
 * @param[in]  refvStrList   Vector of strings.
 *
 * @retval This function always retutn ITK_ok.
 */
int LMI_vector_remove_empty_string
(
    vector<string>& refvStrList
)
{
    int iRetCode = ITK_ok;

    for (int iDx = 0; iDx < refvStrList.size(); iDx++)
    {
        if (strcmp(refvStrList[iDx].c_str(), EMPTY_STRING) == 0)
        {
            refvStrList.erase(refvStrList.begin() + iDx);

            iDx = iDx - 1;
        }
    }

    return iRetCode;
}

/*
 * This function takes a string vector as input parameter and returns a list of strings.
 *
 * @param[in]   vStrValues   Vector of strings.
 * @param[out]  piCount      Count of values in list.
 * @param[out]  pppszValues  List of string values.
 *
 * @retval This function always retutn ITK_ok.
 */
int LMI_str_vector_to_char_dbl_ptr
(
    const vector<string>& vStrValues,
    int*    piCount,
    char*** pppszValues
)
{
    int iRetCode = ITK_ok;

    /* Initializing out parameter of this function. */
    (*piCount)     = 0;  
    (*pppszValues) = NULL;
                            
    vector<string>::const_iterator iterStr;

    for (iterStr = vStrValues.begin(); iterStr < vStrValues.end(); iterStr++)
    {
        LMI_UPDATE_STRING_ARRAY((*piCount), (*pppszValues), (*iterStr).c_str()); 
    }

    return iRetCode;
}

/*
 * This function takes a vector of logical values as input parameter and returns a list of logical values.
 *
 * @param[in]   vbValues   Vector of logical values.
 * @param[out]  piCount    Count of values in list.
 * @param[out]  ppbValues  List of logical values.
 *
 * @retval This function always retutn ITK_ok.
 */
int LMI_bool_vector_to_bool_ptr
(
    const vector<logical>& vbValues,
    int*      piCount,
    logical** ppbValues
)
{
    int iRetCode = ITK_ok;

    /* Initializing out parameter of this function. */
    (*piCount)   = 0;  
    (*ppbValues) = NULL;
                            
    vector<logical>::const_iterator iterLog;

    for (iterLog = vbValues.begin(); iterLog < vbValues.end(); iterLog++)
    {
        LMI_UPDATE_LOGICAL_ARRAY((*piCount), (*ppbValues), (*iterLog)); 
    }

    return iRetCode;
}

/*
 * This function takes a vector of integers as input parameter and returns a list of integer values.
 *
 * @param[in]   viValues   Vector of integer values.
 * @param[out]  piCount    Count of values in list.
 * @param[out]  ppiValues  List of integer values.
 *
 * @retval This function always retutn ITK_ok.
 */
int LMI_int_vector_to_int_ptr
(
    const vector<int>& viValues,
    int*  piCount,
    int** ppiValues
)
{
    int iRetCode = ITK_ok;

    /* Initializing out parameter of this function. */
    (*piCount)   = 0;  
    (*ppiValues) = NULL;
                            
    vector<int>::const_iterator iterInt;

    for (iterInt = viValues.begin(); iterInt < viValues.end(); iterInt++)
    {
        LMI_UPDATE_INT_ARRAY((*piCount), (*ppiValues), (*iterInt)); 
    }

    return iRetCode;
}

/*
 * This function takes a vector of double values as input parameter and returns a list of double values.
 *
 * @param[in]   vdValues   Vector of double values.
 * @param[out]  piCount    Count of values in list.
 * @param[out]  ppdValues  List of double values.
 *
 * @retval This function always retutn ITK_ok.
 */
int LMI_double_vector_to_double_ptr
(
    const vector<double>& vdValues,
    int*     piCount,
    double** ppdValues
)
{
    int iRetCode = ITK_ok;

    /* Initializing out parameter of this function. */
    (*piCount)   = 0;  
    (*ppdValues) = NULL;
                            
    vector<double>::const_iterator iterDoubleVal;

    for (iterDoubleVal = vdValues.begin(); iterDoubleVal < vdValues.end(); iterDoubleVal++)
    {
        LMI_UPDATE_DOUBLE_ARRAY((*piCount), (*ppdValues), (*iterDoubleVal)); 
    }

    return iRetCode;
}

/*
 * This function takes a vector of date values as input parameter and returns a list of date values.
 *
 * @param[in]   vdateValues   Vector of date values.
 * @param[out]  piCount       Count of values in list.
 * @param[out]  ppdateValues  List of date values.
 *
 * @retval This function always retutn ITK_ok.
 */
int LMI_date_vector_to_date_ptr
(
    const vector<date_t>& vdateValues,
    int*     piCount,
    date_t** ppdateValues
)
{
    int iRetCode = ITK_ok;

    /* Initializing out parameter of this function. */
    (*piCount)   = 0;  
    (*ppdateValues) = NULL;
                            
    vector<date_t>::const_iterator iterDate;

    for (iterDate = vdateValues.begin(); iterDate < vdateValues.end(); iterDate++)
    {
        LMI_UPDATE_DATE_ARRAY((*piCount), (*ppdateValues), (*iterDate)); 
    }

    return iRetCode;
}

/*
 * This function takes a vector of tags as input parameter and returns a list of tags.
 *
 * @param[in]   vtags   Vector of tags.
 * @param[out]  piCount Count of values in list.
 * @param[out]  pptags  List of tags.
 *
 * @retval This function always retutn ITK_ok.
 */
int LMI_tag_vector_to_tag_ptr
(
    const vector<tag_t>& vtags,
    int*    piCount,
    tag_t** pptags
)
{
    int iRetCode = ITK_ok;

    /* Initializing out parameter of this function. */
    (*piCount)   = 0;  
    (*pptags) = NULL;
                            
    vector<tag_t>::const_iterator iterTag;

    for (iterTag = vtags.begin(); iterTag < vtags.end(); iterTag++)
    {
        LMI_UPDATE_TAG_ARRAY((*piCount), (*pptags), (*iterTag)); 
    }

    return iRetCode;
}


