/*======================================================================================================================================================================
                                                    Copyright 2019  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_process_oed_object.cxx

    Description: This file contains function that is used .

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
15-Sep-19     Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#include <lmi_process_oed_object.hxx>
#include <lmi_library.hxx>
#include <lmi_process_oed_string.hxx>

/**
 * This function will process the OED string "objOEDString" and will retrieve actual final object tags using starting input object "tObject". Function will then find the value of property 
 * "strOEDProperty" for all final tags.
 *
 * @param[in]  tObject        Object tag using which OED string evaluation will be processed.
 * @param[in]  objOEDString   Object storing OED string information. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                            not change the value being referenced.)
 * @param[in]  vstrNoteTypes  List of Note types.
 * @param[out] refobjSrcInfo  Resulted object that hold traversed tags and their values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIProcessOEDObject::LMI_process_oed_obj
(
    tag_t tObject,
    const LMIOEDString& objOEDString,
    const vector<string>& vstrNoteTypes,
    LMIOEDObjectInfo& refobjOEDInfo
)
{
    int   iRetCode        = ITK_ok;
    bool  bIsPropTypeNote = false;
    PROP_value_type_t objPropValType;
    vector<LMIPropValue> vPropVals;
    LMIProcessOEDString objProcessOEDStr;

    objPropValType = PROP_untyped;
    refobjOEDInfo.strPropName.assign(objOEDString.strOEDProperty);

    /* Get the final tags */
    LMI_ITKCALL(iRetCode, objProcessOEDStr.LMI_retrieve_obj_info_from_oed(tObject, objOEDString.vobjOEDSubSegments, vstrNoteTypes, bIsPropTypeNote, refobjOEDInfo));
    
    /* Get property value and property type(array or single) */
    LMI_ITKCALL(iRetCode, objProcessOEDStr.LMI_process_oed_obj_value(bIsPropTypeNote, refobjOEDInfo));

    return iRetCode;
}


