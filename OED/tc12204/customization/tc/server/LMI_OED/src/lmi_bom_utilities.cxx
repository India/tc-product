/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: lmi_bom_utilities.cxx

    Description: This File contains functions helpful in bom traversal. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
01-Dec-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>

/**
 * This function retrieves the tag of the BOM view type attached to input Item revision depending upon the input BOM view type.
 *   
 * @param[in]  tItemRev         Tag of Item Revision
 * @param[in]  pszInputViewType Type of BOMView
 * @param[out] ptBomView        Tag of the required BOMView
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI_bom_get_bv_of_input_type
(
    tag_t        tItemRev,
    const char*  pszViewType,
    tag_t*       ptBOMView
)
{
    int    iRetCode = ITK_ok;
    int    iBVCount = 0;
    tag_t  tItem    = NULLTAG;
    tag_t* ptItemBV = NULL;

    LMI_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));
    
    LMI_ITKCALL(iRetCode, ITEM_list_bom_views(tItem, &iBVCount, &ptItemBV));
   
    /* Cheching the BOM views attached to the ItemRev. */
    for(int iDx = 0; iDx < iBVCount && iRetCode == ITK_ok; iDx++)
    {
        tag_t tType       = NULLTAG;
        char* pszTypeName = NULL;
        
        LMI_ITKCALL(iRetCode, PS_ask_bom_view_type(ptItemBV[iDx], &tType));
        
        LMI_ITKCALL(iRetCode, PS_ask_view_type_name(tType, &pszTypeName));

        if(tc_strcmp(pszTypeName, pszViewType) == 0)
        {
            (*ptBOMView) = ptItemBV[iDx];
            break;
        }

        LMI_MEM_TCFREE(pszTypeName);
    }

    LMI_MEM_TCFREE(ptItemBV);

    return iRetCode;
}

/**
 * Applies a given config rule to the given BOM window
 *
 * @param[in] tBomWindow Tag of BOMview to apply the rule to
 * @param[in] pszRule    Name of the config rule to be applied
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_bom_apply_config_rule
(
    tag_t       tBomWindow,
    const char* pszRule
)
{    
    int   iRetCode = ITK_ok;
    tag_t tRevRule = NULLTAG;

    /* Find rule tag with CFM_find */
    LMI_ITKCALL(iRetCode, CFM_find(pszRule, &tRevRule));

    /* Set the config rule with BOM_set_window_config_rule */
    LMI_ITKCALL(iRetCode, BOM_set_window_config_rule(tBomWindow, tRevRule));
    
    return iRetCode;
}

/**
 * Given the bom view type, gets the tag associated with it.
 *
 * @param[in]  pszBOMViewType BOM View Type
 * @param[out] ptViewTypeTag  Tag of the bom view type
 *
 * @retval BOM_VIEW_TYPE_NOT_DEFINED    If bom view type not found
 * @retval ITK_ok   If successfull in getting bom view tag else ITK error code.
 */
int LMI_bom_find_view_type_tag
(
    const char*  pszBOMViewType,
    tag_t*       ptViewTypeTag
)
{
    int iRetCode = ITK_ok;

    LMI_ITKCALL(iRetCode, PS_find_view_type(pszBOMViewType, ptViewTypeTag));

    if((*ptViewTypeTag) == NULLTAG)
    {
        iRetCode = BOM_VIEW_TYPE_NOT_DEFINED;
        /*  Store error on error stack   */
        EMH_store_error(EMH_severity_error,iRetCode);
    }

    return iRetCode;
}

/**
 * Given the item, item revision and BOM view type returns the tag for the BOM view connected to item and the BOM view revision tag connected to item revision.
 * 
 * @param[in]  tItemRev    Tag of item revision
 * @param[in]  pszViewType BOM view type which needs to searched/created
 * @param[out] ptBVR       Tag of BOM View Revision connected to Item Revision.
 *
 * @retval  BOM_VIEW_TYPE_NOT_DEFINED   If BOM view type not found
 * @retval ITK_ok   If successful in getting BOM view tag else ITK error code.
 */
int LMI_bom_get_bvr
(
   tag_t       tItemRev,
   const char* pszViewType,
   tag_t*      ptBVR
)
{
    int    iRetCode     = ITK_ok;
    int    iBVRCnt      = 0;
    tag_t  tViewType    = NULLTAG;
    tag_t* ptBOMViewRev = NULL;

    /* Initializing the Out Parameter of this function. */
    (*ptBVR) = NULLTAG;

    LMI_ITKCALL(iRetCode, LMI_bom_find_view_type_tag(pszViewType, &tViewType));

    /* Get all BOM View Revisions connected to Item Revision */
    LMI_ITKCALL(iRetCode, ITEM_rev_list_bom_view_revs(tItemRev, &iBVRCnt, &ptBOMViewRev));

    for(int iDx = 0; iDx < iBVRCnt && iRetCode == ITK_ok; iDx++)
    {
        tag_t tBOMView = NULLTAG;
        tag_t tType    = NULLTAG;
        
        /* Get the BOM view from the BVR. There are no direct API to get the type tag of BVR so we get the BV and then get the type tag of BV.
           BV and BVR will have same type tag. 
        */
        LMI_ITKCALL(iRetCode, PS_ask_bom_view_of_bvr(ptBOMViewRev[iDx], &tBOMView));

        LMI_ITKCALL(iRetCode, PS_ask_bom_view_type(tBOMView, &tType));

        if(tType == tViewType)
        {
            (*ptBVR) = ptBOMViewRev[iDx];
            break;
        }
    }

    LMI_MEM_TCFREE(ptBOMViewRev);

    return iRetCode;
}

/**
 * This function creates a new BOM window and applies input Revision Rule to the window. All Items in the BOM will automatically be reconfigured using the new
 * Rule. Function also sets the top line of the BOM window to contain the input Item Revision.
 *
 * @note The function uses error protect mark to protects the current state of the error store. BOM commands clears the error stack which result in loss of
 *       previous errors. In order to avoid loss of error function uses protect mark commands.
 *
 * @param[in]  tItemRev        Tag of input Item revision.
 * @param[in]  pszRevisionRule Revision rule used to configure the
 * @param[in]  pszViewType     Type of "BOM View"
 * @param[out] ptBOMWindow     Tag of the newly created window.
 * @param[out] ptTopLine       Tag of the top line of the BOM window to contain the input Item Revision
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI_bomline_get_top_line
(
    tag_t         tItemRev,
    const char*   pszRevRule,
    const char*   pszViewType,
    tag_t*        ptBOMWindow,
    tag_t*        ptTopLine
)
{
    int   iRetCode     = ITK_ok;
    int   iMark        = 0;
    tag_t tBOMViewType = NULLTAG;

    /* Set error protect mark to protects the current state of the error store. BOM commands clears the error stack which result in loss of previous errors.
       In order to avoid loss of error use protect mark commands.
    */
    EMH_set_protect_mark(&iMark);

    LMI_ITKCALL(iRetCode, LMI_bom_get_bv_of_input_type(tItemRev, pszViewType, &tBOMViewType));
    
    if(tBOMViewType != NULLTAG)
    {
        LMI_ITKCALL(iRetCode, BOM_create_window(ptBOMWindow));

        LMI_ITKCALL(iRetCode, LMI_bom_apply_config_rule(*ptBOMWindow, pszRevRule));
        
        LMI_ITKCALL(iRetCode, BOM_set_window_top_line(*ptBOMWindow, NULLTAG, tItemRev, tBOMViewType, ptTopLine));
       
        /* Do a refresh of the BOM window. The content of the structure might have been changed by a different process. */
        LMI_ITKCALL(iRetCode, BOM_refresh_window(*ptBOMWindow));
    }
    else
    {
        char* pszObjStr = NULL;

        LMI_ITKCALL(iRetCode, AOM_ask_value_string(tItemRev, LMI_ATTR_OBJECT_STRING, &pszObjStr));

        TC_write_syslog("View of type %s is not attached to the %s.\n", pszViewType, pszObjStr);

        LMI_MEM_TCFREE(pszObjStr);
    }

    EMH_clear_protect_mark(iMark);

    return iRetCode;
}

int LMI_get_item_rev_for_bom_line
(
    tag_t  tBOMLine,
    tag_t* tItemRev
)
{
    int iRetCode = ITK_ok;

    /*    Initialiing Output argument */
    (*tItemRev) = NULLTAG;

    LMI_ITKCALL(iRetCode, AOM_ask_value_tag(tBOMLine, LMI_ATTR_BL_REVISION, tItemRev));

    return iRetCode;
}