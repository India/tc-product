/*======================================================================================================================================================================
                                                    Copyright 2019  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: prop_propagation_func_ext.cxx

    Description:  This File contains function to propagate values.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-July-19    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>
#include <lmi_oed_object_info.hxx>
#include <lmi_prop_propagation_func_ext.hxx>

/**
 * This function sets the attribute value for input TC objects.
 *
 * @param[in]  objSrcPropVal Property values
 * @param[in]  objValueType  Property value type
 * @param[out] bValueExist   Boolean value to indicate whether given type property value exist in "objSrcPropVal" or not.
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI_src_value_exist
(
    const LMIPropValue& objSrcPropVal,
    const PROP_value_type_t& objValueType,
    logical& bValueExist
)
{
    int iRetCode = ITK_ok;

    /*out variable of function*/
    bValueExist = false;

    switch (objValueType)
    {
        case PROP_string :
        case PROP_note   :
            {
                if (objSrcPropVal.vStrValue.size() > 0)
                {
                    bValueExist = true;
                }
                break;
            }
        case PROP_logical :
            {
                if (objSrcPropVal.vLogicalValue.size() > 0)
                {
                    bValueExist = true;
                }
                break;
            }
        case PROP_int   :
        case PROP_short :
            {
                if (objSrcPropVal.vIntValue.size() > 0)
                {
                    bValueExist = true;
                }
                break;
            }
        case PROP_float  :
        case PROP_double :
            {
                if (objSrcPropVal.vDoubleValue.size() > 0)
                {
                    bValueExist = true;
                }
                break;
            }
        case PROP_date:
            {
                if (objSrcPropVal.vDateValue.size() > 0)
                {
                    bValueExist = true;
                }
                break;
            }
        case PROP_typed_reference    :
        case PROP_untyped_reference  :
        case PROP_external_reference :
        case PROP_typed_relation     :
        case PROP_untyped_relation   :
            {
                if (objSrcPropVal.vTagValue.size() > 0)
                {
                    bValueExist = true;
                }
                break;
            }
    }

    return iRetCode;
}

/**
 * This function sets the attribute value for input TC objects.
 *
 * @param[in] objDestInfo Object containing target objects and attribute info
 * @param[in] vtObjects   Vector of object tags whose attribute value needs to be set
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI_propagate_attribute_info
(
    const LMIOEDObjectInfo& objDestInfo,
    const LMIPropValue& objPropValues
)
{
    int iRetCode = ITK_ok;

    vector<LMIObjAttrValInfo>::const_iterator iterAttrValInfo;

    for(iterAttrValInfo = objDestInfo.vLMIAttrValInfos.begin(); iterAttrValInfo < objDestInfo.vLMIAttrValInfos.end() && iRetCode == ITK_ok; iterAttrValInfo++)
    {
        switch (objDestInfo.objPropValType)
        {
            case PROP_string :
            case PROP_note   :
                {
                    /*  Lock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, true));

                    if (objDestInfo.bPropIsArray == true)
                    {
                        int    iCount     = 0;
                        char** ppszValues = NULL;
                            
                        LMI_ITKCALL(iRetCode, LMI_str_vector_to_char_dbl_ptr(objPropValues.vStrValue, &iCount, &ppszValues));

                        LMI_ITKCALL(iRetCode, AOM_set_value_strings(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), iCount, ppszValues));

                        LMI_MEM_TCFREE(ppszValues);
                    }
                    else
                    {
                        LMI_ITKCALL(iRetCode, AOM_set_value_string(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), objPropValues.vStrValue[0].c_str()));
                    }

                    /*  Save the object.  */
                    LMI_ITKCALL(iRetCode, AOM_save(iterAttrValInfo->tObject));

                    /*  Unlock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, false));

                    break;
                }
            case PROP_logical :
                {
                    /*  Lock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, true));

                    if (objDestInfo.bPropIsArray == true)
                    {
                        int      iCount   = 0;
                        logical* pbValues = NULL;
                            
                        LMI_ITKCALL(iRetCode, LMI_bool_vector_to_bool_ptr(objPropValues.vLogicalValue, &iCount, &pbValues));

                        LMI_ITKCALL(iRetCode, AOM_set_value_logicals(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), iCount, pbValues));

                        LMI_MEM_TCFREE(pbValues);
                    }
                    else
                    {
                        LMI_ITKCALL(iRetCode, AOM_set_value_logical(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), objPropValues.vLogicalValue[0]));   
                    }

                    /*  Save the object.  */
                    LMI_ITKCALL(iRetCode, AOM_save(iterAttrValInfo->tObject));

                    /*  Unlock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, false));

                    break;
                }
            case PROP_int   :
            case PROP_short :
                {
                    /*  Lock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, true));

                    if (objDestInfo.bPropIsArray == true)
                    {
                        int  iCount   = 0;
                        int* piValues = NULL;
                            
                        LMI_ITKCALL(iRetCode, LMI_int_vector_to_int_ptr(objPropValues.vIntValue, &iCount, &piValues));

                        LMI_ITKCALL(iRetCode, AOM_set_value_ints(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), iCount, piValues));

                        LMI_MEM_TCFREE(piValues);
                    }
                    else
                    {
                        LMI_ITKCALL(iRetCode, AOM_set_value_int(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), objPropValues.vIntValue[0]));     
                    }

                    /*  Save the object.  */
                    LMI_ITKCALL(iRetCode, AOM_save(iterAttrValInfo->tObject));

                    /*  Unlock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, false));

                    break;
                }
            case PROP_float  :
            case PROP_double :
                {
                    /*  Lock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, true));

                    if (objDestInfo.bPropIsArray == true)
                    {
                        int     iCount   = 0;
                        double* pdValues = NULL;
                            
                        LMI_ITKCALL(iRetCode, LMI_double_vector_to_double_ptr(objPropValues.vDoubleValue, &iCount, &pdValues));

                        LMI_ITKCALL(iRetCode, AOM_set_value_doubles(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), iCount, pdValues));

                        LMI_MEM_TCFREE(pdValues);
                    }
                    else
                    {
                        LMI_ITKCALL(iRetCode, AOM_set_value_double(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), objPropValues.vDoubleValue[0]));   
                    }

                    /*  Save the object.  */
                    LMI_ITKCALL(iRetCode, AOM_save(iterAttrValInfo->tObject));

                    /*  Unlock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, false));

                    break;
                }
            case PROP_date :
                {
                    /*  Lock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, true));

                    if (objDestInfo.bPropIsArray == true)
                    {
                        int     iCount      = 0;
                        date_t* pdateValues = NULL;
                            
                        LMI_ITKCALL(iRetCode, LMI_date_vector_to_date_ptr(objPropValues.vDateValue, &iCount, &pdateValues));

                        LMI_ITKCALL(iRetCode, AOM_set_value_dates(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), iCount, pdateValues));

                        LMI_MEM_TCFREE(pdateValues);
                    }
                    else
                    {
                        LMI_ITKCALL(iRetCode, AOM_set_value_date(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), objPropValues.vDateValue[0])); 
                    }

                    /*  Save the object.  */
                    LMI_ITKCALL(iRetCode, AOM_save(iterAttrValInfo->tObject));

                    /*  Unlock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, false));

                    break;
                }
            case PROP_typed_reference    :
            case PROP_untyped_reference  :
            case PROP_external_reference :
            case PROP_typed_relation     :
            case PROP_untyped_relation   :   
                {
                    /*  Lock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, true));

                    if (objDestInfo.bPropIsArray == true)
                    {
                        int    iCount   = 0;
                        tag_t* ptValues = NULL;
                            
                        LMI_ITKCALL(iRetCode, LMI_tag_vector_to_tag_ptr(objPropValues.vTagValue, &iCount, &ptValues));

                        LMI_ITKCALL(iRetCode, AOM_set_value_tags(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), iCount, ptValues));

                        LMI_MEM_TCFREE(ptValues);
                    }
                    else
                    {
                        LMI_ITKCALL(iRetCode, AOM_set_value_tag(iterAttrValInfo->tObject, objDestInfo.strPropName.c_str(), objPropValues.vTagValue[0]));   
                    }

                    /*  Save the object.  */
                    LMI_ITKCALL(iRetCode, AOM_save(iterAttrValInfo->tObject));

                    /*  Unlock the object.  */
                    LMI_ITKCALL(iRetCode, AOM_refresh(iterAttrValInfo->tObject, false));

                    break;
                }
        }
    }

    return iRetCode;
}


