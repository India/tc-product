/*======================================================================================================================================================================
                                                     Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_process_op_wfl.cxx

    Description: This file contains function that is used to process WFL prefix.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
09-DEC-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#include <lmi_oed_operation_ext.hxx>
#include <lmi_library.hxx>

/*
 * This function finds objects of specified type that are attached to given workflow root task and then process depth.
 *
 * @param[in]  tObject         Target object tag.
 * @param[in]  objPrefValPart  OED string sub segment. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                               not change the value being referenced.)
 * @param[out] refvTags        Vector of tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_op_wfl::LMI_process_prefix_wfl
(
    tag_t tObject,
    const LMIOEDSubSegment& objOEDSubSegment,
    vector<tag_t>& refvTags
)
{
    int    iRetCode          = ITK_ok;
    int    iAttachmentType   = 0;
    int    iAttachmentCount  = 0;
    tag_t* ptAttachmentTags  = NULL;
    LMI_process_OED_depth objProcessDepth;

    /* Set attachment type */
    if (tc_strcasecmp(objOEDSubSegment.strRefRelName.c_str(), LMI_WFL_TARGET) == 0)
    {
        iAttachmentType = EPM_target_attachment;
    }
    else if(tc_strcasecmp(objOEDSubSegment.strRefRelName.c_str(), LMI_WFL_REFERENCE) == 0)
    {
        iAttachmentType = EPM_reference_attachment;
    }

    /* Find objects either from Target or Reference attachments based on "iAttachmentType" */
    LMI_ITKCALL(iRetCode, EPM_ask_attachments(tObject, iAttachmentType, &iAttachmentCount, &ptAttachmentTags));

    if (iAttachmentCount > 0 && iRetCode == ITK_ok)
    {
        int    iReleventObjCount = 0;
        tag_t* ptReleventTags    = NULL;

        /* Filter attachment tags based on specific object type. */
        LMI_ITKCALL(iRetCode, LMI_obj_get_objects_of_input_type(iAttachmentCount, ptAttachmentTags, objOEDSubSegment.strObjectType.c_str(), &iReleventObjCount, &ptReleventTags));

        if (iReleventObjCount > 0)
        {
            /* Process depth */
            LMI_ITKCALL(iRetCode, objProcessDepth.LMI_process_depth(objOEDSubSegment.strDepth,iReleventObjCount, ptReleventTags, refvTags));
        }

        LMI_MEM_TCFREE(ptReleventTags);
    }

    LMI_MEM_TCFREE(ptAttachmentTags);
    return iRetCode;
}


