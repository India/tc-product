/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_rule_handlers.cxx

    Description: This file contains function that registers all Rule Handlers for LMI_OED_Test dll.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>
#include <lmi_rule_handlers.hxx>

/**
 * Registers the rule handlers for workflows.
 * 
 * @return Status of registration of rule handlers.
 */
int LMI_register_rule_handlers
(
    void
)
{
    int iRetCode = ITK_ok;
    
    TC_write_syslog("In LMI OED Test Register Rule Handlers Operation\n");

    return iRetCode;    
}


