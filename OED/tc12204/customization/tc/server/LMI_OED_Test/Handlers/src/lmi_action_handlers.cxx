/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_action_handlers.cxx

    Description: This file contains function that registers all Action Handlers for KM4 Custom DLL

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_action_handlers.hxx>
#include <lmi_library.hxx>
#include <lmi_oed_test.hxx>


/**
 * Functon to register the action handlers to be used in the workflows.
 *
 * @return Status of registration of action handlers. 
 */
int LMI_register_action_handlers
(
    void
)
{
    int iRetCode = ITK_ok;
    
    TC_write_syslog("In LMI OED Test Library Register Action Handlers Operation \n");

    LMI_ITKCALL( iRetCode, EPM_register_action_handler(LMI_TEST_OED, "", LMI_test_oed));

    return iRetCode;
}   



