/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: lmi_com_part.hxx

    Description:  Header File for lmi_com_part.cxx.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#ifndef LMI_COM_PART_HXX
#define LMI_COM_PART_HXX

#include <epm/epm.h>
#include <unidefs.h>
#include <lmi_oed_info.hxx>

int LMI_test_oed
(
    EPM_action_message_t msg
);

int LMI_validate_operation
(
    char* pszOperation
);

int LMI_process_test_oed
( 
    tag_t tObject,
    char* pszPrefPostfix,
    char* pszOEDPref, 
    int   iOEDStrCount, 
    char** ppszOEDStrings,
    char* pszOperation,
    char* pszOEDInPairs
);

int LMI_process_test_oed2
( 
    int    iCount,
    tag_t* ptObjects,
    char*  pszOEDPref, 
    int    iOEDStrCount, 
    char** ppszOEDStrings
);

int LMI_process_test_oed3
( 
    int    iCount,
    tag_t* ptObjects,
    char*  pszOEDPref, 
    int    iOEDStrCount, 
    char** ppszOEDStrings
);

int LMI_process_test_oed4
( 
    int    iCount,
    tag_t* ptObjects,
    char*  pszOEDPref, 
    int    iOEDStrCount, 
    char** ppszOEDStrings
);

int LMI_src_dest_bom_close_windows(LMIOEDInfo objOEDInfo);

int LMI_BOM_close_windows
(
    const LMIOEDObjectInfo& refOEDObjInfo 
);

int LMI_info_bom_close_windows(const vector<LMIOEDObjectInfo>& vobjInfos);

#endif