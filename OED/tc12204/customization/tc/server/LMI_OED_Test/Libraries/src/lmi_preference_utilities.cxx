/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: lmi_preference_utilities.cxx

    Description: This File contains functions for Retrieving and Manipulating Preference. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>

/**
 * Function to read Teamcenter preference with string value
 *
 * @param[in]  pszPrefName   Preference Name
 * @param[out] ppszPrefValue String value of preference
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI_pref_get_string_value1
(
	const char* pszPrefName,
	char**      ppszPrefValue
)
{
	int iRetCode = ITK_ok;

	LMI_ITKCALL(iRetCode, PREF_initialize());

	LMI_ITKCALL(iRetCode, PREF_ask_char_value(pszPrefName, 0, ppszPrefValue));

	return iRetCode;
}

/**
 * Function to read Teamcenter preference with string value
 *
 * @param[in]  pszPrefName   Preference Name
 * @param[in]  eScope        Search scope
 * @param[out] ppszPrefValue String value of preference
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI_pref_get_string_value2
(
	const char*                  pszPrefName,
	TC_preference_search_scope_e eScope,
	char**                       ppszPrefValue
)
{
	int iRetCode = ITK_ok;

	LMI_ITKCALL(iRetCode, PREF_initialize());

	LMI_ITKCALL(iRetCode, PREF_ask_char_value_at_location(pszPrefName, eScope, 0, ppszPrefValue));

	return iRetCode;
}

/**
 * Function to read Teamcenter preference with Multiple string value
 *
 * @param[in]  pszPrefName   Preference Name
 * @param[out] piPrefValCnt  Total number of preference values
 * @param[out] pppszPrefVals String array of preference values
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI_pref_get_string_values1
(
	const char* pszPrefName,
	int*        piPrefValCnt,
	char***     pppszPrefVals
)
{
	int iRetCode = ITK_ok;

	LMI_ITKCALL(iRetCode, PREF_initialize());

	LMI_ITKCALL(iRetCode, PREF_ask_char_values(pszPrefName, piPrefValCnt, pppszPrefVals));

	return iRetCode;
}

/**
 * Function to read Teamcenter preference with Multiple string value
 *
 * @param[in]  pszPrefName   Preference Name
 * @param[in]  eScope        Search scope
 * @param[out] piPrefValCnt  Total number of preference values
 * @param[out] pppszPrefVals String array of preference values
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI_pref_get_string_values2
(
	const char*                  pszPrefName,
	TC_preference_search_scope_e eScope,
	int*                         piPrefValCnt,
	char***                      pppszPrefVals
)
{
	int iRetCode = ITK_ok;

	LMI_ITKCALL(iRetCode, PREF_initialize());

	LMI_ITKCALL(iRetCode, PREF_ask_char_values_at_location(pszPrefName, eScope, piPrefValCnt, pppszPrefVals));

	return iRetCode;
}


