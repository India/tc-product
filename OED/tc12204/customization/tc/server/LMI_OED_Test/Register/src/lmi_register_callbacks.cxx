/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_register_callbacks.cxx

    Description: This is the entry point function for LMI OED Test DLL designed for handlers.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <lmi_register_callbacks.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Function to register Callback function. Entry point for LMI_OED_Test dll.
 *
 * Returns: Status of execution.
 */
extern DLLAPI int LMI_OED_Test_register_callbacks() 
{
    int  iRetCode   = ITK_ok;
        
    printf("INFO: Registered LMI OED Test Library Exits. \n");

    iRetCode = CUSTOM_register_exit ("LMI_OED_Test", "USER_gs_shell_init_module", (CUSTOM_EXIT_ftn_t)  LMI_register_custom_handlers);
    
    if(iRetCode != ITK_ok)
    {
        printf("ERROR: Unable to register custom handlers.");
    }
    
    return iRetCode;
}




