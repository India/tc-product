/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_oed_src_dest_info.hxx

    Description: A container class that represents processed information for one pair of oed strings.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-Nov-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_OED_SRC_DEST_INFO_HXX
#define LMI_OED_SRC_DEST_INFO_HXX

#include <lmi_oed_object_info.hxx>

class LMIOEDSrcDestInfo
{
    public:
        LMIOEDObjectInfo objSrcInfo;
        LMIOEDObjectInfo objDestInfo;        
};

#endif

