/*======================================================================================================================================================================
                                                    Copyright 2019  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_oed_object_info.hxx

    Description: A container class that hold processed oed object info.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Sep-19    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_OED_OBJECT_INFO_HXX
#define LMI_OED_OBJECT_INFO_HXX

#include <unidefs.h>
#include <property\propdesc.h>
#include <lmi_prop_value.hxx>
#include <lmi_bom_info.hxx>
#include <lmi_obj_attr_val_info.hxx>

class LMIOEDObjectInfo
{
    public:

        LMIOEDObjectInfo()
        {
            bPropIsArray = false;
            bIsBLAttr    = false;
            objPropValType = PROP_untyped;
        }

        string                    strPropName;
        logical                   bPropIsArray;
        logical                   bIsBLAttr;
        PROP_value_type_t         objPropValType;
        vector<LMIBOMInfo>        vLMIBOMInfos;
        vector<LMIObjAttrValInfo> vLMIAttrValInfos;        
};

#endif

