/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_oed.hxx

    Description:  Header File for lmi_oed.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_OED_HXX
#define LMI_OED_HXX
    
#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <lmi_oed_object_info.hxx>
#include <lmi_oed_info.hxx>
#include <string.h>
#include <vector>

/**
 * This function retrieves OED final object tags, attribute name and values. Function will first retrieves OED strings from constructed preference. Preference name is generated based
 * on Object Type and input post-fix string "strOEDPrefPostfix" for e.g. If input object "tObject" is of type "PartRevision" and input post fix string argument "strOEDPrefPostfix" is
 * "_Prop_Information" then name of the preference containing OED string will be: 
 *                              "PartRevision_Prop_Information".
 * @note: 
 *       If no post-fix string will be provided as input argument to this function then function will use "_OED_Strings_Information" as default post-fix.
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 */
DLLAPI int LMI_process_oed_by_pref_postfix(tag_t tObject, string strOEDPrefPostfix, vector<LMIOEDObjectInfo>& refvObjInfos);
    
/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input object "tObject". 
 * Function retrieves OED final object tags, attribute name and values.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 *
 */
DLLAPI int LMI_process_oed_preference1(tag_t tObject, string strOEDPrefName, vector<LMIOEDObjectInfo>& refvObjInfos);

/**
 * This function will process input OED strings vector "refvstrOEDStrings" using input object tag "tObject".
 * Function retrieves OED final object tags, attribute name and values.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 *
 */
DLLAPI int LMI_process_oed_strings1(tag_t tObject, const vector<string>& refvOEDStrings, vector<LMIOEDObjectInfo>& refvObjInfos);

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input objects "vtObjects".
 * Here count of input objects and count of OED strings retrieved from prefernce will be equal. For every input object , there will be a corresponding OED string.
 * Function retrieves OED final object tags, attribute name and values.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 */
DLLAPI int LMI_process_oed_preference2(vector<tag_t> vtObjects, string strOEDPrefName, vector<LMIOEDObjectInfo>& refvObjInfos);

/**
 * This function will process input OED strings vector "refvOEDStrings" using input object tags "vtObjects". Here count of input objects and count of OED strings will be 
 * equal. For every input object , there will be a corresponding OED string.
 * Function retrieves OED final object tags, attribute name and values.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 */
DLLAPI int LMI_process_oed_strings2(vector<tag_t> vtObjects, const vector<string>& refvOEDStrings, vector<LMIOEDObjectInfo>& refvObjInfos);

/**
 * This function retrieves OED source and destination attribute name, values and object tags. Function will first retrieves OED strings from constructed preference. Preference name is generated based
 * on Object Type and input post-fix string "strOEDPrefPostfix" for e.g. If input object "tObject" is of type "PartRevision" and input post fix string argument "strOEDPrefPostfix" is
 * "_Prop_Information" then name of the preference containing OED string will be: 
 *                              "PartRevision_Prop_Information".
 * @note: 
 *       If no post-fix string will be provided as input argument to this function then function will use "_OED_Strings_Information" as default post-fix.
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 */
DLLAPI int LMI_process_src_dest_oed_by_pref_postfix(tag_t tObject, string strOEDPrefPostfix, LMIOEDInfo& refobjOEDInfo);
    
/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input object "tObject". 
 * Function retrieves OED source and destination attribute name, values and object tags.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 *
 */
DLLAPI int LMI_process_src_dest_oed_preference1(tag_t tObject, string strOEDPrefName, LMIOEDInfo& refobjOEDInfo);

/**
 * This function will process input OED strings vector "refvstrOEDStrings" using input object tag "tObject".
 * Function retrieves OED source and destination attribute name, values and object tags.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 */
DLLAPI int LMI_process_src_dest_oed_strings1(tag_t tObject, const vector<string>& refvOEDStrings, LMIOEDInfo& refobjOEDInfo);

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input objects "vtObjects".
 * Here count of input objects and count of OED strings retrieved from prefernce will be equal. For every input object , there will be a corresponding OED string.
 * Function retrieves OED source and destination attribute name, values and object tags.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 */
DLLAPI int LMI_process_src_dest_oed_preference2(vector<tag_t> vtObjects, string strOEDPrefName, LMIOEDInfo& refobjOEDInfo);

/**
 * This function will process input OED strings vector "refvOEDStrings" using input object tags "vtObjects". Here count of input objects and count of OED strings will be 
 * equal. For every input object , there will be a corresponding OED string.
 * Function retrieves OED source and destination attribute name, values and object tags.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 */
DLLAPI int LMI_process_src_dest_oed_strings2(vector<tag_t> vtObjects, const vector<string>& refvOEDStrings, LMIOEDInfo& refobjOEDInfo);

/**
 * This function process and set OED expression values. Function gets OED strings from constructed preference name which is generated based on Object Type and input post-fix string "strOEDPrefPostfix"
 * for e.g. If input object "tObject" is of  type "PartRevision" and input post fix string argument "strOEDPrefPostfix" is "_Prop_Information" then name of the preference containing OED string 
 * will be:
 *          "PartRevision_Prop_Information".
 * @note: 
 *       If no post-fix string will be provided as input argument to this function then function will use "_OED_Strings_Information" as default post-fix.
 */
DLLAPI int LMI_propagate_oed_by_pref_postfix1(tag_t tObject, string strOEDPrefPostfix);

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input object "tObject". 
 * This method propogate values from source to destination objects using OED expression.
 */
DLLAPI int LMI_propagate_oed_preference_exp_val1(tag_t tObject, string strOEDPrefName);

/**
 * This function will process input OED strings vector "refvstrOEDStrings" using input object tag "tObject".
 * This method propogate values from source to destination objects using OED expression.
 */
DLLAPI int LMI_propagate_oed_strings_exp_val1(tag_t tObject, const vector<string>& refvOEDStrings);

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input objects "vtObjects". 
 * Here count of input objects and count of OED strings retrieved from prefernce will be same and even. For every input object , there will be a corresponding OED string.
 * Input objects and retrieved OED string both will alternatively considered as Source and Destination. This method propogate values from source to destination objects 
 * using OED expression.
 */
DLLAPI int LMI_propagate_oed_preference_exp_val2(vector<tag_t> vtObjects, string strOEDPrefName);

/**
 * This function will process input OED strings vector "refvstrOEDStrings" using input object tags "vtObjects". Here count of input objects and count of OED strings will be 
 * same and even. For every input object , there will be a corresponding OED string. Input objects and OED string both will alternatively considered as Source and Destination.
 * This method propogate values from source to destination objects using OED expression.
 */
DLLAPI int LMI_propagate_oed_strings_exp_val2(vector<tag_t> vtObjects, const vector<string>& refvOEDStrings);

/**
 * This function retrieves OED final object tags, attribute name and values. Function will first retrieves OED strings from constructed preference. Preference name is generated based
 * on Object Type and input post-fix string "strOEDPrefPostfix" for e.g. If input object "tObject" is of type "PartRevision" and input post fix string argument "strOEDPrefPostfix" is
 * "_Prop_Information" then name of the preference containing OED string will be: 
 *                              "PartRevision_Prop_Information".
 * @note: 
 *       If no post-fix string will be provided as input argument to this function then function will use "_OED_Strings_Information" as default post-fix.
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 */
DLLAPI int LMI_uid_process_oed_by_pref_postfix(string strUID, string strOEDPrefPostfix, vector<LMIOEDObjectInfo>& refvObjInfos);
    
/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input object "tObject". 
 * Function retrieves OED final object tags, attribute name and values.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 *
 */
DLLAPI int LMI_uid_process_oed_preference1(string strUID, string strOEDPrefName, vector<LMIOEDObjectInfo>& refvObjInfos);

/**
 * This function will process input OED strings vector "refvstrOEDStrings" using input object tag "tObject".
 * Function retrieves OED final object tags, attribute name and values.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 *
 */
DLLAPI int LMI_uid_process_oed_strings1(string strUID, const vector<string>& refvOEDStrings, vector<LMIOEDObjectInfo>& refvObjInfos);

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input objects "vtObjects".
 * Here count of input objects and count of OED strings retrieved from prefernce will be equal. For every input object , there will be a corresponding OED string.
 * Function retrieves OED final object tags, attribute name and values.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 */
DLLAPI int LMI_uid_process_oed_preference2(vector<string> vstrUIDs, string strOEDPrefName, vector<LMIOEDObjectInfo>& refvObjInfos);

/**
 * This function will process input OED strings vector "refvOEDStrings" using input object tags "vtObjects". Here count of input objects and count of OED strings will be 
 * equal. For every input object , there will be a corresponding OED string.
 * Function retrieves OED final object tags, attribute name and values.
 * @note: 
 *       If BOM line property values are retrieved then BOM window will need to be close manually.
 */
DLLAPI int LMI_uid_process_oed_strings2(vector<string> vstrUIDs, const vector<string>& refvOEDStrings, vector<LMIOEDObjectInfo>& refvObjInfos);

#endif


