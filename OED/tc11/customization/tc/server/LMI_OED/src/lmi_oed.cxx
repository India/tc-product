/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_oed.cxx

    Description:  This file contains function those are called to process oed.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_oed.hxx>
#include <lmi_library.hxx>
#include <lmi_expand_oed_exp.hxx>
#include <lmi_oed_processing_service.hxx>

/**
 * This function retrieves OED final object tags, attribute name and values. Function will first retrieves OED strings from constructed preference. Preference name is generated based
 * on Object Type and input post-fix string "strOEDPrefPostfix" for e.g. If input object "tObject" is of type "PartRevision" and input post fix string argument "strOEDPrefPostfix" is
 * "_Prop_Information" then name of the preference containing OED string will be: 
 *                              "PartRevision_Prop_Information".
 * @note: 
 *       If no post-fix string will be provided as input argument to this function then function will use "_OED_Strings_Information" as default post-fix.
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  tObject           Object tag which will be used to expand OED strings information.
 * @param[in]  strOEDPrefPostfix Preference postfix string used to construct OED string preference.
 * @param[out] refobjOEDInfo     Vector of "LMIOEDObjectInfo" objects containing OED final object tags, attribute name and values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */ 
int LMI_process_oed_by_pref_postfix
(
    tag_t  tObject, 
    string strOEDPrefPostfix,
    vector<LMIOEDObjectInfo>& refvObjsInfo
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* Initializing out parameters */
    refvObjsInfo.clear();

    /* validating input parameters */
    if (tObject == NULLTAG)
    {
        iRetCode = PROCESS_OED_NULL_OBJECT;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Retrieve OED Strings from the preference and validates these OED strings syntactically and logically. If validation succeeds then function returns OED string in vector of 
       "LMIOEDString" objects.
    */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_construct_and_store_valid_OED_pref_vals(tObject, strOEDPrefPostfix, false, vobjOEDStrings));

    /* Expanding OED strings to get final object/s with attribute names and its corresponding values  */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_oed1(tObject, vobjOEDStrings, refvObjsInfo));
    
    return iRetCode;
}

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input object "tObject". 
 * Function retrieves OED final object tags, attribute name and values.
 *
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  tObject        Object tag which will be used to expand OED strings information.
 * @param[in]  strOEDPrefName Preference name whose values are OED strings.
 * @param[out] refobjOEDInfo  Vector of "LMIOEDObjectInfo" objects containing OED final object tags, attribute name and values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_oed_preference1
(
    tag_t  tObject, 
    string strOEDPrefName, 
    vector<LMIOEDObjectInfo>& refvObjsInfo
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* Initializing out parameters */
    refvObjsInfo.clear();

    /* validating input parameters */
    if (tObject == NULLTAG)
    {
        iRetCode = PROCESS_OED_NULL_OBJECT;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (strOEDPrefName.length() <= 0)
    {
        iRetCode = PROCESS_OED_EMPTY_OED_PREF_NAME;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Retrieve and validates OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_pref_vals(strOEDPrefName, false, 0, vobjOEDStrings));

    /* Expanding OED strings to get final object/s with attribute names and its corresponding values */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_oed1(tObject, vobjOEDStrings, refvObjsInfo));
    
    return iRetCode;
}

/**
 * This function will process input OED strings vector "refvstrOEDStrings" using input object tag "tObject".
 * Function retrieves OED final object tags, attribute name and values.
 *
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  tObject           Object tag which will be used to expand OED strings information.
 * @param[in]  refvstrOEDStrings Vector containing OED strings.
 * @param[out] refobjOEDInfo     Vector of "LMIOEDObjectInfo" objects containing OED final object tags, attribute name and values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_oed_strings1
(
    tag_t tObject, 
    const vector<string>& refvstrOEDStrings, 
    vector<LMIOEDObjectInfo>& refvObjsInfo
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* Initializing out parameters */
    refvObjsInfo.clear();

    /* validating input parameters */
    if (tObject == NULLTAG)
    {
        iRetCode = PROCESS_OED_NULL_OBJECT;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (refvstrOEDStrings.empty() == true)
    {
        iRetCode = PROCESS_OED_EMPTY_OED_STRINGS;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Validating OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_string(refvstrOEDStrings, vobjOEDStrings));

    /* Expanding OED strings to get final object/s with attribute names and its corresponding values */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_oed1(tObject, vobjOEDStrings, refvObjsInfo));
    
    return iRetCode;
}

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input objects "vtObjects".
 * Here count of input objects and count of OED strings retrieved from prefernce will be equal. For every input object , there will be a corresponding OED string. 
 * For example:
 *                  Input tag: Tag present in vtObjects at second position.
 *      Applicable OED string: OED string present at second position in vector of OED strings which is retrieved by preference "strOEDPrefName".
 *
 * Function retrieves OED final object tags, attribute name and values.
 *
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  vtObjects      Object tags which will be used to expand OED strings information.
 * @param[in]  strOEDPrefName Preference name whose values are OED strings.
 * @param[out] refvObjInfos   Vector of "LMIOEDObjectInfo" objects containing OED final object tags, attribute name and values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_oed_preference2
(
    vector<tag_t> vtObjects, 
    string strOEDPrefName, 
    vector<LMIOEDObjectInfo>& refvObjInfos
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* Initializing out parameters */
    refvObjInfos.clear();

    /* validating input parameters */
    if (vtObjects.size() == 0)
    {
        iRetCode = PROCESS_OED_EMPTY_OBJECTS;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (strOEDPrefName.length() <= 0)
    {
        iRetCode = PROCESS_OED_EMPTY_OED_PREF_NAME;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Retrieve and validates OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_pref_vals(strOEDPrefName, true, vtObjects.size(), vobjOEDStrings));

    /* Expanding OED strings to get final object/s with attribute names and its corresponding values */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_oed2(vtObjects, vobjOEDStrings, refvObjInfos));
    
    return iRetCode;
}

/**
 * This function will process input OED strings vector "refvOEDStrings" using input object tags "vtObjects". Here count of input objects and count of OED strings will be 
 * equal. For every input object , there will be a corresponding OED string.
 * For example:
 *                  Input tag: Tag present in vtObjects at second position.
 *      Applicable OED string: OED string present in refvOEDStrings at second position.
 *
 * Function retrieves OED final object tags, attribute name and values.
 *
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  vtObjects      Object tags which will be used to expand OED strings information.
 * @param[in]  refvOEDStrings Vector containing OED strings.
 * @param[out] refvObjInfos   Vector of "LMIOEDObjectInfo" objects containing OED final object tags, attribute name and values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_oed_strings2
(
    vector<tag_t> vtObjects, 
    const vector<string>& refvOEDStrings, 
    vector<LMIOEDObjectInfo>& refvObjInfos
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* Initializing out parameters */
    refvObjInfos.clear();

    /* validating input parameters */
    if (vtObjects.size() == 0)
    {
        iRetCode = PROCESS_OED_EMPTY_OBJECTS;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (refvOEDStrings.size() != vtObjects.size())
    {
        iRetCode = PROCESS_OED_STRINGS_COUNT_INVALID;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Validating OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_string(refvOEDStrings, vobjOEDStrings));

    /* Expanding OED strings to get final object/s with attribute names and its corresponding values */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_oed2(vtObjects, vobjOEDStrings, refvObjInfos));
    
    return iRetCode;
}

/**
 * This function retrieves OED source and destination attribute name, values and object tags. Function will first retrieves OED strings from constructed preference. Preference name is generated based
 * on Object Type and input post-fix string "strOEDPrefPostfix" for e.g. If input object "tObject" is of type "PartRevision" and input post fix string argument "strOEDPrefPostfix" is
 * "_Prop_Information" then name of the preference containing OED string will be: 
 *                              "PartRevision_Prop_Information".
 * @note: 
 *       If no post-fix string will be provided as input argument to this function then function will use "_OED_Strings_Information" as default post-fix.
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  tObject           Object tag which will be used to expand OED strings information.
 * @param[in]  strOEDPrefPostfix Preference postfix string used to construct OED string preference.
 * @param[out] refobjOEDInfo     "LMIOEDInfo" object containing source and destination attribute name, values and object tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */ 
int LMI_process_src_dest_oed_by_pref_postfix
(
    tag_t  tObject, 
    string strOEDPrefPostfix,
    LMIOEDInfo& refobjOEDInfo
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* Initializing out parameters */
    refobjOEDInfo.vOEDSrcDestInfo.clear();

    /* validating input parameters */
    if (tObject == NULLTAG)
    {
        iRetCode = PROCESS_OED_NULL_OBJECT;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Retrieve OED Strings from the preference and validates these OED strings syntactically and logically. If validation succeeds then function returns OED string in vector of 
       "LMIOEDString" objects.
    */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_construct_and_store_valid_OED_pref_vals(tObject, strOEDPrefPostfix, true, vobjOEDStrings));

    /* Expanding OED strings to get final object/s with attribute names and its corresponding values  */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_src_dest_oed1(tObject, vobjOEDStrings, refobjOEDInfo));
    
    return iRetCode;
}

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input object "tObject". 
 * Function retrieves OED source and destination attribute name, values and object tags.
 *
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  tObject        Object tag which will be used to expand OED strings information.
 * @param[in]  strOEDPrefName Preference name whose values are OED strings.
 * @param[out] refobjOEDInfo  "LMIOEDInfo" object containing source and destination attribute name, values and object tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_src_dest_oed_preference1
(
    tag_t  tObject, 
    string strOEDPrefName, 
    LMIOEDInfo& refobjOEDInfo
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* Initializing out parameters */
    refobjOEDInfo.vOEDSrcDestInfo.clear();

    /* validating input parameters */
    if (tObject == NULLTAG)
    {
        iRetCode = PROCESS_OED_NULL_OBJECT;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (strOEDPrefName.length() <= 0)
    {
        iRetCode = PROCESS_OED_EMPTY_OED_PREF_NAME;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Retrieve and validates OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_pref_vals(strOEDPrefName, true, 0, vobjOEDStrings));

    /* Expanding OED strings to get final object/s with attribute names and its corresponding values */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_src_dest_oed1(tObject, vobjOEDStrings, refobjOEDInfo));
    
    return iRetCode;
}

/**
 * This function will process input OED strings vector "refvstrOEDStrings" using input object tag "tObject".
 * Function retrieves OED source and destination attribute name, values and object tags.
 *
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  tObject           Object tag which will be used to expand OED strings information.
 * @param[in]  refvstrOEDStrings Vector containing OED strings.
 * @param[out] refobjOEDInfo     "LMIOEDInfo" object containing source and destination attribute name, values and object tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_src_dest_oed_strings1
(
    tag_t tObject, 
    const vector<string>& refvstrOEDStrings, 
    LMIOEDInfo& refobjOEDInfo
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* Initializing out parameters */
    refobjOEDInfo.vOEDSrcDestInfo.clear();

    /* validating input parameters */
    if (tObject == NULLTAG)
    {
        iRetCode = PROCESS_OED_NULL_OBJECT;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (refvstrOEDStrings.empty() == true)
    {
        iRetCode = PROCESS_OED_EMPTY_OED_STRINGS;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (((refvstrOEDStrings.size()) % 2) != 0)
    {
        iRetCode = PROCESS_OED_STRINGS_COUNT_INVALID;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Validating OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_string(refvstrOEDStrings, vobjOEDStrings));

    /* Expanding OED strings to get final object/s with attribute names and its corresponding values */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_src_dest_oed1(tObject, vobjOEDStrings, refobjOEDInfo));
    
    return iRetCode;
}

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input objects "vtObjects".
 * Here count of input objects and count of OED strings retrieved from prefernce will be equal. For every input object , there will be a corresponding OED string.
 * For example:
 *              Source
 *                              Input tag: Tag present in vtObjects at 0th index.
 *                  Applicable OED string: OED string present at 0th index in vector of OED strings which is retrieved by preference "strOEDPrefName".
 *              Destination
 *                              Input tag: Tag present in vtObjects at 1th index.
 *                  Applicable OED string: OED string present at 1th index in vector of OED strings which is retrieved by preference "strOEDPrefName".
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  vtObjects      Object tags which will be used to expand OED strings information.
 * @param[in]  strOEDPrefName Preference name whose values are OED strings.
 * @param[out] refobjOEDInfo  "LMIOEDInfo" object containing source and destination attribute name, values and object tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_src_dest_oed_preference2
(
    vector<tag_t> vtObjects, 
    string strOEDPrefName, 
    LMIOEDInfo& refobjOEDInfo
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* Initializing out parameters */
    refobjOEDInfo.vOEDSrcDestInfo.clear();

    /* validating input parameters */
    if (vtObjects.size() == 0)
    {
        iRetCode = PROCESS_OED_EMPTY_OBJECTS;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (strOEDPrefName.length() <= 0)
    {
        iRetCode = PROCESS_OED_EMPTY_OED_PREF_NAME;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Retrieve and validates OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_pref_vals(strOEDPrefName, true, vtObjects.size(), vobjOEDStrings));

    /* Expanding OED strings to get final object/s with attribute names and its corresponding values */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_src_dest_oed2(vtObjects, vobjOEDStrings, refobjOEDInfo));
    
    return iRetCode;
}

/**
 * This function will process input OED strings vector "refvOEDStrings" using input object tags "vtObjects". Here count of input objects and count of OED strings will be 
 * equal. For every input object , there will be a corresponding OED string.
 * For example:
 *              Source
 *                              Input tag: Tag present in vtObjects at 0th index.
 *                  Applicable OED string: OED string present at 0th index in vector refvOEDStrings.
 *              Destination
 *                              Input tag: Tag present in vtObjects at 1th index.
 *                  Applicable OED string: OED string present at 1th index in vector refvOEDStrings.
 *
 * Function retrieves OED source and destination attribute name, values and object tags.
 *
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  tObject        Object tags which will be used to expand OED strings information.
 * @param[in]  refvOEDStrings Vector containing OED strings.
 * @param[out] refobjOEDInfo  "LMIOEDInfo" object containing source and destination attribute name, values and object tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_src_dest_oed_strings2
(
    vector<tag_t> vtObjects, 
    const vector<string>& refvOEDStrings, 
    LMIOEDInfo& refobjOEDInfo
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* Initializing out parameters */
    refobjOEDInfo.vOEDSrcDestInfo.clear();

    /* validating input parameters */
    if (vtObjects.size() == 0)
    {
        iRetCode = PROCESS_OED_EMPTY_OBJECTS;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if ((vtObjects.size()%2) != 0)
    {
        iRetCode = PROCESS_OED_OBJ_COUNT_INVALID;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (vtObjects.size() != refvOEDStrings.size())
    {
        iRetCode = PROCESS_OED_STRINGS_COUNT_INVALID;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Validating OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_string(refvOEDStrings, vobjOEDStrings));

    /* Expanding OED strings to get final object/s with attribute names and its corresponding values */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_src_dest_oed2(vtObjects, vobjOEDStrings, refobjOEDInfo));
    
    return iRetCode;
}

/**
 * This function process and set OED expression values. Function gets OED strings from constructed preference name which is generated based on Object Type and input post-fix string "strOEDPrefPostfix"
 * for e.g. If input object "tObject" is of  type "PartRevision" and input post fix string argument "strOEDPrefPostfix" is "_Prop_Information" then name of the preference containing OED string 
 * will be:
 *          "PartRevision_Prop_Information".
 * @note: 
 *       If no post-fix string will be provided as input argument to this function then function will use "_OED_Strings_Information" as default post-fix.
 *       This method does not propagate in case if destination final tags are BOM line tags.
 *
 * This method propogate values from source to destination objects using OED expression.
 *
 * @param[in] tObject           Object tag which will be used to expand OED strings information.
 * @param[in] strOEDPrefPostfix Preference postfix string used to construct OED string preference.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_propagate_oed_by_pref_postfix1
(
    tag_t  tObject, 
    string strOEDPrefPostfix
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* validating input parameters */
    if (tObject == NULLTAG)
    {
        iRetCode = PROPAGATION_NULL_OBJECT;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Retrieve and validates OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects. */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_construct_and_store_valid_OED_pref_vals(tObject, strOEDPrefPostfix, true, vobjOEDStrings));

    /* Processing OED strings propogate values from source object to destination objects. */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_oed_prop_propogation(tObject, vobjOEDStrings));
    
    return iRetCode;
}

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input object "tObject". 
 * This method propogate values from source to destination objects using OED expression.
 *
 * @note: 
 *       This method does not propagate in case if destination final tags are BOM line tags.
 *
 * @param[in]  tObject        Object tag which will be used to expand OED strings information.
 * @param[in]  strOEDPrefName Preference name whose values are OED strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_propagate_oed_preference_exp_val1
(
    tag_t  tObject, 
    string strOEDPrefName
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* validating input parameters */
    if (tObject == NULLTAG)
    {
        iRetCode = PROPAGATION_NULL_OBJECT;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (strOEDPrefName.length() <= 0)
    {
        iRetCode = PROPAGATION_EMPTY_OED_PREF_NAME;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Retrieve and validates OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_pref_vals(strOEDPrefName, true, 0, vobjOEDStrings));

    /* Processing OED strings propogate values from source object to destination objects. */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_oed_prop_propogation(tObject, vobjOEDStrings));
    
    return iRetCode;
}

/**
 * This function will process input OED strings vector "refvstrOEDStrings" using input object tag "tObject".
 * This method propogate values from source to destination objects using OED expression.
 * @note: 
 *       This method does not propagate in case if destination final tags are BOM line tags.
 *
 * @param[in]  tObject           Object tag which will be used to expand OED strings information.
 * @param[in]  refvstrOEDStrings Vector containing OED strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_propagate_oed_strings_exp_val1
(
    tag_t tObject, 
    const vector<string>& refvstrOEDStrings
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* validating input parameters */
    if (tObject == NULLTAG)
    {
        iRetCode = PROPAGATION_NULL_OBJECT;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (refvstrOEDStrings.empty() == true)
    {
        iRetCode = PROPAGATION_EMPTY_OED_STRINGS;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (((refvstrOEDStrings.size()) % 2) != 0)
    {
        iRetCode = PROPAGATION_STRINGS_COUNT_INVALID;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }  
    
    /* Validating OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_string(refvstrOEDStrings, vobjOEDStrings));

    /* Processing OED strings propogate values from source object to destination objects. */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_oed_prop_propogation(tObject, vobjOEDStrings));
    
    return iRetCode;
}

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input objects "vtObjects". 
 * Here count of input objects and count of OED strings retrieved from prefernce will be same and even. For every input object , there will be a corresponding OED string.
 * Input objects and retrieved OED string both will alternatively considered as Source and Destination. 
 * For example:
 *              Source
 *                              Input tag: Tag present in vtObjects at 0th index.
 *                  Applicable OED string: OED string present at 0th index in vector of OED strings which is retrieved by preference "strOEDPrefName".
 *              Destination
 *                              Input tag: Tag present in vtObjects at 1th index.
 *                  Applicable OED string: OED string present at 1th index in vector of OED strings which is retrieved by preference "strOEDPrefName".
 *
 * This method propogate values from source to destination objects using OED expression.
 *
 * @note: 
 *       This method does not propagate in case if destination final tags are BOM line tags.
 *
 * @param[in]  vtObjects      Object tags which will be used as starting points for expanding OED strings information.
 * @param[in]  strOEDPrefName Preference name whose values are OED strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_propagate_oed_preference_exp_val2
(
    vector<tag_t> vtObjects, 
    string strOEDPrefName
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* validating input parameters */
    if (vtObjects.empty() == true)
    {
        iRetCode = PROPAGATION_EMPTY_OBJECTS;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (vtObjects.size()%2 != 0)
    {
        iRetCode = PROPAGATION_OBJ_COUNT_INVALID;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (strOEDPrefName.length() <= 0)
    {
        iRetCode = PROPAGATION_EMPTY_OED_PREF_NAME;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Retrieve and validates OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_pref_vals(strOEDPrefName, true, vtObjects.size(), vobjOEDStrings));

    /* Processing OED strings propogate values from source object to destination objects. */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_oed_prop_propogation2(vtObjects, vobjOEDStrings));
    
    return iRetCode;
}

/**
 * This function will process input OED strings vector "refvstrOEDStrings" using input object tags "vtObjects". Here count of input objects and count of OED strings will be 
 * same and even. For every input object , there will be a corresponding OED string. Input objects and OED string both will alternatively considered as Source and Destination.
 * For example:
 *              Source
 *                              Input tag: Tag present in vtObjects at 0th index.
 *                  Applicable OED string: OED string present at 0th index in vector refvOEDStrings.
 *              Destination
 *                              Input tag: Tag present in vtObjects at 1th index.
 *                  Applicable OED string: OED string present at 1th index in vector refvOEDStrings.
 *
 * This method propogate values from source to destination objects using OED expression.
 *
 * @note: 
 *       This method does not propagate in case if destination final tags are BOM line tags.
 *
 * @param[in]  vtObjects      Objects tag which will be used as starting points for expanding OED strings information.
 * @param[in]  refvOEDStrings Vector containing OED strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_propagate_oed_strings_exp_val2
(
    vector<tag_t> vtObjects, 
    const vector<string>& refvOEDStrings
)
{
    int iRetCode = ITK_ok;  
    LMIExpandOEDExp objExpandOEDExp;
    LMIOEDProcessingService objOEDProcessingService;
    vector<LMIOEDString> vobjOEDStrings;

    /* validating input parameters */
    if (vtObjects.empty() == true)
    {
        iRetCode = PROPAGATION_EMPTY_OBJECTS;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (vtObjects.size()%2 != 0)
    {
        iRetCode = PROPAGATION_OBJ_COUNT_INVALID;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    else if (vtObjects.size() != refvOEDStrings.size())
    {
        iRetCode = PROPAGATION_STRINGS_COUNT_INVALID;

        EMH_store_error(EMH_severity_error, iRetCode); 
    }
    
    /* Validating OED Strings syntactically and logically. If validation succeeds then it returns an vector of "LMIOEDString" objects.  */
    LMI_ITKCALL(iRetCode, objExpandOEDExp.LMI_store_valid_OED_string(refvOEDStrings, vobjOEDStrings));

    /* Processing OED strings propogate values from source object to destination objects. */
    LMI_ITKCALL(iRetCode, objOEDProcessingService.LMI_process_oed_prop_propogation2(vtObjects, vobjOEDStrings));
    
    return iRetCode;
}

/**
 * This function retrieves OED final object tags, attribute name and values. Function will first retrieves OED strings from constructed preference. Preference name is generated based
 * on Object Type and input post-fix string "strOEDPrefPostfix" for e.g. If input object with UID "strUID" is of type "PartRevision" and input post fix string argument "strOEDPrefPostfix" 
 * is "_Prop_Information" then name of the preference containing OED string will be: 
 *                              "PartRevision_Prop_Information".
 * @note: 
 *       If no post-fix string will be provided as input argument to this function then function will use "_OED_Strings_Information" as default post-fix.
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  strUID            UID of Object tag which will be used to expand OED strings information.
 * @param[in]  strOEDPrefPostfix Preference postfix string used to construct OED string preference.
 * @param[out] refobjOEDInfo     Vector of "LMIOEDObjectInfo" objects containing OED final object tags, attribute name and values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */ 
int LMI_uid_process_oed_by_pref_postfix
(
    string strUID, 
    string strOEDPrefPostfix,
    vector<LMIOEDObjectInfo>& refvObjsInfo
)
{
    int   iRetCode = ITK_ok;  
    tag_t tObject  = NULLTAG; 
    
    LMI_ITKCALL(iRetCode, POM_string_to_tag(strUID.c_str(), &tObject));

    LMI_ITKCALL(iRetCode, LMI_process_oed_by_pref_postfix(tObject, strOEDPrefPostfix, refvObjsInfo));

    return iRetCode;
}

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input object with UID "strUID". 
 * Function retrieves OED final object tags, attribute name and values.
 *
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  strUID         UID of Object tag which will be used to expand OED strings information.
 * @param[in]  strOEDPrefName Preference name whose values are OED strings.
 * @param[out] refobjOEDInfo  Vector of "LMIOEDObjectInfo" objects containing OED final object tags, attribute name and values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_uid_process_oed_preference1
(
    string strUID, 
    string strOEDPrefName, 
    vector<LMIOEDObjectInfo>& refvObjsInfo
)
{
    int   iRetCode = ITK_ok;  
    tag_t tObject  = NULLTAG; 
    
    LMI_ITKCALL(iRetCode, POM_string_to_tag(strUID.c_str(), &tObject));

    LMI_ITKCALL(iRetCode, LMI_process_oed_preference1(tObject, strOEDPrefName, refvObjsInfo));
    
    return iRetCode;
}

/**
 * This function will process input OED strings vector "refvstrOEDStrings" using input object tag with UID "strUID".
 * Function retrieves OED final object tags, attribute name and values.
 *
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  strUID            UID of Object tag which will be used to expand OED strings information.
 * @param[in]  refvstrOEDStrings Vector containing OED strings.
 * @param[out] refobjOEDInfo     Vector of "LMIOEDObjectInfo" objects containing OED final object tags, attribute name and values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_uid_process_oed_strings1
(
    string strUID, 
    const vector<string>& refvstrOEDStrings, 
    vector<LMIOEDObjectInfo>& refvObjsInfo
)
{
    int   iRetCode = ITK_ok;  
    tag_t tObject  = NULLTAG; 
    
    LMI_ITKCALL(iRetCode, POM_string_to_tag(strUID.c_str(), &tObject));

    LMI_ITKCALL(iRetCode, LMI_process_oed_strings1(tObject, refvstrOEDStrings, refvObjsInfo));

    return iRetCode;
}

/**
 * This function retrieves OED strings from input preference "strOEDPrefName". Preference values are OED expression which are evaluated using input objects with UIDs "vstrUIDs".
 * Here count of input objects and count of OED strings retrieved from prefernce will be equal. For every input UID, there will be a corresponding OED string.
 * For example:
 *                  Input UID: UID present in vstrUIDs at second position.
 *      Applicable OED string: OED string present at second position in vector of OED strings which is retrieved by preference "strOEDPrefName".
 *
 * Function retrieves OED final object tags, attribute name and values.
 *
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  vstrUIDs       UIDs of Object tags which will be used to expand OED strings information.
 * @param[in]  strOEDPrefName Preference name whose values are OED strings.
 * @param[out] refvObjInfos   Vector of "LMIOEDObjectInfo" objects containing OED final object tags, attribute name and values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_uid_process_oed_preference2
(
    vector<string> vstrUIDs, 
    string strOEDPrefName, 
    vector<LMIOEDObjectInfo>& refvObjInfos
)
{
    int iRetCode = ITK_ok;  
    vector<tag_t> vtObjects; 
    
    LMI_ITKCALL(iRetCode, LMI_strings_to_tags(vstrUIDs, vtObjects));

    LMI_ITKCALL(iRetCode, LMI_process_oed_preference2(vtObjects, strOEDPrefName, refvObjInfos));

    return iRetCode;
}

/**
 * This function will process input OED strings vector "refvOEDStrings" using input objects with UIDs "vstrUIDs". Here count of input UIDs and count of OED strings will be 
 * equal. For every input UID , there will be a corresponding OED string.
 * For example:
 *                  Input UID: UID present in vstrUIDs at second position.
 *      Applicable OED string: OED string present in refvOEDStrings at second position.
 *
 * Function retrieves OED final object tags, attribute name and values.
 *
 * @note: 
 *       If BOM line property values are retrieved then BOM windows have to close manually.
 *
 * @param[in]  vstrUIDs       UIDs of Object tags which will be used to expand OED strings information.
 * @param[in]  refvOEDStrings Vector containing OED strings.
 * @param[out] refvObjInfos   Vector of "LMIOEDObjectInfo" objects containing OED final object tags, attribute name and values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_uid_process_oed_strings2
(
    vector<string> vstrUIDs, 
    const vector<string>& refvOEDStrings, 
    vector<LMIOEDObjectInfo>& refvObjInfos
)
{
    int iRetCode = ITK_ok;  
    vector<tag_t> vtObjects; 
    
    LMI_ITKCALL(iRetCode, LMI_strings_to_tags(vstrUIDs, vtObjects));

    LMI_ITKCALL(iRetCode, LMI_process_oed_strings2(vtObjects, refvOEDStrings, refvObjInfos));

    return iRetCode;
}


