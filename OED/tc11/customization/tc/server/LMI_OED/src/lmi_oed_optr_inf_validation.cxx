/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_oed_optr_inf_validation.cxx

    Description:  This file contains function related to read and validate the OED operator preference.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-DEC-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_oed_optr_inf_validation.hxx>
#include <lmi_library.hxx>

/*
 * This function reads and validates operator preference. Operator preference is a multivalued preference where each line represents an operator information. Operators can be
 * optional. If "OP" string occurs before the Operator sign it is considered as optional operator. Corresponding to each operator an object of class "LMIOperator" is created and all of these objects
 * are stored in a vector.
 * Operator preference is searched by name "LMI_OED_Operator_Information". Operator preference value sample is shown below and may follow given below pattern:
 *               - ( , OP: OP$ )
 *                 Operator preference value can never have optional First and last operators. In the above example operator ":" and "$" are optional.
 *
 * @param[in]  strPrefName         Name of OED operator preference.
 * @param[out] refvLMIOperators    Vector of "LMIOperator" class to hold information as operator sign and a logical variable to represent whether operator is optional
 *                                 or not.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDOptrInfoValidation::LMI_get_and_validate_optr_pref_values
(
    const string strOptPrefName,
    vector<LMIOperator>& refvobjLMIOptrs
)
{
    int iRetCode = ITK_ok;
    vector<string> vstrPrefVals;

    /* Initializing out parameter of this function. */
    refvobjLMIOptrs.clear();

    /* Reading operator preference */
    LMI_ITKCALL(iRetCode, LMI_pref_get_string_values(strOptPrefName.c_str(), vstrPrefVals));

    if (iRetCode == ITK_ok)
    {
        if (vstrPrefVals.size() > 0)
        {
            int iCount        = 0;
            int iVSize        = 0;
            int iOprPrefixLen = 0;
            vector<string>::iterator iterOptrPrefVal; 

            iVSize        = (int) (vstrPrefVals.size());
            iOprPrefixLen = (int)tc_strlen(LMI_OPTIONAL_OPTR_PREFIX);

            for (iterOptrPrefVal = vstrPrefVals.begin(); iterOptrPrefVal < vstrPrefVals.end() && iRetCode == ITK_ok; iterOptrPrefVal++)
            {
                iCount = iCount + 1;

                /* Check for reserved operators */
                if (tc_strcasecmp(((*iterOptrPrefVal).c_str()), LMI_OPTR_EXACT_BOM) == 0)
                {
                    iRetCode = OPTR_PREF_VAL_USING_RESERVED_OPTR;

                    EMH_store_error_s2(EMH_severity_error, iRetCode, LMI_OED_OPERATOR_INFORMATION, LMI_OPTR_EXACT_BOM);
                }

                if (iRetCode == ITK_ok)
                {
                    /* Length of operator preference value should be 1 if operator is not optional. Ex ")"  */
                    if (iterOptrPrefVal->length() == LMI_OPTR_LENGTH)
                    {
                        LMIOperator objLMIOptr;

                        objLMIOptr.strOperatorSign.assign((*iterOptrPrefVal));

                        objLMIOptr.bIsOptional = false;

                        refvobjLMIOptrs.push_back(objLMIOptr);
                    }
                    else if(iterOptrPrefVal->length() == (iOprPrefixLen + 1) && strncmp(((*iterOptrPrefVal).c_str()), LMI_OPTIONAL_OPTR_PREFIX, iOprPrefixLen) == 0)
                    {
                        /* if operator is optional, length of operator preference value should be 1 more then the length of "LMI_OPTIONAL_OPTR_PREFIX" and it must start with "LMI_OPTIONAL_OPTR_PREFIX". Ex "OP�" */
                        
                        if ( iCount > 1 && iCount < iVSize )
                        {
                            /* This condition ensures that first and last operator can not be optional. */

                            LMIOperator objLMIOptr;

                            objLMIOptr.strOperatorSign.assign(iterOptrPrefVal->substr(iOprPrefixLen, 1));

                            objLMIOptr.bIsOptional = true;

                            refvobjLMIOptrs.push_back(objLMIOptr);
                        }
                        else
                        {
                            iRetCode = OPTR_PREF_HAS_FIRST_OR_LAST_VALUE_OPTIONAL_OPT;

                            EMH_store_error_s2(EMH_severity_error, iRetCode, LMI_OED_OPERATOR_INFORMATION, (iterOptrPrefVal->c_str()));     
                        }  
                    }
                    else
                    {
                        iRetCode = OPTR_PREF_VAL_INVALID;

                        EMH_store_error_s2(EMH_severity_error, iRetCode, LMI_OED_OPERATOR_INFORMATION, (iterOptrPrefVal->c_str()));
                    }
                }
            }
        }
        else
        {
            iRetCode = OPTR_PREF_VAL_NOT_EXIST;

            EMH_store_error_s1(EMH_severity_error, iRetCode, LMI_OED_OPERATOR_INFORMATION); 
        }
    }

    return iRetCode;
}

/*
 * This function returns OED string segment separator. OED string segment separator value is retrieved from "LMI_OED_Expression_Seprator" preference. If preference do not exist then "." is used as 
 * default OED string segment separator value.
 *
 * @param[in]  vobjLMIOptrs           Vector of OED operators. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                      not change the value being referenced.)
 * @param[out] refstrOEDExprSeparator OED string segment separator. 
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDOptrInfoValidation::LMI_get_and_validate_expr_separator
(
    const vector<LMIOperator>& vobjLMIOptrs,
    string& refstrOEDExprSeparator
)
{
    int   iRetCode    = ITK_ok;
    char* pszPrefVal = NULL;

    /* Initializing out parameter of this function. */
    refstrOEDExprSeparator.clear();
    
    LMI_ITKCALL(iRetCode, LMI_pref_get_string_value(LMI_OED_EXPRESSION_SEPARATOR, &pszPrefVal));

    if (iRetCode == ITK_ok)
    {
        /* If OED expression separator preference value is null or empty then "LMI_OED_EXPRESSION_SEPARATOR" is used as default */
        if (pszPrefVal == NULL || tc_strlen(pszPrefVal) == 0)
        {
            refstrOEDExprSeparator.assign(LMI_OED_EXPRESSION_SEPARATOR_DEFAULT);  
        }
        else
        {
             refstrOEDExprSeparator.assign(pszPrefVal);
        }
        
        /* Check for reserved operators */
        if (tc_strcasecmp((refstrOEDExprSeparator.c_str()), LMI_OPTR_EXACT_BOM) == 0)
        {
            iRetCode = OPTR_PREF_VAL_USING_RESERVED_OPTR;

            EMH_store_error_s1(EMH_severity_error, iRetCode, refstrOEDExprSeparator.c_str());
        }

        vector<LMIOperator>::const_iterator iterOptr;

        /* check to ensure that OED expression separator does not appear in OED operators */
        for (iterOptr = vobjLMIOptrs.begin(); iterOptr < vobjLMIOptrs.end() && iRetCode == ITK_ok; iterOptr++)
        {
            if (tc_strcmp(iterOptr->strOperatorSign.c_str(), refstrOEDExprSeparator.c_str()) == 0)
            {
                iRetCode = OPTR_SGMNT_SEPTR_EXIT_IN_OPTRS;

                EMH_store_error_s1(EMH_severity_error, iRetCode, refstrOEDExprSeparator.c_str()); 

                break;
            }
        }

        if (iRetCode == ITK_ok)
        {
            TC_write_syslog("INFO(lmi_oed.dll,LMI_get_and_validate_expr_separator): String %s will be used as an OED expression separator.\n", refstrOEDExprSeparator.c_str());
        }
    }

    LMI_MEM_TCFREE(pszPrefVal);

    return iRetCode;
}


