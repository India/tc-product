/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_arguments_utilities.cxx

    Description:  This File contains functions for string parsing operations and functions for retrieving arguments passed in workflow action or rule handlers
                  These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>
#include <lmi_errors.hxx>

/**
 * This function retrieves tags of the attachments to the workflow whose types are one of the input Types which is given as an input parameter.
 *
 * @param[in]  tTask           Tag of the task on which action is triggered
 * @param[in]  iAttachmentType Determines of the Valid objects needs to be retrieved from target or Reference of the workflow. Valid values of this parameter are 'EPM_reference_attachment'
 *                             or 'EPM_target_attachment'
 * @param[in]  ppszObjTypeList List of Type of the Object according to which attachment is retrieved.
 * @param[in]  iTypeCnt        Count of input types.
 * @param[out] piCount         Number of attachment that are retrieved.
 * @param[out] pptObject       Array of attachment tags.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI_arg_get_attached_obj_of_input_type_list
(
    tag_t   tTask,
    int     iAttachmentType,
    char**  ppszObjTypeList,
    int     iTypeCnt,
    int*    piCount,
    tag_t** pptObject
)
{
    int    iRetCode      = ITK_ok;
    int    iTargetCount  = 0;
    int    iObjCount     = 0;
    tag_t  tRootTask     = NULLTAG;
    tag_t* ptTargets     = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*pptObject) = NULL;
    (*piCount)   = 0;

    /* Validate input */
    if(ppszObjTypeList != NULL && iTypeCnt > 0)
    {
        LMI_ITKCALL(iRetCode, EPM_ask_root_task(tTask, &tRootTask));
          
        /* Get the target objects. */
        LMI_ITKCALL(iRetCode, EPM_ask_attachments(tRootTask, iAttachmentType, &iTargetCount, &ptTargets));
        
        /* For each target, check if it is an Item Revision. If so, try to get the children.
           Check the status of the returned Item Revision and compare themn with the given list.
        */
        for (int iDx = 0; iDx < iTargetCount && iRetCode == ITK_ok; iDx++)
        {
            for(int iJx = 0; iJx < iTypeCnt && iRetCode == ITK_ok; iJx++)
            {
                char* pszObjType = NULL;
            
                LMI_ITKCALL(iRetCode, LMI_obj_ask_type(ptTargets[iDx], &pszObjType));
                        
                if (tc_strcmp(ppszObjTypeList[iJx], pszObjType) == 0)
                { 
                    int     iActiveSeq = 0;
                    logical bIsWSObj   = false;

                    LMI_ITKCALL(iRetCode, LMI_obj_is_type_of(ptTargets[iDx], LMI_CLASS_WORKSPACEOBJECT, &bIsWSObj));

                    if(iRetCode == ITK_ok)
                    {
                        if(bIsWSObj == true)
                        {
                            LMI_ITKCALL(iRetCode, AOM_ask_value_int(ptTargets[iDx], LMI_ATTR_ACTIVE_SEQ, &iActiveSeq));
                        }
                        else
                        {
                            iActiveSeq = 1;
                        }
                    }

                    /* This check indicates that Object with Active sequence will be considered */
                    if(iActiveSeq != 0 && iRetCode == ITK_ok)
                    {
                        LMI_UPDATE_TAG_ARRAY(iObjCount, (*pptObject), ptTargets[iDx]);
                    }
                }

                LMI_MEM_TCFREE(pszObjType);
            }   
        }

        (*piCount) = iObjCount;
    }

    LMI_MEM_TCFREE(ptTargets);    
    return iRetCode;
}

/**
 * This function retrieves tag of the attachment as per the "Type"  of the attachment. Type of the attachment is given as an input parameter.
 *
 * e.g. Suppose 'PartRevision' has class type of 'Item'. If we pass 'pszAttachedObjType' as 'PartRevision' function will specifically return all the
 *      attachments of type 'PartRevision' only.
 *
 * @param[in]  task               Tag of the task on which action is triggered
 * @param[in]  pszAttachedObjType Type of the Object according to which attachment is retrieved.
 * @param[out] piCount            Number of attachment that are retrieved.
 * @param[out] pptObject          Array of attachment tags.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI_arg_get_attached_objects_of_type
(
    tag_t   task,
    char*   pszAttachedObjType,
    int*    piCount,
    tag_t** pptObject
)
{
    int    iRetCode     = ITK_ok;
    int    iTargetCount = 0;
    tag_t  tRootTask    = NULLTAG;
    tag_t* ptTargets    = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*pptObject) = NULL;
    (*piCount)   = 0;

    /* Validate input */
    if(pszAttachedObjType != NULL)
    {
        LMI_ITKCALL(iRetCode, EPM_ask_root_task(task, &tRootTask));
          
        /* Get the target objects. */
        LMI_ITKCALL(iRetCode, EPM_ask_attachments(tRootTask, EPM_target_attachment, &iTargetCount, &ptTargets));
        
        /* For each target, check if it is an Item Revision. If so, try to get the children.
           Check the status of the returned Item Revision and compare themn with the given list.
        */
        for (int iDx = 0; iDx < iTargetCount && iRetCode == ITK_ok; iDx++)
        {
            char* pszObjType = NULL;
            
            LMI_ITKCALL(iRetCode, LMI_obj_ask_type(ptTargets[iDx], &pszObjType));
            
            if (tc_strcmp(pszObjType, pszAttachedObjType) == 0)
            { 
                int     iActiveSeq = 0;
                logical bIsWSObj   = false;

                LMI_ITKCALL(iRetCode, LMI_obj_is_type_of(ptTargets[iDx], LMI_CLASS_WORKSPACEOBJECT, &bIsWSObj));

                if(iRetCode == ITK_ok)
                {
                    if(bIsWSObj == true)
                    {
                        LMI_ITKCALL(iRetCode, AOM_ask_value_int(ptTargets[iDx], LMI_ATTR_ACTIVE_SEQ, &iActiveSeq));
                    }
                    else
                    {
                        iActiveSeq = 1;
                    }
                }

                /* This check indicates that Object with Active sequence will be considered */
                if(iActiveSeq != 0 && iRetCode == ITK_ok)
                {
                    LMI_UPDATE_TAG_ARRAY((*piCount), (*pptObject), ptTargets[iDx]);
                }
            }

            LMI_MEM_TCFREE(pszObjType);
        }
    }

    LMI_MEM_TCFREE(ptTargets);    
    return iRetCode;
}

/**
 * This function loops through the arguments given in a handler definition list and searches for the given switch. It gives back a list with values of the switch. Depending on the lOptional the
 * argument must be present in the definition of the handler or not.
 *
 * @note Example: -type=item;document \n
 *       where -type is the switch and \n
 *       item and document are the two values which appear in the array.
 *
 * @note The separator for multiple values can be semi-colon (;) or coma(,). This is generic function and takes input as character separator on that basis function will process the value list.
 *       
 * @param[in]  pArgList           Argument list provided by the handler msg structure
 * @param[in]  tTask              Task tag of the workflow task
 * @param[in]  pszArgument        Name of the argument to be used
 * @param[in]  pszSeparator       Separator
 * @param[in]  bOptional          If FALSE the argument is required, else optional
 * @param[out] piValCount         Number of values returned
 * @param[out] pppszArgValue      Array of values returned
 * @param[out] ppszOriginalArgVal Value of the argument with separator as passed in the workflow 
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI_arg_get_arguments
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    const char*         pszArgument,
    char*               pszSeparator,
    logical             bOptional,
    int*                piValCount,
    char***             pppszArgValue,
    char**              ppszOriginalArgVal
)
{
    /* Use TC_init_argument_list and TC_number_of_arguments to get the argument list find the right argument using ITK_ask_argument_named_value if found, split the value to an array of values using
       the separator
    */
    int     iRetCode  = ITK_ok;
    int     iArgCount = 0;
    logical bFoundArg = false;

    /* Initializing the Out Parameter to this function. */
    (*pppszArgValue)      = NULL;
    (*piValCount)         = 0;
    (*ppszOriginalArgVal) = NULL;

    /* Validate input */
    if((pszArgument == NULL) || (pszSeparator == '\0'))
    {
        TC_write_syslog( "ERROR: Missing input data:Name of the argument or separator is null \n");
    }
    else
    {
        TC_init_argument_list(pArgList);

        iArgCount = TC_number_of_arguments(pArgList);

        for(int iDx = 0; iDx < iArgCount && iRetCode == ITK_ok; iDx++)
        {
            char* pszSwitch       = NULL;
            char* pszValue        = NULL;
            char* pszNextArgument = NULL;

            pszNextArgument = TC_next_argument(pArgList);

            LMI_ITKCALL(iRetCode, ITK_ask_argument_named_value(pszNextArgument, &pszSwitch, &pszValue));
            
            if(tc_strcasecmp(pszSwitch, pszArgument) == 0)
            {
                LMI_STRCPY((*ppszOriginalArgVal), pszValue);

                LMI_ITKCALL(iRetCode, LMI_str_parse_string(pszValue, pszSeparator, piValCount, pppszArgValue));

                if(iRetCode != ITK_ok)
                {
                    /*Store error into error stack */
                    iRetCode = UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES;
                    EMH_store_error(EMH_severity_error, iRetCode);
                    break;
                }
                else
                {
                    bFoundArg = true;
                    break;
                }
            }
            
            LMI_MEM_TCFREE(pszSwitch);
            LMI_MEM_TCFREE(pszValue);
        }

        /*
         * When the requested argument is not found: If lOptional flag is set to TRUE, Do Nothing. Else If lOptional flag is set to FALSE. Throw an Error.
         */
        if(bOptional == false && bFoundArg == false)
        {
            iRetCode = ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND;
            EMH_store_error(EMH_severity_error, iRetCode);
        }
    }

    return iRetCode;
}

/**
 * This function loops through the arguments given in a handler definition list and searches for the given argument name. It gives back single values of the input argument name. Depending on the
 * bOptional the argument must be present in the definition of the handler or not.
 *       
 * @param[in]  pArgList     Argument list provided by the handler msg structure
 * @param[in]  tTask        Task tag of the workflow task
 * @param[in]  pszArgument  Name of the argument whose value has to be retrieved
 * @param[in]  bOptional    If FALSE the argument is required, else optional
 * @param[out] ppszArgValue Array of values returned
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI_arg_get_wf_argument_value
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    const char*         pszArgument,
    logical             bOptional,
    char**              ppszArgValue
)
{
    /* Use TC_init_argument_list and TC_number_of_arguments to get the argument list find the right argument using ITK_ask_argument_named_value if found, split the value to an array of values using
      the separator.
    */
    int     iRetCode  = ITK_ok;
    int     iArgCount = 0;
    logical bFoundArg = false;

    /* Initializing the Out Parameter to this function. */
    (*ppszArgValue) = NULL;

    TC_init_argument_list(pArgList);

    iArgCount = TC_number_of_arguments(pArgList);

    for(int iDx = 0; iDx < iArgCount && iRetCode == ITK_ok; iDx++)
    {
        char* pszSwitch       = NULL;
        char* pszValue        = NULL;
        char* pszNextArgument = NULL;

        pszNextArgument = TC_next_argument(pArgList);

        LMI_ITKCALL(iRetCode, ITK_ask_argument_named_value(pszNextArgument, &pszSwitch, &pszValue));
        
        if(tc_strcasecmp(pszSwitch, pszArgument) == 0)
        {
            LMI_STRCPY((*ppszArgValue), pszValue);
            bFoundArg = true;
            break;
        }
        
        LMI_MEM_TCFREE(pszSwitch);
        LMI_MEM_TCFREE(pszValue);
    }

    /* When the requested argument is not found: If lOptional flag is set to TRUE, Do Nothing. Else If lOptional flag is set to FALSE. Throw an Error.  */
    if(bOptional == false && bFoundArg == false)
    {
        iRetCode = ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND;
        EMH_store_error(EMH_severity_error, iRetCode);
    }

    return iRetCode;
}

/**
 * This function loops through the arguments given in a handler definition list and searches for the given argument name. It gives back single values of the input argument name. Depending on the
 * lOptional the argument must be present in the definition of the handler or not.
 *
 * @note: This function returns a logical argument that let the calling function know if argument is supplied or not. It could me possible that argument is supplied without any value and this
 *        information is need by calling function
 *       
 * @param[in]  pArgList     Argument list provided by the handler msg structure
 * @param[in]  tTask        Task tag of the workflow task
 * @param[in]  pszArgument  Name of the argument whose value has to be retrieved
 * @param[in]  bOptional    If FALSE the argument is required, else optional
 * @param[out] pbFoundArg   TRUE if argument is found in the list, else FALSE
 * @param[out] ppszArgValue Array of values returned
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI_arg_get_wf_argument_info_and_value
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    const char*         pszArgument,
    logical             bOptional,
    logical*            pbFoundArg,
    char**              ppszArgValue
)
{
    /* Use TC_init_argument_list and TC_number_of_arguments to get the argument list find the right argument using ITK_ask_argument_named_value if found, split the value to an array of values using 
       the separator.
    */
    int iRetCode  = ITK_ok;
    int iArgCount = 0;

    /* Initializing the Out Parameter to this function. */
    (*pbFoundArg)   = false;
    (*ppszArgValue) = NULL;

    TC_init_argument_list(pArgList);

    iArgCount = TC_number_of_arguments(pArgList);

    for(int iDx = 0; iDx < iArgCount && iRetCode == ITK_ok; iDx++)
    {
        char* pszSwitch       = NULL;
        char* pszValue        = NULL;
        char* pszNextArgument = NULL;

        pszNextArgument = TC_next_argument(pArgList);

        LMI_ITKCALL(iRetCode, ITK_ask_argument_named_value(pszNextArgument, &pszSwitch, &pszValue));
        
        if(tc_strcasecmp(pszSwitch, pszArgument) == 0)
        {
            LMI_STRCPY((*ppszArgValue), pszValue);
            (*pbFoundArg) = true;
            break;
        }
        
        LMI_MEM_TCFREE(pszSwitch);
        LMI_MEM_TCFREE(pszValue);
    }

    /* When the requested argument is not found: If lOptional flag is set to TRUE, Do Nothing. Else If lOptional flag is set to FALSE. Throw an Error. */
    if(bOptional == false && (*pbFoundArg) == false)
    {
        iRetCode = ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND;
        EMH_store_error(EMH_severity_error, iRetCode);
    }

    return iRetCode;
}

/**
 * This function evaluated the input string "pszAtchType" and sets the return argument "piAtchType" to value "EPM_target_attachment" or "EPM_reference_attachment". If input string "pszAtchType" has 
 * any other value apart from "target" or "reference" then default value of "piAtchType" will be "EPM_target_attachment".
 *       
 * @param[in]  pszAtchType Attachment type in string format. 
 * @param[out] piAtchType  Attachment type macro value which can be used by ITK API.
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI_arg_get_wf_atch_type
(
    char* pszAtchType,
    int*  piAtchType  
)
{
    int iRetCode = ITK_ok;

    if(tc_strcasecmp(pszAtchType, LMI_WF_ARG_VAL_TARGET) == 0)
    {
        (*piAtchType) = EPM_target_attachment;
    }
    else
    {
        if(tc_strcasecmp(pszAtchType, LMI_WF_ARG_VAL_REFERENCE) == 0)
        {
            (*piAtchType) = EPM_reference_attachment;
        }
        else
        {
            (*piAtchType) = EPM_target_attachment;
        }
    }

    return iRetCode;
}

