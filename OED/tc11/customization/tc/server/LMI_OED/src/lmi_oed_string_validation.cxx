/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_oed_string_validation.cxx

    Description:  This file contains method to read and validate the oed preference.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-DEC-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_oed_string_validation.hxx>
#include <lmi_library.hxx>
#include <lmi_operator.hxx>
#include <lmi_oed_syntax_validation.hxx>
#include <lmi_oed_logical_validation.hxx>
#include <lmi_oed_optr_inf_validation.hxx>

/*
 * This function reads OED preference and returns vector of OED strings. OED preferences must always exist in pairs i.e. first line of preference represents OED string which act as source and 
 * second preference line acts as destination OED string. Function validates that count of OED preference must be even.
 *
 * @param[in]  pszPrefName        OED preference name. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                  not change the value being referenced.)
 * @param[in]  bValidateValCount  Boolean value to indicate whether to validate preference values count or not.
 * @param[in]  iValidValCount     Input objects count against which preference value count to be validated.
 * @param[out] refvstrOEDStrings  Vector of string to hold OED strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDStringValidation::LMI_read_oed_string_pref
(
    const char*   pszPrefName,
    const logical bValidateValCount,
    const int     iValidValCount,
    vector<string>& refvstrOEDStrings
)
{
    int iRetCode  = ITK_ok;

    /* Initializing out parameter */
    refvstrOEDStrings.clear();
        
    /* Retrieve OED strings from OED preference. */
    LMI_ITKCALL(iRetCode, LMI_pref_get_string_values(pszPrefName, refvstrOEDStrings));

    if (iRetCode == ITK_ok)
    {
        if (refvstrOEDStrings.size() == 0)
        {
            iRetCode = OED_PREF_VAL_NOT_EXIST;

            EMH_store_error_s1(EMH_severity_error, iRetCode, pszPrefName); 
        }
        else if (bValidateValCount == true)
        {
            if (iValidValCount > 0)
            {
                if (refvstrOEDStrings.size() != iValidValCount)
                {
                    iRetCode = OED_PREF_VAL_COUNT_NOT_EQUAL_INPUT_TAGS;

                    EMH_store_error_s1(EMH_severity_error, iRetCode, pszPrefName); 
                }
            }
            else if ((refvstrOEDStrings.size()) % 2 != 0)
            {
                /* OED Preference values count must be an even number */
                iRetCode = OED_PREF_VAL_COUNT_INVALID;

                EMH_store_error_s1(EMH_severity_error, iRetCode, pszPrefName);
            } 
        }       
    }

    return iRetCode;
}

/*
 * This function will validates OED strings syntax and also check if logical construct of input OED string is appropriate. 
 * OED Syntax Validation:
 * - Function will check occurrence order and count of Open and Close brackets in each OED string. Occurrence count of both Open and Close brackets in OED string must be same.
 * - Function will also reflector OED string by replacing multiple consecutive occurrence of OED operators (e.g. "," "�" ":" etc.) or separator (e.g. ".") with appropriate single ones.
 *   Function will exclude OED grouping operators i.e. Open and Close brackets from refactoring in this step.
 * - Function will remove all grouping operators (Open and Close brackets) except First and last Open and Close bracket from the input string. Function will parse the OED string and store following
 *   information in "LMIOEDString" object: 
 *                 -- Operation Prefix such as "REFBY", "GRMS2P" etc.
 *                 -- Property Name
 *                 -- Object Type and it position such as "ALL", "LAST" & "FIRST"
 *                 -- BOM Line Level and Revision rule
 * OED Logical Validation:
 *
 * @param[in]  strOEDExprSeparator OED string expression separator.
 * @param[in]  vstrOEDStrings      Vector of OED strings. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                   not change the value being referenced.)
 * @param[in]  vobjLMIOptrs        Vector of operators. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                   not change the value being referenced.)
 * @param[out] refvobjOEDStrings   Vector of "LMIOEDString" objects. 
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDStringValidation::LMI_validate_oed_strings
(
    const string strOEDExprSeparator,
    const vector<string>& vstrOEDStrings,
    const vector<LMIOperator>& vobjLMIOptrs,
    vector<LMIOEDString>& refvobjOEDStrings
)
{
    int iRetCode = ITK_ok;
    LMIOEDSyntaxValidation objSyntaxValidation;
    LMIOEDLogicalValidation objLogicalValidation;

    /* Validating OED strings syntax. If succeeds then it gets OED strings in object format. */
    LMI_ITKCALL(iRetCode, objSyntaxValidation.LMI_validate_oed_syntax(strOEDExprSeparator, vstrOEDStrings, vobjLMIOptrs, refvobjOEDStrings));

    /* Validating OED strings (objects) logically */
    LMI_ITKCALL(iRetCode, objLogicalValidation.LMI_OED_validate_logically(refvobjOEDStrings));

    return iRetCode;
}

/*
 * This function retrieves OED operators and OED expression separator. Based on operators and separator, OED strings are validated syntactically and logically. Function will store
 * valid parsed OED string information in object of class "LMIOEDString".
 *
 * @param[in]  vstrOEDStrings    Vector of OED strings. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                 not change the value being referenced.)
 * @param[out] refvobjOEDStrings Vector of objects of "LMIOEDString" class. 
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDStringValidation::LMI_store_oed_string_objects
(
    const vector<string>& vstrOEDStrings, 
    vector<LMIOEDString>& refvobjOEDStrings
)
{
    int    iRetCode            = ITK_ok;
    string strOEDExprSeparator = EMPTY_STRING;
    vector<LMIOperator> vobjLMIOptrs;
    LMIOEDOptrInfoValidation objOptrInfValidation;
    LMIOEDStringValidation objOEDStrValidation;

    /* Validating and retrieving OED operators */
    LMI_ITKCALL(iRetCode, objOptrInfValidation.LMI_get_and_validate_optr_pref_values(LMI_OED_OPERATOR_INFORMATION, vobjLMIOptrs));

    /* Validating and retrieving OED string expression separator */
    LMI_ITKCALL(iRetCode, objOptrInfValidation.LMI_get_and_validate_expr_separator(vobjLMIOptrs, strOEDExprSeparator));

    /* Performing syntactical and logical validation on OED strings (objects) */
    LMI_ITKCALL(iRetCode, objOEDStrValidation.LMI_validate_oed_strings(strOEDExprSeparator, vstrOEDStrings, vobjLMIOptrs, refvobjOEDStrings));
    
    return iRetCode;
}


