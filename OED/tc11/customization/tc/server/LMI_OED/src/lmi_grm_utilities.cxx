/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_grm_utilities.cxx

    Description:  This File contains custom functions for GRM to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>


/**
 * This function get's the object's related to input business object 'tObject'. Related objects are retrieved based on various input criteria such as:
 *
 *   <b> Relation Type between business objects </b>
 *   <b> Type of related object </b>
 *   <b> Related objects can be excluded based on status </b>
 * 
 * @note: This function is generic function and is capable of retrieving Primary and Secondary relation objects based on value of input arguments 'bGetPrimary'. If argument 'pszRelationTypeName'
 *        is NULL then all related objects irrespective of relation type will be retrieved. If 'pszObjType' is NOT NULL then related objects of type 'pszObjType' will be retrieved.
 *
 * @param[in]  tObject            Tag of Object
 * @param[in]  pszRelationType    Relation Type. NULLTAG means all relation types.
 * @param[in]  pszObjType         Specific type of object which is related to input object 'tObject'. If NULL then any type of related object.
 * @param[in]  pszExcludeStatus   Related objects with this status will be excluded from valid related objects list
 * @param[in]  bGetPrimary        'true' of Primary related objects needs to be retrieved from input objects else secondary objects will be retrieved. 
 * @param[out] pptValidRelatedObj List of valid related objects
 * @param[out] piRelatedObjCnt    Count of valid related objects.
 *
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMI_grm_get_related_obj
(
    tag_t   tObject,
    char*   pszRelationType,
    char*   pszObjType,
    char*   pszExcludeStatus,
    logical bGetPrimary,
    tag_t** pptValidRelatedObj,
    int*    piRelatedObjCnt
)
{
    int    iRetCode              = ITK_ok;
    int    iCount                = 0;
    int    iRelatedObjCnt        = 0;    
    tag_t  tRelationType         = NULLTAG;
    tag_t* ptLoadedObj           = NULL;
    GRM_relation_t* ptRelatedObj = NULL;
    
    /* Initializing out parameter of this function. */
    (*piRelatedObjCnt)    = 0;
    (*pptValidRelatedObj) = NULL;

    if(pszRelationType != NULL)
    {
        /* Get the relation type tag e.g. "IMAN_specification"  */
        LMI_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationType, &tRelationType)); 
    }
    
    if(bGetPrimary == true)
    {
        LMI_ITKCALL(iRetCode, GRM_list_primary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }
    else
    {
        LMI_ITKCALL(iRetCode, GRM_list_secondary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }

    /* Related objects are deliberately collected in this loop so that they can be loaded using POM API in one shot */                 
    for(int iNx = 0; iNx < iRelatedObjCnt && iRetCode == ITK_ok; iNx++)
    {
        int     iActiveSeq = 0;
        tag_t   tObject    = NULLTAG;
        logical bIsValid   = false;
        logical bIsWSObj   = false;
        
        if(bGetPrimary == true)
        {
            tObject = ptRelatedObj[iNx].primary;
        }
        else
        {
            tObject = ptRelatedObj[iNx].secondary;
        }

        LMI_ITKCALL(iRetCode, LMI_obj_is_type_of(tObject, LMI_CLASS_WORKSPACEOBJECT, &bIsWSObj));

        if(iRetCode == ITK_ok)
        {
            if(bIsWSObj == true)
            {
                LMI_ITKCALL(iRetCode, AOM_ask_value_int(tObject, LMI_ATTR_ACTIVE_SEQ, &iActiveSeq));
            }
            else
            {
                iActiveSeq = 1;
            }
        }
    
        /* This check indicates that Item Revisions with Active sequence will be considered */
        if(iActiveSeq != 0 && iRetCode == ITK_ok)
        {
            logical bBelongsToClass = false;

            if(pszObjType != NULL && tc_strlen(pszObjType) > 0)
            {
                LMI_ITKCALL(iRetCode, LMI_obj_is_type_of(tObject, pszObjType, &bBelongsToClass));
            }
            else
            {
                bBelongsToClass = true;
            }

            if( bBelongsToClass ==  true && iRetCode == ITK_ok)
            {
                tag_t  tStatus  = NULLTAG;

                if(pszExcludeStatus != NULL && tc_strlen(pszExcludeStatus) > 0)
                {
                    LMI_ITKCALL(iRetCode, LMI_obj_ask_status_tag(tObject, pszExcludeStatus, &tStatus));
                }

                if(tStatus == NULLTAG && iRetCode == ITK_ok)
                {
                    LMI_UPDATE_TAG_ARRAY(iCount, (*pptValidRelatedObj), tObject);
                }
            }
        }
    }
  
    if(iRetCode == ITK_ok)
    {
        (*piRelatedObjCnt) = iCount;
    }

    LMI_MEM_TCFREE(ptLoadedObj);
    LMI_MEM_TCFREE(ptRelatedObj);

    return iRetCode;
}
    
/**
 * This function creates specification relationship between primary and secondary object.
 * 
 * @param[in]  tPrimaryObj     Tag of the primary object.
 * @param[in]  tSecondaryObj   Tag of the secondary object.
 * @param[in]  pszRelationType Name of relation type.
 * @param[out] ptRelation      Tag to newly created relation type.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMI_grm_create_relation
(
    tag_t  tPrimaryObj,
    tag_t  tSecondaryObj,
    char*  pszRelationType,
    tag_t* ptRelation
)
{
    int   iRetCode      = ITK_ok;
    tag_t tRelationType = NULLTAG;
    tag_t tNewRelation  = NULLTAG;

    /* Initializing the Out Parameter to this function. */
    (*ptRelation) = NULLTAG;

    /* Get relation type */
    LMI_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationType, &tRelationType));

    if(iRetCode == ITK_ok)
    {
        if(tRelationType == NULLTAG)
        {
            iRetCode = GRM_invalid_relation_type;
        }
        else if(tPrimaryObj == NULLTAG)
        {
            iRetCode = GRM_invalid_primary_object;
        }
        else if(tSecondaryObj == NULLTAG)
        {
            iRetCode = GRM_invalid_secondary_object;
        }
    }

     /*     Find relation to check if relation already exists   */
    LMI_ITKCALL(iRetCode, GRM_find_relation(tPrimaryObj, tSecondaryObj, tRelationType, &tNewRelation));

    if(tNewRelation == NULLTAG && iRetCode == ITK_ok)
    {
        LMI_ITKCALL(iRetCode, GRM_create_relation(tPrimaryObj, tSecondaryObj, tRelationType, NULLTAG, &tNewRelation));

        if(tNewRelation != NULLTAG && iRetCode == ITK_ok)
        {
            LMI_ITKCALL(iRetCode, GRM_save_relation(tNewRelation));
        }
    }

    if(iRetCode == ITK_ok)
    {
        (*ptRelation) = tNewRelation;
    }
    
    return iRetCode;
}


