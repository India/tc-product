/*======================================================================================================================================================================
                                                     Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_process_op_grm.cxx

    Description: .

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
09-DEC-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#include <lmi_oed_operation_ext.hxx>
#include <lmi_library.hxx>

/*
 * This function filters the given list of tags on the bases of depth value.
 *
 * @param[in]  strDepth     Depth, it can be "ALL", "FIRST" or "LAST".
 * @param[in]  iCount       Count of input tags.
 * @param[in]  ptInTags     List of input tags.
 * @param[out] refvOutTags  Vector of tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_OED_depth::LMI_process_depth
(
    string         strDepth,
    int            iCount,
    tag_t*         ptInTags,
    vector<tag_t>& refvOutTags
)
{
    int iRetCode = ITK_ok;

    if(iCount > 0)
    {
        if(tc_strcasecmp(strDepth.c_str(), LMI_DEPTH_ALL) == 0 || strDepth.length() <= 0)
        {
            for (int iDx = 0; iDx < iCount; iDx++)
            {
                refvOutTags.push_back(ptInTags[iDx]);
            }
        }
        else if (tc_strcasecmp(strDepth.c_str(), LMI_DEPTH_FIRST) == 0)
        {
            refvOutTags.push_back(ptInTags[0]);
        }
        else if(tc_strcasecmp(strDepth.c_str(), LMI_DEPTH_LAST) == 0)
        {
            refvOutTags.push_back(ptInTags[iCount-1]);
        }
    }

    return iRetCode;
}

/*
 * This function filters the given vector of tags on the bases of depth value.
 *
 * @param[in]  strDepth     Depth, it can be "ALL", "FIRST" or "LAST".
 * @param[in]  vtInTags     Vector of input tags.
 * @param[out] refvOutTags  Vector of tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_OED_depth::LMI_process_depth
(
    string               strDepth,
    const vector<tag_t>& vtInTags,
    vector<tag_t>&       refvOutTags
)
{
    int iRetCode = ITK_ok;

    if(tc_strcasecmp(strDepth.c_str(), LMI_DEPTH_ALL) == 0 || strDepth.length() <= 0)
    {
        LMI_ITKCALL(iRetCode, LMI_stl_copy_unique_content_to_vector(vtInTags, refvOutTags));
    }
    else if (tc_strcasecmp(strDepth.c_str(), LMI_DEPTH_FIRST) == 0)
    {
        refvOutTags.push_back(vtInTags.front());
    }
    else if(tc_strcasecmp(strDepth.c_str(), LMI_DEPTH_LAST) == 0)
    {
        refvOutTags.push_back(vtInTags.back());
    }

    return iRetCode;
}
