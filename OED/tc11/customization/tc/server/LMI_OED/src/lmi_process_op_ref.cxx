/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_process_op_ref.cxx

    Description: This file contain functions those are used to process REF prefix.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
09-DEC-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#include <lmi_oed_operation_ext.hxx>
#include <lmi_library.hxx>

/*
 * This function process the ref prefix. 
 *
 * @param[in]  bIsPropTypeNote  Boolean value to indicate OED property is note type or not.
 * @param[in]  tObject          Target object tag.
 * @param[in]  objOEDSubSegment OED sub segment. (Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                not change the value being referenced.)
 * @param[in]  bIsLastSgmnt     Boolean value to indicate whether current OED sub segment is last in current "LMIOEDString" object or not.
 * @param[out] refOEDObjInfo    Resulted object which contains final tags, open BOM window tags and BOM line tags.
 * @param[out] refvTags         Vector of tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIProcessOpRef::LMI_process_prefix_ref
(
    bool   bIsPropTypeNote,
    tag_t  tObject,
    const  LMIOEDSubSegment& objOEDSubSegment,
    bool   bIsLastSgmnt,
    LMIOEDObjectInfo& refOEDObjInfo,
    vector<tag_t>& refvTags
)
{
    int iRetCode = ITK_ok;

    /* In case of REF prefix if "objPrefValPart.strRefRelName" equals to "LMI_ATTR_STRUCTURE_REVISIONS" then we have to consider it as BOM case. */
    if (tc_strcasecmp(objOEDSubSegment.strRefRelName.c_str(), LMI_ATTR_STRUCTURE_REVISIONS) == 0)
    {
        bool bBOMPropPrefix  = false; 
        
        /* Check to see if property name starts with "LMI_BOMLINE_PROP_PREFIX"(bl_). If so then it is BOM line property. */
        if (tc_strncasecmp(refOEDObjInfo.strPropName.c_str(), LMI_BOMLINE_PROP_PREFIX, tc_strlen(LMI_BOMLINE_PROP_PREFIX)) == 0)
        {
            bBOMPropPrefix = true;
        }

        /* Check to decide whether we need to process only BOM line or their corresponding item revisions too. */
        if (bIsLastSgmnt == true && (bIsPropTypeNote == true || bBOMPropPrefix == true))
        {
            refOEDObjInfo.bIsBLAttr = true;
        }
       
        /* Process BOM */
        LMI_ITKCALL(iRetCode, LMI_process_bom(tObject, objOEDSubSegment, refOEDObjInfo, refvTags));
    }
    else
    {
        /* Process REF */
        LMI_ITKCALL(iRetCode, LMI_process_ref(tObject, objOEDSubSegment, refvTags));
    }  

    return iRetCode;
}

/*
 * This function find all the objects of given type those are referenced by given target object and then process depth. 
 *
 * @param[in]  tObject           Target object tag.
 * @param[in]  objOEDSubSegment  OED sub segment. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                 not change the value being referenced.)
 * @param[out] refvTags          Vector of tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIProcessOpRef::LMI_process_ref
(
    tag_t tObject,
    const LMIOEDSubSegment& objOEDSubSegment,
    vector<tag_t>& refvTags
)
{
    int     iRetCode         = ITK_ok; 
    int     iRefTagsCnt      = 0;
    logical bIsPropTypeArray = false;
    tag_t*  ptRefTags        = NULL;
    
    LMI_ITKCALL(iRetCode, LMI_obj_is_property_type_array(tObject, objOEDSubSegment.strRefRelName, bIsPropTypeArray));

    if (bIsPropTypeArray == true)
    {
        /* Get reference property value */
        LMI_ITKCALL(iRetCode, AOM_ask_value_tags(tObject, objOEDSubSegment.strRefRelName.c_str(), &iRefTagsCnt, &ptRefTags));
    }
    else
    {
        tag_t tRefTag = NULLTAG;

        /* Get reference property value */
        LMI_ITKCALL(iRetCode, AOM_ask_value_tag(tObject, objOEDSubSegment.strRefRelName.c_str(), &tRefTag));

        if (tRefTag != NULLTAG && iRetCode == ITK_ok)
        {
            LMI_UPDATE_TAG_ARRAY(iRefTagsCnt, ptRefTags, tRefTag);
        }  
    }

    if (iRefTagsCnt > 0 && iRetCode == ITK_ok)
    {
        int    iRelevantTagsCnt = 0;
        tag_t* ptRelevantTags   = NULL;

        /* Filter retrieved objects for a specific object type */
        LMI_ITKCALL(iRetCode, LMI_obj_get_objects_of_input_type(iRefTagsCnt, ptRefTags, objOEDSubSegment.strObjectType.c_str(), &iRelevantTagsCnt, &ptRelevantTags));

        if (iRelevantTagsCnt > 0 && iRetCode == ITK_ok)
        {
            LMI_process_OED_depth objProcessDepth;

            /* Process depth */
            LMI_ITKCALL(iRetCode, objProcessDepth.LMI_process_depth(objOEDSubSegment.strDepth, iRelevantTagsCnt, ptRelevantTags, refvTags));
        }

        LMI_MEM_TCFREE(ptRelevantTags);
    }

    LMI_MEM_TCFREE(ptRefTags);
    return iRetCode;
}

/*
 * This function find all the child bom lines upto a given BOM level for a given item revision object and then retrieve item revision object corrresponding to each of
 * these child BOM line.
 *
 * @param[in]  tObject           Target object tag.
 * @param[in]  objOEDSubSegment  OED sub segment. (Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                 not change the value being referenced.)
 * @param[out] refOEDObjInfo     Resulted object which contains final tags, open BOM window tags and BOM line tags.
 * @param[out] refvTags          Vector of tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIProcessOpRef::LMI_process_bom
(
    tag_t tObject,
    const LMIOEDSubSegment& objOEDSubSegment,
    LMIOEDObjectInfo& refOEDObjInfo,
    vector<tag_t>& refvTags
)
{
    int   iRetCode   = ITK_ok;
    int   iBOMLevel  = atoi(objOEDSubSegment.strBOMLineLevel.c_str());

    if (refOEDObjInfo.bIsBLAttr == false)
    {
        tag_t tBOMWindow = NULLTAG;
        tag_t tTopLine   = NULLTAG;

        /* Get top line */
        LMI_ITKCALL(iRetCode, LMI_bomline_get_top_line(tObject, objOEDSubSegment.strRevisionRule.c_str(), objOEDSubSegment.strObjectType.c_str(), &tBOMWindow, &tTopLine));

        if (tTopLine != NULLTAG && tBOMWindow != NULLTAG && iRetCode == ITK_ok)
        {
            vector<tag_t> vtAllLines;

            /* Get child lines */
            LMI_ITKCALL(iRetCode, LMI_get_child_lines_by_level(tTopLine, 0, iBOMLevel, objOEDSubSegment.bExactBOMLevel, vtAllLines));

            if (iRetCode == ITK_ok)
            {
                if ((objOEDSubSegment.bExactBOMLevel == false) || (objOEDSubSegment.bExactBOMLevel == true && iBOMLevel == 0))
                {
                    vtAllLines.push_back(tTopLine);
                }

                vector<tag_t>::iterator iterBOMLine;
                /* Loop over bom lines to find their corresponding item revision */
                for (iterBOMLine = vtAllLines.begin(); iterBOMLine < vtAllLines.end() && iRetCode == ITK_ok; iterBOMLine++)
                {
                    tag_t tItemRev = NULLTAG;

                    /* Get Item revision */
                    LMI_ITKCALL(iRetCode, LMI_get_item_rev_for_bom_line((*iterBOMLine), &tItemRev));

                    if (tItemRev != NULLTAG && iRetCode == ITK_ok)
                    { 
                        refvTags.push_back(tItemRev);       
                    }
                }
            }

            /* Closing BOM window */
            LMI_ITKCALL(iRetCode, BOM_close_window(tBOMWindow));
        }
    }
    else
    {
        LMIBOMInfo objBOMInfo;

        /* Get top line */
        LMI_ITKCALL(iRetCode, LMI_bomline_get_top_line(tObject, objOEDSubSegment.strRevisionRule.c_str(), objOEDSubSegment.strObjectType.c_str(), &objBOMInfo.tBOMWindow, &objBOMInfo.tTopLine));

        if (objBOMInfo.tTopLine != NULLTAG && objBOMInfo.tBOMWindow != NULLTAG && iRetCode == ITK_ok)
        { 
            /* Get Item revision */
            LMI_ITKCALL(iRetCode, LMI_get_item_rev_for_bom_line(objBOMInfo.tTopLine, &objBOMInfo.tTopLineItemRev));

            if (objBOMInfo.tTopLineItemRev != NULLTAG && iRetCode == ITK_ok)
            { 
                vector<tag_t> vtAllLines;

                /* Get child lines */
                LMI_ITKCALL(iRetCode, LMI_get_child_lines_by_level(objBOMInfo.tTopLine, 0, iBOMLevel, objOEDSubSegment.bExactBOMLevel, vtAllLines));  

                vector<tag_t>::iterator iterBOMLine;
                /* Loop over bom lines to find their corresponding item revision */
                for (iterBOMLine = vtAllLines.begin(); iterBOMLine < vtAllLines.end() && iRetCode == ITK_ok; iterBOMLine++)
                {
                    tag_t tItemRev = NULLTAG;

                    /* Get Item revision */
                    LMI_ITKCALL(iRetCode, LMI_get_item_rev_for_bom_line((*iterBOMLine), &tItemRev));

                    if (tItemRev != NULLTAG && iRetCode == ITK_ok)
                    { 
                        LMIBLInfo objBLInfo;

                        objBLInfo.tBOMLine = (*iterBOMLine);

                        objBLInfo.tBLItemRev = tItemRev;

                        objBOMInfo.vLMIBLInfo.push_back(objBLInfo);     
                    }
                }

                refOEDObjInfo.vLMIBOMInfos.push_back(objBOMInfo);
            }
        }     
    }

    return iRetCode;
}

/*
 * This function will Traverse and expand the BOM of input BOM Top line to the provide BOM level depth which is provided as input argument. 
 *
 * @param[in]  tTopLine       Top line tag.
 * @param[in]  iCurrentLevel  Current BOM level.
 * @param[in]  iEndLevel      BOM level until we need to get child lines.
 * @param[in]  bExactBOMLevel Boolean value to indicate whether we need to get child lines only from a specific level or all child lines untill that level.
 * @param[out] refvTags       Vector of tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIProcessOpRef::LMI_get_child_lines_by_level
(
    tag_t tTopLine,
    int   iCurrentLevel,
    int   iEndLevel,
    bool  bExactBOMLevel,
    vector<tag_t>& refvtChildLines
)
{
    int iRetCode = ITK_ok;                
    int iCount   = 0;
    
    if(iCurrentLevel < iEndLevel)
    {
        iCurrentLevel = iCurrentLevel +  1;
        
        tag_t* ptChildBL = NULL;

        /* Get the child lines */
        LMI_ITKCALL(iRetCode, BOM_line_ask_child_lines(tTopLine, &iCount, &ptChildBL));

        if (iCount > 0 && iRetCode == ITK_ok)
        {
            for (int iDx = 0; iDx < iCount && iRetCode == ITK_ok; iDx++)
            {
                if (bExactBOMLevel == false || ( bExactBOMLevel == true && iCurrentLevel == iEndLevel ))
                {
                    refvtChildLines.push_back(ptChildBL[iDx]);
                }

                LMI_ITKCALL(iRetCode, LMI_get_child_lines_by_level(ptChildBL[iDx], iCurrentLevel, iEndLevel, bExactBOMLevel, refvtChildLines));
            }
        }

        LMI_MEM_TCFREE(ptChildBL); 

    }

    return iRetCode;
}


