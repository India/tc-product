/*======================================================================================================================================================================
                                                     Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_process_op_grms2prel.cxx

    Description: .

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
09-DEC-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#include <lmi_oed_operation_ext.hxx>
#include <lmi_library.hxx>

/*
 * This function finds all primary objects of specified type that are attached to target object with the specified relation and process them by depth. Then it
 *  finds the tags of relations that exist between these primary objects and given target object(being secondary). 
 *
 * @param[in]  tObject           target object tag.
 * @param[in]  objOEDSubSegment  OED string sub segment. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                 not change the value being referenced.)
 * @param[out] refvTags          Vector of tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_op_grms2prel::LMI_process_prefix_grms2prel
(
    tag_t tObject,
    const LMIOEDSubSegment& objOEDSubSegment,
    vector<tag_t>& refvTags
)
{
    int    iRetCode      = ITK_ok;
    int    iPrimObjCnt    = 0;
    tag_t  tRelationType = NULLTAG;
    tag_t* ptPrimObjs     = NULL;

    LMI_ITKCALL(iRetCode, GRM_find_relation_type((char*)(objOEDSubSegment.strRefRelName.c_str()), &tRelationType)); 

    /* Get primary objects with specified relation type and object type */
    LMI_ITKCALL(iRetCode, LMI_grm_get_related_obj(tObject, (char*)(objOEDSubSegment.strRefRelName.c_str()), (char*)(objOEDSubSegment.strObjectType.c_str()), NULL, true, &ptPrimObjs, &iPrimObjCnt));

    if (iPrimObjCnt > 0 && iRetCode == ITK_ok)
    {
        vector<tag_t> vtPrimObjTags;
        LMI_process_OED_depth objProcessDepth;
        
        /* Process depth */
        LMI_ITKCALL(iRetCode, objProcessDepth.LMI_process_depth(objOEDSubSegment.strDepth, iPrimObjCnt, ptPrimObjs, vtPrimObjTags));

        for (int iDx = 0; iDx < vtPrimObjTags.size() && iRetCode == ITK_ok; iDx++)
        {
            tag_t tRelTag = NULLTAG;

            /* Find relation tag between primary and secondary object */
            LMI_ITKCALL(iRetCode, GRM_find_relation(vtPrimObjTags[iDx], tObject, tRelationType, &tRelTag));

            if (tRelTag != NULLTAG && iRetCode == ITK_ok)
            {
                refvTags.push_back(tRelTag);
            }
        }
    }

    LMI_MEM_TCFREE(ptPrimObjs);
    return iRetCode;
}

