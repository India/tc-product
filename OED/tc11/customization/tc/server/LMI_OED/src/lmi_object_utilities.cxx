/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_object_utilities.cxx

    Description:  This File contains custom functions for POM classes to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>

/**
 * This function will get the tag of the status if input object "tObject" has status of type "pszStatusName" attached to it.
 *
 * @param[in]  tObject       Tag of object whose release status has to be checked.
 * @param[in]  pszStatusName Name of status.
 * @param[out] ptStatusTag   Status tag if input status "pszStatusName" is attached to input object "tObject".
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_obj_ask_status_tag
(
    tag_t  tObject,
    char*  pszStatusName,
    tag_t* ptStatusTag
)
{
    int    iRetCode          = ITK_ok;
    int    iNoOfStatuses     = 0;
    char*  pszRelStatusType  = NULL;
    tag_t  item_rev          = NULLTAG;
    tag_t* ptStatusList      = NULL;

    /* Initializing out parameter of this function. */
    (*ptStatusTag) = NULLTAG;

    LMI_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, &iNoOfStatuses, &ptStatusList));
    
    for (int iDx = 0; iDx < iNoOfStatuses && iRetCode == ITK_ok; iDx++)
    {
        LMI_ITKCALL(iRetCode, RELSTAT_ask_release_status_type(ptStatusList[iDx], &pszRelStatusType));
        
        if(tc_strcmp(pszStatusName, pszRelStatusType) == 0)
        {
              (*ptStatusTag) = ptStatusList[iDx];
              break;
        }
    }
      
    LMI_MEM_TCFREE(pszRelStatusType);
    LMI_MEM_TCFREE(ptStatusList);
    return iRetCode;
}

/**
 * This function gets the name of type for the input business objet.
 *
 * e.g. Consider Item type 'LMI_ProjectRevision'. If we pass tag to an instance of 'LMI_ProjectRevision' this function will return string 'LMI_ProjectRevision'
 *      and not 'ItemRevision'. Consider Dataset of type 'catia'. If we pass tag to an instance of 'catia' this function will return 'catia' and not 'Dataset'.
 *
 * @param[in]  tObject     Business Object tag
 * @param[out] ppszObjType Object type
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_obj_ask_type
(
    tag_t  tObject,
    char** ppszObjType
)
{
    int   iRetCode     = ITK_ok;
    tag_t tObjectType  = NULLTAG;

    /* Initializing out parameter of this function. */
    (*ppszObjType) = NULL;
    
    LMI_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tObjectType));
    
    LMI_ITKCALL(iRetCode, TCTYPE_ask_name2(tObjectType, ppszObjType));

    return iRetCode;
}

/**
 * This function gets the Display name of input business object Type.
 *
 * e.g. Consider if ItemRevision type 'LMI_ProjectRevision' is pass to this function then the Function will return the display name of this Item type i.e. 'Project Container Revision'
 *
 * @param[in]  pszType             Business object data model Type Name
 * @param[out] ppszTypeDisplayName Business object display name
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_obj_ask_type_display_name
(
    char*  pszType,
    char** ppszTypeDisplayName
)
{
    int   iRetCode = ITK_ok;
    tag_t tTypeTag = NULLTAG;

    /* Initializing out parameter of this function. */
    (*ppszTypeDisplayName) = NULL;
    
    LMI_ITKCALL(iRetCode, TCTYPE_find_type(pszType, NULL, &tTypeTag));
    
    LMI_ITKCALL(iRetCode, TCTYPE_ask_display_name(tTypeTag, ppszTypeDisplayName));
   
    return iRetCode;
}

/**
 * Save and unlock the given object
 *
 * @note The object must be an instance of Application Object class or a subclass of that.
 *
 * @param[in] tObject tag of object to be saved and unloaded
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_obj_save_and_unlock
(
    tag_t tObject
)
{
    int iRetCode  = ITK_ok;

    /*  Save the object.  */
    LMI_ITKCALL(iRetCode, AOM_save( tObject));
   
    /* Unlock the object. */
    LMI_ITKCALL(iRetCode, AOM_unlock( tObject));
    
    return iRetCode;
}

/**
 * Set the string attribute value.
 *
 * @note The object must be an instance of Application Object class or a subclass of that.
 *
 * @param[in] tObject         Tag of object to be saved and unloaded
 * @param[in] pszPropertyName Name of the property
 * @param[in] pszPropVal      Value of the property to be set
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_obj_set_str_attribute_val
(
    tag_t tObject,
    char* pszPropertyName,
    char* pszPropVal
)
{
    int iRetCode = ITK_ok;

    /*  Lock the object.  */
    LMI_ITKCALL(iRetCode, AOM_refresh(tObject, true));
   
    /* Set the attribute string value. */    
    LMI_ITKCALL(iRetCode, AOM_set_value_string(tObject, pszPropertyName, pszPropVal));

    /*  Save the object.  */
    LMI_ITKCALL(iRetCode, AOM_save(tObject));

    /*  Unlock the object.  */
    LMI_ITKCALL(iRetCode, AOM_refresh(tObject, false));

    return iRetCode;
}

/**
 * Function to set reference(tag) value on object
 *
 * @param[in] tObject         Tag of object 
 * @param[in] pszPropertyName Name of the property
 * @param[in] tAttrVal        Value of the property to be set
 *
 * @note The object must be an instance of Application Object class or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_obj_set_tag_value
(
    tag_t tObject,
    char* pszPropertyName,
    tag_t tAttrVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    LMI_ITKCALL(iRetCode, AOM_refresh(tObject, true));
   
    /* Set the attribute Tag value.  */    
    LMI_ITKCALL(iRetCode, AOM_set_value_tag(tObject, pszPropertyName, tAttrVal));

    /*  Save the object.  */
    LMI_ITKCALL(iRetCode, AOM_save(tObject));

    /*  Unlock the object.  */
     LMI_ITKCALL(iRetCode, AOM_refresh(tObject, false));

    return iRetCode;
}

/**
 * This function validates if input Object is one of the sub type of the type that is supplied as input argument 'pszTypeName' or if input object type is of the same type that is supplied to
 * this function.
 *
 * @param[in]  tObject               Object Tag
 * @param[in]  pszTypeName           Name of the Type
 * @param[out] pbBelongsToInputClass Flag indicating whether 'tObject' is the same as 'pszTypeName' or is a subtype of 'pszTypeName'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int LMI_obj_is_type_of
(
    tag_t    tObject,
    char*    pszTypeName,
    logical* pbBelongsToInputClass
)
{
    int   iRetCode      = ITK_ok;  
    tag_t tType         = NULLTAG;
    tag_t tInputObjType = NULLTAG;

    LMI_ITKCALL(iRetCode, TCTYPE_find_type(pszTypeName, NULL, &tType));

    LMI_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tInputObjType));

    LMI_ITKCALL(iRetCode, TCTYPE_is_type_of(tInputObjType, tType, pbBelongsToInputClass));
    
    return iRetCode;
}

/**
 * This function validates if input Object is type of the type that is supplied as input argument 'pszObjType' or not.
 *
 * @param[in]  tObject     Object Tag
 * @param[in]  pszObjType  Name of the Type
 * @param[out] pbSameType  Flag indicating whether 'tObject' is the same as 'pbSameType' or not.
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int LMI_obj_is_same_type
(
    tag_t       tObject,
    const char* pszObjType,
    logical*    pbSameType
)
{
    int   iRetCode      = ITK_ok;
    char* pszObjectType = NULL;

    (*pbSameType) = false;   

    LMI_ITKCALL(iRetCode, LMI_obj_ask_type(tObject, &pszObjectType));

    if (tc_strcmp( pszObjectType, pszObjType) == 0 && iRetCode == ITK_ok)
    {
        (*pbSameType) = true; 
    }

    LMI_MEM_TCFREE(pszObjectType);

    return iRetCode;
}

/**
 * This function validates if "pszSrcType" is one of the sub type of "pszDestType" that is supplied as input argument if "pszSrcType" is of the same type
 * as "pszDestType" that is supplied to this function.
 *
 * @param[in]  pszSrcType            Source type name
 * @param[in]  pszDestType           Destination type name
 * @param[out] pbBelongsToInputClass Flag indicating whether 'pszSrcType' is of same type as 'pszDestType' or if 'pszSrcType' is subtype of 'pszDestType'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int LMI_obj_is_type_of_2
(
    char*    pszSrcType,
    char*    pszDestType,
    logical* bIsNotBOType,
    logical* pbBelongsToInputClass
)
{
    int   iRetCode  = ITK_ok;  
    tag_t tSrcType  = NULLTAG;
    tag_t tDestType = NULLTAG;

    /* Initializing out parameter of this function. */
    (*bIsNotBOType) = false;
    (*pbBelongsToInputClass) = false;

    LMI_ITKCALL(iRetCode, TCTYPE_find_type(pszSrcType, NULL, &tSrcType));

    LMI_ITKCALL(iRetCode, TCTYPE_find_type(pszDestType, NULL, &tDestType));

    if(tSrcType != NULLTAG && tDestType != NULLTAG)
    {
        LMI_ITKCALL(iRetCode, TCTYPE_is_type_of(tSrcType, tDestType, pbBelongsToInputClass));
    }
    else
    {
        (*bIsNotBOType) = true;
    }
    
    return iRetCode;
}

/**
 * This function validated if input object has write access and object is not checked-out. If object does not full fill the validation criteria
 * then function will return the modifiable information as 'false'.
 * 
 * @param[in]  tObject             Tag of Object to be validated
 * @param[out] pbWriteAccessDenied 'true' if Object has write access  
 * @param[out] pbObjCheckedOut     'true' if Object is not checked out
 * @param[out] pbModifiable        'true' if object is Modifiable i.e. Object has write access and is not checked-out
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI_check_privilege
(
    tag_t    tObject,
    logical* pbWriteAccessDenied,
    logical* pbObjCheckedOut,
    logical* pbModifiable
)
{
    int     iRetCode = ITK_ok;
    logical bAccess  = false;

    /* Initializing out parameter of this function. */
    (*pbModifiable)        = false;
    (*pbWriteAccessDenied) = false;
    (*pbObjCheckedOut)     = false;

    LMI_ITKCALL(iRetCode, RES_assert_object_modifiable (tObject));

    LMI_ITKCALL(iRetCode, AM_check_privilege (tObject, LMI_WRITE_ACCESS, &bAccess));

    if(iRetCode == ITK_ok)
    {
        if(bAccess == false)
        {
            char* pszAttrVal = NULL;

            (*pbModifiable)        = false;
            (*pbWriteAccessDenied) = true;
            
            LMI_ITKCALL(iRetCode, AOM_ask_value_string(tObject, LMI_ATTR_OBJECT_STRING, &pszAttrVal));

            TC_write_syslog ("Write access is denied on %s. \n", pszAttrVal);

            LMI_MEM_TCFREE(pszAttrVal);
        }
        else
        {
            logical bCheckedOut = false;

            LMI_ITKCALL(iRetCode, RES_is_checked_out (tObject, &bCheckedOut));

            if(bCheckedOut == false)
            {
                (*pbModifiable) = true;
            }
            else
            {
                char* pszUser         = NULL;
                tag_t tReservationObj = NULLTAG;
                tag_t tCheckOutUser   = NULLTAG;
                tag_t tUser           = NULLTAG;

                LMI_ITKCALL(iRetCode, RES_ask_reservation_object (tObject, &tReservationObj));

                LMI_ITKCALL(iRetCode, RES_ask_user (tReservationObj, &tCheckOutUser));

                LMI_ITKCALL(iRetCode, POM_get_user (&pszUser, &tUser));

                if (tUser != NULLTAG && tUser == tCheckOutUser)
                {
                    char* pszObjAttrVal = NULL;

                    LMI_ITKCALL(iRetCode, AOM_ask_value_string(tObject, LMI_ATTR_OBJECT_STRING, &pszObjAttrVal));

                    TC_write_syslog ("%s object is checked-out by same user\n", pszObjAttrVal);

                    (*pbModifiable) = true;

                    LMI_MEM_TCFREE(pszObjAttrVal);
                } 
                else 
                {
                    (*pbModifiable)    = false;
                    (*pbObjCheckedOut) = true;
                }

                LMI_MEM_TCFREE(pszUser);
            }
        }
    }

    return iRetCode;
}

/**
 * This function traverse through input list of objects and find the very first object that matches the type "pszObjType". This function is capable of matching the exact type or to find if any
 * one of the object belongs to input type "pszObjType"
 * 
 * @param[in]  iObjCnt      Count of Object 
 * @param[in]  ptObject     List of objects
 * @param[in]  pszObjType   Object type name 
 * @param[out] ptCorrectObj Tag of object which belongs to input type "pszObjType"
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI_obj_get_obj_of_input_type
(
    int    iObjCnt,
    tag_t* ptObject,
    char*  pszObjType,
    tag_t* ptCorrectObj
)
{
    int iRetCode = ITK_ok;
    
    /* Initializing out parameter of this function. */
    (*ptCorrectObj) = NULLTAG;

    for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
    {
        logical bIsCorrectType = false;

        LMI_ITKCALL(iRetCode, LMI_obj_is_type_of(ptObject[iDx], pszObjType, &bIsCorrectType));

        if(bIsCorrectType == true && iRetCode == ITK_ok)
        {
            (*ptCorrectObj) = ptObject[iDx];
            break;
        }
    }

    return iRetCode;
}

int LMI_obj_get_objects_of_input_type
(
    int         iObjCnt,
    tag_t*      ptObject,
    const char* pszObjType,
    int*        piCorrectObjCount,
    tag_t**     pptCorrectObj
)
{
    int iRetCode = ITK_ok;
    
    /* Initializing out parameter of this function. */
    (*pptCorrectObj)     = NULL;
    (*piCorrectObjCount) = 0;

    for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
    {
        logical bSameType = false;

        LMI_ITKCALL(iRetCode, LMI_obj_is_same_type(ptObject[iDx], pszObjType, &bSameType));

        if(bSameType == true && iRetCode == ITK_ok)
        {
            LMI_UPDATE_TAG_ARRAY((*piCorrectObjCount), (*pptCorrectObj), ptObject[iDx]);
        }
    }

    return iRetCode;
}


/**
 * This function gets the attribute value for input TC object in string format.
 * This function will convert value of following attribute type to string value:
 *      - String
 *      - Integer
 *      - Float
 *      - Double
 *      - Logical
 * 
 * @param[in]   tObject           Tag of Object whose attribute value needs to be extracted
 * @param[n]    pszAttributeName  Name of Attribute of input object
 * @param[out]  ppszAttrValue     String Value of Attribute
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI_obj_aom_get_attribute_value
(
    tag_t  tObject,
    char*  pszAttributeName,
    char** ppszAttrValue
)
{
    int   iRetCode   = ITK_ok;
    char* pszValType = NULL;
    PROP_value_type_t ValueTypeOfProp = PROP_untyped;

    /* Initializing the Out Parameter to this function. */
    (*ppszAttrValue) = NULL;

    /* Get the type of the property value */
    LMI_ITKCALL(iRetCode, AOM_ask_value_type(tObject, pszAttributeName, &ValueTypeOfProp, &pszValType));

    if(iRetCode == ITK_ok && ValueTypeOfProp != PROP_string)
    {
        (*ppszAttrValue) = (char*) MEM_alloc(sizeof (char) * LMI_STRING_ATTR_LEN);
    }

    /* Reset the property based on its value type */
    if (iRetCode == ITK_ok)
    {
        switch(ValueTypeOfProp)
        {
            case PROP_string :
            case PROP_note   :
                {
                    LMI_ITKCALL(iRetCode, AOM_ask_value_string(tObject, pszAttributeName, ppszAttrValue));
                    break;
                }
            case PROP_logical :
                {
                    logical bYesNo = false;

                    LMI_ITKCALL(iRetCode, AOM_ask_value_logical(tObject, pszAttributeName, &bYesNo));

                    bYesNo ? sprintf(*ppszAttrValue , "%s" , LMI_CAPS_YES) : sprintf (*ppszAttrValue , "%s", LMI_CAPS_NO);
                    break;
                }
            case PROP_int   :
            case PROP_short :
                {
                    int iValue = 0;

                    LMI_ITKCALL(iRetCode, AOM_ask_value_int(tObject, pszAttributeName, &iValue));

                    if(iValue == 0)
                    {
                        LMI_STRCPY(*ppszAttrValue, LMI_STRING_BLANK);
                    }
                    else
                    {
                        sprintf(*ppszAttrValue, "%d",  iValue);
                    }
                    break;
                }
            case PROP_float  :
            case PROP_double :
                {
                    double dValue = 0.0;

                    LMI_ITKCALL(iRetCode, AOM_ask_value_double(tObject, pszAttributeName, &dValue));

                    if(dValue == 0.0)
                    {
                        LMI_STRCPY(*ppszAttrValue, LMI_STRING_BLANK);
                    }
                    else
                    {
                        sprintf (*ppszAttrValue, "%f",  dValue);
                    }

                    break;
                }
            case PROP_typed_reference    :
            case PROP_untyped_reference  :
            case PROP_external_reference :
            case PROP_typed_relation     :
            case PROP_untyped_relation   :
                {
                    tag_t tValue         = NULLTAG;
                    char* pszStringVal = NULL;

                    LMI_ITKCALL(iRetCode, AOM_ask_value_tag(tObject, pszAttributeName, &tValue));

                    if(tValue != NULLTAG)
                    {
                        LMI_ITKCALL(iRetCode, PROP_tag_to_string(tValue, &pszStringVal));

                        LMI_STRCPY(*ppszAttrValue, pszStringVal);
                    }
                    else
                    {
                        LMI_STRCPY(*ppszAttrValue, LMI_STRING_BLANK);
                    }

                    LMI_MEM_TCFREE(pszStringVal);
                    break;
                }
            case PROP_date :
                {
                    char*  pszDateFormat = NULL;
                    date_t dValue        = NULLDATE;

                    LMI_ITKCALL(iRetCode, AOM_ask_value_date(tObject, pszAttributeName, &dValue));

                    if( DATE_IS_NULL(dValue) != 1)
                    {
                        LMI_ITKCALL(iRetCode, DATE_default_date_format(&pszDateFormat));    

                        LMI_ITKCALL(iRetCode, DATE_date_to_string(dValue, pszDateFormat, ppszAttrValue ));
                    }
                    else
                    {
                        LMI_STRCPY(*ppszAttrValue, LMI_STRING_BLANK);
                    }

                    LMI_MEM_TCFREE(pszDateFormat);
                    break;
                }
        }
    }

    LMI_MEM_TCFREE(pszValType);
    return iRetCode;
}

/**
 * This function first checks if the value to be set for an attribute is already an existing value for that attribute. If this is the
 * the situation then value is not set or else set the string attribute value.
 *
 * @note The object must be an instance of Application Object class or a subclass of that.
 *
 * @param[in] tObject         Tag of object to be saved and unloaded
 * @param[in] pszPropertyName Name of the property
 * @param[in] pszPropVal      Value of the property to be set
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_obj_check_and_set_str_attribute_val
(
    tag_t tObject,
    char* pszPropertyName,
    char* pszPropVal
)
{
    int     iRetCode           = ITK_ok;
    logical bWriteAccessDenied = false;
    logical bObjCheckedOut     = false;
    logical bModifiable        = false;

    LMI_ITKCALL(iRetCode, LMI_check_privilege(tObject, &bWriteAccessDenied, &bObjCheckedOut, &bModifiable));

    if(bModifiable == true)
    {
        char* pszVal = NULL;

        LMI_ITKCALL(iRetCode, AOM_ask_value_string(tObject, pszPropertyName, &pszVal));

        if(tc_strcmp(pszVal, pszPropVal) != 0)
        {
            /*  Lock the object.  */
            LMI_ITKCALL(iRetCode, AOM_refresh(tObject, true));
   
            /*
             * Set the attribute string value.
             */    
            LMI_ITKCALL(iRetCode, AOM_set_value_string(tObject, pszPropertyName, pszPropVal));

            /*  Save the object.  */
            LMI_ITKCALL(iRetCode, AOM_save(tObject));

            /*  Unlock the object.  */
            LMI_ITKCALL(iRetCode, AOM_refresh(tObject, false));
        }

        LMI_MEM_TCFREE(pszVal);
    }
    else
    {
        char* pszObjAttrVal = NULL;

        LMI_ITKCALL(iRetCode, AOM_ask_value_string(tObject, LMI_ATTR_OBJECT_STRING, &pszObjAttrVal));

        TC_write_syslog("Warning: No write privileges on object '%s'.\n", pszObjAttrVal);

        LMI_MEM_TCFREE(pszObjAttrVal);
    }

    return iRetCode;
}

/**
 * This function sets the attribute value for input TC object.
 * This function can set value for following attribute types:
 *      - String
 *      - Integer
 *      - Float
 *      - Double
 *      - Logical
 *      - Date
 * 
 * @param[in]   tObject           Tag of Object whose attribute value needs to be set
 * @param[in]   pszAttributeName  Name of Attribute of input object
 * @param[in]   pszAttrValue      String Value of Attribute
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI_obj_aom_set_attribute_value
(
    tag_t tObject,
    char* pszAttributeName,
    char* pszAttrValue
)
{
    int               iRetCode        = ITK_ok;
    char*             pszValType      = NULL;
    PROP_value_type_t ValueTypeOfProp = PROP_untyped;

    /* Get the type of the property value */
    LMI_ITKCALL(iRetCode, AOM_ask_value_type(tObject, pszAttributeName, &ValueTypeOfProp, &pszValType));

    /* Set the property based on its value type */
    if (iRetCode == ITK_ok)
    {
        switch(ValueTypeOfProp)
        {
            case PROP_string :
            case PROP_note   :
                {
                    LMI_ITKCALL(iRetCode, AOM_set_value_string(tObject, pszAttributeName, pszAttrValue));
                    break;
                }
            case PROP_logical :
                {
                    logical bYesNo = tc_strcmp(pszAttrValue, LMI_CAPS_YES) == 0 ? true : false;

                    LMI_ITKCALL(iRetCode, AOM_set_value_logical(tObject, pszAttributeName, bYesNo));
                    break;
                }
            case PROP_int   :
            case PROP_short :
                {
                    int iValue = atoi(pszAttrValue);

                    LMI_ITKCALL(iRetCode, AOM_set_value_int(tObject, pszAttributeName, iValue));
                    break;
                }
            case PROP_float  :
            case PROP_double :
                {
                    double dValue = atof(pszAttrValue);

                    LMI_ITKCALL(iRetCode, AOM_set_value_double(tObject, pszAttributeName, dValue));
                    break;
                }
            case PROP_date :
                {
                    logical  isValid = false;
                    date_t   dValue  = NULLDATE;
                    
                    LMI_ITKCALL(iRetCode, DATE_string_to_date_t(pszAttrValue, &isValid, &dValue));

                    if (isValid && DATE_IS_NULL(dValue) != 1 && iRetCode ==ITK_ok)
                    {
                        LMI_ITKCALL(iRetCode, AOM_set_value_date(tObject, pszAttributeName, dValue));
                    }
                    break;
                }
        }
    }

    LMI_MEM_TCFREE(pszValType);
    return iRetCode;
}



/**
 * This function will get the tag of first found status if input object "tObject" has any status from list of types "pszStatusNames" attached to it.
 *
 * @param[in]  tObject        Tag of object whose release status has to be checked.
 * @param[in]  iValidStatus   Count of valid status list
 * @param[in]  pszStatusNames Name list of valid status.
 * @param[out] ptStatus       Status tag if any of input status from list "pszStatusNames" is attached to input object "tObject".
 * @param[out] pbFound        Reutrn whether status is found or not.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_obj_is_any_input_status_attached
(
    tag_t    tObject,
    int      iValidStatus,
    char**   ppszStatusNames,
    tag_t*   ptStatus,
    logical* pbFound
)
{
    int    iRetCode          = ITK_ok;
    int    iNoOfStatuses     = 0;
    tag_t* ptStatusList      = NULL;
     
    /* Initializing out parameter of this function. */
    (*ptStatus) = NULLTAG;
    (*pbFound)  = false;

    if (tObject != NULLTAG && iValidStatus > 0)
    {
        LMI_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, &iNoOfStatuses, &ptStatusList));

        if (iNoOfStatuses > 0 && iRetCode == ITK_ok)
        {
            for (int iJx = 0; iJx < iNoOfStatuses && (*pbFound) == false && iRetCode == ITK_ok; iJx++)
            {
                char* pszRelStatusType = NULL;

                LMI_ITKCALL(iRetCode, RELSTAT_ask_release_status_type(ptStatusList[iJx], &pszRelStatusType));

                for (int iKx = 0; iKx < iValidStatus && iRetCode == ITK_ok; iKx++)
                {
                    if(tc_strcmp(ppszStatusNames[iKx], pszRelStatusType) == 0)
                    {
                            (*pbFound)  = true;
                            (*ptStatus) = ptStatusList[iJx];
                            break;
                    }
                }

                LMI_MEM_TCFREE(pszRelStatusType);
            }
        }
    }

    LMI_MEM_TCFREE(ptStatusList);

    return iRetCode;
}

/**
 * For a given target, based on ppszAllowStatus parameter, function decides whether "LMI_CommPart" should be created or not.
 *
 * @param[in]  tObject         Tag of target object
 * @param[in]  iValidStatus    Valid status count
 * @param[in]  ppszValidStatus List of status that are valid for target object
 * @param[out] ptStatus        Tag of valid status found on target.
 * @param[out] pbFound         Whether status from valid status list is found on target or not.
 * @param[out] pbValid         Whether target is valid or not as per valid status values
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int LMI_check_against_allowed_status
(
    tag_t    tObject,
    int      iValidStatus,
    char**   ppszValidStatus,
    tag_t*   ptStatus,
    logical* pbFound,
    logical* pbValid
)
{
    int     iRetCode      = ITK_ok;
    int     iNoOfStatuses = 0;
    tag_t*  ptStatusList  = NULL;
    logical bNone         = false;
    logical bAny          = false;


    /* Initializing the out parameter of this function. */
    (*ptStatus) = NULLTAG;
    (*pbValid)  = false;
    (*pbFound)  = false;

    if (tObject != NULLTAG && iValidStatus > 0)
    {
        LMI_ITKCALL(iRetCode, LMI_validate_input_status_list(iValidStatus, ppszValidStatus, &bNone, &bAny));

        LMI_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, &iNoOfStatuses, &ptStatusList));

        if (iRetCode == ITK_ok)
        {
            //if valid status contains none or any
            if (bAny == true)
            {
                (*pbValid) = iNoOfStatuses > 0 ? true : false;
            }
            else if (bNone == true)
            {
                (*pbValid) = iNoOfStatuses == 0 ? true : false;
            }
            else if (bAny == false && bNone == false)
            {
                //if valid status contains actual status 
                for (int iJx = 0; iJx < iNoOfStatuses && (*pbValid) == false && iRetCode == ITK_ok; iJx++)
                {
                    char* pszRelStatusType = NULL;

                    LMI_ITKCALL(iRetCode, RELSTAT_ask_release_status_type(ptStatusList[iJx], &pszRelStatusType));

                    for (int iKx = 0; iKx < iValidStatus && iRetCode == ITK_ok; iKx++)
                    {
                        if(tc_strcmp(ppszValidStatus[iKx], pszRelStatusType) == 0)
                        {
                                (*pbValid)  = true; 
                                (*pbFound)  = true;
                                (*ptStatus) = ptStatusList[iJx];
                                break;
                        }
                    }

                    LMI_MEM_TCFREE(pszRelStatusType);
                }
            }
        }
    }

    LMI_MEM_TCFREE(ptStatusList);

    return iRetCode;
}

/**
 * This function checks whether given list of status names is valid or not, if not it sets the error code
 *
 * @param[in]  iValidStatus     Tag of object whose release status has to be checked.
 * @param[in]  ppszValidStatus  Count of valid status list
 * @param[in]  pbNone           
 * @param[in]  pbAny            
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_validate_input_status_list
(
    int      iValidStatus,
    char**   ppszValidStatus,
    logical* pbNone,
    logical* pbAny
)
{
    int iRetCode = ITK_ok;
    /* Initializing the out parameter of this function. */
    (*pbNone) = false;
    (*pbAny)  = false;
    
    LMI_ITKCALL(iRetCode, LMI_contains_string_value(LMI_WF_ARG_VAL_NONE, iValidStatus, ppszValidStatus, pbNone));

    LMI_ITKCALL(iRetCode, LMI_contains_string_value(LMI_WF_ARG_VAL_ANY, iValidStatus, ppszValidStatus, pbAny));

    if (((*pbNone) == true || (*pbAny) == true) && iValidStatus > 1 && iRetCode == ITK_ok)
    {
        iRetCode = INVALID_WORKFLOW_HANDLER_ARGUMENT_VALUE;

        EMH_store_error(EMH_severity_error, iRetCode);
    }

    return iRetCode;
}

int LMI_obj_aom_get_attribute_value2
(
    tag_t  tObject,
    string strAttributeName,
    LMIPropValue& refValueData
)
{
    int     iRetCode         = ITK_ok;
    int     iPropValCount    = 0;
    logical bIsPropTypeArray = false;
    char*   pszValType       = NULL;
    PROP_value_type_t ValueTypeOfProp = PROP_untyped;

    /* Get the type of the property value */
    LMI_ITKCALL(iRetCode, AOM_ask_value_type(tObject, strAttributeName.c_str(), &ValueTypeOfProp, &pszValType));
    
    LMI_ITKCALL(iRetCode, LMI_obj_is_property_type_array(tObject, strAttributeName.c_str(), bIsPropTypeArray));

     /* Set the property based on its value type */
    if (iRetCode == ITK_ok)
    {
        switch(ValueTypeOfProp)
        {
            case PROP_string :
            case PROP_note   :
                {
                    if (bIsPropTypeArray == false)
                    {
                        char* pszAttrValue = NULL;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_string(tObject, strAttributeName.c_str(), &pszAttrValue));

                        if (pszAttrValue != NULL && tc_strlen(pszAttrValue) > 0 && iRetCode == ITK_ok)
                        {
                            refValueData.vStrValue.push_back(pszAttrValue);
                        }

                        LMI_MEM_TCFREE(pszAttrValue);  
                    }
                    else
                    {
                        char** ppszAttrValue = NULL;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_strings(tObject, strAttributeName.c_str(), &iPropValCount, &ppszAttrValue));

                        if (iPropValCount > 0 && iRetCode == ITK_ok)
                        {
                            LMI_ITKCALL(iRetCode, LMI_vector_store_string_array(iPropValCount, ppszAttrValue, false, refValueData.vStrValue));
                        }

                        LMI_MEM_TCFREE(ppszAttrValue);       
                    }

                    break;
                }
            case PROP_logical :
                {
                    if (bIsPropTypeArray == false)
                    {
                        logical pbAttrValue = false;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_logical(tObject, strAttributeName.c_str(), &pbAttrValue));

                        if (iRetCode == ITK_ok)
                        {
                            refValueData.vLogicalValue.push_back(pbAttrValue);
                        }
                    }
                    else
                    {
                        logical* pbAttrValues = NULL;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_logicals(tObject, strAttributeName.c_str(), &iPropValCount, &pbAttrValues));

                        if (iPropValCount > 0 && iRetCode == ITK_ok)
                        {
                            for(int iDx = 0; iDx < iPropValCount; iDx++)
                            {
                                refValueData.vLogicalValue.push_back(pbAttrValues[iDx]);
                            }
                        }

                        LMI_MEM_TCFREE(pbAttrValues);       
                    }

                    break;
                }
            case PROP_int   :
            case PROP_short :
                {
                    if (bIsPropTypeArray == false)
                    {
                        int iAttrValue = 0;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_int(tObject, strAttributeName.c_str(), &iAttrValue));

                        if (iRetCode == ITK_ok)
                        {
                            refValueData.vIntValue.push_back(iAttrValue);
                        }
                    }
                    else
                    {
                        int* piAttrValues = NULL;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_ints(tObject, strAttributeName.c_str(), &iPropValCount, &piAttrValues));

                        if (iPropValCount > 0 && iRetCode == ITK_ok)
                        {
                            for(int iDx = 0; iDx < iPropValCount; iDx++)
                            {
                                refValueData.vIntValue.push_back(piAttrValues[iDx]);
                            }
                        }

                        LMI_MEM_TCFREE(piAttrValues);       
                    }
                  
                    break;
                }
            case PROP_float  :
            case PROP_double :
                {
                    if (bIsPropTypeArray == false)
                    {
                        double dAttrValue = 0;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_double(tObject, strAttributeName.c_str(), &dAttrValue));

                        if (iRetCode == ITK_ok)
                        {
                            refValueData.vDoubleValue.push_back(dAttrValue);
                        }
                    }
                    else
                    {
                        double* pdAttrValues = NULL;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_doubles(tObject, strAttributeName.c_str(), &iPropValCount, &pdAttrValues));

                        if (iPropValCount > 0 && iRetCode == ITK_ok)
                        {
                            for(int iDx = 0; iDx < iPropValCount; iDx++)
                            {
                                refValueData.vDoubleValue.push_back(pdAttrValues[iDx]);
                            }
                        }

                        LMI_MEM_TCFREE(pdAttrValues);       
                    }

                    break;
                }
            case PROP_typed_reference    :
            case PROP_untyped_reference  :
            case PROP_external_reference :
            case PROP_typed_relation     :
            case PROP_untyped_relation   :
                {
                    if (bIsPropTypeArray == false)
                    {
                        tag_t tAttrValue = NULLTAG;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_tag(tObject, strAttributeName.c_str(), &tAttrValue));

                        if (tAttrValue != NULLTAG && iRetCode == ITK_ok)
                        {
                            refValueData.vTagValue.push_back(tAttrValue);
                        }
                    }
                    else
                    {
                        tag_t* ptAttrValue = NULL;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_tags(tObject, strAttributeName.c_str(), &iPropValCount, &ptAttrValue));

                        if (iPropValCount > 0)
                        {
                            for(int iDx = 0; iDx < iPropValCount; iDx++)
                            {
                                refValueData.vTagValue.push_back(ptAttrValue[iDx]);
                            }
                        }

                        LMI_MEM_TCFREE(ptAttrValue);       
                    }

                    break;
                }
            case PROP_date :
                {
                    if (bIsPropTypeArray == false)
                    {
                        date_t dateAttrValue = NULLDATE;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_date(tObject, strAttributeName.c_str(), &dateAttrValue));

                        if (DATE_IS_NULL(dateAttrValue) != 1 && iRetCode == ITK_ok)
                        {
                            refValueData.vDateValue.push_back(dateAttrValue);
                        }
                    }
                    else
                    {
                        date_t* pdateAttrValues = NULL;

                        LMI_ITKCALL(iRetCode, AOM_ask_value_dates(tObject, strAttributeName.c_str(), &iPropValCount, &pdateAttrValues));

                        if (iPropValCount > 0 && iRetCode == ITK_ok)
                        {
                            for(int iDx = 0; iDx < iPropValCount; iDx++)
                            {
                                refValueData.vDateValue.push_back(pdateAttrValues[iDx]);
                            }
                        }

                        LMI_MEM_TCFREE(pdateAttrValues);  
                    }
               
                    break;
                }
        }
    }

    LMI_MEM_TCFREE(pszValType);

    return iRetCode;
}


/**
 * This function returns "true" if the input property "strAttrName" of object "tObject" is an array.  
 *
 * @param[in]  tObject             Business Object tag
 * @param[in]  strAttrName         Name of Property
 * @param[out] refbIsPropTypeArray 'true' if input property is of type array.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_obj_is_property_type_array
(
    tag_t    tObject,
    string   strAttrName,
    logical& refbIsPropTypeArray
)
{
    int iRetCode        = ITK_ok;
    int iMaxNumElements = 0;
        
    LMI_ITKCALL(iRetCode, AOM_ask_max_num_elements(tObject, (strAttrName.c_str()), &iMaxNumElements));

    if (iRetCode == ITK_ok)
    {
        /* Set multiFlag based on maxNumElements results
                  (-1) list property
                  (1)  single value
                  (positive)  array property
        */
        if(iMaxNumElements != 1)
        {
            refbIsPropTypeArray = true;
        }
    }

    return iRetCode;
}


int LMI_strings_to_tags
(
    const vector<string>& vstrUIDs,
    vector<tag_t>& refvtObjects
)
{
    int iRetCode = ITK_ok;
  
    vector<string>::const_iterator iterStrUID;

    /* Running loop on OED string objects */
    for(iterStrUID = vstrUIDs.begin(); iterStrUID < vstrUIDs.end() && iRetCode == ITK_ok; iterStrUID++)
    {
        tag_t tObject  = NULLTAG; 
    
        LMI_ITKCALL(iRetCode, POM_string_to_tag(iterStrUID->c_str(), &tObject));

        if (tObject!= NULLTAG && iRetCode == ITK_ok)
        {
            refvtObjects.push_back(tObject);
        }
    }

    return iRetCode;
}


