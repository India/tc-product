/*======================================================================================================================================================================
                                                     Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_process_op_refby.cxx

    Description: This file contains function that is used to process REFBY prefix.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
09-DEC-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#include <lmi_oed_operation_ext.hxx>
#include <lmi_library.hxx>

/*
 * This function finds all objects of given type that contain references to given target object and then process depth. 
 *
 * @param[in]  tObject           Target object.
 * @param[in]  objOEDSubSegment  OED string sub segment object. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                 not change the value being referenced.)
 * @param[out] refvTags          Resulted vector of objects.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMI_process_op_refby::LMI_process_prefix_refby
(
    tag_t tObject,
    const LMIOEDSubSegment& objOEDSubSegment,
    vector<tag_t>& refvTags
)
{
    int    iRetCode          = ITK_ok;
    int    iRefObjCount      = 0;
    int    iClasses          = 0;
    int*   piObjLevels       = NULL;
    int*   piWhereFound      = NULL;
    int*   piClassLevels     = NULL;
    int*   piClassWhereFound = NULL;
    tag_t* ptRefClasses      = NULL;
    tag_t* ptRefObjects      = NULL;
    vector<tag_t> vtFinalTags;
    LMI_process_OED_depth objDepth;

    /* Get the objects that contain references to the given object. */
    LMI_ITKCALL(iRetCode, POM_referencers_of_instance(tObject, LMI_POM_REF_SEARCH_LEVEL_ONE, POM_in_ds_and_db, &iRefObjCount, &ptRefObjects, &piObjLevels, &piWhereFound, &iClasses, &ptRefClasses,
                                                      &piClassLevels, &piClassWhereFound));

    if (iRefObjCount > 0 && iRetCode == ITK_ok)
    {
        int     iRelevantTagsCnt = 0;
        tag_t*  ptRelevantTags   = NULL;
        logical bIsPropArray     = false;
        logical bFound           = false;

        /* Filter retrieved objects for a specific object type */
        LMI_ITKCALL(iRetCode, LMI_obj_get_objects_of_input_type(iRefObjCount, ptRefObjects, objOEDSubSegment.strObjectType.c_str(), &iRelevantTagsCnt, &ptRelevantTags));

        for (int iDx = 0; iDx < iRelevantTagsCnt && iRetCode == ITK_ok; iDx++)
        {
            int    iRefTagsCnt = 0;
            tag_t* ptRefTags   = NULL;

            if (bFound == false)
            {
                bFound = true;

                LMI_ITKCALL(iRetCode, LMI_obj_is_property_type_array(ptRelevantTags[iDx], objOEDSubSegment.strRefRelName.c_str(), bIsPropArray)); 
            }

            if (bIsPropArray == true)
            {
                /* Get reference property value */
                LMI_ITKCALL(iRetCode, AOM_ask_value_tags(ptRelevantTags[iDx], objOEDSubSegment.strRefRelName.c_str(), &iRefTagsCnt, &ptRefTags));
            }
            else
            {
                tag_t tRefTag = NULLTAG;

                /* Get reference property value */
                LMI_ITKCALL(iRetCode, AOM_ask_value_tag(ptRelevantTags[iDx], objOEDSubSegment.strRefRelName.c_str(), &tRefTag));

                if (tRefTag != NULLTAG && iRetCode == ITK_ok)
                {
                    LMI_UPDATE_TAG_ARRAY(iRefTagsCnt, ptRefTags, tRefTag);
                }    
            }

            /* Check whether "ptRelevantTags[iDx]" object contain reference to given target object or not */
            LMI_ITKCALL(iRetCode, LMI_get_valid_obj_tags(tObject, ptRelevantTags[iDx], ptRefTags, iRefTagsCnt, vtFinalTags));

            LMI_MEM_TCFREE(ptRefTags);
        }

        LMI_MEM_TCFREE(ptRelevantTags);
    }

    /* Process depth */
    LMI_ITKCALL(iRetCode, objDepth.LMI_process_depth(objOEDSubSegment.strDepth, vtFinalTags, refvTags));

    LMI_MEM_TCFREE(piObjLevels);
    LMI_MEM_TCFREE(piWhereFound);
    LMI_MEM_TCFREE(piClassLevels);
    LMI_MEM_TCFREE(piClassWhereFound);
    LMI_MEM_TCFREE(ptRefClasses);
    LMI_MEM_TCFREE(ptRefObjects);

    return iRetCode;
}

int LMI_process_op_refby::LMI_get_valid_obj_tags
(
    tag_t   tObject,
    tag_t   tReleventTag,   
    tag_t*  ptRefTags,
    int     iRefTagsCnt,
    vector<tag_t>& refvtFinalTags
)
{
    int iRetCode = ITK_ok;

    for (int iDx = 0; iDx < iRefTagsCnt && iRetCode == ITK_ok; iDx++)
    {
        if (ptRefTags[iDx] == tObject)
        {
            refvtFinalTags.push_back(tReleventTag);

            break;
        }
    }

    return iRetCode;
}
