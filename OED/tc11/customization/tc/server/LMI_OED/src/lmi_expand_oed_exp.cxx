/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_expand_oed_exp.cxx

    Description:  This file contains functions that are used to perform OED validations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_expand_oed_exp.hxx>
#include <lmi_library.hxx>
#include <lmi_oed_optr_inf_validation.hxx>
#include <lmi_oed_string_validation.hxx>

/*
 * This function takes object tag and OED preference postfix as input arguments and retrieves OED strings it construct preference name by concatenating object type with preference postfix. 
 * If given preference postfix is empty then function will construct preference using postfix "LMI_PREF_OED_STRING_POSTFIX" is default value. Function will retrieve preference values and then perform
 * syntactically and logical validation on these OED strings. If validation is successful then function will store information in vector of objects of "LMIOEDString".
 *
 * @param[in]  tObject               Object tag for which OED to be process.
 * @param[in]  strOEDPrefPostfix     Postfix string used for constructing OED preference name.
 * @param[in]  bValidatePrefValCount Boolean value to indicate whether to validate preference value count or not.
 * @param[out] refvobjOEDStrings     Vector of "LMIOEDString" objects which will hold OED strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIExpandOEDExp::LMI_construct_and_store_valid_OED_pref_vals
(
    tag_t  tObject,
    string strOEDPrefPostfix,
    logical bValidatePrefValCount,
    vector<LMIOEDString>& refvobjOEDStrings
)
{
    int   iRetCode      = ITK_ok; 
    char* pszObjectType = NULL;
    char* pszPrefName   = NULL;
    vector<string> vstrOEDStrings;
    LMIOEDStringValidation objOEDStrValidation;

    /* Get object type */
    LMI_ITKCALL(iRetCode, LMI_obj_ask_type(tObject, &pszObjectType));

    if (pszObjectType != NULL && tc_strlen(pszObjectType) > 0 && iRetCode == ITK_ok)
    {
        LMI_STRCAT(pszPrefName, pszObjectType);

        /* 
         * Append OED preference postfix to object type to generate preference name. If OED preference postfix is not given in input argument then "_OED_String_Information"
         * is used as postfix .
         */
        if (strOEDPrefPostfix.length() > 0)
        {
            LMI_STRCAT(pszPrefName, strOEDPrefPostfix.c_str());
        }
        else
        {
            LMI_STRCAT(pszPrefName, LMI_PREF_OED_STRING_POSTFIX);
        }
    }

    /* Retrieving OED string values */
    LMI_ITKCALL(iRetCode, objOEDStrValidation.LMI_read_oed_string_pref(pszPrefName, bValidatePrefValCount, 0, vstrOEDStrings));

    /* Performing syntactical and logical validation on OED strings. If validation succeeds then it returns corresponding vector of "LMIOEDString" objects. */
    LMI_ITKCALL(iRetCode, objOEDStrValidation.LMI_store_oed_string_objects(vstrOEDStrings, refvobjOEDStrings));

    LMI_MEM_TCFREE(pszObjectType);
    LMI_MEM_TCFREE(pszPrefName);

    return iRetCode;
}

/*
 * This function takes object tag and OED strings preference name as input arguments. Function retrieves OED strings from given preference and performs syntax and logical validation on them.
 * If validation succeeds then function returns vector of "LMIOEDString" objects.
 *
 * @param[in]  strOEDPrefName         OED preference name.
 * @param[in]  bValidatePrefValCount  Boolean value to indicate whether to validate preference value count or not.
 * @param[in]  iValidValCount         Input objects count against which preference value count to be validated.
 * @param[out] refvobjOEDStrings      Vector of "LMIOEDString" class to hold the OED strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIExpandOEDExp::LMI_store_valid_OED_pref_vals
(
    string strOEDPrefName,
    logical bValidatePrefValCount,
    int     iValidValCount,
    vector<LMIOEDString>& refvobjOEDStrings
)
{
    int iRetCode = ITK_ok; 
    vector<string> vstrOEDStrings;
    LMIOEDStringValidation objOEDStrValidation;

    /* Retrieving OED string values */
    LMI_ITKCALL(iRetCode, objOEDStrValidation.LMI_read_oed_string_pref(strOEDPrefName.c_str(), bValidatePrefValCount, iValidValCount, vstrOEDStrings));

    /* Performing syntactical and logical validation on OED strings. If validation succeeds then it returns corresponding vector of "LMIOEDString" objects. */
    LMI_ITKCALL(iRetCode, objOEDStrValidation.LMI_store_oed_string_objects(vstrOEDStrings, refvobjOEDStrings));

    return iRetCode;
}

/*
 * This function takes object tag and OED strings as input arguments. OED strings are validated syntactically and logically. If validation succeeds then it gets 
 * the vector of objects of "LMIOEDString" class. 
 *
 * @param[in]  vstrOEDStrings    Vector of OED strings.
 * @param[out] refvobjOEDStrings Vector of "LMIOEDString" class to hold the oed strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIExpandOEDExp::LMI_store_valid_OED_string
(
    const vector<string>& vstrOEDStrings,
    vector<LMIOEDString>& refvobjOEDStrings
)
{
    int iRetCode = ITK_ok;  
    LMIOEDStringValidation objOEDStrValidation;

    /* Performing syntactical and logical validation on oed strings. If validation succeeds then it returns corresponding vector of "LMIOEDString" objects. */
    LMI_ITKCALL(iRetCode, objOEDStrValidation.LMI_store_oed_string_objects(vstrOEDStrings, refvobjOEDStrings));

    return iRetCode;
}


