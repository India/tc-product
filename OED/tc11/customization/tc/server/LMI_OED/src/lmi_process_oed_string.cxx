/*======================================================================================================================================================================
                                                     Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_process_oed_string.cxx

    Description: This file contains functions those are used in traversing tags. These functions are implementation of "LMI_process_OED_string" class.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
09-DEC-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#include <lmi_process_oed_string.hxx>
#include <lmi_library.hxx>
#include <lmi_oed_operation_ext.hxx>

/**
 * This function will process input object "vobjOEDSubSegments" containing OED sub-string information using which function will retrieve actual source object tags using input object "tObject".
 * Vector "vector<LMIOEDSubSegment>" contains information about all valid sub-segments of an OED string. Each OED sub-segment may contain any of the following information:
 *  - Keyword such as REF, REFBY, GRM, GRMS2P, GRMREL, GRMS2PREL, WFL etc. are stored in data member of Class "LMIOEDSubSegment" and will be evaluated by this function to expand traversal
 *    information. For e.g. if keyword is REF then function will retrieve value of reference property whose name is stored in data-member "strRefRelName" of Class "LMIOEDSubSegment" and is present
 *    on input object "tObject". 
 *  - Reference property value will be one or more objects. OED sub-segment can also contain information about specific Objects types that should be processed for further evaluation. Information
 *    about specific object type is also stored in data-member "strRefRelName" of Class "LMIOEDSubSegment".
 *  - Objects extracted for each sub-segment is an input Object for next sub-segment until function retrieves final destination objects and gets the final attribute values.
 *
 * Note: "strOEDProperty" can also be BOM Line Note Type. In-order to verify this function also accepts vector of BOM Line Note Types "vstrNoteTypes" that are present in the system.
 *
 * @param[in]  tObject            Object tag for which OED to be process.
 * @param[in]  vobjOEDSubSegments Vector of OED sub segments. (Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                                not change the value being referenced.)
 * @param[in]  vstrNoteTypes      All Note types.( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                                not change the value being referenced.)
 * @param[out] refbIsPropTypeNote Boolean value to indicate OED property is note type or not.
 * @param[out] refOEDObjInfo      Resulted object which contains final tags, open BOM window tags and BOM line tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIProcessOEDString::LMI_retrieve_obj_info_from_oed
(
    tag_t tObject,
    const vector<LMIOEDSubSegment>& vobjOEDSubSegments,
    const vector<string>& vstrNoteTypes,
    bool& refbIsPropTypeNote,
    LMIOEDObjectInfo& refOEDObjInfo
)
{
    int iRetCode = ITK_ok;

    /* Initializing out parameters. */
    refbIsPropTypeNote = false;
    refOEDObjInfo.vLMIBOMInfos.clear();

    /* If vector of OED string sub segments is empty then no need to retrieve anything, just return the current tag. */
    if (vobjOEDSubSegments.size() == 0)
    {
        LMIObjAttrValInfo objAttrValInfo;

        objAttrValInfo.tObject = tObject;

        refOEDObjInfo.vLMIAttrValInfos.push_back(objAttrValInfo);
    }
    else
    {
        int  iIDx              = 0;
        int  iVecSubSgmntsSize = 0;
        bool bIsLastSgmnt      = false;
        vector<tag_t> vInputTags;
        vector<tag_t> vOutputTags;
        vector<LMIOEDSubSegment>::const_iterator iterSubSegment;

        iVecSubSgmntsSize = (int)(vobjOEDSubSegments.size());

        /* Loop over OED sub segments */
        for (iterSubSegment = vobjOEDSubSegments.begin(); iterSubSegment < vobjOEDSubSegments.end() && iRetCode == ITK_ok; iterSubSegment++)
        {
            if(iIDx == 0)
            {
                vInputTags.push_back(tObject);
            }

            if (iIDx == iVecSubSgmntsSize - 1)
            {
                bIsLastSgmnt = true;
            }

            iIDx++;

            /* Retrieving objects for given input tags based on current given OED string sub segment.
             * This function clears the output vector in each iteration before processing the input objects.
             */
            LMI_ITKCALL(iRetCode, LMI_collect_objs_from_oed_sub_segment(vInputTags, (*iterSubSegment), bIsLastSgmnt, vstrNoteTypes, refbIsPropTypeNote, refOEDObjInfo, vOutputTags));

            /* if at any iteration output tags cout is 0 then no need to process further */
            if(vOutputTags.size() == 0 )
            {
                break;
            }
            else
            {
                vInputTags.clear();

                LMI_ITKCALL(iRetCode, LMI_stl_copy_content_to_vector(vOutputTags, vInputTags, false));
            }
        } 

        if(iRetCode == ITK_ok)
        {
            /* Assign final Tags */
            LMI_ITKCALL(iRetCode, LMI_fill_oed_obj_vector(vOutputTags, refOEDObjInfo.vLMIAttrValInfos));
        }
    }

    return iRetCode;
}

/**
 * This function will retrieve tags of source Objects by processing OED string sub-segment information which is present in object "objOEDSubSegment". Function will start processing OED sub-segment
 * starting from input vector "vtInputTags".
 * Vector "vector<LMIOEDSubSegment>" contains information about all valid sub-segments of an OED string. Each OED sub-segment may contain any of the following information:
 *  - Keyword such as REF, REFBY, GRM, GRMS2P, GRMREL, GRMS2PREL, WFL etc. are stored in data member of Class "LMIOEDSubSegment" and will be evaluated by this function to expand traversal
 *    information. For e.g. if keyword is REF then function will retrieve value of reference property whose name is stored in data-member "strRefRelName" of Class "LMIOEDSubSegment" and is present
 *    on input object "tObject". 
 *  - Reference property value will be one or more objects. OED sub-segment can also contain information about specific Objects types that should be processed for further evaluation. Information
 *    about specific object type is also stored in data-member "strRefRelName" of Class "LMIOEDSubSegment".
 *  - Objects extracted for each sub-segment is an input Object for next sub-segment until function retrieves final destination objects and gets the final attribute values.
 *
 * Note: "strOEDProperty" can also be BOM Line Note Type. In-order to verify this function also accepts vector of BOM Line Note Types "vstrNoteTypes" that are present in the system.
 *
 * @param[in]  vtInputTags        Vector of tags. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                                not change the value being referenced.)
 * @param[in]  objOEDSubSegment   OED string sub segment. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                                not change the value being referenced.)
 * @param[in]  bIsLastSgmnt       Boolean value to indicate whether current OED sub segment is last in current "LMIOEDString" object or not.
 * @param[in]  vstrNoteTypes      All Note types.( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                                not change the value being referenced.)
 * @param[out] refbIsPropTypeNote Boolean value to indicate OED property is note type or not.
 * @param[out] refOEDObjInfo      Resulted object which contains final tags, open BOM window tags and BOM line tags.
 * @param[out] refvtOutTags       Resulted vector of tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIProcessOEDString::LMI_collect_objs_from_oed_sub_segment
(
    const  vector<tag_t>& vtInputTags,
    const  LMIOEDSubSegment& objOEDSubSegment,
    bool   bIsLastSgmnt,
    const  vector<string>& vstrNoteTypes,
    bool&  refbIsPropTypeNote,
    LMIOEDObjectInfo& refOEDObjInfo,
    vector<tag_t>& refvtOutTags
)
{
    int  iRetCode = ITK_ok;
    bool bIsFirst = true;
    vector<tag_t>::const_iterator iterObj;

    /* Initializing out parameter of this function. */
    refvtOutTags.clear();

    /* Loop over input tags */
    for (iterObj = vtInputTags.begin(); iterObj < vtInputTags.end() && iRetCode == ITK_ok; iterObj++)
    { 
        /* Need to set "refObjsInfo.bIsPropTypeNote" only if we are processing last segment of current string. Property type is note or not matters only if it is BOM case. */
        if (bIsLastSgmnt == true && bIsFirst == true)
        {
            bIsFirst = false;
            
            LMI_ITKCALL(iRetCode, LMI_is_prop_type_note(refOEDObjInfo.strPropName, vstrNoteTypes, refbIsPropTypeNote));
        }
            
        /* Retrieve tags for a tag based on OED sub segment. */
        LMI_ITKCALL(iRetCode, LMI_process_oed_sub_segment((*iterObj), objOEDSubSegment, bIsLastSgmnt, refbIsPropTypeNote, refOEDObjInfo, refvtOutTags));
    }

    return iRetCode;
}

/**
 * This Function will retrieve OED property value using input object "tInputTag" based on Operations Prefix information which can have value such as "GRM", "GRMS2P", "GRMREL", "GRMS2PREL", "WFL"
 * "REF" and "REFBY.
 * - If Operation Prefix is "REF" and "strOEDProperty" is reference property "structure_revisions" then function will traverse BOM of input object "tInputTag" till level "strBOMLineLevel" by 
 *  applying Revision Rule "strRevisionRule". This information is stored as data member of "LMIOEDSubSegment". Function will get valid source object based on object type information criterial from
 *  the list of BOM objects. These objects attribute "strOEDProperty" value will be stored in  vector "refvtOutTags"
 *   These objects attribute "strOEDProperty" value will be stored in  vector "refvtOutTags". 
 * - If Operation Prefix is "REF" and "strOEDProperty" is any other reference property apart from "structure_revisions" then function will get value of reference attribute "strOEDProperty" using
 *   input object "tInputTag". Objects retrieved from value of reference attribute are stored in  vector "refvtOutTags" based on Depth information. If Depth is "ALL" then all object of reference
 *   attribute are retrieved. If Depth is "LAST" or  "FIRST" then either last or first reference attribute value will be stored in  vector "refvtOutTags".
 * - If Operation Prefix is "REFBY" then function will traversal objects using Reference property in Reverse Direction.
 * - If Operation Prefix is "GRM" then function will get secondary objects using GRM rules that are attached to input object "tInputTag" and using these objects function will find value of input
 *   "strOEDProperty". If Depth is "ALL" then all secondary objects are used and their attribute "strOEDProperty" value is retrieved. If Depth is "LAST" or  "FIRST" then either last or first secondary
 *   object will be used to get input attribute "strOEDProperty" value will be stored in  vector "refvtOutTags".
 * - If Operation Prefix is "GRMS2P" then function will get primary objects using GRM rules that are attached to input object "tInputTag" and using these objects function will find value of input
 *   "strOEDProperty". If Depth is "ALL" then all primary objects are used and their attribute "strOEDProperty" value is retrieved. If Depth is "LAST" or  "FIRST" then either last or first primary
 *   object will be used to get input attribute "strOEDProperty" value will be stored in  vector "refvtOutTags".
 * - If Operation Prefix is "GRMREL" then function will get secondary objects relation tags using GRM rules that are attached to input object "tInputTag" and using these secondary relation objects
 *   function will find value of input relation attribute "strOEDProperty". If Depth is "ALL" then all secondary relation objects are used and their relation attribute "strOEDProperty" value is
 *   retrieved. If Depth is "LAST" or  "FIRST" then either last or first secondary relation object will be used to get input relation attribute "strOEDProperty" value will be stored in  vector 
 *   "refvtOutTags".
 * - If Operation Prefix is "GRMS2PREL" then function will get primary objects relation tags using GRM rules that are attached to input object "tInputTag" and using these primary relation objects
 *   function will find value of input relation attribute "strOEDProperty". If Depth is "ALL" then all primary relation objects are used and their relation attribute "strOEDProperty" value is
 *   retrieved. If Depth is "LAST" or  "FIRST" then either last or first primary relation object will be used to get input relation attribute "strOEDProperty" value will be stored in  vector 
 *   "refvtOutTags".
 * - In case if Operation Prefix is "WFL" then function will consider input object "tInputTag" as EPM task object and will get all workflow root task Target or Reference objects based on attachment 
 *   type which can have value either "$TARGET" or "$REFERENCE". Function will get valid source object based on object type information criterial from the list of Target or Reference objects. 
 *   These objects attribute "strOEDProperty" value will be stored in  vector "refvtOutTags".
 *
 * @param[in]  tInputTag        Object tag.
 * @param[in]  objOEDSubSegment OED string sub segment. ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                not change the value being referenced.)
 * @param[in]  bIsLastSgmnt     Boolean value to indicate whether current OED sub segment is last in current "LMIOEDString" object or not.
 * @param[in]  bIsPropTypeNote  Boolean value to indicate OED property is note type or not.
 * @param[out] refObjsInfo      Resulted object which contains final tags, open BOM window tags and BOM line tags.
 * @param[out] refvtOutTags     Resulted vector of tags.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIProcessOEDString::LMI_process_oed_sub_segment
(
    tag_t  tInputTag,
    const  LMIOEDSubSegment& objOEDSubSegment,
    bool   bIsLastSgmnt,
    bool   bIsPropTypeNote,
    LMIOEDObjectInfo& refOEDObjInfo,
    vector<tag_t>& refvtOutTags
)
{
    int iRetCode = ITK_ok;

    if (tc_strcasecmp(objOEDSubSegment.strPrefix.c_str(), LMI_DCP_PREFIX_REF) == 0)
    {
        LMIProcessOpRef objOpRef;
        
        /* Process prefix ref */
        LMI_ITKCALL(iRetCode, objOpRef.LMI_process_prefix_ref(bIsPropTypeNote, tInputTag, objOEDSubSegment, bIsLastSgmnt, refOEDObjInfo, refvtOutTags));            
    }
    else if(tc_strcasecmp(objOEDSubSegment.strPrefix.c_str(), LMI_DCP_PREFIX_REFBY) == 0)
    {
        LMI_process_op_refby objOpRefby;

        /* Process prefix refby */
        LMI_ITKCALL(iRetCode, objOpRefby.LMI_process_prefix_refby(tInputTag, objOEDSubSegment, refvtOutTags));
    }
    else if(tc_strcasecmp(objOEDSubSegment.strPrefix.c_str(), LMI_DCP_PREFIX_GRM) == 0)
    {
        LMI_process_op_grm objOpGrm;

        /* Process prefix grm */
        LMI_ITKCALL(iRetCode, objOpGrm.LMI_process_prefix_grm(tInputTag, objOEDSubSegment, refvtOutTags));
    }
    else if(tc_strcasecmp(objOEDSubSegment.strPrefix.c_str(), LMI_DCP_PREFIX_GRMS2P) == 0)
    {
        LMI_process_op_grms2p objOpGrms2p;

        /* Process prefix grms2p */
        LMI_ITKCALL(iRetCode, objOpGrms2p.LMI_process_prefix_grms2p(tInputTag, objOEDSubSegment, refvtOutTags));
    }
    else if(tc_strcasecmp(objOEDSubSegment.strPrefix.c_str(), LMI_DCP_PREFIX_GRMREL) == 0)
    {
        LMI_process_op_grmrel objOpGrmrel;

        /* Process prefix grmrel */
        LMI_ITKCALL(iRetCode, objOpGrmrel.LMI_process_prefix_grmrel(tInputTag, objOEDSubSegment, refvtOutTags));
    }
    else if(tc_strcasecmp(objOEDSubSegment.strPrefix.c_str(), LMI_DCP_PREFIX_GRMS2PREL) == 0)
    {
        LMI_process_op_grms2prel objOpGrms2prel;

        /* Process prefix grmrels2p */
        LMI_ITKCALL(iRetCode, objOpGrms2prel.LMI_process_prefix_grms2prel(tInputTag, objOEDSubSegment, refvtOutTags));
    }
    else if(tc_strcasecmp(objOEDSubSegment.strPrefix.c_str(), LMI_DCP_PREFIX_WFL) == 0)
    {
        LMI_process_op_wfl objOpWfl;

        /* Process prefix wfl */
        LMI_ITKCALL(iRetCode, objOpWfl.LMI_process_prefix_wfl(tInputTag, objOEDSubSegment, refvtOutTags));
    }

    return iRetCode;
}

int LMIProcessOEDString::LMI_fill_oed_obj_vector
(
    vector<tag_t>  vtObjects,
    vector<LMIObjAttrValInfo>& refvLMIAttrValInfos
)
{
    int iRetCode = ITK_ok;
    vector<tag_t>::const_iterator iterObj;

    /* Loop over input tags */
    for (iterObj = vtObjects.begin(); iterObj < vtObjects.end() && iRetCode == ITK_ok; iterObj++)
    {
        char* pszUID = NULL;
        LMIObjAttrValInfo objAttrValInfo;

        LMI_ITKCALL(iRetCode, POM_tag_to_uid((*iterObj), &pszUID));

        if (pszUID != NULL && iRetCode == ITK_ok)
        {
            objAttrValInfo.tObject = (*iterObj);

            objAttrValInfo.strObjUID.assign(pszUID);

            refvLMIAttrValInfos.push_back(objAttrValInfo);
        }

        LMI_MEM_TCFREE(pszUID);
    }

    return iRetCode;
}

/**
 * This function retrieves the property value for given final tags.
 *
 * @param[in]  bIsPropTypeNote  Boolean value to indicate weather property present in refOEDObjInfo object is note type or not.
 * @param[out] refOEDObjInfo    Resulted object that contain final tags and values .
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIProcessOEDString::LMI_process_oed_obj_value
(
    bool bIsPropTypeNote,
    LMIOEDObjectInfo& refOEDObjInfo
)
{
    int  iRetCode       = ITK_ok;
    bool bCommonPropSet = false;
    
    /* Check for BOM line property */
    if (refOEDObjInfo.bIsBLAttr == false)
    {
        vector<LMIObjAttrValInfo>::iterator iterAttrValInfo;

        /* Loop over target objects */
        for (iterAttrValInfo = refOEDObjInfo.vLMIAttrValInfos.begin(); iterAttrValInfo < refOEDObjInfo.vLMIAttrValInfos.end() && iRetCode == ITK_ok; iterAttrValInfo++)
        {
            LMI_ITKCALL(iRetCode, LMI_process_oed_obj_value_ext(iterAttrValInfo->tObject, refOEDObjInfo.strPropName, bIsPropTypeNote, 
                bCommonPropSet, refOEDObjInfo.bPropIsArray, refOEDObjInfo.objPropValType, iterAttrValInfo->objPropValue));
        }
    }
    else
    {
        vector<LMIBOMInfo>::iterator iterBOMInfo;

        /* Loop over BOM windows */
        for (iterBOMInfo = refOEDObjInfo.vLMIBOMInfos.begin(); iterBOMInfo < refOEDObjInfo.vLMIBOMInfos.end() && iRetCode == ITK_ok; iterBOMInfo++)
        {
            vector<LMIBLInfo>::iterator iterBLInfo;

            /* Loop over BOM lines */
            for (iterBLInfo = iterBOMInfo->vLMIBLInfo.begin(); iterBLInfo < iterBOMInfo->vLMIBLInfo.end() && iRetCode == ITK_ok; iterBLInfo++)
            {
                LMI_ITKCALL(iRetCode, LMI_process_oed_obj_value_ext(iterBLInfo->tBOMLine, refOEDObjInfo.strPropName, bIsPropTypeNote, 
                    bCommonPropSet, refOEDObjInfo.bPropIsArray, refOEDObjInfo.objPropValType, iterBLInfo->objPropValue)); 
            }
        }
    }

    return iRetCode;
}

/**
 * This function retrieves the property value for given tag.
 *
 * @param[in]  tObject           Input object for which property to be retrieve.
 * @param[in]  strOEDProperty    Property name 
 * @param[in]  bIsPropTypeNote   Boolean value to indicate that given property is note type or not.
 * @param[out] refbCommonPropSet Boolean value to indicate that common properties for all objects is already set or not.
 * @param[out] refbPropIsArray   Boolean value to indicate that given property is array or not.
 * @param[out] refPropValType    Given property value type
 * @param[out] refPropValue      Property value for given tag.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int  LMIProcessOEDString::LMI_process_oed_obj_value_ext
( 
    tag_t  tObject,
    string strOEDProperty, 
    bool   bIsPropTypeNote,
    bool&  refbCommonPropSet,
    bool&  refbPropIsArray,
    PROP_value_type_t& refPropValType,
    LMIPropValue& refPropValue 
)
{
    int iRetCode = ITK_ok;

    if (refbCommonPropSet == false)
    {
        char* pszValType = NULL;

        if (bIsPropTypeNote == false)
        {
            /* Set property type is array or not. */
            LMI_ITKCALL(iRetCode, LMI_obj_is_property_type_array(tObject, strOEDProperty.c_str(), refbPropIsArray));
        }

        LMI_ITKCALL(iRetCode, AOM_ask_value_type(tObject, strOEDProperty.c_str(), &refPropValType, &pszValType));

        refbCommonPropSet = true;

        LMI_MEM_TCFREE(pszValType);
    }
   
    /* Get property value for current tag */   
    LMI_ITKCALL(iRetCode, LMI_obj_aom_get_attribute_value2(tObject, strOEDProperty, refPropValue));

    return iRetCode;
}

/**
 * This function validates if input attribute "strOEDProperty" is a BOM line Note Type or not.
 *
 * @param[in]  strOEDProperty   Name of the property.
 * @param[in]  vstrNoteTypes    All Note types.( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                              not change the value being referenced.)
 * @param[out] bIsPropTypeNote  "true" if input property "strOEDProperty" is a valid Note type else false.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIProcessOEDString::LMI_is_prop_type_note
(
   string strOEDProperty,
   const vector<string>& vstrNoteTypes,
   bool& bIsPropTypeNote
)
{
    int iRetCode = ITK_ok;

    vector<string>::const_iterator iterNoteType;

    iterNoteType = find(vstrNoteTypes.begin(), vstrNoteTypes.end(), strOEDProperty);
        
    if(iterNoteType != vstrNoteTypes.end())
    {
        bIsPropTypeNote = true;
    }

    return iRetCode;
}


