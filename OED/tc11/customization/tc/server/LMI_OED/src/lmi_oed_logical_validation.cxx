/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_oed_logical_validation.cxx

    Description: This file contains functions those are used in validating oed strings logically.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-DEC-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_oed_logical_validation.hxx>
#include <lmi_library.hxx>

/*
 * This function validates logical construct of valid OED strings which are parsed and stored in "LMIOEDString" object. Function will perform following logical validations:
 * - Function will validate OED strings if it have correct Operations Prefix such as "GRM", "GRMS2P", "REFBY" etc..
 * - In case if Operations Prefix is "REF" and reference property is "structure_revisions" then function will check that Level ('strBOMLineLevel') information stored in data member of 
 *   "LMIOEDString" ('strRevisionRule') should be numeric and Revision rule stored in "LMIOEDString" should exist in the system.
 * - In case if Operations Prefix is "REF" and reference property is any other property apart from "structure_revisions" then function will also check Object type for which this property is
 *   mentioned exist in Teamcenter. Function also checks if Depth information is correct i.e. it is any one of these strings i.e. "ALL", "LAST" and "FIRST". 
 * - If Operation Prefix is "REFBY" then function will traversal objects using Reference property in Reverse Direction.
 * - In case if Operations Prefix is "GRM" or "GRMS2P" or "GRMREL" or "GRMS2PREL" function will check if relation type information stored in OED belongs to valid Teamcenter IMAN Relation class.
 *   Function also checks if Depth information is correct i.e. it is any one of these strings i.e. "ALL", "LAST" and "FIRST". 
 * - In case if Operations Prefix is "WFL" function will check if attachment type information store is either "$TARGET" or "$REFERENCE". Function will also check if Object type information present
 *   in OED is a valid Teamcenter type. 
 *
 * @param[in] vobjOEDStrings  Vector of OED strings(objects). ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                              not change the value being referenced.)
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDLogicalValidation::LMI_OED_validate_logically
(
    const vector<LMIOEDString>& vobjOEDStrings
)
{
    int iRetCode = ITK_ok;
    vector<LMIOEDString>::const_iterator iterOEDString;

    /* Loop over OED strings(objects). */
    for (iterOEDString = vobjOEDStrings.begin(); iterOEDString < vobjOEDStrings.end() && iRetCode == ITK_ok; iterOEDString++) 
    {
        /* When validating an OED string(object), we do not need to validate attribute name so just consider the OED sub segments. */        
        vector<LMIOEDSubSegment>::const_iterator iterSubSegment;
        logical bIsFirst = false;

        /* Loop over OED string sub segments */
        for (iterSubSegment = (iterOEDString->vobjOEDSubSegments.begin()); iterSubSegment < (iterOEDString->vobjOEDSubSegments.end()) && iRetCode == ITK_ok; iterSubSegment++)
        {
            if(bIsFirst == false)
            {
                bIsFirst = true;
            }
            else
            {
                /* Check to ensure that prefix "WFL" can not appear anywhere in OED string other then first OED string sub segment. */
                if (tc_strcasecmp(iterSubSegment->strPrefix.c_str(), LMI_DCP_PREFIX_WFL) == 0 )
                {
                    iRetCode = LOG_VAL_WFL_POS_INVALID;

                    EMH_store_error(EMH_severity_error, iRetCode);
                }
            }

            /* Validating OED string sub segment logically. */
            LMI_ITKCALL(iRetCode, LMI_validate_oed_sub_segment_logically((*iterSubSegment)));
        }
    }

    return iRetCode;
}

/*
 * This function validates OED string sub segment object logically. Based on prefix value this function perform different operations to validate logically.
 * - Function will validate OED strings if it have correct Operations Prefix such as "GRM", "GRMS2P", "REFBY" etc..
 * - In case if Operation Prefix is "REF" and reference property is "structure_revisions" then function will check that Level ('strBOMLineLevel') information stored in data member of 
 *   "LMIOEDString" ('strRevisionRule') should be numeric and Revision rule stored in "LMIOEDString" should exist in the system.
 * - In case if Operation Prefix is "REF" and reference property is any other property apart from "structure_revisions" then function will also check Object type for which this property is
 *   mentioned exist in Teamcenter. Function also checks if Depth information is correct i.e. it is any one of these strings i.e. "ALL", "LAST" and "FIRST". 
 * - If Operation Prefix is "REFBY" then function will traversal objects using Reference property in Reverse Direction.
 * - In case if Operations Prefix is "GRM" or "GRMS2P" or "GRMREL" or "GRMS2PREL" function will check if relation type information stored in OED belongs to valid Teamcenter IMAN Relation class.
 *   Function also checks if Depth information is correct i.e. it is any one of these strings i.e. "ALL", "LAST" and "FIRST". 
 * - In case if Operations Prefix is "WFL" function will check if attachment type information store is either "$TARGET" or "$REFERENCE". Function will also check if Object type information present
 *   in OED is a valid Teamcenter type.
 *
 * @param[in] objOEDSubSegment  OED string sub segment(object). ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                not change the value being referenced.)
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDLogicalValidation::LMI_validate_oed_sub_segment_logically
(
    const LMIOEDSubSegment& objOEDSubSegment
)
{
    int iRetCode = ITK_ok;

    if(tc_strcasecmp((objOEDSubSegment.strPrefix.c_str()), LMI_DCP_PREFIX_REF) == 0 || tc_strcasecmp((objOEDSubSegment.strPrefix.c_str()), LMI_DCP_PREFIX_REFBY) == 0 )
    {
        /* Validating REF or REFBY prefix */
        LMI_ITKCALL(iRetCode, LMI_validate_ref_and_refby(objOEDSubSegment));
    }     
    else if( tc_strcasecmp((objOEDSubSegment.strPrefix.c_str()), LMI_DCP_PREFIX_GRM) == 0  || tc_strcasecmp((objOEDSubSegment.strPrefix.c_str()), LMI_DCP_PREFIX_GRMS2P) == 0 || 
             tc_strcasecmp((objOEDSubSegment.strPrefix.c_str()), LMI_DCP_PREFIX_GRMREL) == 0 || tc_strcasecmp((objOEDSubSegment.strPrefix.c_str()), LMI_DCP_PREFIX_GRMS2PREL) == 0 )
    {
        /* Validating GRM prefix's (GRM, GRMS2P, GRMREL, GRMS2PREL) */
        LMI_ITKCALL(iRetCode, LMI_validate_grm(objOEDSubSegment));
    }
    else if(tc_strcasecmp((objOEDSubSegment.strPrefix.c_str()), LMI_DCP_PREFIX_WFL) == 0)
    {
        /* Validating WFL prefix */
        LMI_ITKCALL(iRetCode, LMI_validate_wfl(objOEDSubSegment));
    }
    else
    {
        iRetCode = LOG_VAL_SUBSEGMENT_PREFIX_INVALID;

        EMH_store_error_s1(EMH_severity_error, iRetCode, objOEDSubSegment.strPrefix.c_str());
    }

    return iRetCode;
}

/*
 * This function validates OED string sub segment object logically in case if prefix is REF or REFBY. If reference property is "structure_revisions" then function will check if Level 
 * ('strBOMLineLevel') information which is stored in data member of "LMIOEDString" ('strRevisionRule') is numeric and Revision rule stored in "LMIOEDString" should exist in the system.
 * In case if Operation Prefix is "REF" and reference property is any other property apart from "structure_revisions" then function will also check Object type for which this property is
 * mentioned exist in Teamcenter. Function also checks if Depth information is correct i.e. it is any one of these strings i.e. "ALL", "LAST" and "FIRST". 
 * If Operation Prefix is "REFBY" then function will traversal objects using Reference property in Reverse Direction.
 *
 * @param[in] objOEDSubSegment  OED string sub segment(object). ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                not change the value being referenced.)
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDLogicalValidation::LMI_validate_ref_and_refby
(
    const LMIOEDSubSegment& objOEDSubSegment
)
{
    int iRetCode = ITK_ok;

    /* In case if "strPrefix" equals to "LMI_DCP_PREFIX_REF" and "strRefRelName" equals to "LMI_ATTR_STRUCTURE_REVISIONS" then we need to validate it as for bom. */
    if (tc_strcasecmp((objOEDSubSegment.strRefRelName.c_str()), LMI_ATTR_STRUCTURE_REVISIONS) == 0 && tc_strcasecmp((objOEDSubSegment.strPrefix.c_str()), LMI_DCP_PREFIX_REF) == 0)
    {     
        bool bIsNum = false;

        /* BOM level must be a number. */
        LMI_ITKCALL(iRetCode, LMI_is_string_numeric(objOEDSubSegment.strBOMLineLevel, bIsNum));

        if(iRetCode == ITK_ok)
        {
            if (bIsNum == false)
            {
                iRetCode = LOG_VAL_SUBSEGMENT_BOMLEVEL_INVALID;

                EMH_store_error_s1(EMH_severity_error, iRetCode, (objOEDSubSegment.strBOMLineLevel.c_str()));
            }
            else
            {
                tag_t tRevRule = NULLTAG;

                /* Find revision rule. */
                LMI_ITKCALL(iRetCode, CFM_find(objOEDSubSegment.strRevisionRule.c_str(), &tRevRule));
                       
                if (iRetCode == ITK_ok && tRevRule == NULLTAG)
                {
                    iRetCode = LOG_VAL_SUBSEGMENT_REVISION_RULE_INVALID;

                    EMH_store_error_s1(EMH_severity_error, iRetCode, objOEDSubSegment.strRevisionRule.c_str());
                }
            }
        }
    }
    else
    {
        /* Logically validation for REF and REFBY prefix is same other then the bom case. */
        tag_t tObjectType = NULLTAG;
        
        /* Find object type */
        LMI_ITKCALL(iRetCode, TCTYPE_find_type((objOEDSubSegment.strObjectType.c_str()), NULL, &tObjectType));

        if (tObjectType == NULLTAG && iRetCode == ITK_ok)
        {
            iRetCode = LOG_VAL_SUBSEGMENT_OBJECT_TYPE_INVALID;

            EMH_store_error_s1(EMH_severity_error, iRetCode, objOEDSubSegment.strObjectType.c_str());
        } 

        /* Validate depth */
        LMI_ITKCALL(iRetCode, LMI_validate_depth_info(objOEDSubSegment.strDepth));
    }

    return iRetCode;
}

/*
 * This function validates OED string sub segment object logically in case if prefix belongs to grm family (GRM, GRMS2P, GRMREL, GRMS2PREL). Function will check if relation type information stored
 * in OED belongs to valid Teamcenter IMAN Relation class. Function also checks if Depth information is correct i.e. it is any one of these strings i.e. "ALL", "LAST" and "FIRST". 
 *
 * @param[in] objOEDSubSegment  OED string sub segment(object). ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
                                not change the value being referenced.)
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDLogicalValidation::LMI_validate_grm
(
    const LMIOEDSubSegment& objOEDSubSegment
)
{
    int   iRetCode = ITK_ok;
    tag_t tRelType = NULLTAG;

    /* Get relation type */
    LMI_ITKCALL(iRetCode, GRM_find_relation_type((objOEDSubSegment.strRefRelName.c_str()), &tRelType));

    if (iRetCode == ITK_ok)
    {
        if (tRelType == NULLTAG)
        {
            iRetCode = LOG_VAL_SUBSEGMENT_RELATION_TYPE_INVALID;

            EMH_store_error_s1(EMH_severity_error, iRetCode, objOEDSubSegment.strRefRelName.c_str());
        }
        else
        {
            tag_t tObjectType = NULLTAG;

            /* Get object type */
            LMI_ITKCALL(iRetCode, TCTYPE_find_type((objOEDSubSegment.strObjectType.c_str()), NULL, &tObjectType));

            if(tObjectType == NULLTAG && iRetCode == ITK_ok)
            {
                iRetCode = LOG_VAL_SUBSEGMENT_OBJECT_TYPE_INVALID;

                EMH_store_error_s1(EMH_severity_error, iRetCode, (objOEDSubSegment.strObjectType.c_str()));
            }
        }
    }

    /* Validate depth */
    LMI_ITKCALL(iRetCode, LMI_validate_depth_info(objOEDSubSegment.strDepth));

    return iRetCode;
}

/*
 * This function validates OED string sub segment object logically in case if prefix is "WFL". Function will check if attachment type information store is either "$TARGET" or "$REFERENCE".
 * Function will also check if Object type information present in OED is a valid Teamcenter type.
 *
 * @param[in] objOEDSubSegment  OED string sub-segment (object). ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                              not change the value being referenced.)
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDLogicalValidation::LMI_validate_wfl
(
    const LMIOEDSubSegment& objOEDSubSegment
)
{
    int   iRetCode    = ITK_ok;
    tag_t tObjectType = NULLTAG;
        
    /* Find object type */
    LMI_ITKCALL(iRetCode, TCTYPE_find_type((objOEDSubSegment.strObjectType.c_str()), NULL, &tObjectType));

    if (iRetCode == ITK_ok)
    {
        if (tObjectType != NULLTAG)
        {
            /* In case of "WFL" prefix, "objPrefValPart.strRefRelName" represents attachment type which can be either target or reference. */
            if (tc_strcasecmp((objOEDSubSegment.strRefRelName.c_str()), LMI_WFL_REFERENCE) != 0 && tc_strcasecmp((objOEDSubSegment.strRefRelName.c_str()), LMI_WFL_TARGET) != 0)
            {
                iRetCode = LOG_VAL_SUBSEGMENT_WFL_ATTACHMENT_TYPE_INVALID;

                EMH_store_error_s1(EMH_severity_error, iRetCode, (objOEDSubSegment.strRefRelName.c_str()));
            }
        }
        else
        {
            iRetCode = LOG_VAL_SUBSEGMENT_OBJECT_TYPE_INVALID;

            EMH_store_error_s1(EMH_severity_error, iRetCode, (objOEDSubSegment.strObjectType.c_str()));
        }
    }

    /* Validate depth */
    LMI_ITKCALL(iRetCode, LMI_validate_depth_info(objOEDSubSegment.strDepth));

    return iRetCode;
}

/*
 * This function validates depth which can be either "LMI_DEPTH_ALL", "LMI_DEPTH_FIRST", "LMI_DEPTH_LAST" or empty.
 *
 * @param[in]  strDepth  Depth string value.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDLogicalValidation::LMI_validate_depth_info
(
    string strDepth
)
{
    int iRetCode = ITK_ok;

    /* No need to validate if depth is empty. */
    if (strDepth.length() > 0)
    {
        if (tc_strcasecmp(strDepth.c_str(), LMI_DEPTH_ALL) != 0  && tc_strcasecmp(strDepth.c_str(), LMI_DEPTH_FIRST) != 0 && tc_strcasecmp(strDepth.c_str(), LMI_DEPTH_LAST) != 0)
        {
            iRetCode = LOG_VAL_SUBSEGMENT_DEPTH_INVALID;

            EMH_store_error_s1(EMH_severity_error, iRetCode, strDepth.c_str());
        }
    }

    return iRetCode;
}


