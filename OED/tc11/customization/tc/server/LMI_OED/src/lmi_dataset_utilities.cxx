/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_dataset_utilities.cxx

    Description:  This file contains custom functions for dataset to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>

/**
 * This function creates the dataset using the information that is provided as input argument. If "pszDSTool" is empty then default tool is used.
 *
 * @param[in]  pszDataName  Name of dataset.
 * @param[in]  pszDSType    Dataset type.
 * @param[in]  pszDSTool    Dataset tool name.
 * @param[in]  pszRefFormat Dataset reference format name
 * @param[out] ptDataset    Newly created dataset tag.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMI_create_dataset
(
    char*  pszDataName,
    char*  pszDSType,
    char*  pszDSTool,
    char*  pszRefFormat,
    tag_t* ptDataset
)
{
    int   iRetCode     = ITK_ok;
    tag_t tDatasetType = NULLTAG;
    tag_t tTool        = NULLTAG;
    tag_t tDataset     = NULLTAG;

    LMI_ITKCALL(iRetCode, AE_find_datasettype2(pszDSType, &tDatasetType));
    
    LMI_ITKCALL(iRetCode, AE_create_dataset_with_id(tDatasetType, pszDataName, pszDataName, 0, 0, ptDataset));
    
    if(pszDSTool != NULL && tc_strlen(pszDSTool) > 0)
    {
        LMI_ITKCALL(iRetCode, AE_find_tool2(pszDSTool, &tTool));
    }
    else
    {
        LMI_ITKCALL(iRetCode, LMI_get_dataset_default_tool(pszDSType, &tTool));
    }

    LMI_ITKCALL(iRetCode, AE_set_dataset_tool(*ptDataset, tTool));

    LMI_ITKCALL(iRetCode, AE_set_dataset_format2(*ptDataset, pszRefFormat));

    LMI_ITKCALL(iRetCode, AOM_save(*ptDataset));

    LMI_ITKCALL(iRetCode, AOM_refresh(*ptDataset, false));

    return iRetCode;
}

/**
 * This function finds the dataset "Reference" type and "Format" using input argument dataset type and named reference file type.
 *
 * @param[in]  pszDSType       Dataset Type.
 * @param[in]  pszFileType     File type or file extension.
 * @param[out] ppszDSReference Dataset reference type name.
 * @param[out] ppszRefFormat   Dataset reference format.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMI_get_dataset_reference_and_format
(
    char*  pszDSType,
    char*  pszFileType,
    char** ppszDSReference,
    char** ppszRefFormat
)
{
    int     iRetCode     = ITK_ok;
    int     iRefCnt      = 0;
    char**  ppszRefList  = NULL;
    tag_t   tDatasetType = NULLTAG;
    logical bFoundRef    = false;

    /* Initializing the out parameter of this function. */
    (*ppszDSReference) = NULL;

    LMI_ITKCALL(iRetCode, AE_find_datasettype2(pszDSType, &tDatasetType));

    LMI_ITKCALL(iRetCode, AE_ask_datasettype_refs(tDatasetType, &iRefCnt, &ppszRefList));

    for(int iDx = 0; iDx < iRefCnt && iRetCode == ITK_ok && bFoundRef == false; iDx++)
    {
        int    iCount         = 0;
        char** ppszFileType   = NULL;
        char** ppszRefFormats = NULL;

        LMI_ITKCALL(iRetCode, AE_ask_datasettype_file_refs(tDatasetType, ppszRefList[iDx], &iCount, &ppszFileType, &ppszRefFormats));

        for(int iNx = 0; iNx < iCount && iRetCode == ITK_ok && bFoundRef == false; iNx++)
        {
            if(STRNG_ends_with(ppszFileType[iNx], pszFileType)  == true || tc_strcmp(ppszFileType[iNx], LMI_STRING_ASTERISK) == 0)
            {
                LMI_STRCPY(*ppszDSReference, ppszRefList[iDx]);
                LMI_STRCPY(*ppszRefFormat, ppszRefFormats[iNx]);
                bFoundRef = true;
            }
        }

        LMI_MEM_TCFREE(ppszFileType);
        LMI_MEM_TCFREE(ppszRefFormats);
    }

    LMI_MEM_TCFREE(ppszRefList);
    return iRetCode;
}

/**
 * This function gets the default tool for the input dataset type.
 *
 * @param[in]  pszDSType Dataset Type.
 * @param[out] ptDefTool Tag of tool which is the first tool in the list.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMI_get_dataset_default_tool
(
    char*  pszDSType,
    tag_t* ptDefTool
)
{
    int   iRetCode     = ITK_ok;
    tag_t tDatasetType = NULLTAG;

    /* Initializing the out parameter of this function. */
    (*ptDefTool) = NULLTAG;

    LMI_ITKCALL(iRetCode, AE_find_datasettype2(pszDSType, &tDatasetType));

    LMI_ITKCALL(iRetCode, AE_ask_datasettype_def_tool(tDatasetType, ptDefTool));

    return iRetCode;
}

