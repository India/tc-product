/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_oed_processing_service.cxx

    Description:  This file contains functions that are used to process the oed strings.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-Nov-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#include <lmi_oed_processing_service.hxx>
#include <lmi_library.hxx>
#include <lmi_oed_object_info.hxx>
#include <lmi_process_oed_object.hxx>
#include <lmi_prop_propagation_func_ext.hxx>

/**
 * This function is an entry point for processing OED strings that are stored in vector "vobjOEDStrings" using input object "tObject". For each OED string in vector "vobjOEDStrings" function 
 * will create an object of class "LMIOEDObjectInfo" which will contain processed OED string of input object with attribute name, value and other info. Each elements of vector "vobjOEDStrings"
 * is an object of class "LMIOEDString". 
 * Class "LMIOEDString" contains two date members:
 * - String data member "strOEDProperty" contains name of the attribute whose value needs to be retrieved. Attribute "strOEDProperty" exist on some TC business object whose information will be 
 *   extracted by this function. Starting point to find the Object on which attribute "strOEDProperty" resides is input object "tObject". Information about further traversal from "tObject" to the
 *   related objects is stored in 'vector<LMIOEDSubSegment>' data member of the  class "LMIOEDString".
 * - OED string is constructed using multiple OED string segments which are separated using segment separator. Segment separator is specified in preference "LMI_OED_Expression_Seprator". This function
 *   will process all the segments for each OED that are stored in data member vector "vector<LMIOEDSubSegment>" of Class "LMIOEDString" object.
 * - Vector "vector<LMIOEDSubSegment>" contains information about all valid sub-segments of an OED string. Each OED sub-segment may contain any of the following information:
 *     -- Keyword such as REF, REFBY, GRM, GRMS2P, GRMREL, GRMS2PREL, WFL etc. are stored in data member of Class "LMIOEDSubSegment" and will be evaluated by this function to expand traversal
 *        information. For e.g. if keyword is REF then function will retrieve value of reference property whose name is stored in data-member "strRefRelName" of Class "LMIOEDSubSegment" and is present
 *        on input object "tObject". 
 *     -- Reference property value will be one or more objects. OED sub-segment can also contain information about specific Objects types that should be processed for further evaluation. Information
 *        about specific object type is also stored in data-member "strRefRelName" of Class "LMIOEDSubSegment".
 *     -- Objects extracted for each sub-segment is an input Object for next sub-segment until function retrieves final destination objects and gets the final attribute values.
 *
 * @param[in]  tObject        Object tag for which OED to be process.
 * @param[in]  vobjOEDStrings Vector of OED strings(objects). ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                            not change the value being referenced.)
 * @param[out] refvObjsInfo   List of "LMIOEDObjectInfo" objects that holds the result of processed OED strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDProcessingService::LMI_process_oed1
(
    tag_t tObject,
    const vector<LMIOEDString>& vobjOEDStrings,
    vector<LMIOEDObjectInfo>& refvObjsInfo
)
{   
    int iRetCode = ITK_ok;
    vector<string> vstrNoteTypes;
    LMIProcessOEDObject objProcessOEDObj;

    vector<LMIOEDString>::const_iterator iterOEDString;

    /* Retrieving all note types */
    LMI_ITKCALL(iRetCode, LMI_ps_get_all_note_type(vstrNoteTypes));

    /* Running loop on OED string objects */
    for(iterOEDString = vobjOEDStrings.begin(); iterOEDString < vobjOEDStrings.end() && iRetCode == ITK_ok; iterOEDString++)
    {
        LMIOEDObjectInfo objOEDObjInfo;

        /* Process OED string */
        LMI_ITKCALL(iRetCode, objProcessOEDObj.LMI_process_oed_obj(tObject, (*iterOEDString), vstrNoteTypes, objOEDObjInfo));
             
        if (iRetCode == ITK_ok)
        {
            refvObjsInfo.push_back(objOEDObjInfo);
        }
    }
     
    return iRetCode;
}

/**
 * This function is an entry point for processing OED strings that are stored in vector "vobjOEDStrings" using input object "tObject". Function will process input vector "vobjOEDStrings" in pairs 
 * where vector content at even position will be Source OED string and vector content at odd position will be Destination OED string. Function will process first OED string info stored in vector as 
 * Source info and second OED string info stored in vector as Destination info. For each pair of OED strings in vector "vobjOEDStrings" function will create an object of class "LMIOEDSrcDestInfo"
 * which will contain processed OED string of Source and Destination with attribute name, value and other info. Each elements of vector "vobjOEDStrings" is an object of class "LMIOEDString". 
 * Class "LMIOEDString" contains two date members:
 * - String data member "strOEDProperty" contains name of the attribute whose value needs to be retrieved. Attribute "strOEDProperty" exist on some TC business object whose information will be 
 *   extracted by this function. Starting point to find the Object on which attribute "strOEDProperty" resides is input object "tObject". Information about further traversal from "tObject" to the
 *   related objects is stored in 'vector<LMIOEDSubSegment>' data member of the  class "LMIOEDString".
 * - OED string is constructed using multiple OED string segments which are separated using segment separator. Segment separator is specified in preference "LMI_OED_Expression_Seprator". This function
 *   will process all the segments for each OED that are stored in data member vector "vector<LMIOEDSubSegment>" of Class "LMIOEDString" object.
 * - Vector "vector<LMIOEDSubSegment>" contains information about all valid sub-segments of an OED string. Each OED sub-segment may contain any of the following information:
 *     -- Keyword such as REF, REFBY, GRM, GRMS2P, GRMREL, GRMS2PREL, WFL etc. are stored in data member of Class "LMIOEDSubSegment" and will be evaluated by this function to expand traversal
 *        information. For e.g. if keyword is REF then function will retrieve value of reference property whose name is stored in data-member "strRefRelName" of Class "LMIOEDSubSegment" and is present
 *        on input object "tObject". 
 *     -- Reference property value will be one or more objects. OED sub-segment can also contain information about specific Objects types that should be processed for further evaluation. Information
 *        about specific object type is also stored in data-member "strRefRelName" of Class "LMIOEDSubSegment".
 *     -- Objects extracted for each sub-segment is an input Object for next sub-segment until function retrieves final destination objects and gets the final attribute values.
 *
 * @param[in]  tObject        Object tag for which OED to be process.
 * @param[in]  vobjOEDStrings Vector of OED strings(objects). ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                            not change the value being referenced.)
 * @param[out] refobjOEDInfo  Object that holds the result of processed OED strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDProcessingService::LMI_process_src_dest_oed1
(
    tag_t tObject,
    const vector<LMIOEDString>& vobjOEDStrings,
    LMIOEDInfo& refobjOEDInfo
)
{   
    int iRetCode = ITK_ok;
    vector<string> vstrNoteTypes;
    LMIProcessOEDObject objProcessOEDObj;

    vector<LMIOEDString>::const_iterator iterOEDString;

    /* Retrieving all note types */
    LMI_ITKCALL(iRetCode, LMI_ps_get_all_note_type(vstrNoteTypes));

    /* Running loop on source and destination string pairs */
    for(iterOEDString = vobjOEDStrings.begin(); iterOEDString < vobjOEDStrings.end() && iRetCode == ITK_ok; iterOEDString++)
    {
        LMIOEDSrcDestInfo objOEDSrcDestInfo;
        LMIOEDObjectInfo objSrcInfo;
        LMIOEDObjectInfo objDestInfo;

        /* Process source string */
        LMI_ITKCALL(iRetCode, objProcessOEDObj.LMI_process_oed_obj(tObject, (*iterOEDString), vstrNoteTypes, objSrcInfo));
        
        /* Incrementing Iterator to read next value from vector which logically represents Destination string information */
        if(iRetCode == ITK_ok)
        {
            iterOEDString++;
        }

        /* Handle case where destination string is not given */
        if (iterOEDString < vobjOEDStrings.end() && iRetCode == ITK_ok)
        {
            /* Process destination string */
            LMI_ITKCALL(iRetCode, objProcessOEDObj.LMI_process_oed_obj(tObject, (*iterOEDString), vstrNoteTypes, objDestInfo));
        }
        else
        {
            /* Deliberating breaking the loop to avoid incrementing of iterator in case of error or when code is processing last vector element */
            break;
        }
        
        if (iRetCode == ITK_ok)
        {
            objOEDSrcDestInfo.objSrcInfo = objSrcInfo;

            objOEDSrcDestInfo.objDestInfo = objDestInfo;

            refobjOEDInfo.vOEDSrcDestInfo.push_back(objOEDSrcDestInfo);
        }
    }
     
    return iRetCode;
}

/**
 * This function is an entry point for processing OED strings that are stored in vector "vobjOEDStrings" using input objects "vtObjects". There will be one to one mapping between input objects and
 * OED strings. For each OED string in vector "vobjOEDStrings" function will create an object of class "LMIOEDObjectInfo" which will contain processed OED string of input object with attribute name, 
 * value and other info. Each elements of vector "vobjOEDStrings" is an object of class "LMIOEDString". 
 * Class "LMIOEDString" contains two date members:
 * - String data member "strOEDProperty" contains name of the attribute whose value needs to be retrieved. Attribute "strOEDProperty" exist on some TC business object whose information will be 
 *   extracted by this function. Starting point to find the Object on which attribute "strOEDProperty" resides is input object "tObject". Information about further traversal from "tObject" to the
 *   related objects is stored in 'vector<LMIOEDSubSegment>' data member of the  class "LMIOEDString".
 * - OED string is constructed using multiple OED string segments which are separated using segment separator. Segment separator is specified in preference "LMI_OED_Expression_Seprator". This function
 *   will process all the segments for each OED that are stored in data member vector "vector<LMIOEDSubSegment>" of Class "LMIOEDString" object.
 * - Vector "vector<LMIOEDSubSegment>" contains information about all valid sub-segments of an OED string. Each OED sub-segment may contain any of the following information:
 *     -- Keyword such as REF, REFBY, GRM, GRMS2P, GRMREL, GRMS2PREL, WFL etc. are stored in data member of Class "LMIOEDSubSegment" and will be evaluated by this function to expand traversal
 *        information. For e.g. if keyword is REF then function will retrieve value of reference property whose name is stored in data-member "strRefRelName" of Class "LMIOEDSubSegment" and is present
 *        on input object "tObject". 
 *     -- Reference property value will be one or more objects. OED sub-segment can also contain information about specific Objects types that should be processed for further evaluation. Information
 *        about specific object type is also stored in data-member "strRefRelName" of Class "LMIOEDSubSegment".
 *     -- Objects extracted for each sub-segment is an input Object for next sub-segment until function retrieves final destination objects and gets the final attribute values.
 *
 * @param[in]  vtObjects      Object tags for which OED to be process.
 * @param[in]  vobjOEDStrings Vector of OED strings(objects). ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                            not change the value being referenced.)
 * @param[out] refvObjsInfo   List of "LMIOEDObjectInfo" objects that holds the result of processed OED strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDProcessingService::LMI_process_oed2
( 
    vector<tag_t> vtObjects, 
    const vector<LMIOEDString>& vobjOEDStrings, 
    vector<LMIOEDObjectInfo>& refvObjsInfo 
)
{
    int iRetCode = ITK_ok;
    vector<string> vstrNoteTypes;
    LMIProcessOEDObject objProcessOEDObj;

    vector<tag_t>::const_iterator iterObj;
    vector<LMIOEDString>::const_iterator iterOEDString;

    /* Retrieving all note types */
    LMI_ITKCALL(iRetCode, LMI_ps_get_all_note_type(vstrNoteTypes));

    /* Running loop on OED string objects */
    for(iterOEDString = vobjOEDStrings.begin(),iterObj = vtObjects.begin(); iterOEDString < vobjOEDStrings.end() && iterObj < vtObjects.end() && iRetCode == ITK_ok; iterOEDString++,iterObj++)
    {
        LMIOEDObjectInfo objOEDObjInfo;

        /* Process OED string */
        LMI_ITKCALL(iRetCode, objProcessOEDObj.LMI_process_oed_obj((*iterObj), (*iterOEDString), vstrNoteTypes, objOEDObjInfo));
             
        if (iRetCode == ITK_ok)
        {
            refvObjsInfo.push_back(objOEDObjInfo);
        }
    }
     
    return iRetCode;
}

/**
 * This function is an entry point for processing OED strings that are stored in vector "vobjOEDStrings" using input object "vtObjects". There will be one to one mapping between input objects and
 * OED strings. Function will process input vector "vobjOEDStrings" in pairs where vector content at even position will be Source OED string and vector content at odd position will be Destination OED string. 
 * Function will process first OED string info stored in vector as Source info and second OED string info stored in vector as Destination info. For each pair of OED strings in vector "vobjOEDStrings" 
 * function will create an object of class "LMIOEDSrcDestInfo" which will contain processed OED string of Source and Destination with attribute name, value and other info. Each elements of vector 
 * "vobjOEDStrings" is an object of class "LMIOEDString". 
 * Class "LMIOEDString" contains two date members:
 * - String data member "strOEDProperty" contains name of the attribute whose value needs to be retrieved. Attribute "strOEDProperty" exist on some TC business object whose information will be 
 *   extracted by this function. Starting point to find the Object on which attribute "strOEDProperty" resides is input object "tObject". Information about further traversal from "tObject" to the
 *   related objects is stored in 'vector<LMIOEDSubSegment>' data member of the  class "LMIOEDString".
 * - OED string is constructed using multiple OED string segments which are separated using segment separator. Segment separator is specified in preference "LMI_OED_Expression_Seprator". This function
 *   will process all the segments for each OED that are stored in data member vector "vector<LMIOEDSubSegment>" of Class "LMIOEDString" object.
 * - Vector "vector<LMIOEDSubSegment>" contains information about all valid sub-segments of an OED string. Each OED sub-segment may contain any of the following information:
 *     -- Keyword such as REF, REFBY, GRM, GRMS2P, GRMREL, GRMS2PREL, WFL etc. are stored in data member of Class "LMIOEDSubSegment" and will be evaluated by this function to expand traversal
 *        information. For e.g. if keyword is REF then function will retrieve value of reference property whose name is stored in data-member "strRefRelName" of Class "LMIOEDSubSegment" and is present
 *        on input object "tObject". 
 *     -- Reference property value will be one or more objects. OED sub-segment can also contain information about specific Objects types that should be processed for further evaluation. Information
 *        about specific object type is also stored in data-member "strRefRelName" of Class "LMIOEDSubSegment".
 *     -- Objects extracted for each sub-segment is an input Object for next sub-segment until function retrieves final destination objects and gets the final attribute values.
 *
 * @param[in]  vtObjects      Object tags for which OED to be process.
 * @param[in]  vobjOEDStrings Vector of OED strings(objects). ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                            not change the value being referenced.)
 * @param[out] refobjOEDInfo  Object that holds the result of processed OED strings.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDProcessingService::LMI_process_src_dest_oed2
(
    vector<tag_t> vtObjects,
    const vector<LMIOEDString>& vobjOEDStrings,
    LMIOEDInfo& refobjOEDInfo
)
{   
    int iRetCode = ITK_ok;
    vector<string> vstrNoteTypes;
    LMIProcessOEDObject objProcessOEDObj;

    vector<tag_t>::const_iterator iterObj;
    vector<LMIOEDString>::const_iterator iterOEDString;

    /* Retrieving all note types */
    LMI_ITKCALL(iRetCode, LMI_ps_get_all_note_type(vstrNoteTypes));

    /* Running loop on source and destination string pairs and input objects */
    for(iterOEDString = vobjOEDStrings.begin(),iterObj = vtObjects.begin(); iterOEDString < vobjOEDStrings.end() && iterObj < vtObjects.end() && iRetCode == ITK_ok; iterOEDString++,iterObj++)
    {
        LMIOEDSrcDestInfo objOEDSrcDestInfo;
        LMIOEDObjectInfo objSrcInfo;
        LMIOEDObjectInfo objDestInfo;

        /* Process source string */
        LMI_ITKCALL(iRetCode, objProcessOEDObj.LMI_process_oed_obj((*iterObj), (*iterOEDString), vstrNoteTypes, objSrcInfo));
        
        /* Incrementing Iterator to read next value from vector which logically represents Destination string information */
        if(iRetCode == ITK_ok)
        {
            iterObj++;
            iterOEDString++;
        }

        /* Handle case where destination string is not given */
        if (iterOEDString < vobjOEDStrings.end() && iterObj < vtObjects.end() && iRetCode == ITK_ok)
        {
            /* Process destination string */
            LMI_ITKCALL(iRetCode, objProcessOEDObj.LMI_process_oed_obj((*iterObj), (*iterOEDString), vstrNoteTypes, objDestInfo));
        }
        else
        {
            /* Deliberating breaking the loop to avoid incrementing of iterator in case of error or when code is processing last vector element */
            break;
        }
        
        if (iRetCode == ITK_ok)
        {
            objOEDSrcDestInfo.objSrcInfo = objSrcInfo;

            objOEDSrcDestInfo.objDestInfo = objDestInfo;

            refobjOEDInfo.vOEDSrcDestInfo.push_back(objOEDSrcDestInfo);
        }
    }
     
    return iRetCode;
}

/**
 * This function is an entry point for processing OED strings and propagating retrieved value of attribute that are stored in vector "vobjOEDStrings" using input object "tObject" from source to 
 * destination objects.
 * Function will process input vector "vobjOEDStrings" in pairs where vector content at even position will be Source OED string and vector content at odd position will be Destination OED string.
 * Function will process first OED string info stored in vector as Source info and second OED string info stored in vector as Destination info. For each pair of OED strings in vector "vobjOEDStrings" 
 * function will create an object of class "LMIOEDSrcDestInfo" which will contain processed OED string of Source and Destination with attribute name, value and other info. Each elements of vector 
 * "vobjOEDStrings" is an object of class "LMIOEDString". Class "LMIOEDString" contains two date members:
 * - String data member "strOEDProperty" contains name of the attribute whose value needs to be retrieved. Attribute "strOEDProperty" exist on some TC business object whose information will be 
 *   extracted by this function. Starting point to find the Object on which attribute "strOEDProperty" resides is input object "tObject". Information about further traversal from "tObject" to the
 *   related objects is stored in 'vector<LMIOEDSubSegment>' data member of the  class "LMIOEDString".
 * - OED string is constructed using multiple OED string segments which are separated using segment separator. Segment separator is specified in preference "LMI_OED_Expression_Seprator". This function
 *   will process all the segments for each OED that are stored in data member vector "vector<LMIOEDSubSegment>" of Class "LMIOEDString" object.
 * - Vector "vector<LMIOEDSubSegment>" contains information about all valid sub-segments of an OED string. Each OED sub-segment may contain any of the following information:
 *     -- Keyword such as REF, REFBY, GRM, GRMS2P, GRMREL, GRMS2PREL, WFL etc. are stored in data member of Class "LMIOEDSubSegment" and will be evaluated by this function to expand traversal
 *        information. For e.g. if keyword is REF then function will retrieve value of reference property whose name is stored in data-member "strRefRelName" of Class "LMIOEDSubSegment" and is present
 *        on input object "tObject". 
 *     -- Reference property value will be one or more objects. OED sub-segment can also contain information about specific Objects types that should be processed for further evaluation. Information
 *        about specific object type is also stored in data-member "strRefRelName" of Class "LMIOEDSubSegment".
 *     -- Objects extracted for each sub-segment is an input Object for next sub-segment until function retrieves final destination objects and gets the final attribute values.
 *
 * @param[in]  tObject          Object tag for which OED propogation to be done.
 * @param[in]  vobjOEDStrings   Vector of OED strings(objects). ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                              not change the value being referenced.)
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDProcessingService::LMI_process_oed_prop_propogation
(
    tag_t tObject,
    const vector<LMIOEDString>& vobjOEDStrings
)
{   
    int iRetCode = ITK_ok;
    LMIOEDInfo objOEDInfo; 
        
    /* Retrieving source and destination final tags and values. */
    LMI_ITKCALL(iRetCode, LMI_process_src_dest_oed1(tObject, vobjOEDStrings, objOEDInfo));

    /* Close all BOM windows (BOM windows remains open only in case if BOM line property is accessed)*/
    LMI_ITKCALL(iRetCode, LMI_src_dest_bom_close_windows(objOEDInfo.vOEDSrcDestInfo));

    if (objOEDInfo.vOEDSrcDestInfo.size() > 0 && iRetCode == ITK_ok)
    {
        vector<LMIOEDSrcDestInfo>::const_iterator iterSrcDestInfo;

        for(iterSrcDestInfo = objOEDInfo.vOEDSrcDestInfo.begin(); iterSrcDestInfo < objOEDInfo.vOEDSrcDestInfo.end() && iRetCode == ITK_ok; iterSrcDestInfo++)
        {
            /* Propogating values from source to destination objects. */
            LMI_ITKCALL(iRetCode, LMI_propogate_src_to_dest(iterSrcDestInfo->objSrcInfo, iterSrcDestInfo->objDestInfo)); 
        }   
    }
     
    return iRetCode;
}

/**
 * This function is an entry point for processing OED strings and propagating retrieved value of attribute that are stored in vector "vobjOEDStrings" using input objects "tObjects" from source to 
 * destination objects. There will be a one to one mapping between input objects and input OED strings.
 * Function will process input vector "vobjOEDStrings" in pairs where vector content at even position will be Source OED string and vector content at odd position will be Destination OED string.
 * Function will process first OED string info stored in vector as Source info and second OED string info stored in vector as Destination info. For each pair of OED strings in vector "vobjOEDStrings" 
 * function will create an object of class "LMIOEDSrcDestInfo" which will contain processed OED string of Source and Destination with attribute name, value and other info. Each elements of vector 
 * "vobjOEDStrings" is an object of class "LMIOEDString". Class "LMIOEDString" contains two date members:
 * - String data member "strOEDProperty" contains name of the attribute whose value needs to be retrieved. Attribute "strOEDProperty" exist on some TC business object whose information will be 
 *   extracted by this function. Starting point to find the Object on which attribute "strOEDProperty" resides is input object "tObject". Information about further traversal from "tObject" to the
 *   related objects is stored in 'vector<LMIOEDSubSegment>' data member of the  class "LMIOEDString".
 * - OED string is constructed using multiple OED string segments which are separated using segment separator. Segment separator is specified in preference "LMI_OED_Expression_Seprator". This function
 *   will process all the segments for each OED that are stored in data member vector "vector<LMIOEDSubSegment>" of Class "LMIOEDString" object.
 * - Vector "vector<LMIOEDSubSegment>" contains information about all valid sub-segments of an OED string. Each OED sub-segment may contain any of the following information:
 *     -- Keyword such as REF, REFBY, GRM, GRMS2P, GRMREL, GRMS2PREL, WFL etc. are stored in data member of Class "LMIOEDSubSegment" and will be evaluated by this function to expand traversal
 *        information. For e.g. if keyword is REF then function will retrieve value of reference property whose name is stored in data-member "strRefRelName" of Class "LMIOEDSubSegment" and is present
 *        on input object "tObject". 
 *     -- Reference property value will be one or more objects. OED sub-segment can also contain information about specific Objects types that should be processed for further evaluation. Information
 *        about specific object type is also stored in data-member "strRefRelName" of Class "LMIOEDSubSegment".
 *     -- Objects extracted for each sub-segment is an input Object for next sub-segment until function retrieves final destination objects and gets the final attribute values.
 *
 * @param[in]  tObjects         Object tags for which OED propogation to be done.
 * @param[in]  vobjOEDStrings   Vector of OED strings(objects). ( Const ref allows us to access the argument without making a copy of it, while guaranteeing that the function will
 *                              not change the value being referenced.)
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise.
 */
int LMIOEDProcessingService::LMI_process_oed_prop_propogation2
(
    const vector<tag_t> tObjects,
    const vector<LMIOEDString>& vobjOEDStrings
)
{
    int iRetCode = ITK_ok;
    LMIOEDInfo objOEDInfo; 
        
    /* Retrieving source and destination final tags and values. */
    LMI_ITKCALL(iRetCode, LMI_process_src_dest_oed2(tObjects, vobjOEDStrings, objOEDInfo));

    /* Close all BOM windows (BOM windows remains open only in case if BOM line property is accessed)*/
    LMI_ITKCALL(iRetCode, LMI_src_dest_bom_close_windows(objOEDInfo.vOEDSrcDestInfo));

    if (objOEDInfo.vOEDSrcDestInfo.size() > 0 && iRetCode == ITK_ok)
    {
        vector<LMIOEDSrcDestInfo>::const_iterator iterSrcDestInfo;

        for(iterSrcDestInfo = objOEDInfo.vOEDSrcDestInfo.begin(); iterSrcDestInfo < objOEDInfo.vOEDSrcDestInfo.end() && iRetCode == ITK_ok; iterSrcDestInfo++)
        {
            /* Propogating values from source to destination objects. */
            LMI_ITKCALL(iRetCode, LMI_propogate_src_to_dest(iterSrcDestInfo->objSrcInfo, iterSrcDestInfo->objDestInfo)); 
        }   
    }
     
    return iRetCode;
}

/**
 * This function will process the input object "objSrcInfo" and retrieve the first source object from the data member "vSourceTags". This source object will be used to retrieve value of attribute
 * "strPropName" and will be propagated to destination attribute "strPropName" of all destination objects "vDestTags" which are present as data member of class object "strPropName".
 *
 * @param[in] objSrcInfo  Object containing information about OED source string object whose attribute value will be propagated.  
 * @param[in] objDestInfo Object containing information about OED destination string.
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMIOEDProcessingService::LMI_propogate_src_to_dest
(
    const LMIOEDObjectInfo& objSrcInfo, 
    const LMIOEDObjectInfo& objDestInfo
)
{
    int iRetCode = ITK_ok;

    /* Propagation cannot be done on BOM line */
    if (objDestInfo.bIsBLAttr == true)
    {
        TC_write_syslog("WARNING(lmi_oed.dll,LMI_propogate_src_to_dest): Unable to propagate. Propagation cannot be done on BOM line. \n");
    }
    else
    {
        /* Make sure that we have destination tags and property value type for source and destination tags are same. */
        if(objDestInfo.vLMIAttrValInfos.size() > 0 && objSrcInfo.objPropValType == objDestInfo.objPropValType) 
          {
                tag_t tSrcObject = NULLTAG;
                LMIPropValue objSrcPropVal;

                if (objSrcInfo.bIsBLAttr == true)
                {
                    if (objSrcInfo.vLMIBOMInfos.size() > 0)
                    {
                        if (objSrcInfo.vLMIBOMInfos[0].vLMIBLInfo.size() > 0)
                        {
                            tSrcObject    = objSrcInfo.vLMIBOMInfos[0].vLMIBLInfo[0].tBOMLine;
                            objSrcPropVal = objSrcInfo.vLMIBOMInfos[0].vLMIBLInfo[0].objPropValue;
                        }
                    }
                }
                else
                {
                    if (objSrcInfo.vLMIAttrValInfos.size() > 0)
                    {
                        tSrcObject    = objSrcInfo.vLMIAttrValInfos[0].tObject;
                        objSrcPropVal = objSrcInfo.vLMIAttrValInfos[0].objPropValue;
                    }
                }

                /* Make sure we have source object. */
                if (tSrcObject != NULLTAG)
                {
                    logical bValueExist = false;

                    /* Ensuring that property value on source is neither NULL or empty */
                    LMI_ITKCALL(iRetCode, LMI_src_value_exist(objSrcPropVal, objSrcInfo.objPropValType, bValueExist));

                    if (bValueExist == true)
                    {
                        /* Setting attribute values on destination tags. */
                        LMI_ITKCALL(iRetCode, LMI_propagate_attribute_info(objDestInfo, objSrcPropVal));
                    }
                    else
                    {
                        char* pszObjString = NULL;

                        LMI_ITKCALL(iRetCode, LMI_obj_aom_get_attribute_value(tSrcObject, LMI_ATTR_OBJECT_STRING, &pszObjString));

                        if (pszObjString != NULL && iRetCode == ITK_ok)
                        {
                            TC_write_syslog("WARNING(lmi_oed.dll,LMI_propogate_src_to_dest): Unable to propagate. Property %s value on source object %s is null. \n", objSrcInfo.strPropName.c_str(), pszObjString);
                        }

                        LMI_MEM_TCFREE(pszObjString);
                    }
                }
          }
    }

    return iRetCode;
}

/**
 * This function will retrieve all valid Note types from the system
 *
 * @param[out] piCount        Note Type Count
 * @param[out] pptNoteType    List of Note Type Tags
 * @param[out] pppszNoteTypes List of Note Type Names
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMIOEDProcessingService::LMI_ps_get_all_note_type
(
    vector<string>& refvstrNoteTypes
)
{
    int    iRetCode     = ITK_ok;
    int    iNoteTypeCnt = 0;
    tag_t* ptNote       = NULL;
   
    LMI_ITKCALL(iRetCode, PS_note_type_extent(&iNoteTypeCnt, &ptNote));

    for(int iDx = 0; iDx < iNoteTypeCnt && iRetCode == ITK_ok; iDx++)
    {
        char* pszTemp = NULL;

        LMI_ITKCALL(iRetCode, PS_ask_note_type_name(ptNote[iDx], &pszTemp));

        if(iRetCode == ITK_ok)
        {
            refvstrNoteTypes.push_back(pszTemp);
        }

        LMI_MEM_TCFREE(pszTemp);
    }

    LMI_MEM_TCFREE(ptNote);

    return iRetCode;
}

/**
 * This function will close all BOM windows associated with OED info objects.
 *
 * @param[in] refvOEDSrcDestInfo Vector of "LMIOEDSrcDestInfo" objects.
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMIOEDProcessingService:: LMI_src_dest_bom_close_windows
(
    const vector<LMIOEDSrcDestInfo>& refvOEDSrcDestInfo
)
{
    int iRetCode = ITK_ok;

    if (refvOEDSrcDestInfo.size() > 0)
    {
        vector<LMIOEDSrcDestInfo>::const_iterator iterSrcDestInfo;

        for(iterSrcDestInfo = refvOEDSrcDestInfo.begin(); iterSrcDestInfo < refvOEDSrcDestInfo.end() && iRetCode == ITK_ok; iterSrcDestInfo++)
        {
            LMI_ITKCALL(iRetCode, LMI_BOM_close_windows(iterSrcDestInfo->objSrcInfo));

            LMI_ITKCALL(iRetCode, LMI_BOM_close_windows(iterSrcDestInfo->objDestInfo));
        }   
    }

    return iRetCode;
}

/**
 * This function will close all BOM windows associated with OED info object.
 *
 * @param[in] refOEDObjInfo OED Info object
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMIOEDProcessingService::LMI_BOM_close_windows
(
    const LMIOEDObjectInfo& refOEDObjInfo 
)
{
    int iRetCode = ITK_ok;

    /* Need to close BOM line only if property value is retrieved from BOM line */
    if (refOEDObjInfo.bIsBLAttr == true && refOEDObjInfo.vLMIBOMInfos.size() > 0)
    {
        vector<LMIBOMInfo>::const_iterator iterBOMInfo;

        for (iterBOMInfo = refOEDObjInfo.vLMIBOMInfos.begin(); iterBOMInfo < refOEDObjInfo.vLMIBOMInfos.end() && iRetCode == ITK_ok; iterBOMInfo++)
        {
            LMI_ITKCALL(iRetCode, BOM_close_window(iterBOMInfo->tBOMWindow));
        }
    }

    return iRetCode;
}


