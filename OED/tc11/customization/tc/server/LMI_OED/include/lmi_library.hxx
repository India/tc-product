/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: LMI_library.hxx

    Description:  Header File containing declarations of function spread across all LMI_<UTILITY NAME>_utilities.cxx 
                  For e.g. LMI_dataset_utilities.cxx/LMI_object_utilities.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-May-18     Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#ifndef LMI_LIBRARY_HXX
#define LMI_LIBRARY_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <tccore/releasestatus.h>
#include <epm/epm_task_template_itk.h>
#include <tccore/tctype.h>
#include <tc/folder.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <epm/epm.h>
#include <epm/signoff.h>
#include <sa/user.h>
#include <sa/role.h>
#include <bom/bom.h>
#include <pom/pom/pom.h>
#include <ps/ps.h>
#include <cfm/cfm.h>
#include <ctype.h>
#include <pom/enq/enq.h>
#include <me/me.h>
#include <form/form.h>
#include <tccore/project.h>
#include <tc/folder.h>
#include <tccore/tctype.h>
#include <fclasses/tc_date.h>
#include <res/res_itk.h>
#include <sa/am.h>
#include <map>
#include <string>
#include <vector>
#include <stack>
#include <regex>
#include <algorithm>
#include <lmi_itk_errors.hxx>
#include <lmi_const.hxx>
#include <lmi_itk_mem_free.hxx>
#include <lmi_errors.hxx>
#include <lmi_string_macros.hxx>
#include <lmi_prop_value.hxx>
#include <lmi_regex_info.hxx>
#include <lmi_stl_utilities.hxx>

using namespace std;

/*        Utility Functions For Error Logging        */
int LMI_err_write_to_logfile
(
    char* pszErrorFunction,
    int   iErrorNumber,
    char* pszFileName,
    int   iLineNumber
);

int LMI_err_start_debug
(
    char* pszCallingFunction,
    int   iErrorNumber,
    char* pszFileName,
    int   iLineNumber
);

/*        Utility Functions For Objects        */
int LMI_obj_ask_status_tag
(
    tag_t  tObject,
    char*  pszStatusName,
    tag_t* ptStatusTag
);

int LMI_obj_ask_type
(
    tag_t  tObject,
    char** ppszObjType
);

int LMI_obj_ask_type_display_name
(
    char*  pszType,
    char** ppszTypeDisplayName
);

int LMI_obj_save_and_unlock
(
    tag_t tObject
);

int LMI_obj_set_str_attribute_val
(
    tag_t tObject,
    char* pszPropertyName,
    char* pszPropVal
);

int LMI_obj_set_tag_value
(
    tag_t tObject,
    char* pszPropertyName,
    tag_t tAttrVal
);

int LMI_obj_is_type_of
(
    tag_t    tObject,
    char*    pszTypeName,
    logical* pbBelongsToInputClass
);

int LMI_obj_is_same_type
(
    tag_t       tObject,
    const char* pszObjType,
    logical*    pbSameType
);

int LMI_obj_is_type_of_2
(
    char*    pszSrcType,
    char*    pszDestType,
    logical* bIsNotBOType,
    logical* pbBelongsToInputClass
);

int LMI_check_privilege
(
    tag_t    tObject,
    logical* pbWriteAccessDenied,
    logical* pbObjCheckedOut,
    logical* pbModifiable
);

int LMI_obj_get_obj_of_input_type
(
    int    iObjCnt,
    tag_t* ptObject,
    char*  pszObjType,
    tag_t* ptCorrectObj
);

int LMI_obj_aom_get_attribute_value
(
    tag_t  tObject,
    char*  pszAttributeName,
    char** ppszAttrValue
);

int LMI_obj_check_and_set_str_attribute_val
(
    tag_t tObject,
    char* pszPropertyName,
    char* pszPropVal
);

int LMI_obj_is_any_input_status_attached
(
    tag_t     tObject,
    int       iValidStatus,
    char**    ppszStatusNames,
    tag_t*    ptStatus,
    logical*  pbFound
);

int LMI_obj_aom_set_attribute_value
(
    tag_t tObject,
    char* pszAttributeName,
    char* pszAttrValue
);

int LMI_check_against_allowed_status
(
    tag_t    tObject,
    int      iValidStatus,
    char**   ppszValidStatus,
    tag_t*   ptStatus,
    logical* pbFound,
    logical* pbValid
);

int LMI_validate_input_status_list
(
    int      iValidStatus,
    char**   ppszValidStatus,
    logical* pbNone,
    logical* pbAny
);

int LMI_obj_aom_get_attribute_value2
(
    tag_t           tObject,
    string          strAttributeName,
    LMIPropValue& refValueData
);

int LMI_obj_get_objects_of_input_type
(
    int           iObjCnt,
    tag_t*        ptObject,
    const char*   pszObjType,
    int*          piCorrectObjCount,
    tag_t**       pptCorrectObj
);

int LMI_obj_is_property_type_array
(
    tag_t    tObject,
    string   strAttrName,
    logical& refbIsPropTypeArray
);

int LMI_strings_to_tags
(
    const vector<string>& vstrUIDs,
    vector<tag_t>& refvtObjects
);

/*        Utility Functions For GRM        */
int LMI_grm_get_related_obj
(
    tag_t   tObject,
    char*   pszRelationType,
    char*   pszObjType,
    char*   pszExcludeStatus,
    logical bGetPrimary,
    tag_t** pptValidRelatedObj,
    int*    piRelatedObjCnt
);

int LMI_grm_create_relation
(
    tag_t  tPrimaryObj,
    tag_t  tSecondaryObj,
    char*  pszRelationType,
    tag_t* ptRelation
);

/*        Utility Functions For Dataset        */
int LMI_create_dataset
(
    char*  pszDataName,
    char*  pszDSType,
    char*  pszDSTool,
    char*  pszRefFormat,
    tag_t* ptDataset
);

int LMI_get_dataset_reference_and_format
(
    char*  pszDSType,
    char*  pszFileType,
    char** ppszDSReference,
    char** ppszRefFormat
);

int LMI_get_dataset_default_tool
(
    char*  pszDSType,
    tag_t* ptDefTool
);

/*        Utility Functions For Strings        */
int LMI_str_parse_string
(
    const char* pszList,
    char*       pszSeparator,
    int*        iCount,
    char***     pppszValuelist
);

int LMI_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
);

int LMI_contains_string_value
(
    char*    pszStrVal, 
    int      iTotalStrVals, 
    char**   pppszStrVals,
    logical* pbFound
);

int LMI_replace_sub_string
(
    char*  pszInputStr, 
    char*  pszSubstring,
    char*  pszReplacementStr,
    char** ppszNewString
);

int LMI_string_find_sub_str
(
    char*    pszInputString,
    char*    pszSubString,
    logical* pbSubStrFound
);

int LMI_get_str_after_delimiter
(
    char*      pszInputStr,
    const char szDelim, 
    char**     ppszTrimmedStr
);

int LMI_is_string_numeric
(
    string str, 
    bool&  refbIsNumber
);

int LMI_rep_regx_ptrns_with_strings
(
    const vector<LMIRegexInfo> vregxInfo,
    string& refstrFinalString
);

/*        Utility Functions For Preferences        */
int LMI_pref_get_string_value
(
    char*  pszPrefName, 
    char** ppszPrefValue
);

int LMI_pref_get_string_values
(
    const char*   pszPrefName, 
    int*          iPrefValCnt, 
    char***       pppszPrefVals
);

int LMI_pref_get_string_values
(
    const char*      pszPrefName, 
    vector<string>&  refvPrefVals
);

/*        Utility Functions For Organization utilities      */
int LMI_get_logged_in_user
(
    tag_t* ptLoggedInUser,
    char** ppszUserID
); 


/*        Utility Functions For Organization utilities        */

int LMI_create_custom_folder
(
    char*  pszFLType,
    char*  pszFLName,
    char*  pszFLDesc,
    tag_t* ptFolder
);

int LMI_insert_content_to_folder
(
    tag_t tFolder,
    tag_t tObject,
    int   iPosition
);

int LMI_ask_folder_references
(
    tag_t   tFolder,
    int     iTypCnt,
    char**  ppszTypes,
    int*    piCnt,
    tag_t** pptObj
);

/*        Argument Utilities        */

int LMI_arg_get_attached_obj_of_input_type_list
(
    tag_t   tTask,
    int     iAttachmentType,
    char**  ppszObjTypeList,
    int     iTypeCnt,
    int*    piCount,
    tag_t** pptObject
);

int LMI_arg_get_attached_objects_of_type
(
    tag_t   task,
    char*   pszAttachedObjType,
    int*    piCount,
    tag_t** pptObject
);

int LMI_arg_get_arguments
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    const char*         pszArgument,
    char*               pszSeparator,
    logical             bOptional,
    int*                piValCount,
    char***             pppszArgValue,
    char**              ppszOriginalArgVal
);

int LMI_arg_get_wf_argument_value
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    const char*         pszArgument,
    logical             bOptional,
    char**              ppszArgValue
);

int LMI_arg_get_wf_argument_info_and_value
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    const char*         pszArgument,
    logical             bOptional,
    logical*            pbFoundArg,
    char**              ppszArgValue
);

int LMI_arg_get_wf_atch_type
(
    char* pszAtchType,
    int*  piAtchType  
);

/* BOM Utilities */

int LMI_bom_find_view_type_tag
(
    const char*  pszBOMViewType,
    tag_t*       ptViewTypeTag
);

int LMI_bom_get_bvr
(
   tag_t         tItemRev,
   const char*   pszViewType,
   tag_t*        ptBVR
);

int LMI_bom_apply_config_rule
(
    tag_t       tBomWindow,
    const char* pszRule
);

int LMI_bomline_get_top_line
(
    tag_t       tItemRev,
    const char* pszRevRule,
    const char* pszViewType,
    tag_t*      ptBOMWindow,
    tag_t*      ptTopLine
);

int LMI_bom_get_bv_of_input_type
(
    tag_t        tItemRev,
    const char*  pszViewType,
    tag_t*       ptBOMView
);

int LMI_get_item_rev_for_bom_line
(
    tag_t  tBOMLine,
    tag_t* tItemRev
);

#endif


