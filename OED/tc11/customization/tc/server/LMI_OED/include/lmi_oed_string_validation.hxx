/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_oed_string_validation.hxx

    Description:  Header File for lmi_oed_string_validation.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-DEC-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_OED_STRING_VALIDATION_HXX
#define LMI_OED_STRING_VALIDATION_HXX
    
#include <string>
#include <vector>
#include <unidefs.h>
#include <lmi_operator.hxx>
#include <lmi_oed_string.hxx>

using namespace std;

class LMIOEDStringValidation
{
    public:
        int LMI_read_oed_string_pref( const char* pszPrefName, const logical bValidateValCount, const int iValidValCount, vector<string>& refvstrOEDStrings );

        int LMI_validate_oed_strings( string strOEDExprSeparator, const vector<string>& vstrOEDPrefVals, const vector<LMIOperator>& vobjLMIOptrs, vector<LMIOEDString>& refvobjOEDStrings );        
        
        int LMI_store_oed_string_objects( const vector<string>& vstrOEDStrings, vector<LMIOEDString>& refvobjOEDStrings );
};


#endif

