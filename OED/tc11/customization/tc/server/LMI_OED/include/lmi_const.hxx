/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: lmi_const.hxx

    Description:  Header File for constants used during scope of LMI_OED dll.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_CONST_HXX
#define LMI_CONST_HXX

/* Date Time Format */
#define LMI_DATE_TIME_FORMAT                                     "%d.%m.%Y"

/* Prefixes */
#define LMI_BMIDE_PREFIX                                         "LMI_"
#define LMI_OPTIONAL_OPTR_PREFIX                                 "OP"
#define LMI_BOMLINE_PROP_PREFIX                                  "bl_" 

/* Postfixes */
#define LMI_PREF_OED_STRING_POSTFIX                              "_OED_Strings_Information"

/* Infix */

/* Preferences */
#define LMI_OED_OPERATOR_INFORMATION                             "LMI_OED_Operator_Information"
#define LMI_OED_EXPRESSION_SEPARATOR                             "LMI_OED_Expression_Seprator"

/* Relation */
#define LMI_IMAN_SPECIFICATION                                   "IMAN_specification"
#define LMI_IMAN_BASED_ON                                        "IMAN_based_on"
#define LMI_IMAN_MASTER_FORM                                     "IMAN_master_form"
#define LMI_IMAN_REFERENCE                                       "IMAN_reference"

/* Revision Rules*/
#define LMI_REVISION_RULE_LATEST_WORKING                         "Latest Working"


/* Attributes  */
#define LMI_ATTR_DATE_RELEASED                                   "date_released"
#define LMI_ATTR_RELEASE_STATUS_LIST                             "release_status_list"
#define LMI_ATTR_RELEASE_STATUS_NAME                             "name"
#define LMI_ATTR_OBJECT_NAME                                     "object_name"
#define LMI_ATTR_DESCRIPTION                                     "object_desc"
#define LMI_ATTR_ITEM_TAG                                        "items_tag"
#define LMI_ATTR_ITEM_ID                                         "item_id"
#define LMI_ATTR_ITEM_REVISION_ID                                "item_revision_id"
#define LMI_ATTR_OBJECT_TYPE                                     "object_type"
#define LMI_ATTR_OBJECT_STRING                                   "object_string"
#define LMI_ATTR_OBJECT_CREATION_DATE                            "creation_date"
#define LMI_ATTR_PROJ_LIST                                       "project_list"
#define LMI_ATTR_PROCESS_STAGE_LIST                              "process_stage_list"
#define LMI_ATTR_PSOCC_CHILD_ITEM                                "child_item"
#define LMI_ATTR_PSOCC_PARENT_BVR                                "parent_bvr"
#define LMI_ATTR_USER_DATA_1                                     "user_data_1"
#define LMI_ATTR_SIGNOFF_ATTACHMENTS                             "signoff_attachments"
#define LMI_ATTR_PARENT_PROCESSES                                "parent_processes"
#define LMI_ATTR_ALL_TASKS                                       "all_tasks"
#define LMI_ATTR_ACTIVE_SEQ                                      "active_seq"
#define LMI_ATTR_EPM_TASK_ATTACHMENTS                            "attachments"
#define LMI_ATTR_PRIMARY_OBJ                                     "primaryObjects"
#define LMI_ATTR_SECONDARY_OBJ                                   "secondaryObjects"
#define LMI_ATTR_STATE                                           "state"
#define LMI_ATTR_UOM_TAG                                         "uom_tag"
#define LMI_ATTR_CONTENTS                                        "contents"
#define LMI_ATTR_REVISION                                         "revision"
#define LMI_ATTR_STRUCTURE_REVISIONS                             "structure_revisions"
#define LMI_ATTR_BL_REVISION                                     "bl_revision"

/* Class Name */
#define LMI_CLASS_RELEASE_STATUS                                 "releasestatus"
#define LMI_CLASS_WORKSPACEOBJECT                                "WorkspaceObject"
#define LMI_CLASS_FORM                                           "Form"
#define LMI_CLASS_STATUS                                         "ReleaseStatus"
#define LMI_CLASS_ITEMREVISION                                   "ItemRevision"
#define LMI_CLASS_ITEM                                           "Item"
#define LMI_CLASS_DATASET                                        "Dataset"
#define LMI_CLASS_EPM_TASK                                       "EPMTask"
#define LMI_CLASS_IMANRELATION                                   "ImanRelation"
#define LMI_CLASS_PSOCCURRENCE                                   "PSOccurrence"
#define LMI_CLASS_PDF_DATASET                                    "PDF"
#define LMI_CLASS_FOLDER                                         "Folder"
#define LMI_CLASS_FOLDER_BASE                                    "LMI_Folder_Base"
#define LMI_CLASS_ENVELOPE                                       "Envelope"
#define LMI_CLASS_MASTER_FORM                                    "IMAN_master_form"
#define LMI_CLASS_PART_REVISION                                  "Part Revision"


/* Query Constants */


/* Type Name */
#define LMI_TYPE_NEWSTUFF_FOLDER                                 "Newstuff Folder"

/* Name Of Privileges  */
#define LMI_WRITE_ACCESS                                         "WRITE"

/* Environment Variable */
#define LMI_USER_TEMP_DIR                                        "TMP"

/* Logical String Response */
#define LMI_CAPS_YES                                             "Y"
#define LMI_SMALL_YES                                            "y"
#define LMI_CAPS_NO                                              "N"
#define LMI_SMALL_NO                                             "n"

/* String Macro  */
#define LMI_I                                                    "I"
#define LMI_IR                                                   "IR"
#define LMI_NO_VAL                                               "NO_VAL"
#define EMPTY_STRING                                             ""

/* Folder Names String */
#define LMI_NEWSTUFF_FOLDER                                      "newstuff"
#define LMI_HOME_FOLDER                                          "home"

/* Separators */
#define LMI_STRING_UNDERSCORE                                    "_"
#define LMI_STRING_COMMA                                         ","
#define LMI_STRING_SEMICOLON                                     ";"
#define LMI_CHAR_SEMICOLON                                       ';'
#define LMI_STRING_FORWARD_SLASH                                 "/"
#define LMI_STRING_BACKWARD_SLASH                                "\\"
#define LMI_STRING_SPACE                                         " "
#define LMI_STRING_COLON                                         ":"
#define LMI_STRING_DOT                                           "."
#define LMI_STRING_OPENING                                       "\""
#define LMI_STRING_ZERO                                          "0"
#define LMI_STRING_DOLLAR                                        "$"
#define LMI_STRING_AMPERSAND                                     "&"
#define LMI_STRING_BLANK                                         ""
#define LMI_STRING_GREATER_THAN                                  ">"
#define LMI_STRING_LESS_THAN                                     "<"
#define LMI_STRING_ASTERISK                                         "*"
#define LMI_CHAR_DOLLAR                                          '$'
#define LMI_CHAR_BRACKET_OPEN                                    '('
#define LMI_CHAR_BRACKET_CLOSE                                   ')'
#define LMI_CHAR_COMMA                                           ','
#define LMI_CHAR_COLON                                           ':'
#define LMI_CHAR_DOT                                             '.'

#define LMI_STRING_ATTR_LEN                                      256
#define LMI_SEPARATOR_LENGTH                                     1
#define LMI_NUMERIC_ERROR_LENGTH                                 12


/* Dataset Tools */
#define LMI_DS_TA_PDF_TOOL                                       "PDF_Tool"
#define LMI_DS_TA_ADOBE_ACROBAT_TOOL                             "Adobe Acrobat"

/* Dataset References Format */
#define LMI_DS_REFERENCES_FORMAT_BINARY                          "BINARY"

/* Constant Object Names */
#define LMI_DS_PDX_NAME                                          "PDX D1"
#define LMI_DS_PDF_NAME                                          "PDF D1"
#define LMI_FUNCTION_OBJ_NAME                                    "Function F1"

/* keywords */
#define LMI_KW_TARGET                                            "$TARGET"
#define LMI_KW_REFERENCE                                         "$REFERENCE"

/* Workflow Argument Names */
#define LMI_WF_ARG_INCLUDE_TYPE                                  "include_type"
#define LMI_WF_ARG_ALLOWED_STATUS                                "allowed_status"

/* Workflow Argument Values */
#define LMI_WF_ARG_VAL_ALL                                       "all"
#define LMI_WF_ARG_VAL_TARGET                                    "target"
#define LMI_WF_ARG_VAL_REFERENCE                                 "reference"
#define LMI_WF_ARG_VAL_USER                                      "user"
#define LMI_WF_ARG_VAL_PERSON                                    "person"
#define LMI_WF_ARG_VAL_ANY                                       "any"
#define LMI_WF_ARG_VAL_NONE                                      "none"

/* Workflow Action Handler Names  */
#define LMI_WF_AH_CREATE_COM_PART                                "KM4-create-com-part"

/* Orerator Indexes */
#define LMI_OPTR_FIRST_INDEX                                      0
#define LMI_OPTR_NORMAL_SEP_INDEX                                 1
#define LMI_OPTR_OP1_INDEX                                        2
#define LMI_OPTR_OP2_INDEX                                        3
#define LMI_OPTR_LAST_INDEX                                       4
#define LMI_OPTR_SGMNT_SEP_INDEX                                  5

/* Reserved LMI operators */
#define LMI_OPTR_EXACT_BOM                                        "="

/* DCP Prefix */
#define LMI_DCP_PREFIX_REF                                        "REF"
#define LMI_DCP_PREFIX_REFBY                                      "REFBY"
#define LMI_DCP_PREFIX_GRM                                        "GRM"
#define LMI_DCP_PREFIX_GRMS2P                                     "GRMS2P"
#define LMI_DCP_PREFIX_GRMREL                                     "GRMREL"
#define LMI_DCP_PREFIX_GRMS2PREL                                  "GRMS2PREL"
#define LMI_DCP_PREFIX_WFL                                        "WFL"
#define LMI_DEPTH_ALL                                             "ALL"
#define LMI_DEPTH_FIRST                                           "FIRST"
#define LMI_DEPTH_LAST                                            "LAST"
#define LMI_WFL_TARGET                                            "$Target"
#define LMI_WFL_REFERENCE                                         "$Reference"

/* Property Types */
#define LMI_PROP_TYPE_STRING                                      0
#define LMI_PROP_TYPE_LOGICAL                                     1
#define LMI_PROP_TYPE_INT                                         2
#define LMI_PROP_TYPE_DOUBLE                                      3
#define LMI_PROP_TYPE_TAG                                         4
#define LMI_PROP_TYPE_DATE                                        5

/* Constants */
#define LMI_POM_REF_SEARCH_LEVEL_ONE                              1
#define LMI_OPTR_LENGTH                                           1
#define LMI_OPTIONAL_OPTR_LENGTH                                  3
#define LMI_OED_EXPRESSION_SEPARATOR_DEFAULT                      "."
#define LMI_BOM_MAX_LEVEL                                         "99"

/* Regex Pattern */
#define LMI_REGX_PATTERN_START                                    "\\"
#define LMI_REGX_PATTERN_END                                      "{2,}"

#endif

