/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_oed_logical_validation.hxx

    Description: Header File for lmi_oed_logical_validation.cxx.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-DEC-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_OED_LOGICAL_VALIDATION_HXX
#define LMI_OED_LOGICAL_VALIDATION_HXX

#include <vector>
#include <lmi_oed_string.hxx>

using namespace std;

class LMIOEDLogicalValidation
{
    public:
        int LMI_OED_validate_logically( const vector<LMIOEDString>& vobjOEDStrings );

        int LMI_validate_oed_sub_segment_logically( const LMIOEDSubSegment& objOEDSubSegment );

        int LMI_validate_ref_and_refby( const LMIOEDSubSegment& objOEDSubSegment );

        int LMI_validate_grm( const LMIOEDSubSegment& objOEDSubSegment );

        int LMI_validate_wfl( const LMIOEDSubSegment& objOEDSubSegment );

        int LMI_validate_depth_info( string strDepth );
};

#endif

