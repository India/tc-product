/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_process_oed_string.hxx

    Description:  Header File for lmi_process_oed_string.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-DEC-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_PROCESS_OED_STRING_HXX
#define LMI_PROCESS_OED_STRING_HXX

#include <unidefs.h>
#include <property\propdesc.h>
#include <vector>
#include <string>
#include <lmi_oed_sub_segment.hxx>
#include <lmi_prop_value.hxx>
#include <lmi_oed_object_info.hxx>
#include <lmi_obj_attr_val_info.hxx>

class LMIProcessOEDString
{
    public:
        int LMI_retrieve_obj_info_from_oed
        (  
            tag_t tObject, 
            const vector<LMIOEDSubSegment>& vobjOEDSubSegments, 
            const vector<string>& vstrNoteTypes,
            bool& refbIsPropTypeNote,
            LMIOEDObjectInfo& refOEDObjInfo
        );

        int LMI_collect_objs_from_oed_sub_segment
        (  
            const vector<tag_t>& vtInputTags, 
            const LMIOEDSubSegment& objOEDSubSegment, 
            bool bIsLastSgmnt, 
            const vector<string>& vstrNoteTypes,
            bool& refbIsPropTypeNote,
            LMIOEDObjectInfo& refOEDObjInfo, 
            vector<tag_t>& refvtOutTags 
        );

        int LMI_process_oed_sub_segment
        (  
            tag_t tInputTag, 
            const LMIOEDSubSegment& objOEDSubSegment, 
            bool bIsLastSgmnt, 
            bool bIsPropTypeNote,
            LMIOEDObjectInfo& refOEDObjInfo,
            vector<tag_t>& refvtOutTags 
        );

        int LMI_fill_oed_obj_vector
        (
            vector<tag_t>  vtObjects,
            vector<LMIObjAttrValInfo>& refvLMIAttrValInfos
        );

        int LMI_process_oed_obj_value
        ( 
            bool bIsPropTypeNote,
            LMIOEDObjectInfo& refOEDObjInfo 
        );

        int LMI_process_oed_obj_value_ext
        ( 
            tag_t  tObject,
            string strOEDProperty, 
            bool   bIsPropTypeNote,
            bool&  refbCommonPropSet,
            bool&  refbPropIsArray,
            PROP_value_type_t& refPropValType,
            LMIPropValue& refPropValue 
        );

        int LMI_is_prop_type_note
        ( 
            string strOEDProperty, 
            const vector<string>& vstrNoteTypes, 
            bool& bIsPropTypeNote 
        );
};

#endif

