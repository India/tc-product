/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_oed_syntax_validation.hxx

    Description: A container class that hold operators defined in oed operator preference.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-DEC-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_OED_SYNTAX_VALIDATION_HXX
#define LMI_OED_SYNTAX_VALIDATION_HXX

#include <string>
#include <vector>
#include <stack>
#include <lmi_oed_string.hxx>
#include <lmi_operator.hxx>
#include <lmi_regex_info.hxx>

using namespace std;

class LMIOEDSyntaxValidation
{
    public:
        int LMI_validate_oed_syntax( const string strOEDExprSeparator, const vector<string>& vstrOEDStrings, const vector<LMIOperator>& vobjLMIOptrs, vector<LMIOEDString>& refvobjOEDStrings );

        int LMI_validate_oed_string( const string strOEDString, const string strOEDExprSeparator, const vector<LMIOperator>& vobjLMIOptrs, LMIOEDString& refobjOEDString );  

        int LMI_validate_and_store_oed_sub_segment( string strOEDSubSegment, const vector<LMIOperator>& vobjLMIOptrs, bool& refbFoundProp, LMIOEDString& refobjOEDString);

        int LMI_construct_OED_str_obj( string strOEDSubSegment, bool bHasOptr, LMIOEDSubSegment objOEDSubSgmnt, const vector<LMIOperator>& vobjLMIOptrs, LMIOEDString& refobjOEDString);

        int LMI_set_sub_sgmnt_prop(const string strPropVal, const int iCurOptrIndex, LMIOEDSubSegment& refobjOEDSubSegment);
        
        int LMI_refactor_OED_string( const string strOEDExprSeparator, const vector<LMIOperator>& vobjLMIOptrs, string& refstrOEDString);

        int LMI_add_regex_info( const string strCharacter, vector<LMIRegexInfo>& refvregxInfo);

        int LMI_validate_OED_grouping_optr( const string strOEDString, const string strOptrFst, const string strOptrSec );

        int LMI_refactor_sub_sgmnt_brackets(const vector<LMIOperator>& vobjLMIOptrs, string strSubSgmnt, string& refstrUpdatedSubSgmnt);

        int LMI_validate_and_remove_oed_closing_optr(const vector<LMIOperator>& vobjLMIOptrs, char* pszSubSgmntToken, string strLastOptr, string& refstrSubString);

        int LMI_sub_sgmnt_contains_optr(const string strSubSgmnt, const vector<LMIOperator>& vobjLMIOptrs, bool& refbHasOptr);
};

#endif

