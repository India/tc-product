/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_oed_operation_ext.hxx

    Description:   This file defines classes for individual OED operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
09-DEC-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_OED_OPERATION_EXT_HXX
#define LMI_OED_OPERATION_EXT_HXX

#include <unidefs.h>
#include <string>
#include <vector>
#include <lmi_oed_sub_segment.hxx>
#include <lmi_oed_object_info.hxx>

class LMIProcessOpRef
{
    public:
        int LMI_process_prefix_ref
        ( 
            bool   bIsPropTypeNote,
            tag_t  tObject, 
            const  LMIOEDSubSegment& objOEDSubSegment, 
            bool   bIsLastSgmnt, 
            LMIOEDObjectInfo& refOEDObjInfo,
            vector<tag_t>& refvTags 
        );

        int LMI_process_ref( tag_t tObject, const LMIOEDSubSegment& objOEDSubSegment, vector<tag_t>& refvTags );

        int LMI_process_bom( tag_t tObject, const LMIOEDSubSegment& objOEDSubSegment, LMIOEDObjectInfo& refOEDObjInfo, vector<tag_t>& refvTags );

        int LMI_get_child_lines_by_level(tag_t tTopLine, int iCurrentLevel, int iEndLevel, bool bExactBOMLevel, vector<tag_t>& refvtChildLines);
};

class LMI_process_op_refby
{
    public:
        int LMI_process_prefix_refby( tag_t tObject, const LMIOEDSubSegment& objOEDSubSegment, vector<tag_t>& refvTags );

        int LMI_process_op_refby::LMI_get_valid_obj_tags( tag_t tObject, tag_t tReleventTag, tag_t*  ptRefTags, int iRefTagsCnt, vector<tag_t>& refvtFinalTags );
};

class LMI_process_op_grm
{
    public:
        int LMI_process_prefix_grm( tag_t tObject, const LMIOEDSubSegment& objOEDSubSegment, vector<tag_t>& refvTags );
};

class LMI_process_op_grms2p
{
    public:
        int LMI_process_prefix_grms2p( tag_t tObject, const LMIOEDSubSegment& objOEDSubSegment, vector<tag_t>& refvTags );
};

class LMI_process_op_grmrel
{
    public:
        int LMI_process_prefix_grmrel( tag_t tObject, const LMIOEDSubSegment& objOEDSubSegment, vector<tag_t>& refvTags );
};

class LMI_process_op_grms2prel
{
    public:
        int LMI_process_prefix_grms2prel( tag_t tObject, const LMIOEDSubSegment& objOEDSubSegment, vector<tag_t>& refvTags );
};

class LMI_process_op_wfl
{
    public:
        int LMI_process_prefix_wfl( tag_t tObject, const LMIOEDSubSegment& objOEDSubSegment, vector<tag_t>& refvTags );
};


class LMI_process_OED_depth
{
    public:
        int LMI_process_depth( string strDepth, int iCount, tag_t* ptInTags, vector<tag_t>& refvOutTags ); 

        int LMI_process_depth( string strDepth, const vector<tag_t>& vtInTags, vector<tag_t>& refvOutTags );
};

#endif

