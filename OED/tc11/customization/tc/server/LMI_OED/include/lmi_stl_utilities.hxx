/*======================================================================================================================================================================
                                                    Copyright 2019  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_stl_utilities.hxx

    Description:  Header File for lmi_stl_utilities.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-March-19   Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#ifndef LMI_STL_UTILITIES_HXX
#define LMI_STL_UTILITIES_HXX
    
#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <string.h>
#include <vector>

using namespace std;

int LMI_stl_copy_content_to_vector
(
    vector<tag_t>  vtSrcInfo,
    vector<tag_t>& refvtDestInfo,
    logical         bUnique
);

int LMI_stl_copy_unique_content_to_vector
(
    vector<tag_t>  vtSrcInfo,
    vector<tag_t>& refvtDestInfo
);

int LMI_vector_store_string_array
(
    int             iCount,
    char**          ppszValue,
    logical         bUnique,    
    vector<string>& refvFinal
);

int LMI_str_parse_string
(
    string          strInput,
    string          strSeparator,
    vector<string>& refvValueList
);

int LMI_vector_store_string
(
    string          strValue,
    logical         bUnique,
    vector<string>& refvFinal
);

int LMI_str_tokenize
(
    const char*   pszDelim, 
    const char*   pszInpString,
    vector<string>& vOutStrings
);

int LMI_stl_copy_propvalue_to_vector
(
    vector<LMIPropValue>  vtSrcPropValues,
    vector<LMIPropValue>& refvtDestPropValues
);

int LMI_vector_remove_empty_string
(
    vector<string>& refvStrList
);


int LMI_str_vector_to_char_dbl_ptr
(
    const vector<string>& vStrValues,
    int*    piCount,
    char*** ppszValues
);

int LMI_bool_vector_to_bool_ptr
(
    const vector<logical>& vbValues,
    int*      piCount,
    logical** ppbValues
);

int LMI_int_vector_to_int_ptr
(
    const vector<int>& viValues,
    int*  piCount,
    int** ppiValues
);

int LMI_double_vector_to_double_ptr
(
    const vector<double>& vdValues,
    int*     piCount,
    double** ppdValues
);

int LMI_date_vector_to_date_ptr
(
    const vector<date_t>& vdateValues,
    int*     piCount,
    date_t** ppdateValues
);

int LMI_tag_vector_to_tag_ptr
(
    const vector<tag_t>& vtags,
    int*    piCount,
    tag_t** pptags
);

#endif

