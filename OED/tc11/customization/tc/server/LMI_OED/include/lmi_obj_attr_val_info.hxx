/*======================================================================================================================================================================
                                                    Copyright 2019  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_obj_attr_val_info.hxx

    Description: 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-Oct-19     Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#ifndef LMI_OBJ_ATTR_VAL_INFO_HXX
#define LMI_OBJ_ATTR_VAL_INFO_HXX

#include <unidefs.h>
#include <vector>
#include <lmi_prop_value.hxx>

using namespace std;

class LMIObjAttrValInfo
{
    public:

        LMIObjAttrValInfo()
        {
            
        }

        string strObjUID;
        tag_t  tObject;
        LMIPropValue objPropValue;
};

#endif

