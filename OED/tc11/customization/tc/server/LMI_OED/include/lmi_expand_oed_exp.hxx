/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_expand_oed_exp.hxx

    Description:  Header File for lmi_expand_oed_exp.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Oct-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_EXPAND_OED_EXP_HXX
#define LMI_EXPAND_OED_EXP_HXX
    
#include <lmi_operator.hxx>
#include <lmi_oed_string.hxx>
#include <unidefs.h>

class LMIExpandOEDExp
{
    public:
       int LMI_construct_and_store_valid_OED_pref_vals( tag_t tObject, string strOEDPrefPostfix, logical bValidatePrefValCount, vector<LMIOEDString>& refvobjOEDPrefVals );

       int LMI_store_valid_OED_pref_vals( string strOEDPrefName, logical bValidatePrefValCount, int iValidValCount, vector<LMIOEDString>& refvobjOEDPrefVals );
        
       int LMI_store_valid_OED_string( const vector<string>& vstrOEDPrefVals, vector<LMIOEDString>& refvobjOEDPrefVals );
};

#endif

