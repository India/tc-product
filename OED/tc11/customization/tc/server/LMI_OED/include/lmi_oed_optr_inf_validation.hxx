/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_oed_optr_inf_validation.hxx

    Description:  Header File for lmi_oed_optr_inf_validation.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-DEC-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_OED_OPTR_INF_VALIDATION_HXX
#define LMI_OED_OPTR_INF_VALIDATION_HXX

#include <vector>
#include <lmi_operator.hxx>

using namespace std;

class LMIOEDOptrInfoValidation
{
    public:
        int LMI_get_and_validate_optr_pref_values( const string strOptPrefName, vector<LMIOperator>& refvobjLMIOptrs );

        int LMI_get_and_validate_expr_separator(const vector<LMIOperator>& vobjLMIOptrs, string& refstrOEDExprSeparator);
};

#endif

