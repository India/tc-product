/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_prop_value.hxx

    Description: A container class that holds property value and property type.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-Nov-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_PROP_VALUE_HXX
#define LMI_PROP_VALUE_HXX

#include <vector>
#include <string>

using namespace std;

class LMIPropValue
{
    public:
        vector<string>    vStrValue;
        vector<int>       vIntValue;
        vector<double>    vDoubleValue;
        vector<logical>   vLogicalValue;
        vector<tag_t>     vTagValue; 
        vector<date_t>    vDateValue;
};

#endif

