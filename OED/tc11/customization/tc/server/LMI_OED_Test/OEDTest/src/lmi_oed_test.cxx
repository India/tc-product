/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: lmi_oed_test.cxx

    Description: This file contains function that define KM4_WF_AH_CREATE_COM_PART action handler.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Aug-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#include <lmi_oed_test.hxx>
#include <lmi_library.hxx>
#include <lmi_oed.hxx>
#include <lmi_oed_info.hxx>
#include <lmi_oed_object_info.hxx>
#include <string.h>
#include <vector>

/**
 * This method is fired when "LMI-test-oed" action handler is executed.
 *
 * @param[in] msg Received message
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int LMI_test_oed
(
    EPM_action_message_t msg
)
{
    int    iRetCode        = ITK_ok;
    int    iOEDStrCount    = 0;
    char*  pszPrefPostfix  = NULL;
    char*  pszOEDPref      = NULL;
    char*  pszOperation    = NULL;
    char*  pszOEDStrings   = NULL;
    char*  pszOEDWfl       = NULL;
    char*  pszOEDInPairs   = NULL;
    char** ppszOEDStrings  = NULL;

    LMI_ITKCALL(iRetCode, LMI_arg_get_wf_argument_value( msg.arguments, msg.task, LMI_WF_ARG_OED_OPERATION, false, &pszOperation));

    //LMI_ITKCALL(iRetCode, LMI_validate_operation(pszOperation));

    LMI_ITKCALL(iRetCode, LMI_arg_get_wf_argument_value( msg.arguments, msg.task, LMI_WF_ARG_OED_PREF_POSTFIX, true, &pszPrefPostfix));

    LMI_ITKCALL(iRetCode, LMI_arg_get_wf_argument_value( msg.arguments, msg.task, LMI_WF_ARG_OED_WFL, true, &pszOEDWfl));

    LMI_ITKCALL(iRetCode, LMI_arg_get_wf_argument_value( msg.arguments, msg.task, LMI_WF_ARG_OED_IN_PAIRS, true, &pszOEDInPairs));

    LMI_ITKCALL(iRetCode, LMI_arg_get_wf_argument_value( msg.arguments, msg.task, LMI_WF_ARG_OED_PREF, true, &pszOEDPref));

    LMI_ITKCALL(iRetCode, LMI_arg_get_arguments( msg.arguments, msg.task, LMI_WF_ARG_OED_STRINGS, LMI_STRING_AMPERSAND, true, &iOEDStrCount, &ppszOEDStrings, &pszOEDStrings));

    if ( (iOEDStrCount > 0 || pszOEDPref != NULL || pszPrefPostfix != NULL) && iRetCode == ITK_ok)
    {
        tag_t tRootTask = NULLTAG;

        LMI_ITKCALL(iRetCode, EPM_ask_root_task(msg.task, &tRootTask));

        if (tRootTask != NULLTAG && iRetCode == ITK_ok)
        {
            if (pszOEDWfl != NULL && tc_strcasecmp(LMI_WF_ARG_VAL_TRUE, pszOEDWfl) == 0)
            {
                LMI_ITKCALL(iRetCode, LMI_process_test_oed(tRootTask, pszPrefPostfix, pszOEDPref, iOEDStrCount, ppszOEDStrings, pszOperation, pszOEDInPairs));
            }
            else
            {
                int    iTargetCount = 0;
                tag_t* ptTargets    = NULL;

                LMI_ITKCALL(iRetCode, EPM_ask_attachments(tRootTask, EPM_target_attachment, &iTargetCount, &ptTargets));
                
                if (tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_RETRIEVE_3) == 0)
                {
                    LMI_ITKCALL(iRetCode, LMI_process_test_oed3(iTargetCount, ptTargets, pszOEDPref, iOEDStrCount, ppszOEDStrings));   
                }
                else if (tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_RETRIEVE_4) == 0)
                {
                    LMI_ITKCALL(iRetCode, LMI_process_test_oed4(iTargetCount, ptTargets, pszOEDPref, iOEDStrCount, ppszOEDStrings));   
                }
                else if(tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_PROPAGATE_2) == 0)
                {
                     LMI_ITKCALL(iRetCode, LMI_process_test_oed2(iTargetCount, ptTargets, pszOEDPref, iOEDStrCount, ppszOEDStrings));   
                }
                else if(tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_RETRIEVE) == 0 || 
                    tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_RETRIEVE_BY_UID) == 0 ||
                    tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_PROPAGATE_1) == 0
                    )
                {
                    for (int iDx = 0; iDx < iTargetCount && iRetCode == ITK_ok; iDx++)
                    {
                        LMI_ITKCALL(iRetCode, LMI_process_test_oed(ptTargets[iDx], pszPrefPostfix, pszOEDPref, iOEDStrCount, ppszOEDStrings, pszOperation, pszOEDInPairs));
                    }
                }
                

                LMI_MEM_TCFREE(ptTargets);
            }
        }
    } 

    LMI_MEM_TCFREE(pszPrefPostfix);
    LMI_MEM_TCFREE(pszOEDPref);
    LMI_MEM_TCFREE(pszOperation);
    LMI_MEM_TCFREE(pszOEDWfl);
    LMI_MEM_TCFREE(pszOEDStrings);
    LMI_MEM_TCFREE(ppszOEDStrings);

    return iRetCode;
}

int LMI_validate_operation
(
    char* pszOperation
)
{
    int iRetCode = ITK_ok;

    if (tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_RETRIEVE) != 0 && tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_PROPAGATE_1) != 0
        && tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_PROPAGATE_2) != 0)
    {
        iRetCode = INVALID_WORKFLOW_HANDLER_ARGUMENT_VALUE;
    }

    return iRetCode;
}

int LMI_process_test_oed
(
    tag_t tObject,
    char* pszPrefPostfix,
    char* pszOEDPref, 
    int   iOEDStrCount, 
    char** ppszOEDStrings,
    char* pszOperation,
    char* pszOEDInPairs
)
{
    int iRetCode = ITK_ok;

    if (tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_RETRIEVE_BY_UID) == 0)
    {
        char* pszUID = NULL;
        string strUID = "";
        vector<LMIOEDObjectInfo> vobjInfos;

        LMI_ITKCALL(iRetCode, POM_tag_to_uid(tObject, &pszUID));
        strUID.assign(pszUID);

        if (iOEDStrCount > 0)
        {
            vector<string> vstrOEDStrings;

            LMI_ITKCALL(iRetCode, LMI_vector_store_string_array(iOEDStrCount, ppszOEDStrings, false, vstrOEDStrings));

            LMI_ITKCALL(iRetCode, LMI_uid_process_oed_strings1(strUID, vstrOEDStrings, vobjInfos));
        }
        else if (pszOEDPref != NULL && tc_strlen(pszOEDPref) > 0)
        {
            LMI_ITKCALL(iRetCode, LMI_uid_process_oed_preference1(strUID, pszOEDPref, vobjInfos));
        }
        else if (pszPrefPostfix != NULL && tc_strlen(pszPrefPostfix) > 0)
        {
            LMI_ITKCALL(iRetCode, LMI_uid_process_oed_by_pref_postfix(strUID, pszPrefPostfix, vobjInfos));
        }

        LMI_ITKCALL(iRetCode, LMI_info_bom_close_windows(vobjInfos));
        LMI_MEM_TCFREE(pszUID);
    }
    else if (tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_RETRIEVE) == 0)
    {
        if (tc_strcasecmp(pszOEDInPairs, LMI_WF_ARG_VAL_TRUE) == 0)
        {
            LMIOEDInfo objInfo;
   
            if (iOEDStrCount > 0)
            {
                vector<string> vstrOEDStrings;

                LMI_ITKCALL(iRetCode, LMI_vector_store_string_array(iOEDStrCount, ppszOEDStrings, false, vstrOEDStrings));

                LMI_ITKCALL(iRetCode, LMI_process_src_dest_oed_strings1(tObject, vstrOEDStrings, objInfo));
            }
            else if (pszOEDPref != NULL && tc_strlen(pszOEDPref) > 0)
            {
                LMI_ITKCALL(iRetCode, LMI_process_src_dest_oed_preference1(tObject, pszOEDPref, objInfo));
            }
            else if (pszPrefPostfix != NULL && tc_strlen(pszPrefPostfix) > 0)
            {
                LMI_ITKCALL(iRetCode, LMI_process_src_dest_oed_by_pref_postfix(tObject, pszPrefPostfix, objInfo));
            }

            LMI_ITKCALL(iRetCode, LMI_src_dest_bom_close_windows(objInfo));
        }
        else
        {
            vector<LMIOEDObjectInfo> vobjInfos;

            if (iOEDStrCount > 0)
            {
                vector<string> vstrOEDStrings;

                LMI_ITKCALL(iRetCode, LMI_vector_store_string_array(iOEDStrCount, ppszOEDStrings, false, vstrOEDStrings));

                LMI_ITKCALL(iRetCode, LMI_process_oed_strings1(tObject, vstrOEDStrings, vobjInfos));
            }
            else if (pszOEDPref != NULL && tc_strlen(pszOEDPref) > 0)
            {
                LMI_ITKCALL(iRetCode, LMI_process_oed_preference1(tObject, pszOEDPref, vobjInfos));
            }
            else if (pszPrefPostfix != NULL && tc_strlen(pszPrefPostfix) > 0)
            {
                LMI_ITKCALL(iRetCode, LMI_process_oed_by_pref_postfix(tObject, pszPrefPostfix, vobjInfos));
            }

            LMI_ITKCALL(iRetCode, LMI_info_bom_close_windows(vobjInfos));
        }
    }
    else if(tc_strcasecmp(pszOperation, LMI_WF_ARG_VAL_PROPAGATE_1) == 0)
    {
        if (iOEDStrCount > 0)
        {
            vector<string> vstrOEDStrings;

            LMI_ITKCALL(iRetCode, LMI_vector_store_string_array(iOEDStrCount, ppszOEDStrings, false, vstrOEDStrings));

            LMI_ITKCALL(iRetCode, LMI_propagate_oed_strings_exp_val1(tObject, vstrOEDStrings));
        }
        else if (pszOEDPref != NULL && tc_strlen(pszOEDPref) > 0)
        {
            LMI_ITKCALL(iRetCode, LMI_propagate_oed_preference_exp_val1(tObject, pszOEDPref));
        }
        else if (pszPrefPostfix != NULL && tc_strlen(pszPrefPostfix) > 0)
        {
            LMI_ITKCALL(iRetCode, LMI_propagate_oed_by_pref_postfix1(tObject, pszPrefPostfix));
        }
    }

    return iRetCode;
}

int LMI_process_test_oed2
(
    int    iCount,
    tag_t* ptObjects,
    char*  pszOEDPref, 
    int    iOEDStrCount, 
    char** ppszOEDStrings
)
{
    int iRetCode = ITK_ok;
    vector<tag_t> vtObjects;

    LMI_ITKCALL(iRetCode, LMI_vector_store_tag_array(iCount, ptObjects, true, vtObjects));

    if (iOEDStrCount > 0)
    {
        vector<string> vstrOEDStrings;

        LMI_ITKCALL(iRetCode, LMI_vector_store_string_array(iOEDStrCount, ppszOEDStrings, false, vstrOEDStrings));

        LMI_ITKCALL(iRetCode, LMI_propagate_oed_strings_exp_val2(vtObjects, vstrOEDStrings));
    }
    else if (pszOEDPref != NULL && tc_strlen(pszOEDPref) > 0)
    {
        LMI_ITKCALL(iRetCode, LMI_propagate_oed_preference_exp_val2(vtObjects, pszOEDPref));
    }

    return iRetCode;
}

int LMI_process_test_oed3
(
    int    iCount,
    tag_t* ptObjects,
    char*  pszOEDPref, 
    int    iOEDStrCount, 
    char** ppszOEDStrings
)
{
    int iRetCode = ITK_ok;
    vector<tag_t> vtObjects;
    vector<LMIOEDObjectInfo> vObjectsInfo;

    LMI_ITKCALL(iRetCode, LMI_vector_store_tag_array(iCount, ptObjects, true, vtObjects));

    if (iOEDStrCount > 0)
    {
        vector<string> vstrOEDStrings;

        LMI_ITKCALL(iRetCode, LMI_vector_store_string_array(iOEDStrCount, ppszOEDStrings, false, vstrOEDStrings));

        LMI_ITKCALL(iRetCode, LMI_process_oed_strings2(vtObjects, vstrOEDStrings, vObjectsInfo));
    }
    else if (pszOEDPref != NULL && tc_strlen(pszOEDPref) > 0)
    {
        LMI_ITKCALL(iRetCode, LMI_process_oed_preference2(vtObjects, pszOEDPref, vObjectsInfo));
    }

    LMI_ITKCALL(iRetCode, LMI_info_bom_close_windows(vObjectsInfo));

    return iRetCode;
}


int LMI_process_test_oed4
( 
    int    iCount,
    tag_t* ptObjects,
    char*  pszOEDPref, 
    int    iOEDStrCount, 
    char** ppszOEDStrings
)
{
    int iRetCode = ITK_ok;
    vector<tag_t> vtObjects;
    LMIOEDInfo objOEDInfo;

    LMI_ITKCALL(iRetCode, LMI_vector_store_tag_array(iCount, ptObjects, true, vtObjects));

    if (iOEDStrCount > 0)
    {
        vector<string> vstrOEDStrings;

        LMI_ITKCALL(iRetCode, LMI_vector_store_string_array(iOEDStrCount, ppszOEDStrings, false, vstrOEDStrings));

        LMI_ITKCALL(iRetCode, LMI_process_src_dest_oed_strings2(vtObjects, vstrOEDStrings, objOEDInfo));
    }
    else if (pszOEDPref != NULL && tc_strlen(pszOEDPref) > 0)
    {
        LMI_ITKCALL(iRetCode, LMI_process_src_dest_oed_preference2(vtObjects, pszOEDPref, objOEDInfo));
    }

    LMI_ITKCALL(iRetCode, LMI_src_dest_bom_close_windows(objOEDInfo));

    return iRetCode;
}


int LMI_src_dest_bom_close_windows
(
    LMIOEDInfo objOEDInfo
)
{
    int iRetCode = ITK_ok;

    if (objOEDInfo.vOEDSrcDestInfo.size() > 0)
    {
        vector<LMIOEDSrcDestInfo>::const_iterator iterSrcDestInfo;

        for(iterSrcDestInfo = objOEDInfo.vOEDSrcDestInfo.begin(); iterSrcDestInfo < objOEDInfo.vOEDSrcDestInfo.end() && iRetCode == ITK_ok; iterSrcDestInfo++)
        {
            LMI_ITKCALL(iRetCode, LMI_BOM_close_windows(iterSrcDestInfo->objSrcInfo));

            LMI_ITKCALL(iRetCode, LMI_BOM_close_windows(iterSrcDestInfo->objDestInfo));
        }   
    }

    return iRetCode;
}


int LMI_BOM_close_windows
(
    const LMIOEDObjectInfo& refOEDObjInfo 
)
{
    int iRetCode = ITK_ok;

    /* Need to close BOM line only if property value is retrieved from BOM line */
    if (refOEDObjInfo.bIsBLAttr == true && refOEDObjInfo.vLMIBOMInfos.size() > 0)
    {
        vector<LMIBOMInfo>::const_iterator iterBOMInfo;

        for (iterBOMInfo = refOEDObjInfo.vLMIBOMInfos.begin(); iterBOMInfo < refOEDObjInfo.vLMIBOMInfos.end() && iRetCode == ITK_ok; iterBOMInfo++)
        {
            LMI_ITKCALL(iRetCode, BOM_close_window(iterBOMInfo->tBOMWindow));
        }
    }

    return iRetCode;
}


int LMI_info_bom_close_windows(const vector<LMIOEDObjectInfo>& vobjInfos)
{
    int iRetCode = ITK_ok;

    vector<LMIOEDObjectInfo>::const_iterator iterObjInfo;

    for (iterObjInfo = vobjInfos.begin(); iterObjInfo < vobjInfos.end(); iterObjInfo++)
    {
        LMI_ITKCALL(iRetCode, LMI_BOM_close_windows((*iterObjInfo)));
    }

    return iRetCode;
}