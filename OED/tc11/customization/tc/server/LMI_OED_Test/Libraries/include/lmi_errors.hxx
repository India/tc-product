/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_errors.hxx

    Description:  Header File for containing macros for custom errors

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/

#ifndef LMI_ERRORS_HXX
#define LMI_ERRORS_HXX

#include <common/emh_const.h>

#define ERROR_CODE_NOT_STORED_ON_STACK                              (EMH_USER_error_base + 1)
#define INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME                   (EMH_USER_error_base + 2)
#define INVALID_INPUT_TO_FUNCTION                                   (EMH_USER_error_base + 3)
#define INCORRECT_PREFERENCE_VALUE                                  (EMH_USER_error_base + 4)
#define OBJ_EXPANSION_INFO_NOT_DEFINED_PROPERLY                     (EMH_USER_error_base + 5)
#define ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND                       (EMH_USER_error_base + 10)
#define UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES                     (EMH_USER_error_base + 11)
#define INVALID_PATH_TO_CREATE_FILE                                 (EMH_USER_error_base + 12)
#define NOT_A_VALID_FILE_NAME                                       (EMH_USER_error_base + 13)
#define INCORRECT_ATTR_VALUE_FOR_CLONE_FILE_NAME                    (EMH_USER_error_base + 14)
#define NO_DATA_TO_EXPORT_CLONE_FILE                                (EMH_USER_error_base + 15)
#define FAILED_TO_CREATE_CLONE_FILE                                 (EMH_USER_error_base + 16)
#define MISSING_PREFERENCE_FOR_CLONE_FILE_HEADER_OR_PART_INFO       (EMH_USER_error_base + 17)
#define OED_STRING_IS_NOT_CONSTRUCTED_PROPERLY                      (EMH_USER_error_base + 18)
#define NO_VALID_IR_EXIST_FOR_CREATING_REPORT                       (EMH_USER_error_base + 19)
#define INVALID_WORKFLOW_HANDLER_ARGUMENT_VALUE                     (EMH_USER_error_base + 20)
#endif


