/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: lmi_const.hxx

    Description:  Header File for constants used during scope of LMI_OED dll.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_CONST_HXX
#define LMI_CONST_HXX

/* Date Time Format */
#define LMI_DATE_TIME_FORMAT                                     "%d.%m.%Y"

/* Prefixes */
#define LMI_BMIDE_PREFIX                                         "LMI_"
#define LMI_BL_ATTR_NAME_PREFIX                                  "bl_"

/* Postfixes */
#define LMI_PREF_PROPAGATE_ATTR_VALUE_POSTFIX                    "_Propagate_Attribute_Value"
#define LMI_PREF_PROPAGATE_PROJECT_VALUE_POSTFIX                 "_Propagate_Project"

/* Infix */

/* Preferences */
#define LMI_PREF_PROPERTY_PROPAGATION_ITEM_TYPE_LIST             "LMI_Property_Propagation_Item_Type_List"
#define LMI_PREF_EXCLUDE_PROP_PROPAGATION_VALIDATION_ATTR_LIST   "LMI_Exclude_Property_Propagation_Validation_Attribute_List"
#define LMI_PREF_LMI_COMM_PART_CREATE_ATTR_NAME_VAL_INFO         "LMI_CommPart_create_attr_name_val_info"
#define LMI_PREF_LMI_COMM_PART_REV_CREATE_ATTR_NAME_VAL_INFO     "LMI_CommPartRevision_create_attr_name_val_info"
#define LMI_PREF_PDF_Obj_DEFAULT_NAME                            "LMI_Pdf_Obj_Default_Name"
#define LMI_TEST_PREF                                            "LMI_TestPref"

/* Relation */
#define LMI_IMAN_SPECIFICATION                                   "IMAN_specification"
#define LMI_IMAN_BASED_ON                                        "IMAN_based_on"
#define LMI_IMAN_MASTER_FORM                                     "IMAN_master_form"
#define LMI_IMAN_REFERENCE                                       "IMAN_reference"
#define LMI_DERIVED_DATA_RELATION                                "LMI_Derived_Data_rel"
#define LMI_PDF_RELATION                                         "LMI_PDF_Rel"
#define LMI_IMAN_MANIFESTATION                                    "IMAN_manifestation"
#define LMI_HAS_FUNC_REL                                            "LMI_Has_Func_Rel"
#define LMI_HAS_MATL_REL                                            "LMI_Has_Matl_Rel"
#define LMI_DESCRIBES_REL                                        "LMI_Describes_Rel"

/* Status Names*/
#define LMI_STATUS_RELEASE                                       "LMI_Released"

/* Attributes  */
#define LMI_ATTR_DATE_RELEASED                                   "date_released"
#define LMI_ATTR_RELEASE_STATUS_LIST                             "release_status_list"
#define LMI_ATTR_RELEASE_STATUS_NAME                             "name"
#define LMI_ATTR_OBJECT_NAME                                     "object_name"
#define LMI_ATTR_DESCRIPTION                                     "object_desc"
#define LMI_ATTR_ITEM_TAG                                        "items_tag"
#define LMI_ATTR_ITEM_ID                                         "item_id"
#define LMI_ATTR_ITEM_REVISION_ID                                "item_revision_id"
#define LMI_ATTR_OBJECT_TYPE                                     "object_type"
#define LMI_ATTR_OBJECT_STRING                                   "object_string"
#define LMI_ATTR_OBJECT_CREATION_DATE                            "creation_date"
#define LMI_ATTR_PROJ_LIST                                       "project_list"
#define LMI_ATTR_PROCESS_STAGE_LIST                              "process_stage_list"
#define LMI_ATTR_PSOCC_CHILD_ITEM                                "child_item"
#define LMI_ATTR_PSOCC_PARENT_BVR                                "parent_bvr"
#define LMI_ATTR_USER_DATA_1                                     "user_data_1"
#define LMI_ATTR_SIGNOFF_ATTACHMENTS                             "signoff_attachments"
#define LMI_ATTR_PARENT_PROCESSES                                "parent_processes"
#define LMI_ATTR_ALL_TASKS                                       "all_tasks"
#define LMI_ATTR_ACTIVE_SEQ                                      "active_seq"
#define LMI_ATTR_EPM_TASK_ATTACHMENTS                            "attachments"
#define LMI_ATTR_PRIMARY_OBJ                                     "primaryObjects"
#define LMI_ATTR_SECONDARY_OBJ                                   "secondaryObjects"
#define LMI_ATTR_STATE                                           "state"
#define LMI_ATTR_UOM_TAG                                         "uom_tag"
#define LMI_ATTR_CONTENTS                                        "contents"
#define LMI_ATTR_LMI_PDF_Rel                                     "LMI_PDF_Rel"
#define LMI_ATTR_OEM                                             "LMI_oem"
#define LMI_ATTR_MODEL                                             "LMI_model"
#define LMI_ATTR_PART_NUMBER                                     "LMI_part_number"
#define LMI_ATTR_REVISION                                         "revision"

/* Class Name */
#define LMI_CLASS_RELEASE_STATUS                                 "releasestatus"
#define LMI_CLASS_WORKSPACEOBJECT                                "WorkspaceObject"
#define LMI_CLASS_FORM                                           "Form"
#define LMI_CLASS_STATUS                                         "ReleaseStatus"
#define LMI_CLASS_ITEMREVISION                                   "ItemRevision"
#define LMI_CLASS_ITEM                                           "Item"
#define LMI_CLASS_DATASET                                        "Dataset"
#define LMI_CLASS_EPM_TASK                                       "EPMTask"
#define LMI_CLASS_IMANRELATION                                   "ImanRelation"
#define LMI_CLASS_PSOCCURRENCE                                   "PSOccurrence"
#define LMI_CLASS_PDF_DATASET                                    "PDF"
#define LMI_CLASS_FOLDER                                         "Folder"
#define LMI_CLASS_FOLDER_BASE                                    "LMI_Folder_Base"
#define LMI_CLASS_ENVELOPE                                       "Envelope"
#define LMI_CLASS_MASTER_FORM                                    "IMAN_master_form"
#define LMI_CLASS_TEMPLATE_BASEREVISION                          "LMI_Template_BaseRevision"
#define LMI_CLASS_PROJECT_TEMPLATE_REVISION                      "LMI_Proj_TemplateRevision"
#define LMI_CLASS_CUSTOMPDX                                      "LMI_Custom_PDX"
#define LMI_CLASS_COMM_PART_REVISION                             "LMI_CommPartRevision"
#define LMI_CLASS_COMM_PART                                      "LMI_CommPart"
#define LMI_CLASS_SEMI_FINI_PRO_REVISION                         "LMI_SemiFiniProRevision"
#define LMI_CLASS_SYSTEM_REVISION                                "LMI_SystemRevision"
#define LMI_CLASS_MATERIAL_REVISION                              "LMI_MaterialRevision"
#define LMI_CLASS_FUNCTION                                       "LMI_Function"
#define LMI_CLASS_PART_REVISION                                  "Part Revision"


/* Query Constants */


/* Type Name */
#define LMI_TYPE_NEWSTUFF_FOLDER                                 "Newstuff Folder"

/* Name Of Privileges  */
#define LMI_WRITE_ACCESS                                         "WRITE"

/* Environment Variable */
#define LMI_USER_TEMP_DIR                                        "TMP"

/* Logical String Response */
#define LMI_CAPS_YES                                             "Y"
#define LMI_SMALL_YES                                            "y"
#define LMI_CAPS_NO                                              "N"
#define LMI_SMALL_NO                                             "n"

/* String Macro  */
#define LMI_I                                                    "I"
#define LMI_IR                                                   "IR"
#define LMI_NO_VAL                                               "NO_VAL"

/* Folder Names String */
#define LMI_NEWSTUFF_FOLDER                                      "newstuff"
#define LMI_HOME_FOLDER                                          "home"

/* Separators */
#define LMI_STRING_UNDERSCORE                                    "_"
#define LMI_STRING_COMMA                                         ","
#define LMI_STRING_SEMICOLON                                     ";"
#define LMI_CHAR_SEMICOLON                                       ';'
#define LMI_STRING_FORWARD_SLASH                                 "/"
#define LMI_STRING_BACKWARD_SLASH                                "\\"
#define LMI_STRING_SPACE                                         " "
#define LMI_STRING_COLON                                         ":"
#define LMI_STRING_DOT                                           "."
#define LMI_STRING_OPENING                                       "\""
#define LMI_STRING_ZERO                                          "0"
#define LMI_STRING_DOLLAR                                        "$"
#define LMI_STRING_AMPERSAND                                     "&"
#define LMI_STRING_BLANK                                         ""
#define LMI_STRING_GREATER_THAN                                  ">"
#define LMI_STRING_LESS_THAN                                     "<"
#define LMI_STRING_ASTERISK                                         "*"
#define LMI_CHAR_DOLLAR                                          '$'

#define LMI_STRING_ATTR_LEN                                      256
#define LMI_SEPARATOR_LENGTH                                     1
#define LMI_NUMERIC_ERROR_LENGTH                                 12


/* Dataset Tools */
#define LMI_DS_TA_PDF_TOOL                                       "PDF_Tool"
#define LMI_DS_TA_ADOBE_ACROBAT_TOOL                             "Adobe Acrobat"

/* Dataset References Format */
#define LMI_DS_REFERENCES_FORMAT_BINARY                          "BINARY"

/* Constant Object Names */
#define LMI_DS_PDX_NAME                                          "PDX D1"
#define LMI_DS_PDF_NAME                                          "PDF D1"
#define LMI_FUNCTION_OBJ_NAME                                    "Function F1"

/* keywords */
#define LMI_KW_TARGET                                            "$TARGET"
#define LMI_KW_REFERENCE                                         "$REFERENCE"

/* Workflow Argument Names */
#define LMI_WF_ARG_OED_PREF_POSTFIX                              "oed_pref_postfix"
#define LMI_WF_ARG_OED_PREF                                      "oed_pref"
#define LMI_WF_ARG_OED_STRINGS                                   "oed_strings"
#define LMI_WF_ARG_OED_OPERATION                                 "oed_operation"
#define LMI_WF_ARG_OED_WFL                                       "oed_wfl"
#define LMI_WF_ARG_OED_IN_PAIRS                                  "oed_in_pairs"

/* Workflow Argument Values */
#define LMI_WF_ARG_VAL_ALL                                       "all"
#define LMI_WF_ARG_VAL_TARGET                                    "target"
#define LMI_WF_ARG_VAL_REFERENCE                                 "reference"
#define LMI_WF_ARG_VAL_USER                                      "user"
#define LMI_WF_ARG_VAL_PERSON                                    "person"
#define LMI_WF_ARG_VAL_ANY                                       "any"
#define LMI_WF_ARG_VAL_NONE                                      "none"
#define LMI_WF_ARG_VAL_RETRIEVE                                  "retrieve"
#define LMI_WF_ARG_VAL_RETRIEVE_3                                "retrieve3"
#define LMI_WF_ARG_VAL_RETRIEVE_4                                "retrieve4"
#define LMI_WF_ARG_VAL_RETRIEVE_BY_UID                           "retrieve_by_uid"
#define LMI_WF_ARG_VAL_PROPAGATE_1                               "propagate1"
#define LMI_WF_ARG_VAL_PROPAGATE_2                               "propagate2"
#define LMI_WF_ARG_VAL_TRUE                                      "true"

/* Workflow Action Handler Names  */
#define LMI_TEST_OED                                             "LMI-test-oed"

/* Attributes Values */
#define LMI_ATTR_OEM_VAL                                         "Honda"
#define LMI_ATTR_MODEL_VAL                                         "City"

#endif

