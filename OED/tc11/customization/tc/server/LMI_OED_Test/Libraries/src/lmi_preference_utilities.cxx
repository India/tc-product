/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: lmi_preference_utilities.cxx

    Description: This File contains functions for Retrieving and Manipulating Preference. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>

/**
 * Function to read Teamcenter preference with string value
 *
 * @param[in]  pszPrefName   Preference Name
 * @param[out] ppszPrefValue String value of preference
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI_pref_get_string_value
(
    char*  pszPrefName, 
    char** ppszPrefValue
)
{
    int iRetCode = ITK_ok;
    int iPrefCnt = 0;

    TC_preference_search_scope_t prefOldScope;
    
    LMI_ITKCALL(iRetCode, PREF_initialize());

    LMI_ITKCALL(iRetCode, PREF_ask_search_scope(&prefOldScope));

    LMI_ITKCALL(iRetCode, PREF_set_search_scope(TC_preference_site));

    LMI_ITKCALL(iRetCode, PREF_ask_value_count(pszPrefName, &iPrefCnt));

    if(iPrefCnt != 0)
    {
        LMI_ITKCALL(iRetCode, PREF_ask_char_value(pszPrefName, 0, ppszPrefValue));
    }

    LMI_ITKCALL(iRetCode, PREF_set_search_scope(prefOldScope));

    return iRetCode;
}

/**
 * Function to read Teamcenter preference with Multiple string value
 *
 * @param[in]  pszPrefName   Preference Name
 * @param[out] piPrefValCnt  Total number of preference values
 * @param[out] pppszPrefVals String array of preference values
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI_pref_get_string_values
(
    char*   pszPrefName, 
    int*    piPrefValCnt, 
    char*** pppszPrefVals
)
{
    int iRetCode = ITK_ok;
    int iPrefCnt = 0;

    TC_preference_search_scope_t prefOldScope;
    
    LMI_ITKCALL(iRetCode, PREF_initialize());

    LMI_ITKCALL(iRetCode, PREF_ask_search_scope(&prefOldScope));

    LMI_ITKCALL(iRetCode, PREF_set_search_scope(TC_preference_site));

    LMI_ITKCALL(iRetCode, PREF_ask_value_count(pszPrefName, &iPrefCnt));

    if(iPrefCnt != 0)
    {
        LMI_ITKCALL(iRetCode, PREF_ask_char_values(pszPrefName, piPrefValCnt, pppszPrefVals));
    }

    LMI_ITKCALL(iRetCode, PREF_set_search_scope(prefOldScope));

    return iRetCode;
}

