
/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: lmi_string_utilities.cxx

    Description: This File contains functions for manipulating string. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>

/**
 * Parses a string containing values seperated by a given separator into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in]  pszList        Input list to be parsed
 * @param[in]  pszSeparator   Separator 
 * @param[out] iCount         Number of values in the returned string list
 * @param[out] pppszValuelist Returned list of values
 *
 * @note The returned array should be freed by A4_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 */
int LMI_str_parse_string
(
    const char* pszList,
    char*       pszSeparator,
    int*        iCount,
    char***     pppszValuelist
)
{
    int   iRetCode    = ITK_ok;
    int   iCounter    = 0;
    char* pszToken    = NULL;
    char* pszTempList = NULL;

     /* Validate input */
    if(pszList == NULL || pszSeparator == NULL)
    {
        TC_write_syslog( "ERROR: Missing input data: List passed or seprator is null\n");
    }
    else
    {
        /* Copying the input string to NULL terminated string */
        LMI_STRCPY(pszTempList, pszList);

        /* Get the first token */
        pszToken = tc_strtok(pszTempList, pszSeparator);
        
        /* Tokenize the input string */
        while(pszToken != NULL)
        {
            LMI_UPDATE_STRING_ARRAY((*iCount), (*pppszValuelist), pszToken);

            /* Get next token: */
            pszToken = tc_strtok(NULL, pszSeparator);
        }
    }

    /* Free the temporary memory used for tokenizing */
    LMI_MEM_TCFREE(pszTempList);
    return iRetCode;
}

/**
 * This function returns the substring when provided with start and end position on the the input string. Substring is the
 * character sequence that starts at character position 'iStrStartPosition' and ends at character position 'iStrEndPosition'
 *
 * For example If 'pszInputString' = 'Application'
 *                'iStrStartPosition' = 4
 *                'iStrEndPosition'   = 9
 *             then Output Will be: 'licati'
 *
 * Function will copy the characters starting 4th character to the 9th character of input string.
 * 
 * @param[in]  pszInputString    Actual input string
 * @param[in]  iStrStartPosition Position of a character in the current string to be used as starting character for the substring.
 * @param[in]  iStrEndPosition   Position of a character in the current string to be used as last character for the substring.
 * @param[out] ppszCopyedStr     Copied string.
 *
 * @retval INVALID_INPUT_TO_FUNCTION if input not correct
 * @retval ITK_ok on successful execution
 */
int LMI_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
)
{
    int iRetCode = ITK_ok;
    int iStrLen  = 0;

    /* Initializing out parameter of this function. */
    (*ppszCopyedStr) = NULL;

    iStrLen = (int) tc_strlen(pszInputString);
    
    if((pszInputString != NULL) && (iStrStartPosition >= 0) && (iStrEndPosition <=  iStrLen) && (iStrStartPosition <= iStrEndPosition))
    {
        if(tc_strcmp(pszInputString, "") != 0)
        {
            int iDx        = 0;
            int iNewStrLen = 0;

            iNewStrLen = iStrEndPosition - iStrStartPosition + 2;

            (*ppszCopyedStr) = (char*) MEM_alloc(sizeof(char*) * iNewStrLen);

            while(iStrStartPosition <= iStrEndPosition)
            {
                (*ppszCopyedStr)[iDx] = pszInputString[iStrStartPosition-1];
                iDx++;
                iStrStartPosition++;
            }

            (*ppszCopyedStr)[iDx] = '\0';
        }
    }
    else
    {
        iRetCode = INVALID_INPUT_TO_FUNCTION;
        EMH_store_error(EMH_severity_error, iRetCode);
    }

    return iRetCode;
}


/**
 * Function checks if given string exists in string of array or not 
 *
 * @param[in]  pszStrVal     String to be looked into string array
 * @param[in]  iTotalStrVals String Array Size 
 * @param[in]  pppszStrVals  String Array
 * @param[out] pbFound       'true' if input string 'pszStrVal' exist in the string array
 *
 * @return ITK_ok if string is found in the array else return ITK_ok
 */
int LMI_contains_string_value
(
    char*    pszStrVal, 
    int      iTotalStrVals, 
    char**   pppszStrVals,
    logical* pbFound
)
{
    int iRetCode = ITK_ok;

    /* Initializing out parameter of this function. */
    (*pbFound) = false;

    for(int iNx = 0; iNx < iTotalStrVals; iNx++)
    {
        if(tc_strcmp( pszStrVal, pppszStrVals[iNx]) == 0)
        {
            (*pbFound) = true;
        }
    }
    
    return iRetCode;
}

/**
 * Function replaces all occurrences of input sub string within the input source string with replacement string.
 *
 * @note: if sub string does not exist as part of input string then new string will have value same as input string
 *
 * For e.g.       'pszInputStr'       = 'Function Example'
 *                'pszSubstring'      = 'ion'
 *                'pszReplacementStr' = 'x1x'
 *                Output Will be: 'Functx1x Example'
 * 
 *                'pszInputStr'       = 'Fun'
 *                'pszSubstring'      = 'IN'
 *                'pszReplacementStr' = 'x1x'
 *                Output Will be: 'Fun'
 *
 *                'pszInputStr'       = 'Fun'
 *                'pszSubstring'      = 'Fun'
 *                'pszReplacementStr' = 'x1x'
 *                Output Will be: 'x1x'
 *
 * @param[in]  pszInputStr       Source string 
 * @param[in]  pszSubstring      Sub String if found in the source string will be replaced
 *                               with new replacement string.
 * @param[in]  pszReplacementStr Replacement string 
 * @param[out] ppszNewString     New string.
 *
 * @return ITK_ok if string is found in the array else return ITK_ok
 */
int LMI_replace_sub_string
(
    char*  pszInputStr, 
    char*  pszSubstring,
    char*  pszReplacementStr,
    char** ppszNewString
)
{
    int iRetCode = ITK_ok;
    
    /* Validate input */
    if(tc_strlen(pszInputStr) == 0 && pszInputStr == NULL || tc_strlen(pszSubstring) == 0 && pszSubstring == NULL || 
        tc_strlen(pszReplacementStr) == 0 && pszReplacementStr == NULL)
    {
        iRetCode = INVALID_INPUT_TO_FUNCTION;
        EMH_store_error(EMH_severity_error, iRetCode);
    }
    else
    {
        int    iCount        = 0;
        char** ppszValueList = NULL;

        LMI_ITKCALL(iRetCode, LMI_str_parse_string(pszInputStr, pszSubstring, &iCount, &ppszValueList));

        for(int iDx = 0; iDx < iCount && iRetCode == ITK_ok; iDx++)
        {
            if(iCount == 1)
            {
                if(tc_strcmp(ppszValueList[iDx], pszSubstring) == 0)
                {
                    LMI_STRCPY((*ppszNewString), pszReplacementStr);
                }
                else
                {
                    LMI_STRCPY((*ppszNewString), pszInputStr);
                }
            }
            else
            {
                LMI_STRCAT((*ppszNewString), ppszValueList[iDx]);
                
                if(iDx+1 < iCount)
                {
                    LMI_STRCAT((*ppszNewString), pszReplacementStr);
                }
            }
        }

        LMI_MEM_TCFREE(ppszValueList);
    }

    return iRetCode;
}

/**
 * This function checks if input string contains sub-string which is provided as second input argument.
 *
 * @param[in]  pszInputString  Actual input string
 * @param[in]  pszSubString    Sub String to find
 * @param[out] pbSubStrFound   'true' if input string contains Sub String else 'false'
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_string_find_sub_str
(
    char*    pszInputString,
    char*    pszSubString,
    logical* pbSubStrFound
)
{
    int   iRetCode    = ITK_ok;
    char* pszPosition = NULL;  /* Never free this variable because u will actually be trying to free input variable */
    
    pszPosition = tc_strstr(pszInputString, pszSubString);

    if(pszPosition == NULL) 
    {
        (*pbSubStrFound) = false;
    }       
    else
    {
        (*pbSubStrFound) = true;        
    }

    return iRetCode;
}


/**
 * Function split the input string based on very first occurrence of input Delimiter character. This function will return the remaining string after the Delimiter. 
 * For e.g. 
 * If input string is "ENT_01-Test_Data-1" and then the output string will be "Test_Data-1"
 *
 * @param[in]  pszInputStr    String to be checked for Delimiter occurrence
 * @param[in]  szDelim        Delimiter
 * @param[out] ppszTrimmedStr Trimmed String
 *
 * @return 
 */
int LMI_get_str_after_delimiter
(
    char*      pszInputStr,
    const char szDelim, 
    char**     ppszTrimmedStr
)
{
    int iRetCode  = ITK_ok;

    pszInputStr = STRNG_find_first_char((const char*)pszInputStr, szDelim);
   
    if(pszInputStr != NULL)
    {   
        /* We are deliberating increasing the "pszInputStr" to 1 in the command below because we want to skip the delimiter and want to copy rest of the string. */
        LMI_STRCPY((*ppszTrimmedStr), (pszInputStr+1));
    }

    return iRetCode;
}

/**
 * This function loops input array of strings "ppszValue" and add string from input string array to vector "refvFinal". If input argument "bUnique" is 'true' than string from input array is directly stored 
 * in vector "refvFinal" otherwise uniqueness check is performed and only if the value is not found in vector "refvFinal" than it is stored in the vector. 
 * 
 * @param[in]  iCount    Size of the string array "ppszValue"
 * @param[in]  ppszValue Array containing strings
 * @param[in]  bUnique   If "bUnique" is 'true' than value is checked for uniqueness and if found unique than value is pushed into vector.
 * @param[out] refvFinal List of values.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI_vector_store_string_array
(
    int             iCount,
    char**          ppszValue,
    logical         bUnique,    
    vector<string>& refvFinal
)
{
    int iRetCode = ITK_ok;

    for(int iDx = 0; iDx < iCount; iDx++)
    {
        if(ppszValue[iDx] != NULL && tc_strlen(ppszValue[iDx]) > 0)
        {
            if(bUnique == false)
            {
                refvFinal.push_back(ppszValue[iDx]);
            }
            else
            {
                vector<string>::iterator itervFinal;

                itervFinal = find(refvFinal.begin(), refvFinal.end(), ppszValue[iDx]);

                if(itervFinal == refvFinal.end())
                {
                    refvFinal.push_back(ppszValue[iDx]);
                }
            }
        }
    }    

    return iRetCode;
}

int LMI_vector_store_tag_array
(
    int             iCount,
    tag_t*          pObjects,
    logical         bUnique,    
    vector<tag_t>&  refvFinal
)
{
    int iRetCode = ITK_ok;

    for(int iDx = 0; iDx < iCount; iDx++)
    {
        if(pObjects[iDx] != NULL)
        {
            if(bUnique == false)
            {
                refvFinal.push_back(pObjects[iDx]);
            }
            else
            {
                vector<tag_t>::iterator itervFinal;

                itervFinal = find(refvFinal.begin(), refvFinal.end(), pObjects[iDx]);

                if(itervFinal == refvFinal.end())
                {
                    refvFinal.push_back(pObjects[iDx]);
                }
            }
        }
    }    

    return iRetCode;
}

