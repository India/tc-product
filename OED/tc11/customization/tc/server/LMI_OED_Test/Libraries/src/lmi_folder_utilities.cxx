/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: lmi_folder_utilities.cxx

    Description: This File contains custom functions for getting information about Folder to perform generic operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/

#include <lmi_library.hxx>

/**
 * Function creates custom folder and set "Name" and "Description" attribute  with values as specified by input argument to this function.
 *
 * @param[in]  pszFLType Class name of custom folder. e.g. A4_Project_Folder
 * @param[in]  pszFLName Value for attribute 'object_name'
 * @param[in]  pszFLDesc Description to be set for new instance of folder
 * @param[out] ptFolder  Tag to instance of Folder.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int LMI_create_custom_folder
(
    char*  pszFLType,
    char*  pszFLName,
    char*  pszFLDesc,
    tag_t* ptFolder
)
{
    int   iRetCode       = ITK_ok;
    tag_t tFLType        = NULLTAG;
    tag_t tFLCreateInput = NULLTAG;

    LMI_ITKCALL(iRetCode, TCTYPE_find_type(pszFLType, NULL, &tFLType));

    LMI_ITKCALL(iRetCode, TCTYPE_construct_create_input(tFLType, &tFLCreateInput));

    LMI_ITKCALL(iRetCode, AOM_set_value_string(tFLCreateInput, LMI_ATTR_OBJECT_NAME, pszFLName));

    if(tc_strlen(pszFLDesc) > 0 && iRetCode == ITK_ok)
    {
        LMI_ITKCALL(iRetCode, AOM_set_value_string(tFLCreateInput, LMI_ATTR_DESCRIPTION, pszFLDesc));
    }

    LMI_ITKCALL(iRetCode, TCTYPE_create_object(tFLCreateInput, ptFolder));

    LMI_ITKCALL(iRetCode, AOM_save_with_extensions(*ptFolder));

    return iRetCode;
}

/**
 * Function inserts an object reference into a folder at a specified index position.
 *
 * @param[in]  tFolder   Tag to Folder
 * @param[in]  tObject   Object Tag which needs to be inserted in the Folder
 * @param[in]  iPosition It is zero based index, and that the input is larger than the number of objects in the folder, or using 999 to indicate the last position.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int LMI_insert_content_to_folder
(
    tag_t tFolder,
    tag_t tObject,
    int   iPosition
)
{
    int iRetCode = ITK_ok;

    LMI_ITKCALL(iRetCode, AOM_refresh (tFolder, TRUE));

    LMI_ITKCALL(iRetCode, FL_insert(tFolder, tObject, iPosition ));

    LMI_ITKCALL(iRetCode, AOM_save( tFolder ));

    LMI_ITKCALL(iRetCode, AOM_refresh( tFolder, FALSE ));

    return iRetCode;
}

/**
 * Function will filter out the references of input Folder "tFolder" based on Object type list which is passed as input argument "ppszContTypes" to the function. If Object type list "ppszContTypes"
 * is empty then function will return all the content of input Folder "tFolder".
 *
 * @param[in]  tFolder   Tag to Folder
 * @param[in]  iTypCnt   Valid type count
 * @param[in]  ppszTypes Valid Type List
 * @param[out] piCnt     Valid Object Count
 * @param[out] pptObj    Valid Object List retrieved from Folder content
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int LMI_ask_folder_references
(
    tag_t   tFolder,
    int     iTypCnt,
    char**  ppszTypes,
    int*    piCnt,
    tag_t** pptObj
)
{
    int    iRetCode     = ITK_ok;
    int    iRefCnt      = 0;
    tag_t* ptReferences = NULL;

    if(iTypCnt > 0)
    {
        LMI_ITKCALL(iRetCode, FL_ask_references(tFolder, FL_fsc_as_ordered, &iRefCnt, &ptReferences));
    }
    else
    {
        LMI_ITKCALL(iRetCode, FL_ask_references(tFolder, FL_fsc_as_ordered, piCnt, pptObj));
    }

    if(iTypCnt > 0)
    {
        for(int iDx = 0; iDx < iRefCnt && iRetCode == ITK_ok; iDx++)
        {
            logical bIsValid = false;

            for(int iJx = 0; iJx < iTypCnt && iRetCode == ITK_ok; iJx++)
            {
                LMI_ITKCALL(iRetCode, LMI_obj_is_type_of(ptReferences[iDx], ppszTypes[iJx], &bIsValid));

                if(bIsValid == true && iRetCode == ITK_ok)
                {
                    break;
                }
            }

            if(bIsValid == true && iRetCode == ITK_ok)
            {
                LMI_UPDATE_TAG_ARRAY((*piCnt), (*pptObj), ptReferences[iDx]);
            }
        }
    }

    LMI_MEM_TCFREE(ptReferences);
    return iRetCode;
}


