/*======================================================================================================================================================================
                                                     Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_proj_ext_function.cxx

    Description: This File contains custom functions for getting information about organization and system administration to perform generic operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar        Initial Release

========================================================================================================================================================================*/
#include <lmi_library.hxx>

/**
 * This function gets the current logged in user tag and User ID
 *
 * @param[out] ptLoggedInUser Tag of current logged in user 
 * @param[out] ppszUserID     User ID for a user.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI_get_logged_in_user
(
    tag_t* ptLoggedInUser,
    char** ppszUserID
)   
{
    int   iRetCode   = ITK_ok;
    tag_t tCurrentGM = NULLTAG;
    char* pszUserID  = NULL;

    LMI_ITKCALL(iRetCode, SA_ask_current_groupmember(&tCurrentGM));

    LMI_ITKCALL(iRetCode, SA_ask_groupmember_user(tCurrentGM, ptLoggedInUser));

    LMI_ITKCALL(iRetCode, SA_ask_user_identifier2(*ptLoggedInUser, &pszUserID));

    if(iRetCode == ITK_ok)
    {
        LMI_STRCPY(*ppszUserID, pszUserID);
    }

    LMI_MEM_TCFREE(pszUserID);
    return iRetCode;
} 


