/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi_register_custom_handlers.cxx

    Description:  This file contains function registering custom handlers to be used by the workflow.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <lmi_register_callbacks.hxx>

#ifdef __cplusplus
}
#endif

#include <lmi_library.hxx>
#include <lmi_rule_handlers.hxx>
#include <lmi_action_handlers.hxx>
#include <lmi_errors.hxx>



/**
 * Registers custom handlers to be used by the workflow.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
extern DLLAPI int LMI_register_custom_handlers
(
    int*    piDecision, 
    va_list args
)
{
    int iRetCode = ITK_ok;

    TC_write_syslog( "Registering custom handlers ...\n");
    
    (*piDecision)  = ALL_CUSTOMIZATIONS;

        
    iRetCode = LMI_register_rule_handlers();

    if(iRetCode != ITK_ok)
    {
        printf("Unable to register: LMI_register_rule_handlers\n");
    }
    
    iRetCode = LMI_register_action_handlers();

    if(iRetCode != ITK_ok)
    {
        printf("Unable to register: LMI_register_action_handlers\n");
    }
    
    return iRetCode;
}
