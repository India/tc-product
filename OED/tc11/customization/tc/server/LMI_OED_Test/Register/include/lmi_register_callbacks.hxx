/*======================================================================================================================================================================
                                                    Copyright 2018  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename:    lmi_register_callbacks.hxx

    Description:  Header File for lmi_register_callbacks.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Sept-18    Anurag Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_REGISTER_CALLBACKS_HXX
#define LMI_REGISTER_CALLBACKS_HXX
    
#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>

extern DLLAPI int LMI_OED_Test_register_callbacks(void);

extern DLLAPI int LMI_register_custom_handlers
(
    int*    piDecision, 
    va_list args
);


#endif

