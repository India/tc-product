/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_bom_utilities.cxx

    Description: This file contains function that provides various information about BOM and BOM Line. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-Mar-18    Megha Singh   Initial Release

========================================================================================================================================================================*/

#include <lmi4_constants.hxx>
#include <lmi4_errors.hxx>
#include <lmi4_library.hxx>

/**
 * Applies a given config rule to the given BOM window
 *
 * @param[in] tBOMWindow Tag of BOMview to apply the rule to
 * @param[in] strRule    Name of the config rule to be applied
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_bom_apply_config_rule
(
    tag_t  tBOMWindow,
    string strRule
)
{
    int   iRetCode = ITK_ok;
    tag_t tRevRule = NULLTAG;

	LMI4_ITKCALL(iRetCode, CFM_find((strRule.c_str()), &tRevRule));
    
    LMI4_ITKCALL(iRetCode, BOM_set_window_config_rule(tBOMWindow, tRevRule));
    
    return iRetCode;
}

/**
 * This function retrieves the tag of the BOM view type attached to input Item revision depending upon the input BOM view type.
 *   
 * @param[in]  tItemRev         Tag of Item Revision
 * @param[in]  strInputViewType Type of BOMView
 * @param[out] reftBOMViewType    Tag of the required BOMView
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_bom_get_tag_to_view_type
(
    tag_t  tItemRev,
    string strInputViewType,
    tag_t& reftBOMViewType
)
{
    int   iRetCode = ITK_ok;
    tag_t tItem    = NULLTAG;
   	vector<tag_t> vtBOMViewsList;
	vector<tag_t>::iterator itervBOMVList;

    LMI4_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));
    
    LMI4_ITKCALL(iRetCode, LMI4_item_list_bom_views(tItem, vtBOMViewsList));
   
    /* Cheching the BOM views attached to the ItemRev for "View" type */
	for(itervBOMVList = vtBOMViewsList.begin(); itervBOMVList < vtBOMViewsList.end() && iRetCode == ITK_ok; ++itervBOMVList)
    {
        tag_t  tViewType   = NULLTAG;
        string strViewName = "";
        
		LMI4_ITKCALL(iRetCode, PS_ask_bom_view_type((*itervBOMVList), &tViewType));
        
        LMI4_ITKCALL(iRetCode, LMI4_ps_ask_view_type_name(tViewType, strViewName));

		if(strViewName.compare(strInputViewType) == 0)
        {
			reftBOMViewType = (*itervBOMVList);
            break;
        }        
    }

    return iRetCode;
}

/**
 * Given the item, item revision and bom view type returns the tag for the bom view connected to item and the BOM view revison tag connected to item revision.
 *
 * @param[in]  tItemRev       Tag of item revision
 * @param[in]  strViewType    BOM view type which needs to searched/created
 * @param[out] reftBOMViewRev Tag of BVR connected to item revision.
 *
 * @retval ITK_ok if successfull in getting/creating bom view revision or ITK error code.
 */
int LMI4_bom_get_bomview_revision
(
   tag_t  tItemRev,
   string strViewType,
   tag_t& reftBOMViewRev
)
{
    int   iRetCode = ITK_ok;
    tag_t tBvType  = NULLTAG;   

    /* Get bom view type tag */
	LMI4_ITKCALL(iRetCode, PS_find_view_type((strViewType.c_str()), &tBvType));
    
    if(tBvType != NULLTAG)
    {
		vector<tag_t> vtBVRevList;
		vector<tag_t>::iterator itervBVRevList;

        /* Get all the connected bom view to item revision */
        LMI4_ITKCALL(iRetCode, LMI4_item_rev_list_bom_view_revs(tItemRev, vtBVRevList));
       
		for(itervBVRevList = vtBVRevList.begin(); itervBVRevList < vtBVRevList.end() && iRetCode == ITK_ok; itervBVRevList++)
        {
			tag_t tFoundBV      = NULLTAG;
			tag_t tFoundBVRType = NULLTAG;

			LMI4_ITKCALL(iRetCode, PS_ask_bom_view_of_bvr((*itervBVRevList), &tFoundBV));
           
            LMI4_ITKCALL(iRetCode, PS_ask_bom_view_type(tFoundBV, &tFoundBVRType));

            if (tFoundBVRType == tBvType)
            {
				reftBOMViewRev = (*itervBVRevList);
                break;
            }
		}
    }

    return iRetCode;
}

/**
 * This function gets the specific type of item rev tags and BOMlines tags from a bom structure that falls in the valid list of Item revision type.
 * Function will return item revisions from the bom in array of tags.
 *
 * @note The returned lists are not ordered but are returned bottom-up per branch.
 * @note Input BOM Line will be include in the output list of BOM Lines as the very first element of the output array.
 *
 * @note If ptItemRevs is NULL the Item revisions list is not created
 *       If ptBomLines is NULL the BOMlines list is not created
 *
 * @note Properly initialize the variable "piCount" before calling this function.
 *
 * If level <= 0 initialize lists and count
 * If level > 0 add given BOM line and its ItemRev to the lists if not NULL) use BOM_line_look_up_attribute with 
 *              bomAttr_lineItemRevTag to get the id of the attribute
 *              use BOM_line_ask_attribute_tag to get the ItemRev tag from a BOMline
 * If lev < maxlevel or maxlevel == -1:
 *              use BOM_line_ask_child_lines to get the BOM lines from one level
 *              call this function recursively for each BOM line that was returned
 *
 * @warning  This function does not close the BOM Window which is already created nor does it creates new BOM Windows while traversing 
 *           the assembly. Any usage of this function should be followed by BOM window close command if no further processing is required.
 * 
 * @warning  Learning: Sub structure occurring recursively at different level in BOM will not have same BOM Line
 *                     but instead they have unique tags.
 *
 * @param[in]  tBOMLine     The top line of a BOMview
 * @param[in]  iLevel       The start level. 0 = top level
 * @param[in]  iMaxLevel    The number of levels to process. -1 = all levels.
 * @param[in]  bUnique      TRUE returns a unique list of Item revisions and BOM lines, otherwise FALSE.
 * @param[out] refvCount    The number of Item Revisions returned
 * @param[out] refvItem     An array of item Revision tags (may be NULL)
 * @param[out] refvBOMLines An array of BOMline tags (may be NULL)
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_bom_list_structure
(
    tag_t          tBOMLine,
    int            iLevel,
    int            iMaxLevel,
    logical        bUnique,
    vector<tag_t>& refvItem,
    vector<tag_t>& refvItemRevs,
    vector<tag_t>& refvBOMLines
)
{
    int     iRetCode = ITK_ok;
    int     iMark    = 0;
	logical bFound   = false;

    /* Set error protect mark to protects the current state of the error store. BOM commands clears the error stack which result in loss of previous errors.
       In order to avoid loss of error use protect mark commands.
    */
	 EMH_set_protect_mark(&iMark );
	    
    if(iLevel >= 0)
    {
		int   iAttrRevId = 0;
		int   iActiveSeq = 0;
		tag_t tItem      = NULLTAG;
		tag_t tItemRev   = NULLTAG;
		
       LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId));
       
       LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBOMLine, iAttrRevId, &tItemRev));

       LMI4_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));
       
       LMI4_ITKCALL(iRetCode, AOM_ask_value_int(tItemRev, LMI4_ATTR_ACTIVE_SEQ, &iActiveSeq));
        
       /* This check indicates that Item Revisions with Active sequence will be considered */
       if(tItemRev != NULLTAG && iActiveSeq != 0 && iRetCode == ITK_ok)
       {
            logical bAddToList = false;
			vector<tag_t>::iterator itervItemRevs;
       
			for(itervItemRevs = refvItemRevs.begin(); itervItemRevs != refvItemRevs.end() && bUnique == true; itervItemRevs++)
            {				
				if(itervItemRevs->compare(tItemRev))
                {
                   bFound = true;
                   break;
               }
            }

            if(bUnique == true)
            {
               if(bFound == false)
               {
                    bAddToList = true;
               }
               else
               {
                    bAddToList = false;
               }                   
            }
            else
            {
              bAddToList = true;
            }

            if(bAddToList == true)
            {
               if(itervItemRevs == refvItemRevs.begin())
               {
                   itervItemRevs++;
				   LMI4_ITKCALL(iRetCode, LMI4_vector_pushback_tag(tItem, false, refvItem));
				   LMI4_ITKCALL(iRetCode, LMI4_vector_pushback_tag(tItemRev, false, refvItemRevs));
				   LMI4_ITKCALL(iRetCode, LMI4_vector_pushback_tag(tBOMLine, false, refvBOMLines));                   
               }
               else
               {
                   itervItemRevs++;
				   LMI4_ITKCALL(iRetCode, LMI4_vector_pushback_tag(tItem, false, refvItem));
                   LMI4_ITKCALL(iRetCode, LMI4_vector_pushback_tag(tItemRev, false, refvItemRevs));
                   LMI4_ITKCALL(iRetCode, LMI4_vector_pushback_tag(tBOMLine, false, refvBOMLines));                   
               }
            }
        }
    }
    else
    {
        refvItemRevs;		
    }
    
    if ((iLevel < iMaxLevel) || iMaxLevel==-1)
    {
		vector<tag_t> vtChildrenBL;
		vector<tag_t>::iterator itervChildBL;

        LMI4_ITKCALL(iRetCode, LMI4_bom_line_ask_child_lines(tBOMLine, vtChildrenBL));

		for(itervChildBL = vtChildrenBL.begin(); itervChildBL < vtChildrenBL.end() && iRetCode == ITK_ok; itervChildBL++)
        {
			LMI4_ITKCALL(iRetCode, LMI4_bom_list_structure((*itervChildBL), (iLevel+1), iMaxLevel, bUnique, refvItem, refvItemRevs, refvBOMLines));
        }		
    }
	    
    EMH_clear_protect_mark(iMark);

    return iRetCode;
}

/**
 * This function GETs the parent BOM Line and tag of parent BOM View Revision for the input BOM Line.        
 *
 * @param[in]  tBOMLine      Tag of the BOM Line.
 * @param[out] reftParentBL  Tag of the Parent BOM Line for input BOM Line tag.
 * @param[out] reftParentBVR Tag of the Parent BOM View Revision.
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_bom_get_parent_bl_bvr
(
    tag_t  tBOMLine,
    tag_t& reftParentBL,
    tag_t& reftParentBVR
)
{
    int   iRetCode        = ITK_ok;
    int   iBLParentAttrID = 0;
    int   iBVRAttrID      = 0;
	tag_t tParentBL       = NULLTAG;
	tag_t tParentBVR      = NULLTAG;

    LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineParentTag, &iBLParentAttrID));

    /* Get Tag of Parent BOM Line */
    LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBOMLine, iBLParentAttrID, &tParentBL));

    LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineBvrTag, &iBVRAttrID));

    /* Get Tag of Parent BOM View Revision */
    LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tParentBL, iBVRAttrID, &tParentBVR));

	if(iRetCode == ITK_ok)
	{
		if(tParentBL != NULL)
		{
			reftParentBL = tParentBL;
		}
		if(tParentBVR != NULL)
		{
			reftParentBVR = tParentBVR;
		}
	}

    return iRetCode;
}

/**
 * This function retrieves all note types valid for this site.
 *
 * @param[out] refvNoteType  Tag of Note Types
 * @param[out] refvNoteTypes Name of Note Types
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_ps_get_all_note_type
(   
    vector<tag_t>&  refvNoteType,
    vector<string>& refstrNoteTypes
)
{
    int    iRetCode     = ITK_ok;
    int    iNoteTypeCnt = 0;
    tag_t* ptPSNote     = NULL;
   
    LMI4_ITKCALL(iRetCode, PS_note_type_extent(&iNoteTypeCnt, &ptPSNote));

    for(int iDx = 0; iDx < iNoteTypeCnt && iRetCode == ITK_ok; iDx++)
    {
        string strNoteName = "";

        LMI4_ITKCALL(iRetCode, LMI4_ps_ask_note_type_name(ptPSNote[iDx], strNoteName));

        if(iRetCode == ITK_ok)
        {
			LMI4_ITKCALL(iRetCode, LMI4_vector_pushback_str(strNoteName, false, refstrNoteTypes));
             
             LMI4_ITKCALL(iRetCode, LMI4_vector_pushback_tag(ptPSNote[iDx], false, refvNoteType));
        }       
    }

    LMI4_MEM_TCFREE(ptPSNote);
    return iRetCode;
}

/**
 * This function GETs the text of a note of input note type attached to the specified occurrence. An occurrence can have textual notes attached to it. Each note has a note type.
 *
 * @note: Note types are defined in BMIDE.
 *
 * @note: 'PS_ask_occurrence_note_text' this API cannot be used because if note type is not set to a BOM Line then this API throws error.
 *        
 *
 * @param[in]  tBOMLine      Tag of the BOM Line.
 * @param[in]  tNoteType     Tag of Note type whose value needs to be retrieved for input BOM Line
 * @param[in]  strNoteType   Note type whose value needs to be retrieved for input BOM Line
 * @param[out] refstrNoteVal Value of Note of input type which is attached to input BOMLine.

 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_get_ps_occ_note_value
(
    tag_t   tBOMLine,
    tag_t   tNoteType,
    string  strNoteType,
    string& refstrNoteVal
)
{
    int    iRetCode     = ITK_ok;
    int    iBLOccAttrID = 0;
    tag_t  tBLOcc       = NULLTAG;
    tag_t  tParentBL    = NULLTAG;
    tag_t  tParentBVR   = NULLTAG;
    vector<tag_t> vtNoteTypes;
	vector<tag_t>::iterator itervNoteTypList;
    
    LMI4_ITKCALL(iRetCode, LMI4_bom_get_parent_bl_bvr(tBOMLine, tParentBL, tParentBVR));

    LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineOccTag, &iBLOccAttrID));

    /* Get Occurrence tag for BOM Line which is input to this function */
    LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBOMLine, iBLOccAttrID , &tBLOcc));

	if(tNoteType == NULLTAG && strNoteType != "" && tc_strlen(strNoteType.c_str()) > 0)
    {
        LMI4_ITKCALL(iRetCode, PS_find_note_type((strNoteType.c_str()), &tNoteType));
    }

    LMI4_ITKCALL(iRetCode, LMI4_ps_list_occurrence_notes(tParentBVR, tBLOcc, vtNoteTypes));

    /* We have to use this approach of finding Note Type value for BOM Line because if we use API PS_ask_occurrence_note_text then in
       case if value is not set then this API throws error and we want to avoid this. This loop run for number of property in the "All Notes" Category */
	for(itervNoteTypList = vtNoteTypes.begin() ; itervNoteTypList < vtNoteTypes.end() && iRetCode ==  ITK_ok; itervNoteTypList++)
    {        
		if((*itervNoteTypList) == tNoteType)
        {
            LMI4_ITKCALL(iRetCode, LMI4_ps_ask_occurrence_note_text(tParentBVR, tBLOcc, (*itervNoteTypList), refstrNoteVal));
            break;
        }
    }
        
    return iRetCode;
}

/**
 * This function SETs the text of a note of input note type attached to the specified occurrence. An occurrence can have textual notes attached to it. Each note has a note type.
 *
 * @note: Note types are defined in BMIDE.
 *
 * @param[in] tBOMLine    Tag of the BOM Line.
 * @param[in] tNoteType   Tag of Note type whose value needs to be set for input BOM Line
 * @param[in] strNoteType Note type whose value needs to be set for input BOM Line
 * @param[in] strNoteVal  Value of Note that needs to be set for input type which is attached to input BOMLine.

 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_set_ps_occ_note_value
(
    tag_t  tBOMLine,
    tag_t  tNoteType,
    string strNoteType,
    string strNoteVal
)
{
    int    iRetCode     = ITK_ok;
    int    iBLOccAttrID = 0;
    int    iNoOfNotes   = 0;
    tag_t  tBLOcc       = NULLTAG;
    tag_t  tParentBL    = NULLTAG;
    tag_t  tParentBVR   = NULLTAG;
    tag_t* ptNotes      = NULL;
    
    LMI4_ITKCALL(iRetCode, LMI4_bom_get_parent_bl_bvr(tBOMLine, tParentBL, tParentBVR));

    LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineOccTag, &iBLOccAttrID));

    /* Get Occurrence tag for BOM Line which is input to this function */
    LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBOMLine, iBLOccAttrID , &tBLOcc));

	if(tNoteType == NULLTAG && strNoteType != "" && tc_strlen(strNoteType.c_str()) > 0)
    {
		LMI4_ITKCALL(iRetCode, PS_find_note_type((strNoteType.c_str()), &tNoteType));
    }
    
	LMI4_ITKCALL(iRetCode, LMI4_save_ps_occ_note(tParentBVR, tBLOcc, tNoteType, (strNoteVal.c_str())));

    LMI4_MEM_TCFREE(ptNotes);
    return iRetCode;
}

/**
 * This function locks the BVR and SETs the text of a note for input note type.
 *
 * @param[in] tParentBVR Tag of the BOM Line.
 * @param[in] tBLOcc     Occurrence Tag.
 * @param[in] tNoteType  Tag of Note type whose value needs to be set
 * @param[in] strNoteVal Value of Note that needs to be set for input type which is attached to input BOMLine.

 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_save_ps_occ_note
(
    tag_t  tParentBVR,
    tag_t  tBLOcc,
    tag_t  tNoteType,
    string strNoteVal
)
{
    int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AOM_refresh(tParentBVR, true));

	LMI4_ITKCALL(iRetCode, PS_set_occurrence_note_text(tParentBVR, tBLOcc, tNoteType, (strNoteVal.c_str())));
                    
    LMI4_ITKCALL(iRetCode, AOM_save(tParentBVR));

    LMI4_ITKCALL(iRetCode, AOM_refresh(tParentBVR, false));

    return iRetCode;
}

/**
 * This function stores item rev tags of BOMlines as 'Key' of a MAP. Value stored in the 'Key' of a map are Item revisions of BOM Line and 'Value' of map is another STL of type 'VECTOR' which will 
 * store all the BOM Line tags of one specific Item revision in a BOM. Value stored in a map allows other function to access all the BOM line tags of one specific Item Revision in one shot. For e.g.
 * If Item revison '10003/A' occurs 6 times in BOM then this IR will have 6 different value of  BOM Lines. So value stores in a map will appear as:
 *                          map<tag_t, vector<tag_t>
 *                             <IR Tag>  <BL Tags Vector>
 *                               4153    5128
 *                                       5893
 *                                       5897
 *                                       5759
 *                                       5326
 *                                       5486
 *                                       
 * @note Input BOM Line will be include in the output list of BOM Lines as the very first element of the output array.
 *
 * If level <= 0 Initialize lists and count
 * If level > 0 Add given BOM line and its ItemRev to the lists if not NULL) use BOM_line_look_up_attribute with 'bomAttr_lineItemRevTag'
 *              to get the id of the attribute use BOM_line_ask_attribute_tag to get the ItemRev tag from a BOMline
 * If lev < maxlevel or maxlevel == -1:
 *              use BOM_line_ask_child_lines to get the BOM lines from one level call this function recursively for each BOM line that was returned
 *
 * @warning  This function does not close the BOM Window which is already created nor does it creates new BOM Windows while traversing 
 *           the assembly. Any usage of this function should be followed by BOM window close command if no further processing is required.
 * 
 * @warning  Sub structure occurring recursively at different level in BOM will not have same BOM Line but instead they have unique tags.
 *
 * @param[in]  tBOMLine     The top line of a BOMview
 * @param[in]  iLevel       The start level. 0 = top level
 * @param[in]  iMaxLevel    The number of levels to process. -1 = all levels.
 * @param[in]  bUnique      TRUE returns a unique list of Item revisions and BOM lines, otherwise FALSE.
 * @param[out] refmBOMInfo  MAP containing Item Revision tags and corresponding BOM line vector as each index
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_bom_list_structure_cpp
(
    tag_t   tBOMLine,
    int     iLevel,
    int     iMaxLevel,
    logical bUnique,
    map<tag_t, vector<tag_t>>& refmBOMInfo
)
{
    int iRetCode = ITK_ok;
    int iMark    = 0;
 
    /* Set error protect mark to protects the current state of the error store. BOM commands clears the error stack which result in loss 
       of previous errors. In order to avoid loss of error use protect mark commands.
    */
    EMH_set_protect_mark(&iMark);

    if(iLevel >= 0)
    {    
        int   iAttrRevId = 0;  
        int   iActiveSeq = 0;
        tag_t tItemRev   = NULLTAG;

        LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId));
       
        LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBOMLine, iAttrRevId, &tItemRev));
       
        LMI4_ITKCALL(iRetCode, AOM_ask_value_int(tItemRev, LMI4_ATTR_ACTIVE_SEQ, &iActiveSeq));

        /* This check indicates that Item Revisions with Active sequence will be considered */
        if(tItemRev != NULLTAG && iActiveSeq != 0 && iRetCode == ITK_ok)
        {
            std::map<tag_t, vector<tag_t>>::iterator iterBOMInfoMap;
            
            iterBOMInfoMap = refmBOMInfo.find(tItemRev);

            if(iterBOMInfoMap != refmBOMInfo.end())
            {
                if(bUnique == false)
                {
                    iterBOMInfoMap->second.push_back(tBOMLine);
                }
            }
            else
            {
                vector<tag_t> vBOMLine;

                vBOMLine.push_back(tBOMLine);
				refmBOMInfo.insert(std::pair<tag_t, vector<tag_t>>(tItemRev, vBOMLine));
            }
        }
    }

    if ((iLevel < iMaxLevel) || iMaxLevel==-1)
    {
        int iBLCount = 0;
        vector<tag_t> vtChildrenBL;
		vector<tag_t>::iterator itervChildBL;

        LMI4_ITKCALL(iRetCode, LMI4_bom_line_ask_child_lines(tBOMLine, vtChildrenBL));

		for(itervChildBL = vtChildrenBL.begin(); itervChildBL < vtChildrenBL.end() && iRetCode == ITK_ok; itervChildBL++)
        {
			LMI4_ITKCALL(iRetCode, LMI4_bom_list_structure_cpp((*itervChildBL),(iLevel+1), iMaxLevel, bUnique, refmBOMInfo));
        }               
    }

    EMH_clear_protect_mark(iMark);
    return iRetCode;
}






