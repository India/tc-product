/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_property_containers.hxx

    Description:  This File contains declarations for functions to get and set table and name-value property on an object.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/

#ifndef LMI4_PROPERTY_CONTAINERS_HXX
#define LMI4_PROPERTY_CONTAINERS_HXX
#include <lmi4_library.hxx>

class Base
{
	public:
		int iType;	
};

class LMI4_Logic: public Base
{
	public:
		 string  strAttrName; 
		 logical bValue;
		 string  strValue;
    			
};

class LMI4_Integer: public Base
{
	public:
		string strAttrName;
		int    iValue;
		string strValue;
};

class LMI4_String: public Base
{
	public:
		string strAttrName;
		string strValue;    
};

class LMI4_Double: public Base
{
	public :
		string strAttrName;
		double dValue;
		string strValue;	
};

class LMI4_Date: public Base
{
	public:
		string strAttrName;
		date_t dtValue;
		string strValue;
};

class LMI4_NameValuePropSet
{
	public:
		PROP_value_type_e type;
		string            strPropValue;
};

#endif