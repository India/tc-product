/*======================================================================================================================================================================
                                                            Copyright 2018  LMtec India
                                                           Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm4_register_callbacks.hxx

    Description:  Header File for lm4_register_callbacks.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/
#ifndef LMI4_REGISTER_CALLBACKS_HXX
#define LMI4_REGISTER_CALLBACKS_HXX

#include <stdlib.h>
#include <stdio.h>
#include<unidefs.h>
#include<tc/iman.h>
#include<ict/ict_userservice.h>
#include<tccore/custom.h>


extern DLLAPI int LMI4_custom_register_callbacks(void);

extern DLLAPI int LMI4_register_custom_handlers
(
    int*    piDecision, 
    va_list args
);


#endif

