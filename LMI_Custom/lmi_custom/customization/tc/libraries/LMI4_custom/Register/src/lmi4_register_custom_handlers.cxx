/*======================================================================================================================================================================
                                                             Copyright 2018  LMtec India
                                                            Unpublished - All rights reserved

========================================================================================================================================================================

File Description:

    Filename: lmi4_register_custom_handlers.cxx

    Description:  This file contains function registering custom handlers to be used by the workflow.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/
#include <lmi4_library.hxx>
#include <lmi4_action_handlers.hxx>

#ifdef __cplusplus
extern "C" {
#endif

#include <lmi4_register_callbacks.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Registers custom handlers to be used by the workflow.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
extern DLLAPI int LMI4_register_custom_handlers
(
    int*    piDecision, 
    va_list args
)
{
    int iRetCode = ITK_ok;

    TC_write_syslog( "Registering custom handlers ...\n");
    
    (*piDecision)  = ALL_CUSTOMIZATIONS;    
    
    LMI4_ITKCALL(iRetCode, LMI4_register_action_handlers());

    if(iRetCode != ITK_ok)
    {
        printf("Unable to register: LMI4_register_action_handlers\n");
    }
    
    return iRetCode;
}











