/*======================================================================================================================================================================
                                                                Copyright 2018  LMtec India
                                                               Unpublished - All rights reserved

========================================================================================================================================================================

File Description:

    Filename: lmi4_register_callbacks.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/
#include <lmi4_library.hxx>
#ifdef __cplusplus

extern "C" {

#endif

#include <lmi4_register_callbacks.hxx>

#ifdef __cplusplus

}
#endif


/**
 * Function to register Callback function. Entry point for LMI4_custom library code.
 *
 * Returns: Status of execution.
 */
extern DLLAPI int LMI4_custom_register_callbacks() 
{
    int  iRetCode = ITK_ok;
        
    printf("INFO: Registered Rehau Custom Exits. \n");

    LMI4_ITKCALL(iRetCode, CUSTOM_register_exit ("LMI4_custom", "USER_gs_shell_init_module", (CUSTOM_EXIT_ftn_t)  LMI4_register_custom_handlers));
    
    if(iRetCode != ITK_ok)
    {
        printf("ERROR: Unable to register custom handlers.");
    }

    return iRetCode;
}











