/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_get_table_prop.cxx

    Description:  This File contains declarations for functions to get and set table property on an object.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/
#include <lmi4_get_table_prop.hxx>
#include <lmi4_property_containers.hxx>

/**
 * This function returns count of table objects "refiTabCnt" for the given input table property "strTabPropName". And for the input list of properties "vtPropNames" this function returns map
 * "refmGetPropVal". Structure of Map is <key,value> <Base*, PROP_value_type_e>, here Base* is the class and 'PROP_value_type_e' is the typedef of enum.  
 *
 * @note: The values of properties are stored inside datamembers of classes according to type of values(PROP_string, PROP_int...) and these classes are typecasted to parent class 'Base'. Objects of
 * classes (LMI4_String, LMI4_Int....) are typecasted to parent 'Base' class. Since 'Keys' of map can have same names/different types therefore object of class 'Base' is added as 'key' to it. While type
 * (PROP_value_type_e) of property value is added as value of map.   
 *
 * @note: To extract value from map the 'key' ('Base' class) is detypecasted to the type present as value(typedef of enum PROP_value_type_e) of map. The user needs to free objects of classes 
 *		  used in map at the end.
 *
 * @param[in]  tObject         Tag of object.
 * @param[in]  strTabPropName  Table property name 
 * @param[in]  vtPropNames     List of properties whose values are to be found.
 * @param[out] refiTabCnt      Count of table objects
 * @param[out] refmGetPropVal  Map containing object of class 'Base' as key and typedef of enum 'PROP_value_type_e' as value.
 * 
 * @retval ITK_ok is always returned.
 */
int TableProp::LMI4_get_all_table_attr
(
	tag_t          tObject, 
	string         strTabPropNam,
	vector<string> vtPropNames,
	int&           refiTabCnt, 
	map<Base*, PROP_value_type_e>& refmGetPropVal
)
{
	int iRetCode = ITK_ok;
	vector<tag_t> vtTabRows;	
			
	LMI4_ITKCALL(iRetCode, LMI4_aom_ask_table_rows(tObject, strTabPropNam, refiTabCnt, vtTabRows));

	if(refiTabCnt > 0)
	{
		vector<tag_t>::iterator iterRows;

		/* Traversing table row objects. */
		for(iterRows = vtTabRows.begin(); iterRows != vtTabRows.end() && iRetCode == ITK_ok; iterRows++)
		{
			vector<string>::iterator iterPropNames;

			/* Traversing input properties. */ 
			for(iterPropNames = vtPropNames.begin(); iterPropNames < vtPropNames.end() && iRetCode == ITK_ok; iterPropNames++)
			{
				LMI4_ITKCALL(iRetCode, LMI4_ask_prop_typ((*iterRows), (*iterPropNames), refmGetPropVal));
			}
		}
	}

	return iRetCode;
}

/**
 * This function returns map "refmGetPropVal" for given input object of table property 'strPropNam'. Structure of Map is <key,value> <Base*, PROP_value_type_e>, here Base* is the class and
 * 'PROP_value_type_e' is the typedef of enum.
 * 
 * @note: The values of properties are stored inside datamembers of classes according to type of values(PROP_string, PROP_int...) and these classes are typecasted to parent class. Objects of classes
 * (LMI4_String, LMI4_Int....) are typecasted to parent 'Base' class. Since 'Keys' of map can have same names/different types therefore object of class 'Base' is added as 'key' to it. While type
 * (PROP_value_type_e) of property value is added as value of map. To extract the values from map the key('Base' class) is detypecasted to the type present as value of map(PROP_value_type_e).   
 *
 * @param[in]  tObject        Tag of object.
 * @param[in]  strPropNam     Property name 
 * @param[out] refmGetPropVal Map containing object of class Base as 'Key' and typedef of enum 'PROP_value_type_e' as value.
 * 
 * @retval ITK_ok is always returned.
 */
int TableProp::LMI4_ask_prop_typ
(
	tag_t  tObject,
	string strPropNam,
	map<Base*, PROP_value_type_e>& refmGetPropVal
)
{
	int    iRetCode = ITK_ok;
	string strPropTyp;
	stProp stList;
	string strConvertedString;
	string strVal;

	Base *bObjCls  = new Base();

	PROP_value_type_t ValueTypeOfProp = PROP_untyped;
    
	/* Getting type of value of property. */
	LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_type(tObject, strPropNam, ValueTypeOfProp, strPropTyp));

	/* Getting the value of attribute. */
	LMI4_ITKCALL(iRetCode, LMI4_obj_aom_get_attribute_value(tObject, strPropNam, strVal)); 

	if(iRetCode == ITK_ok)
	{
		switch(ValueTypeOfProp)
		{
			case PROP_string:
			{    
				/* Getting the string value of property 'strPropNam'. */
				LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, strPropNam, stList.strPropVal));

				bObjCls->iType =  PROP_string;

				LMI4_String *strObj = new LMI4_String();

				strObj->strAttrName = strPropNam; 

				strObj->strValue    = stList.strPropVal; 
				
				/* Typecasting object of class 'LMI4_String' to parent 'Base' class. */
				bObjCls = (Base*) strObj;

				/* Inserting the values in map. */
				refmGetPropVal.insert(pair <Base*, PROP_value_type_e> (bObjCls, PROP_string));						
				break;

				LMI4_OBJECT_DELETE(strObj);
			}
			case PROP_short:
			case PROP_int:
			{
				/* Getting the integer value of property 'strPropNam'. */
				LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_int(tObject, strPropNam, stList.iPropVal));

				/* Getting the string equivalent 'strConvertedString' of integer value of property 'stList.iPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_int_to_string(stList.iPropVal, strConvertedString));

				bObjCls->iType =  PROP_int;

				LMI4_Integer *iObj = new LMI4_Integer();

				iObj->strAttrName = strPropNam;

				iObj->iValue      = stList.iPropVal;

				iObj->strValue    = strConvertedString; 
		
				/* Typecasting object of class 'LMI4_Integer' to parent 'Base' class. */
				bObjCls = (Base*) iObj;
				
				/* Inserting the values in map. */
				refmGetPropVal.insert(pair <Base*, PROP_value_type_e> (bObjCls, PROP_int));
				break;

				LMI4_OBJECT_DELETE(iObj);
			}
			case PROP_double:
			{
				/* Getting the double value of property 'strPropNam'. */
				LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_double(tObject, strPropNam, stList.dPropVal));

				/* Getting the double equivalent 'strConvertedString' of double value of property 'stList.dPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_double_to_string(stList.dPropVal, strConvertedString));

				bObjCls->iType =  PROP_double;

				LMI4_Double *dObj = new LMI4_Double();

				dObj->strAttrName = strPropNam;

				dObj->dValue      = stList.dPropVal;

				dObj->strValue    = strConvertedString; 
		
				/* Typecasting object of class 'LMI4_Double' to parent 'Base' class. */
				bObjCls = (Base*) dObj;
				
				/* Inserting the values in map. */
				refmGetPropVal.insert(pair <Base*, PROP_value_type_e> (bObjCls, PROP_double));
				break;

				LMI4_OBJECT_DELETE(dObj);
			}
			case PROP_date:
			{
				string strDateToString;

				/* Getting the double value of property 'strPropNam'. */
				LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_date(tObject, strPropNam, stList.dtPropVal));

				/* Getting the double equivalent 'strDateToString' of date value of property 'stList.dtPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_itk_date_to_string(stList.dtPropVal, strDateToString));

				bObjCls->iType =  PROP_date;

				LMI4_Date *dtObj   = new LMI4_Date();

				dtObj->strAttrName = strPropNam;

				dtObj->dtValue     = stList.dtPropVal;

				dtObj->strValue    = strConvertedString; 
		
				/* Typecasting object of class 'LMI4_Date' to parent 'Base' class. */
				bObjCls = (Base*) dtObj;

				/* Inserting the values in map. */
				refmGetPropVal.insert(pair <Base*, PROP_value_type_e> (bObjCls, PROP_date));
																
				break;

				LMI4_OBJECT_DELETE(dtObj);
			}			
			case PROP_logical :
			{
				string strLogicalToString;

				/* Getting the logical value of property 'strPropNam'. */
				LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_logical(tObject, strPropNam, stList.bPropVal));

				/* Getting the string equivalent 'strLogicalToString' of logical value of property 'stList.bPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_logical_to_string(stList.bPropVal, strLogicalToString));

				bObjCls->iType =  PROP_logical;

				LMI4_Logic *bObj  = new LMI4_Logic();

				bObj->strAttrName = strPropNam;

				bObj->bValue      = stList.bPropVal;

				bObj->strValue    = strConvertedString; 
		
				/* Typecasting object of class 'LMI4_Logic' to parent 'Base' class. */
				bObjCls = (Base*) bObj;

				/* Inserting the values in map. */
				refmGetPropVal.insert(pair <Base*, PROP_value_type_e> (bObjCls, PROP_logical));
				break;

				LMI4_OBJECT_DELETE(bObj);
			}
		}
	}

	return iRetCode;
}

/**
 * For given input property "strPropName" which is object of given input table property "strTabPropNam" this function returns map "refmGetPropVal". Structure of Map is <key,value> 
 * <Base*, PROP_value_type_e>, here Base* is the class and 'PROP_value_type_e' is the typedef of enum.ap is containing object of class 'Base' as key and typedef of enum_type_e' for the table property
 * object present at the given input index "iRowIndex".
 *
 * @param[in]  tObject        Tag of object.
 * @param[in]  strTabPropNam  Table property name.
 * @param[in]  strPropNam     Property name 
 * @param[in]  iRowIndex      Table object row index.
 * @param[out] refmGetPropVal Map containing object of class Base as 'Key' and typedef of enum 'PROP_value_type_e' as 'Value'.
 * 
 * @retval ITK_ok is always returned.
 */
int TableProp::LMI4_get_specific_table_attr
(
	tag_t          tObject,
	string         strTabPropNam,
	vector<string> vstrPropName,
	int             iRowIndex, 
	map<Base*, PROP_value_type_e>& refmGetPropVal
)
{
	int   iRetCode = ITK_ok;
	tag_t tTabRow = NULLTAG;
	
	/* Getting the tag of table row object. */
	LMI4_ITKCALL(iRetCode, LMI4_aom_ask_table_row(tObject, strTabPropNam, iRowIndex, tTabRow));

	if(iRetCode == ITK_ok && tTabRow != NULLTAG)
	{
		if(vstrPropName.size() > 0)
		{
			vector<string>::iterator itervPropNam;

			/* Traversing the input properties. */
			for (itervPropNam = vstrPropName.begin(); itervPropNam != vstrPropName.end(); itervPropNam++)
			{	
				/* Getting input property type. */
				LMI4_ITKCALL(iRetCode, LMI4_ask_prop_typ(tTabRow, (*itervPropNam), refmGetPropVal));				
			}
		}		
	}

	return iRetCode;
}

/**
 * This function sets given input property "strTabName" with values contained in input map "mSetPropValues". Structure of Map is <key,value> <Property name, Property value>, here property name is the
 * column name in table attribute. 
 *
 * @param[in] tObject           Tag of object.
 * @param[in] strTabName        Table property name 
 * @param[in] mstrSetPropValues Map containing property name as key and property value to be set as value.
 * 
 * @retval ITK_ok is always returned.
 */
int TableProp::LMI4_set_table_attr_val
(
	tag_t  tObject,
	string strTabName,
	map<string, string> mstrSetPropValues
)
{
	int iRetCode  = ITK_ok;
	vector<tag_t> vtTabRows;
			
	LMI4_ITKCALL(iRetCode, AOM_refresh(tObject, 1));

	if(mstrSetPropValues.size() > 0)
	{
		LMI4_ITKCALL(iRetCode, LMI4_aom_append_table_rows(tObject, strTabName, 1, vtTabRows));
	}
			
	map<string, string> :: iterator itr;
	vector<tag_t> :: iterator itrTabRows;
	
	for(itrTabRows = vtTabRows.begin(); itrTabRows != vtTabRows.end() && iRetCode == ITK_ok; itrTabRows++)
	{		
		for (itr = mstrSetPropValues.begin(); itr != mstrSetPropValues.end() && iRetCode == ITK_ok; itr++)
		{			
			LMI4_ITKCALL(iRetCode, LMI4_set_table_attr((*itrTabRows), itr->first, itr->second));			
		}		
	}

	LMI4_ITKCALL(iRetCode, AOM_save_with_extensions(tObject));

	LMI4_ITKCALL(iRetCode, AOM_refresh(tObject, false));
	
	return iRetCode;
}

/**
 * This function sets given input value "strPropVal" on given input property "strPropNam" present on given input object "tObject".
 *
 * @param[in] tObject    Tag of object.
 * @param[in] strPropNam Property name 
 * @param[in] strPropVal Value of property
 * 
 * @retval ITK_ok is always returned.
 */
int TableProp::LMI4_set_table_attr
(
	tag_t  tObject,
	string strPropNam,
	string strPropVal
)
{
	int     iRetCode     = ITK_ok;
	logical bIsPotential = false;
	stProp  stPrpTempVal;
	string  strPropTyp;
	
	PROP_value_type_t ValueTypeOfProp = PROP_untyped;

	LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_type(tObject, strPropNam, ValueTypeOfProp, strPropTyp));

	if(iRetCode == ITK_ok && strPropTyp.length() != 0)
	{
		switch(ValueTypeOfProp)
		{
			case PROP_string:
			{    
				/* Setting the value of property to 'strPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_string(tObject, strPropNam, strPropVal));
				break;						
			}
			case PROP_int:
			case PROP_short:
			{  
				/* Getting the integer equivalent of input value 'strPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_string_is_Integer(strPropVal, stPrpTempVal.iPropVal, bIsPotential));

				if(iRetCode == ITK_ok && bIsPotential == true)
				{
					/* Setting the value of property to 'stPrpTempVal.iPropVal'. */
					LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_int(tObject, strPropNam, stPrpTempVal.iPropVal));
				}
				break;						
			}				
			case PROP_double:
			{
				/* Getting the double equivalent of input value 'strPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_string_is_Double(strPropVal, stPrpTempVal.dPropVal, bIsPotential));

				if(iRetCode == ITK_ok && bIsPotential == true)
				{
					/* Setting the value of property to 'stPrpTempVal.dPropVal'. */
					LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_double(tObject, strPropNam, stPrpTempVal.dPropVal)); 
				}
				break;					
			}
			case PROP_date:
			{	
				/* Getting the date equivalent of input value 'strPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_itk_string_to_date(strPropVal, stPrpTempVal.dtPropVal));
				
				/* Setting the value of property to 'stPrpTempVal.dtPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_date(tObject, strPropNam, stPrpTempVal.dtPropVal));						
				break;
			}
			case PROP_typed_reference    :
			case PROP_untyped_reference  :
			case PROP_external_reference :
			{	
				/* Getting the tag equivalent of input value 'strPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_pom_string_to_tag(strPropVal, stPrpTempVal.tPropVal));

				/* Setting the value of property to 'stPrpTempVal.tPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_tag(tObject, strPropNam, stPrpTempVal.tPropVal));
				break;
			}
			case PROP_logical :
			{
				/* Getting the logical equivalent of input value 'strPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_string_to_logical(strPropVal, stPrpTempVal.bPropVal));

				/* Setting the value of property to 'stPrpTempVal.bPropVal'. */
				LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_logical(tObject, strPropNam, stPrpTempVal.bPropVal));
				break;
			}
		}
	}

	return iRetCode;
}






































