/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved

========================================================================================================================================================================

File Description:

    Filename: lmi4_get_table_prop.hxx

    Description: 

=======================================================================================================================================================================

Date          Name           Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
04-Sep-18     Megha Singh    Initial Release

========================================================================================================================================================================*/
#ifndef LMI4_GET_TABLE_PROP_HXX
#define LMI4_GET_TABLE_PROP_HXX
#include <lmi4_library.hxx>
#include <lmi4_property_containers.hxx>

class TableProp
{
	public:
		int LMI4_get_all_table_attr(tag_t tObject, string strTabPropNam, vector<string> vtPropNames, int& refiTabCnt, map<Base*, PROP_value_type_e>& refmGetPropVal);
		int LMI4_ask_prop_typ(tag_t tObject, string strPropNam,	map<Base*, PROP_value_type_e>& refmGetPropVal);
		int LMI4_get_specific_table_attr(tag_t tObject, string strTabPropNam, vector<string> vstrPropName, int iRowIndex, map<Base*, PROP_value_type_e>& refmGetPropVal);
		int LMI4_set_table_attr(tag_t  tObject, string strPropNam, string strPropVal);
		int LMI4_set_table_attr_val(tag_t tObject, string strTabName, map<string, string> mstrSetPropValues);
};

#endif