/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_item_itemrev_utilities.cxx

    Description: This File contains custom functions for Item & Item Revision to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>
#include <lmi4_constants.hxx>
#include <lmi4_errors.hxx>

/**
 * Function gets the previous revision tag "reftPreviousIR" for the input item revision "tItemRev". If input item revision is the only revision of the item then function returns 'pbIsOnlyRevision'
 * as 'true'. Also "refbIsFirstRev" returns "true" if input item revision is the first revision of the item. 
 *
 * @note: Suppose the input revision is revision 'A' but the Item of input revision has more than one revision then this function will return 'pbIsFirstRevision' as 'true'.
 *        Function will identify previous revision by comparing the creation date of previously available revisions with the current input item revision. On the basis of this comparison the revision
 *        with latest creation date in context of input revision will be returned.
 *
 * @param[in]  tItemRev       Tag of Item Revision .
 * @param[out] reftPreviousIR Tag of Previous Item Revision
 * @param[out] refbIsOnlyRev  'true' if input Item Revision is the only revision of Item
 * @param[out] refbIsFirstRev 'true' if input Item Revision is the First revision of the Item
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int LMI4_itemrev_get_previous_rev
(
    tag_t    tItemRev,
    tag_t&   reftPreviousIR,
    logical& refbIsOnlyRev,
    logical& refbIsFirstRev
)
{
    int    iRetCode       = ITK_ok;
    tag_t  tItem          = NULLTAG;
    date_t InputIRCreDate = POM_null_date();    	
	vector<tag_t> vtItemRevList;
	       
    LMI4_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));

	LMI4_ITKCALL(iRetCode, LMI4_item_list_all_revs(tItem, vtItemRevList));

    LMI4_ITKCALL(iRetCode, AOM_ask_value_date(tItemRev, LMI4_ATTR_OBJECT_CREATION_DATE, &InputIRCreDate));

    if(iRetCode == ITK_ok)
    {
		if((vtItemRevList.size()) > 0 )
        {
            int   iValidIR    = 0;
			tag_t tTempPrevIR = NULLTAG;            
			vector<tag_t>::iterator itervIRevList;

            refbIsOnlyRev = false;
        
			for(itervIRevList = vtItemRevList.begin(); itervIRevList < vtItemRevList.end() && iRetCode == ITK_ok; itervIRevList++)
            {
                int    iAnswer     = 0;
				date_t TempCreDate = POM_null_date();

                LMI4_ITKCALL(iRetCode, AOM_ask_value_date((*itervIRevList), LMI4_ATTR_OBJECT_CREATION_DATE, &TempCreDate));

                /* Compares two dates for equality. Function result is returned in 'iAnswer' as
                   0 if dates are equal 
                   1 if date1 is later than date2 (date1 > date2)
                   -1 if date1 is earlier than date2 (date1 < date2)
                */  
                LMI4_ITKCALL(iRetCode, POM_compare_dates(InputIRCreDate, TempCreDate, &iAnswer));

                if(iRetCode == ITK_ok && iAnswer == 1)
                {
					date_t PrevIRCreDate = POM_null_date();

                    if(iValidIR == 0)
                    {
                        PrevIRCreDate = TempCreDate;
						tTempPrevIR   = (*itervIRevList);
                    }
                    else
                    {
                        int iResult = 0;

                        LMI4_ITKCALL(iRetCode, POM_compare_dates(PrevIRCreDate, TempCreDate, &iResult));

                        if(iRetCode == ITK_ok && iResult == -1)
                        {
                            PrevIRCreDate = TempCreDate;
                            tTempPrevIR   = (*itervIRevList);
                        }
                    }

                    iValidIR++;
                }
            }

            if(iValidIR == 0)
            {
                refbIsFirstRev  = true;
            }
            else
            {
                reftPreviousIR = tTempPrevIR;
            }
        }
        else
        {
            refbIsOnlyRev = true;
        }
    }

    return iRetCode;
}
 
/**
 * Function gets list of all item revisions "reftPrevIR" that have creation date older than input item revision "tItemRev". If input item revision is the only revision of the item then function
 * returns 'pbIsOnlyRevision' as 'true'. Also if input item revision is the first revision of the item then function returns "refbIsFirstRev" as "true". 
 * 
 * @note: Suppose the input revision is revision 'A' but the Item of input revision has more than one revision then this function will return 'pbIsFirstRevision' as 'true'.
 *
 * @param[in]  tItemRev       Tag of Item Revision .
 * @param[out] refvPrevIR     List of Previous Item Revisions
 * @param[out] refbIsOnlyRev  'true' if input Item Revision is the only revision of Item
 * @param[out] refbIsFirstRev 'true' if input Item Revision is the First revision of the Item
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int LMI4_itemrev_get_all_previous_rev
(
    tag_t          tItemRev,
    vector<tag_t>& refvPrevIR,
    logical&       refbIsOnlyRev,
    logical&       refbIsFirstRev
)
{
    int    iRetCode       = ITK_ok;
    tag_t  tItem          = NULLTAG;
    date_t InputIRCreDate = POM_null_date();   	
	vector<tag_t> vtItemRevList;
		   
    LMI4_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));

    LMI4_ITKCALL(iRetCode, LMI4_item_list_all_revs(tItem, vtItemRevList));

    LMI4_ITKCALL(iRetCode, AOM_ask_value_date(tItemRev, LMI4_ATTR_OBJECT_CREATION_DATE, &InputIRCreDate));

	if(vtItemRevList.size() > 0)
    {
        vector<tag_t>::iterator itervIRevList;

        refbIsOnlyRev = false;

		for(itervIRevList = vtItemRevList.begin(); itervIRevList < vtItemRevList.end() && iRetCode == ITK_ok; itervIRevList++)
        {
            int    iAnswer     = 0;
            date_t TempCreDate = POM_null_date();

			LMI4_ITKCALL(iRetCode, AOM_ask_value_date((*itervIRevList), LMI4_ATTR_OBJECT_CREATION_DATE, &TempCreDate));

            /* Compares two dates for equality. Function result is returned in 'iAnswer' as
               0 if dates are equal 
               1 if date1 is later than date2 (date1 > date2)
               -1 if date1 is earlier than date2 (date1 < date2)
            */  
            LMI4_ITKCALL(iRetCode, POM_compare_dates(InputIRCreDate, TempCreDate, &iAnswer));

            if(iRetCode == ITK_ok && iAnswer == 1)
            {
				LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag((*itervIRevList), false, refvPrevIR));				               
            }
        }

		if(refvPrevIR.empty())
        {
            refbIsFirstRev  = true;
        }        
    }
    else
    {
        refbIsOnlyRev = true;
    }

    return iRetCode;
}


/**
 * Function takes item revision tag and constructs string using combination of various property value in the given below pattern:
 *                      'item_id'/'item_revision_id';'object_name'
 *
 * @param[in]  tItemRev   Tag of Item Revision
 * @param[out] refstrName Constructed string
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_itemrev_construct_name
(
    tag_t   tItemRev,
    string& refstrName
)
{
    int    iRetCode = ITK_ok;
    string strItemID;
    string strItemRevID;
    string strObjName;
    
    LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tItemRev, LMI4_ATTR_ITEM_ID, strItemID));
	
    LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tItemRev, LMI4_ATTR_ITEM_REVISION_ID, strItemRevID));

    LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tItemRev, LMI4_ATTR_OBJECT_NAME_ATTRIBUTE, strObjName));

    if(iRetCode == ITK_ok)
    {
		refstrName.assign(strItemID);
        refstrName.append(LMI4_STRING_FORWARD_SLASH);
		refstrName.append(strItemRevID);
		refstrName.append(LMI4_STRING_SEMICOLON);
		refstrName.append(strObjName);
    }
       
    return iRetCode;
}
















