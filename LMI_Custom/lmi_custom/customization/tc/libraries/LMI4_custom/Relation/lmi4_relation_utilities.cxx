/*======================================================================================================================================================================
                                                    Copyright 2017  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_relation_utilities.cxx

    Description:  This File contains custom functions for GRM to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>
#include <lmi4_constants.hxx>
#include <lmi4_errors.hxx>

/**
 * This function gets the objects related to input business object 'tObject'. Related objects are returned based on various input criteria such as:
 *
 *   <b> Relation Type between business objects </b>
 *   <b> Type of related object </b>
 *   <b> Related objects can be excluded based on status </b>
 * 
 * @note: This function is generic function and is capable of retrieving Primary and Secondary relation objects based on value of input logical argument 'bGetPrimary'. If argument
 *        'strRelationTypeName' is NULL then all related objects irrespective of relation type will be retrieved. If 'strObjType' is NOT NULL then related objects of type 'strObjType' will be 
 *         retrieved.
 *
 * @param[in]  tObject             Tag of Object
 * @param[in]  strRelationType     Relation Type. NULLTAG means all relation types.
 * @param[in]  strObjType          Specific type of object which is related to input object 'tObject'. If NULL then any type of related object.
 * @param[in]  strExcludeStatus    Related objects with this status will be excluded from valid related objects list
 * @param[in]  bGetPrimary         if 'true' then Primary related objects needs to be retrieved from input object else secondary objects will be retrieved. 
 * @param[out] refvValidRelatedObj List of valid related objects
 *
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMI4_grm_get_related_obj
(
    tag_t          tObject,
    string         strRelationType,
    string         strObjType,
    string         strExcludeStatus,
    logical        bGetPrimary,
    vector<tag_t>& refvValidRelatedObj
)
{
    int   iRetCode      = ITK_ok;
    tag_t tRelationType = NULLTAG;
    vector<GRM_relation_t> vstruRelatedObj;
	vector<GRM_relation_t>::iterator itervRelObj;
       
    if(strRelationType != "")
    {
        /* Get the relation type tag e.g. "IMAN_specification"  */
		LMI4_ITKCALL(iRetCode, GRM_find_relation_type((strRelationType.c_str()), &tRelationType)); 
    }
    
    if(bGetPrimary == true)
    {
        LMI4_ITKCALL(iRetCode, LMI4_grm_list_primary_objects(tObject, tRelationType, vstruRelatedObj));
    }
    else
    {
		LMI4_ITKCALL(iRetCode, LMI4_grm_list_secondary_objects(tObject, tRelationType, vstruRelatedObj));
    }

    /* Related objects are deliberately collected in this loop so that they can be loaded using POM API in one shot */                 
	for(itervRelObj = vstruRelatedObj.begin(); itervRelObj < vstruRelatedObj.end() && iRetCode == ITK_ok; itervRelObj++)
    {
        int     iActiveSeq = 0;
        tag_t   tObject    = NULLTAG;
        logical bIsWSObj   = false;
        
        if(bGetPrimary == true)
        {
            tObject = (*itervRelObj).primary;
        }
        else
        {
            tObject = (*itervRelObj).secondary;
        }

        LMI4_ITKCALL(iRetCode, LMI4_obj_is_type_of(tObject, LMI4_CLASS_ITEMREVISION, bIsWSObj));

        if(iRetCode == ITK_ok)
        {
            if(bIsWSObj == true)
            {
                LMI4_ITKCALL(iRetCode, AOM_ask_value_int(tObject, LMI4_ATTR_ACTIVE_SEQ, &iActiveSeq));
            }
            else
            {
                iActiveSeq = 1;
            }
        }
    
        /* This check indicates that Item Revisions with Active sequence will be considered */
        if(iActiveSeq != 0 && iRetCode == ITK_ok)
        {
            logical bBelongsToClass = false;

            if(strObjType.size() > 0)
            {
				LMI4_ITKCALL(iRetCode, LMI4_obj_is_type_of(tObject, (strObjType.c_str()), bBelongsToClass));
            }
            else
            {
                bBelongsToClass = true;
            }

            if( bBelongsToClass ==  true && iRetCode == ITK_ok)
            {
                tag_t  tStatus  = NULLTAG;

                if(strExcludeStatus.size() > 0)
                {
					LMI4_ITKCALL(iRetCode, LMI4_obj_ask_status_tag(tObject, (strExcludeStatus.c_str()), tStatus));
                }

                if(tStatus == NULLTAG && iRetCode == ITK_ok)
                {
					LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag(tObject, false, refvValidRelatedObj));					
                }
            }
        }
    }
  
    return iRetCode;
}
	
/**
 * This function creates a relation between primary and secondary object of input relation type 'strRelationType'. It takes primary object tag "tPrimaryObj", secondary object tag "tSecondaryObj" and 
 * relation type name "strRelationType" as input arguments and returns the tag of newly created relation "reftRelation".
 * 
 * @param[in]  tPrimaryObj     Tag of the primary object.
 * @param[in]  tSecondaryObj   Tag of the secondary object.
 * @param[in]  strRelationType Name of relation type.
 * @param[out] reftRelation    Tag to newly created relation.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMI4_grm_create_relation
(
    tag_t  tPrimaryObj,
    tag_t  tSecondaryObj,
    string strRelationType,
    tag_t& reftRelation
)
{
    int   iRetCode      = ITK_ok;
    tag_t tRelationType = NULLTAG;
    tag_t tNewRelation  = NULLTAG;
	
	/* Get relation type */
	LMI4_ITKCALL(iRetCode, GRM_find_relation_type((strRelationType.c_str()), &tRelationType));

	if(iRetCode == ITK_ok)
	{
		if(tRelationType == NULLTAG)
		{
			iRetCode = GRM_invalid_relation_type;
		}
		else if(tPrimaryObj == NULLTAG)
		{
			iRetCode = GRM_invalid_primary_object;
		}
		else if(tSecondaryObj == NULLTAG)
		{
			iRetCode = GRM_invalid_secondary_object;
		}
	}

     /*     Find relation to check if relation already exists   */
    LMI4_ITKCALL(iRetCode, GRM_find_relation(tPrimaryObj, tSecondaryObj, tRelationType, &tNewRelation));

    if(tNewRelation == NULLTAG && iRetCode == ITK_ok)
    {
        LMI4_ITKCALL(iRetCode, GRM_create_relation(tPrimaryObj, tSecondaryObj, tRelationType, NULLTAG, &tNewRelation));

        if(tNewRelation != NULLTAG && iRetCode == ITK_ok)
        {
            LMI4_ITKCALL(iRetCode, GRM_save_relation(tNewRelation));
        }
    }

    if(iRetCode == ITK_ok)
    {
        reftRelation = tNewRelation;
    }
    
    return iRetCode;
}




















