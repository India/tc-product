/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved

========================================================================================================================================================================

File Description:

    Filename: lmi4_get_nam_val_prop.hxx

    Description: 

=======================================================================================================================================================================

Date          Name           Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
04-Sep-18     Megha Singh    Initial Release

========================================================================================================================================================================*/
#ifndef LMI4_GET_NAM_VAL_PROP_HXX
#define LMI4_GET_NAM_VAL_PROP_HXX
#include <lmi4_property_containers.hxx>

class NamValProp
{
	public:
		int LMI4_get_all_nam_val_prop(tag_t tObject, string strPropName, int& refiTabCnt, map<Base*, PROP_value_type_e>& refmstrPropInfo);
		int LMI4_ask_fnd0namevalue_typ(tag_t tObject, map<Base*, PROP_value_type_e>& refmstrPropInfo);
		int LMI4_get_specific_nam_val_prop(tag_t tObject, string strPropName, int iRowIndex, map<Base*, PROP_value_type_e>& refmstrPropInfo);
		int LMI4_set_nam_val_prop(tag_t tObject, string strPropName, map<string, LMI4_NameValuePropSet*> mSetPropVal);
};

#endif