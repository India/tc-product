/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_get_nam_val_prop.cxx

    Description: This File contains declarations for functions to get and set name-value property on an object.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/
#include <lmi4_get_nam_val_prop.hxx>
#include <lmi4_library.hxx>

/**
 * This function returns map "refmstrPropInfo". Structure of Map is <key,value> <Base*, PROP_value_type_e>, here Base* is the class and 'PROP_value_type_e' is the typedef of enum.
 *
 * @note: The values of properties are stored inside datamembers of classes according to type of values(PROP_string, PROP_int...) and these classes are typecasted to parent class. Objects of classes
 * (LMI4_String, LMI4_Int....) are typecasted to parent 'Base' class. Since 'Keys' of map can have same names/different types therefore object of class 'Base' is added as 'key' to it. While type
 * (PROP_value_type_e) of property value is added as value of map. To extract the values from map the key('Base' class) is detypecasted to the type present as value of map(PROP_value_type_e).   
 *
 * @note: To extract value from map the 'key' ('Base' class) is detypecasted to the type present as value(typedef of enum PROP_value_type_e) of map. The user needs to free objects of classes 
 *		  used in map at the end.
 *
 * @param[in]  tObject         Tag of object
 * @param[in]  strPropName     Property name 
 * @param[out] refiTabCnt      Count of table row objects
 * @param[out] refmstrPropInfo Map containing object of class 'Base' as Key and typedef of enum 'PROP_value_type_e' as Value.
 * 
 * @retval ITK_ok is always returned.
 */
int NamValProp::LMI4_get_all_nam_val_prop
(
	tag_t  tObject,
	string strPropName,
	int&   refiTabCnt, 
	map<Base*, PROP_value_type_e>& refmstrPropInfo
)
{
	int iRetCode = ITK_ok;
 	vector<tag_t> vtTabRow;
			
	LMI4_ITKCALL(iRetCode, LMI4_aom_ask_table_rows(tObject, strPropName, refiTabCnt, vtTabRow));
	
	vector<tag_t> :: iterator itrTabRows;

	/* Traversing table row objects. */
	for(itrTabRows = vtTabRow.begin(); itrTabRows != vtTabRow.end() && iRetCode == ITK_ok; itrTabRows++)
	{				
		LMI4_ITKCALL(iRetCode, LMI4_ask_fnd0namevalue_typ((*itrTabRows), refmstrPropInfo));			
    }   
	
    return iRetCode;
}

/**
 *  This function returns map "refmstrPropInfo". Structure of Map is <key,value> <Base*, PROP_value_type_e>, here Base* is the class and 'PROP_value_type_e' is the typedef of enum.
 *
 * @note: The values of properties are stored inside datamembers of classes according to type of values(PROP_string, PROP_int...) and these classes are typecasted to parent class. Objects of classes
 * (LMI4_String, LMI4_Int....) are typecasted to parent 'Base' class. Since 'Keys' of map can have same names/different types therefore object of class 'Base' is added as 'key' to it. While type
 * (PROP_value_type_e) of property value is added as value of map. To extract the values from map the key('Base' class) is detypecasted to the type present as value of map(PROP_value_type_e).
 * 
 * @note: To extract value from map the 'key' ('Base' class) is detypecasted to the type present as value(typedef of enum PROP_value_type_e) of map.
 *
 * @param[in]  tObject         Tag of object
 * @param[out] refmstrPropInfo Map containing object of class 'Base' as key and typedef of enum 'PROP_value_type_e' as value.
 * 
 * @retval ITK_ok is always returned.
 */
int NamValProp::LMI4_ask_fnd0namevalue_typ
(
	tag_t tObject,
	map<Base*, PROP_value_type_e>& refmstrPropInfo
)
{
	int    iRetCode = ITK_ok;
	tag_t  tType    = NULLTAG;
	string strClassTyp;
	string strName;
	string strConvertedString;
	stProp stPropList;

	Base *bClsObj = new Base();
	
	/* Getting object type. */
	LMI4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tType));

	/* Getting the name of object type. */
	LMI4_ITKCALL(iRetCode, LMI4_tctype_ask_name2(tType, strClassTyp));

	if(strcmp (strClassTyp.c_str(), LMI4_CLASS_FND0NAMEVALUESTRING) == 0 && iRetCode == ITK_ok)
	{    
		/* Getting the value of property 'fnd0Name'. */
		LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, LMI4_ATTR_FND0NAME, strName));

		/* Getting the value of property 'fnd0Value'. */
		LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, LMI4_ATTR_FND0VALUE, stPropList.strPropVal));

		LMI4_String *strObj = new LMI4_String();

		strObj->iType       = PROP_string;
		
		strObj->strAttrName = strName; 

		strObj->strValue    = stPropList.strPropVal;
		
		/* Typecasting object of class 'LMI4_String' to parent class 'Base'to add  class as key in map. */
		/* Since maps can hold keys with same names therefore typecasting is done. */
		bClsObj = (Base*) strObj;	
		
		/* Inserting values inside map. */
		refmstrPropInfo.insert(pair <Base*, PROP_value_type_e> (bClsObj, PROP_string));

		LMI4_OBJECT_DELETE(strObj);
	}
	
	else if(strcmp (strClassTyp.c_str(), LMI4_CLASS_FND0NAMEVALUEINT) == 0 && iRetCode == ITK_ok)
	{
		/* Getting the value of property 'fnd0Name'. */
		LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, LMI4_ATTR_FND0NAME, strName));

		/* Getting the integer value of property 'fnd0Value'. */
		LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_int(tObject, LMI4_ATTR_FND0VALUE, (stPropList.iPropVal)));

		/* Getting the string equivalent 'strConvertedString' of integer value of property 'stList.iPropVal'. */
		LMI4_ITKCALL(iRetCode, LMI4_int_to_string(stPropList.iPropVal, strConvertedString));

		LMI4_Integer *iObj = new LMI4_Integer();

		iObj->iType        = PROP_int;

		iObj->strAttrName  = strName;

		iObj->iValue       = stPropList.iPropVal;

		iObj->strValue     = strConvertedString; 
		
		/* Typecasting object of class 'LMI4_Integer' to parent class 'Base'. */
		bClsObj = (Base*) iObj;	

		/* Inserting the values inside map. */
		refmstrPropInfo.insert(pair <Base*, PROP_value_type_e> (bClsObj, PROP_int));

		LMI4_OBJECT_DELETE(iObj);
	}

	else if(strcmp (strClassTyp.c_str(), LMI4_CLASS_FND0NAMEVALUEDOUBLE) == 0 && iRetCode == ITK_ok)           
	{
		/* Getting the value of property 'fnd0Name'. */
		LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, LMI4_ATTR_FND0NAME, strName));

		/* Getting the double value of property 'fnd0Value'. */
		LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_double(tObject, LMI4_ATTR_FND0VALUE, stPropList.dPropVal));

		/* Getting the string equivalent 'strConvertedString' of double value of property 'stList.dPropVal'. */
		LMI4_ITKCALL(iRetCode, LMI4_double_to_string(stPropList.dPropVal, strConvertedString));

		LMI4_Double *dObj = new LMI4_Double();

		dObj->iType       = PROP_double;

		dObj->strAttrName = strName;

		dObj->dValue      = stPropList.dPropVal;

		dObj->strValue    = strConvertedString; 
		
		/* Typecasting object of class 'LMI4_Double' to parent class 'Base'. */
		bClsObj = (Base*) dObj;	

		/* Inserting values inside map. */
		refmstrPropInfo.insert(pair <Base*, PROP_value_type_e> (bClsObj, PROP_double));	

		LMI4_OBJECT_DELETE(dObj);
	}

	else if(strcmp( strClassTyp.c_str(), LMI4_CLASS_FND0NAMEVALUEDATE) == 0 && iRetCode == ITK_ok)
	{
		string strStrDate;

		/* Getting the value of property 'fnd0Name'. */
		LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, LMI4_ATTR_FND0NAME, strName));

		/* Getting the date value of property 'fnd0Value'. */
		LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_date(tObject, LMI4_ATTR_FND0VALUE, stPropList.dtPropVal));

		/* Getting the string equivalent 'strConvertedString' of date value of property 'stList.dtPropVal'. */
		LMI4_ITKCALL(iRetCode, LMI4_itk_date_to_string(stPropList.dtPropVal, strStrDate));

		LMI4_Date *dtObj   = new LMI4_Date();

		dtObj->iType       = PROP_date;

		dtObj->strAttrName = strName;    

		dtObj->dtValue     = stPropList.dtPropVal;

		dtObj->strValue    = strConvertedString; 
		
		/* Typecasting object of class 'LMI4_Date' to parent class 'Base'. */
		bClsObj = (Base*) dtObj;	

		/* Inserting the values inside map. */
		refmstrPropInfo.insert(pair <Base*, PROP_value_type_e> (bClsObj, PROP_date));

		LMI4_OBJECT_DELETE(dtObj);
	}

	else if(strcmp (strClassTyp.c_str(), LMI4_CLASS_FND0NAMEVALUELOGICAL) == 0 && iRetCode == ITK_ok)
	{
		/* Getting the value of property 'fnd0Name'. */
		LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, LMI4_ATTR_FND0NAME, strName));

		/* Getting the date value of property 'fnd0Value'. */
		LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_logical(tObject, LMI4_ATTR_FND0VALUE, stPropList.bPropVal));

		/* Getting the string equivalent 'strConvertedString' of logical value of property 'stList.bPropVal'. */
		LMI4_ITKCALL(iRetCode, LMI4_logical_to_string(stPropList.bPropVal, strConvertedString)); 

		LMI4_Logic *bObj  = new LMI4_Logic();

		bObj->iType       = PROP_logical;

		bObj->strAttrName = strName; 

		bObj->bValue      = stPropList.bPropVal;

		bObj->strValue    = strConvertedString; 
		
		/* Typecasting object of class 'LMI4_Logic' to parent class 'Base'. */
		bClsObj = (Base*) bObj;	

		/* Inserting values inside map. */
		refmstrPropInfo.insert(pair <Base*, PROP_value_type_e> (bClsObj, PROP_logical));

		LMI4_OBJECT_DELETE(bObj);
	}

	return iRetCode;
}

/**
 * For given input property "strPropName" of table object at given index "iRowIndex" this function returns map "refmstrPropInfo". Structure of Map is <key,value> <Base*, PROP_value_type_e>, here Base* is the class and
 * 'PROP_value_type_e' is the typedef of enum.
 *
 * @param[in]  tObject         Tag of object
 * @param[in]  strPropName     Property name
 * @param[in]  iRowIndex       Index of table row object whose attribute value has to be returned.
 * @param[out] refmstrPropInfo Map containing object of class 'Base' as Key and typedef of 'PROP_value_type_e' as Value.
 * 
 * @retval ITK_ok is always returned.
 */
int NamValProp::LMI4_get_specific_nam_val_prop
(
	tag_t  tObject,
	string strPropName,
	int    iRowIndex,
	map<Base*, PROP_value_type_e>& refmstrPropInfo
)
{
	int   iRetCode = ITK_ok;
 	tag_t tTabRow  = NULLTAG;
	   		
	LMI4_ITKCALL(iRetCode, LMI4_aom_ask_table_row(tObject, strPropName, iRowIndex, tTabRow));
	
	if(iRetCode == ITK_ok && tTabRow != NULLTAG)
	{	
		LMI4_ITKCALL(iRetCode, LMI4_ask_fnd0namevalue_typ(tTabRow, refmstrPropInfo));		
    }   
	
    return iRetCode;
}

/**
 * This function sets given input property "strPropName" with value contained in input map "mSetPropVal". Structure of Map is <key,value> <string, LMI4_NameValuePropSet*>, here string is the property
 * name and "LMI4_NameValuePropSet" is class which contains value of property and typedef of enum PROP_value_type_e as datamembers.
 *
 * @param[in] tObject     Tag of object
 * @param[in] strPropName Property name
 * @param[in] mSetPropVal Map containing property name as key and object of class "LMI4_NameValuePropSet" as value. Class "LMI4_NameValuePropSet" contains value of property and typedef of enum
 *						  PROP_value_type_e as datamemebers.
 * 
 * @retval ITK_ok is always returned.
 */
int NamValProp::LMI4_set_nam_val_prop
(
    tag_t  tObject,
	string strPropName,
    map<string, LMI4_NameValuePropSet*> mSetPropVal
)
{
    int iRetCode = ITK_ok;
	map <string, LMI4_NameValuePropSet*> :: iterator itrSetPropVal;
	
	LMI4_ITKCALL(iRetCode, AOM_refresh(tObject, 1));

	for (itrSetPropVal = mSetPropVal.begin(); itrSetPropVal != mSetPropVal.end() && iRetCode == ITK_ok; itrSetPropVal++)
	{
		vector<tag_t> vtTabRows;
		vector<tag_t> vtValTab;
		int    iLen             = 0;
		int    iCount           = 0;
		int    iMarkPoint       = 0;
		tag_t  tObjType         = NULLTAG;
		tag_t  tObjCreateInput  = NULLTAG;
		tag_t  tItem            = NULLTAG;   
		tag_t* ptArray          = NULL;
		tag_t* ptNewArray       = NULL;


		LMI4_ITKCALL(iRetCode, AOM_ask_value_tags(tObject, strPropName.c_str(), &iCount, &ptArray));		
								
		if (itrSetPropVal->second->type == PROP_string && iRetCode == ITK_ok)
		{
			LMI4_ITKCALL(iRetCode, TCTYPE_find_type(LMI4_CLASS_FND0NAMEVALUESTRING, NULL, &tObjType));
			
			/* Create object of class 'Fnd0NameValueString'. */
			LMI4_ITKCALL(iRetCode, TCTYPE_construct_create_input(tObjType, &tObjCreateInput));

			/* Setting 'itrSetPropVal->first' as value of property 'fnd0Name'. */
			LMI4_ITKCALL(iRetCode, AOM_set_value_string(tObjCreateInput, LMI4_ATTR_FND0NAME, itrSetPropVal->first.c_str()));
			
			/* Setting 'itrSetPropVal->second->strPropValue' as value of property 'fnd0Value'. */
			LMI4_ITKCALL(iRetCode, AOM_set_value_string(tObjCreateInput, LMI4_ATTR_FND0VALUE, itrSetPropVal->second->strPropValue.c_str()));											
		}
		else if (itrSetPropVal->second->type == PROP_int && iRetCode == ITK_ok)
		{
			int     iIntVal = 0;
			logical bIsInt  = false;

			LMI4_ITKCALL(iRetCode, TCTYPE_find_type(LMI4_CLASS_FND0NAMEVALUEINT, NULL, &tObjType));

			/* Create object of class 'Fnd0NameValueInteger'. */
			LMI4_ITKCALL(iRetCode, TCTYPE_construct_create_input(tObjType, &tObjCreateInput));

			/* Setting 'itrSetPropVal->first' as value of property 'fnd0Name'. */
			LMI4_ITKCALL(iRetCode, AOM_set_value_string(tObjCreateInput, LMI4_ATTR_FND0NAME, itrSetPropVal->first.c_str()));

			/* Integer equivalent of string 'itrSetPropVal->second->strPropValue'. */
			LMI4_ITKCALL(iRetCode, LMI4_string_is_Integer(itrSetPropVal->second->strPropValue, iIntVal, bIsInt));

			/* Setting 'iIntVal' as value of property 'fnd0Value'. */
			LMI4_ITKCALL(iRetCode, AOM_set_value_int(tObjCreateInput, LMI4_ATTR_FND0VALUE, iIntVal));											
		}
		else if (itrSetPropVal->second->type == PROP_double && iRetCode == ITK_ok)
		{
			double  dDoublVal = 0;
			logical bIsDouble = false;

			LMI4_ITKCALL(iRetCode, TCTYPE_find_type(LMI4_CLASS_FND0NAMEVALUEDOUBLE, NULL, &tObjType));

			/* Create object of class 'Fnd0NameValueDouble. */
			LMI4_ITKCALL(iRetCode, TCTYPE_construct_create_input(tObjType, &tObjCreateInput));

			/* Setting 'itrSetPropVal->first' as value of property 'fnd0Name'. */
			LMI4_ITKCALL(iRetCode, AOM_set_value_string(tObjCreateInput, LMI4_ATTR_FND0NAME, itrSetPropVal->first.c_str()));

			/* Double equivalent of string 'itrSetPropVal->second->strPropValue'. */
			LMI4_ITKCALL(iRetCode, LMI4_string_is_Double(itrSetPropVal->second->strPropValue, dDoublVal, bIsDouble));

			/* Setting 'dDoubVal' as value of property 'fnd0Value'. */
			LMI4_ITKCALL(iRetCode, AOM_set_value_double(tObjCreateInput, LMI4_ATTR_FND0VALUE, dDoublVal));											
		}
		else if (itrSetPropVal->second->type == PROP_date && iRetCode == ITK_ok)
		{
			logical IsValid = false;
			date_t  dtValue;
			string  strAttrValue;
			string  strDateText;
			
			LMI4_ITKCALL(iRetCode, TCTYPE_find_type(LMI4_CLASS_FND0NAMEVALUEDATE, NULL, &tObjType));

			/* Create object of class 'Fnd0NameValueDate'. */
			LMI4_ITKCALL(iRetCode, TCTYPE_construct_create_input(tObjType, &tObjCreateInput));

			/* Setting 'itrSetPropVal->first' as value of property 'fnd0Name'. */
			LMI4_ITKCALL(iRetCode, AOM_set_value_string(tObjCreateInput, LMI4_ATTR_FND0NAME, itrSetPropVal->first.c_str()));

			/* Date equivalent of string 'itrSetPropVal->second->strPropValue'. */
			LMI4_ITKCALL(iRetCode, LMI4_date_string_to_date_t(itrSetPropVal->second->strPropValue, IsValid, dtValue));

			/* Setting 'dtValue' as value of property 'fnd0Value'. */
			LMI4_ITKCALL(iRetCode, AOM_set_value_date(tObjCreateInput, LMI4_ATTR_FND0VALUE, dtValue));											
		}
		else if (itrSetPropVal->second->type == PROP_logical && iRetCode == ITK_ok)
		{
			logical bConverted = false;

			LMI4_ITKCALL(iRetCode, TCTYPE_find_type(LMI4_CLASS_FND0NAMEVALUELOGICAL, NULL, &tObjType));

			/* Create object of class 'Fnd0NameValueLogical'. */
			LMI4_ITKCALL(iRetCode, TCTYPE_construct_create_input(tObjType, &tObjCreateInput));

			/* Setting 'dDoubVal' as value of property 'fnd0Value'. */
			LMI4_ITKCALL(iRetCode, AOM_set_value_string(tObjCreateInput, LMI4_ATTR_FND0NAME, itrSetPropVal->first.c_str()));

			/* Logical equivalent of string 'itrSetPropVal->second->strPropValue'. */
			LMI4_ITKCALL(iRetCode, LMI4_string_to_logical(itrSetPropVal->second->strPropValue, bConverted));

			/* Setting 'bConverted' as value of property 'fnd0Value'. */
			LMI4_ITKCALL(iRetCode, AOM_set_value_logical(tObjCreateInput, LMI4_ATTR_FND0VALUE, bConverted));										
		}

		LMI4_ITKCALL(iRetCode, POM_place_markpoint(&iMarkPoint));
		
		/* Create object */
		LMI4_ITKCALL(iRetCode, TCTYPE_create_object(tObjCreateInput, &tItem));

		LMI4_ITKCALL(iRetCode, AOM_save_with_extensions(tItem));
						
		if(iRetCode == ITK_ok)
		{
			for(int iDx = 0; iDx < iCount; iDx++)
			{
				LMI4_TC_UPDATE_TAG_ARRAY(ptNewArray, ptArray[iDx], iLen);
			}
			LMI4_TC_UPDATE_TAG_ARRAY(ptNewArray, tItem, iLen);							
		}
			
		LMI4_ITKCALL(iRetCode, AOM_set_value_tags(tObject, strPropName.c_str(), iLen, ptNewArray));		

		LMI4_MEM_TCFREE(ptArray);

		LMI4_MEM_TCFREE(ptNewArray);
	}

	LMI4_ITKCALL(iRetCode, AOM_save_with_extensions(tObject));

	LMI4_ITKCALL(iRetCode, AOM_refresh(tObject, 0));

	return iRetCode;
}























