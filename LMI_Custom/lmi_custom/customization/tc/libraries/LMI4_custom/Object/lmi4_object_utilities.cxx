/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_object_utilities.cxx

    Description:  This File contains custom functions for POM classes to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-Mar-18    Megha Singh   Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>
#include <lmi4_constants.hxx>
#include <lmi4_errors.hxx>
#include <lmi4_property_containers.hxx>

/**
 * This function returns business object type name of input business object "tObject".
 *
 * @param[in]  tObject       Business Object tag
 * @param[out] refstrObjType Object type name 
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_obj_ask_type
(
    tag_t   tObject,
    string& refstrObjType
)
{
    int   iRetCode    = ITK_ok;
	tag_t tObjectType = NULLTAG;
	        
    LMI4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tObjectType));
	 
	LMI4_ITKCALL(iRetCode, LMI4_tctype_ask_name2(tObjectType, refstrObjType));

	return iRetCode;
}


/**
 * This function checks if input Object 'tObject' is sub type or of same type as input type 'strTypeName'. Function returns 'true' if input object belongs to expected valid type. 
 * 
 * @param[in]  tObject                 Tag of object
 * @param[in]  strTypeName             Type name
 * @param[out] refbBelongsToInputClass Flag indicating whether 'tObject' is the same/subtype as 'strTypeName' 
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int LMI4_obj_is_type_of
(
    tag_t    tObject,
    string   strTypeName,
    logical& refbBelongsToInputClass
)
{
    int     iRetCode         = ITK_ok;  
    tag_t   tType            = NULLTAG;
    tag_t   tInputObjType    = NULLTAG;
	logical bBelongsToInpCls = false; 

	LMI4_ITKCALL(iRetCode, LMI4_tctype_find_type(strTypeName, "", tType));

    LMI4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tInputObjType));

	LMI4_ITKCALL(iRetCode, LMI4_tctype_is_type_of(tInputObjType, tType, bBelongsToInpCls));

	if(iRetCode == ITK_ok)
	{
		refbBelongsToInputClass = bBelongsToInpCls;
	}

    return iRetCode;
}

/**
 * This function recieves tag "tObject", name of status "strStatusName" as input parameters and returns tag of status if input object "tObject" has status of type "strStatusName".
 * attached to it.
 *
 * @param[in]  tObject       Tag of object whose status of specific type needs to be retrieved.
 * @param[in]  strStatusName Name of status.
 * @param[out] reftStatus    Tag of status
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_obj_ask_status_tag
(
    tag_t  tObject,
    string strStatusName,
    tag_t& reftStatus
)
{
    int iRetCode      = ITK_ok;
    int iNoOfStatuses = 0;

    vector<tag_t> vtStatusList;
	vector<tag_t>::iterator itervObjList;
	
	LMI4_ITKCALL(iRetCode, LMI4_wsom_ask_release_status_list(tObject, iNoOfStatuses, vtStatusList));

    for (int iDx = 0; iDx < iNoOfStatuses && iRetCode == ITK_ok; iDx++)
    {
		string strRelStatusType;

		for(itervObjList = vtStatusList.begin(); itervObjList < vtStatusList.end(); itervObjList++)
		{
			LMI4_ITKCALL(iRetCode, LMI4_relstat_ask_release_status_type((*itervObjList), strRelStatusType));
        
			if(strStatusName.compare(strRelStatusType) == 0)
			{
				  reftStatus = (*itervObjList);
				  break;
			}
		}        		
    }
        
    return iRetCode;
}

/**
 * This function recieves BO type "strType" and returns display name of input BO Type.
 *
 * e.g. If ItemRevision type 'A4_ProjectRevision' is pass to this function then the Function will return the display name of this Item type i.e. 'Project Container Revision'
 *
 * @param[in]  strType           Business object type
 * @param[out] refstrDisplayName Business object display name
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_obj_ask_type_display_name
(
    string  strType,
    string& refstrDisplayName
)
{
    int   iRetCode = ITK_ok;
    tag_t tType    = NULLTAG;   
    
	LMI4_ITKCALL(iRetCode, LMI4_tctype_find_type(strType, NULL, tType));
    
    LMI4_ITKCALL(iRetCode, LMI4_tctype_ask_display_name(tType, refstrDisplayName));
   
    return iRetCode;
}

/**
 * This function recieves tag of object "tObject" as input argument. It saves and unlock the given input object.
 *
 * @note The object must be an instance of Application Object class or its subclass.
 *
 * @param[in] tObject Tag of object to be saved and unlocked.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_obj_save_and_unlock
(
    tag_t tObject
)
{
    int iRetCode  = ITK_ok;

    /*  Save the object.  */
    LMI4_ITKCALL(iRetCode, AOM_save_with_extensions(tObject));
   
    /* Unlock the object. */
    LMI4_ITKCALL(iRetCode, AOM_unlock(tObject));
    
    return iRetCode;
}

/**
 * This function recieves following input arguments - tag "tObject", name of property "strPropName" and returns value of input property.
 *
 * @note The object must be an instance of Application Object class or its subclass.
 *
 * @param[in] tObject     Tag of object to be saved and unlocked
 * @param[in] strPropName Name of property
 * @param[in] strPropVal  Value of property to be set
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_obj_set_str_attribute_val
(
    tag_t  tObject,
    string strPropName,
    string strPropVal
)
{
    int iRetCode = ITK_ok;

    /*  Lock the object.  */
    LMI4_ITKCALL(iRetCode, AOM_refresh(tObject, 1));
   
    /* Set the attribute string value. */    
	LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_string(tObject, strPropName, strPropVal));

    /*  Save the object.  */
    LMI4_ITKCALL(iRetCode, AOM_save_with_extensions(tObject));

    /*  Unlock the object.  */
    LMI4_ITKCALL(iRetCode, AOM_refresh(tObject, 0));

    return iRetCode;
}

/**
 * This function sets the reference property value for input "tObject" corresponding to property "strPropName".
 *
 * @param[in] tObject     Tag of object 
 * @param[in] strPropName Name of the property
 * @param[in] tAttrVal    Reference (tagged) value of the property to be set
 *
 * @note The object must be an instance of Application Object class or its subclass.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_obj_set_tag_value
(
    tag_t  tObject,
    string strPropName,
    tag_t  tAttrVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    LMI4_ITKCALL(iRetCode, AOM_refresh(tObject, 1));
   
    /* Set the attribute Tag value.  */    
	LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_tag(tObject, (strPropName.c_str()), tAttrVal));

    /*  Save the object.  */
    LMI4_ITKCALL(iRetCode, AOM_save_with_extensions(tObject));

    /*  Unlock the object.  */
     LMI4_ITKCALL(iRetCode, AOM_refresh(tObject, 0));

    return iRetCode;
}

/**
 * This function takes source "strSrcType" and destination "strDestType" type name as input arguments. It returns "refbBelongsToInputClass" as "true" if destination type is of same or subtype of
 * source type. It also returns "refbIsNotBOType" as 'true' if source or destination type strings are invalid.
 *
 * @param[in]  strSrcType              Source type name
 * @param[in]  strDestType             Destination type name
 * @param[out] refbIsNotBOType         'true' if 'strSrcType' or 'strDestType' are of invalid types.
 * @param[out] refbBelongsToInputClass 'true' if 'strDestType' is of same or subtype as 'strSrcType'.
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int LMI4_obj_is_type_of_2
(
    string   strSrcType,
    string   strDestType,
    logical& refbIsNotBOType,
    logical& refbBelongsToInputClass
)
{
    int   iRetCode  = ITK_ok;  
    tag_t tSrcType  = NULLTAG;
    tag_t tDestType = NULLTAG;
    			
	LMI4_ITKCALL(iRetCode, LMI4_tctype_find_type(strSrcType, NULL, tSrcType));

	LMI4_ITKCALL(iRetCode, LMI4_tctype_find_type(strDestType, NULL, tDestType));

    if(iRetCode == ITK_ok)
    {
        if(tSrcType != NULLTAG && tDestType != NULLTAG)
        {
            logical bBelongsToInpClass = false;

            LMI4_ITKCALL(iRetCode, LMI4_tctype_is_type_of(tSrcType, tDestType, bBelongsToInpClass));

            if(iRetCode == ITK_ok)
            {
                refbBelongsToInputClass = bBelongsToInpClass;
            }
        }
        else
        {
            refbIsNotBOType = true;
        }
    }
    
    return iRetCode;
}

/**
 * This function checks if input object "tobject" has write access and object is not checked-out. If input object does not full fill the validation criteria then function returns the output parameters
 * as 'false'.
 * 
 * @param[in]  tObject               Tag of Object to be validated
 * @param[out] refbWriteAccessDenied 'true' if Object has write access  
 * @param[out] refbObjCheckedOut     'true' if Object is not checked out
 * @param[out] refbModifiable        'true' if object is Modifiable i.e. Object has write access and is not checked-out
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_check_privilege
(
    tag_t    tObject,
    logical& refbWriteAccessDenied,
    logical& refbObjCheckedOut,
    logical& refbModifiable
)
{
    int     iRetCode = ITK_ok;
    logical bAccess  = false;
	    
    LMI4_ITKCALL(iRetCode, RES_assert_object_modifiable (tObject));

    LMI4_ITKCALL(iRetCode, LMI4_am_check_privilege (tObject, LMI4_WRITE_ACCESS, bAccess));

    if(iRetCode == ITK_ok)
    {
        if(bAccess == false)
        {
            string strAttrVal;

            refbModifiable        = false;
            refbWriteAccessDenied = true;
            
            LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, LMI4_ATTR_OBJECT_STRING_ATTRIBUTE, strAttrVal));

            TC_write_syslog ("Write access is denied on %s. \n", strAttrVal);
		}
        else
        {
            logical bCheckedOut = false;

            LMI4_ITKCALL(iRetCode, RES_is_checked_out (tObject, &bCheckedOut));

            if(bCheckedOut == false)
            {
                refbModifiable = true;
            }
            else
            {
                string strUserName;
                tag_t  tReservationObj = NULLTAG;
                tag_t  tCheckOutUser   = NULLTAG;
                tag_t  tUser           = NULLTAG;

                LMI4_ITKCALL(iRetCode, RES_ask_reservation_object (tObject, &tReservationObj));
				
                LMI4_ITKCALL(iRetCode, RES_ask_user(tReservationObj, &tCheckOutUser));

                LMI4_ITKCALL(iRetCode, LMI4_pom_get_user(strUserName, tUser));

                if (tUser != NULLTAG && tUser == tCheckOutUser)
                {
                    string strObjAttrVal;

                    LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, LMI4_ATTR_OBJECT_STRING_ATTRIBUTE, strObjAttrVal));

                    TC_write_syslog ("%s object is checked-out by same user\n", strObjAttrVal);

                    refbModifiable = true;                   
                } 
                else 
                {
                    refbModifiable    = false;
                    refbObjCheckedOut = true;
                }
			}
        }
    }

    return iRetCode;
}

/**
 * This function traverses input list of objects "vtObject" and find the very first object that matches the type "strObjType". This function returns the tag of valid very first object from the input
 * objects list that is of the same type as "strObjType".
 * 
 * @param[in]  vtObject       List of objects
 * @param[in]  strObjType     Object type name 
 * @param[out] reftCorrectObj Tag of object which is of input type "strObjType"
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_obj_get_obj_of_input_type
(
    vector<tag_t> vtObject,
    string        strObjType,
    tag_t&        reftCorrectObj
)
{
    int iRetCode = ITK_ok;
	vector<tag_t>::iterator itervObjList;
        
	for(itervObjList = vtObject.begin(); itervObjList < vtObject.end() && iRetCode == ITK_ok; itervObjList++)
    {
        logical bIsCorrectType = false;

		LMI4_ITKCALL(iRetCode, LMI4_obj_is_type_of((*itervObjList), strObjType, bIsCorrectType));

        if(bIsCorrectType == true && iRetCode == ITK_ok)
        {
			reftCorrectObj = (*itervObjList);
            break;
        }
    }

    return iRetCode;
}

/*
/**
 * This function gets the attribute value for input TC object in string format.
 * This function will convert value of following attribute type to string value:
 *      - String
 *      - Integer
 *      - Float
 *      - Double
 *      - Logical
 * 
 * @param[in]   tObject           Tag of Object whose attribute value needs to be extracted
 * @param[in]    strAttributeName  Name of Attribute of input object
 * @param[out]  refstrAttrValue   String Value of Attribute
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_obj_aom_get_attribute_value
(
    tag_t   tObject,
    string  strAttributeName,
    string& refstrAttrValue
)
{
    int    iRetCode   = ITK_ok;
    string strValType;
    PROP_value_type_t ValueTypeOfProp = PROP_untyped; 
	    
    /* Get the type of the property value */
    LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_type(tObject, strAttributeName, ValueTypeOfProp, strValType));

    if(iRetCode == ITK_ok && ValueTypeOfProp != PROP_string)
    {
        refstrAttrValue = (char*) MEM_alloc(sizeof (char) * LMI4_STRING_ATTR_LEN);
    }

    /* Reset the property based on its value type */
    if (iRetCode == ITK_ok)
    {
        switch(ValueTypeOfProp)
        {
            case PROP_string :
            case PROP_note   :
                {
                    LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, strAttributeName, refstrAttrValue));
                    break;
                }
            case PROP_logical :
                {
                    logical bYesNo = false;

                    LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_logical(tObject, strAttributeName, bYesNo));

					//bYesNo ? sprintf((refstrAttrValue.c_str) , "%s" , LMI4_CAPS_YES) : sprintf ((refstrAttrValue.c_str) , "%s", LMI4_CAPS_NO);
                    break;
                }
            case PROP_int   :
            case PROP_short :
                {
                    int iValue = 0;

					LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_int(tObject, strAttributeName, iValue));

                    if(iValue == 0)
                    {
						refstrAttrValue.append(LMI4_STRING_BLANK);
                    }
                    else
                    {
						//sprintf(refstrAttrValue.c_str, "%d",  iValue);
                    }
                    break;
                }
            case PROP_float  :
            case PROP_double :
                {
                    double dValue = 0.0;

                    LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_double(tObject, strAttributeName, dValue));

                    if(dValue == 0.0)
                    {
						refstrAttrValue.append(LMI4_STRING_BLANK);
                    }
                    else
                    {
						//sprintf ((refstrAttrValue.c_str), "%f",  dValue);
                    }

                    break;
                }
            case PROP_typed_reference    :
            case PROP_untyped_reference  :
            case PROP_external_reference :
            case PROP_typed_relation     :
            case PROP_untyped_relation   :
                {
                    tag_t  tValue = NULLTAG;
                    string strStringVal;

					LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_tag(tObject, strAttributeName, tValue));

                    if(tValue != NULLTAG)
                    {
                        LMI4_ITKCALL(iRetCode, LMI4_aom_tag_to_string(tValue, strStringVal));

						(refstrAttrValue.append(strStringVal));
                    }
                    else
                    {
						refstrAttrValue.append(LMI4_STRING_BLANK);
                    }
					                   
                    break;
                }
            case PROP_date :
                {
                    string strDateFormat;
                    date_t dValue = NULLDATE;

					LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_date(tObject, strAttributeName, dValue));

                    if( DATE_IS_NULL(dValue) != 1)
                    {
                        LMI4_ITKCALL(iRetCode, LMI4_date_default_date_format(strDateFormat));	

                        LMI4_ITKCALL(iRetCode, LMI4_date_date_to_string(dValue, strDateFormat, refstrAttrValue));
                    }
                    else
                    {
						refstrAttrValue.assign(LMI4_STRING_BLANK);
                    }

                    break;
                }
        }
    }

    return iRetCode;
}

/**
 * This function returns "true" if the input property "strAttrName" of object "tObject" is an array.  
 *
 * @param[in]  tObject             Business Object tag
 * @param[in]  strAttrName         Name of Property
 * @param[out] refbIsPropTypeArray 'true' if input property is of type array.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_obj_is_property_type_array
(
    tag_t    tObject,
    string   strAttrName,
    logical& refbIsPropTypeArray
)
{
    int iRetCode        = ITK_ok;
    int iMaxNumElements = 0;
	    
	LMI4_ITKCALL(iRetCode, LMI4_aom_ask_max_num_elements(tObject, strAttrName, iMaxNumElements));

    if (iRetCode == ITK_ok)
    {
        /* Set multiFlag based on maxNumElements results
                  (-1) list property
                  (1)  single value
                  (positive)  array property
        */
        if(iMaxNumElements != 1)
        {
            refbIsPropTypeArray = true;
        }
    }

    return iRetCode;
}

/**
 * This function takes tag of object "tObject", property name "strPropName" as input arguments. This functions checks if input string "strPropVal" exists as value for attribute "strPropName"
 * than function will not set the atttribute value else will continue to set "strPropVal" as input value.
 *
 * @note The object must be an instance of POM Application Object class or a subclass of that.
 *
 * @param[in] tObject     Tag of object to be saved and unloaded
 * @param[in] strPropName Name of the property
 * @param[in] strPropVal  Value of the property to be set
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_obj_check_and_set_str_attribute_val
(
    tag_t  tObject,
    string strPropName,
    string strPropVal
)
{
    int     iRetCode           = ITK_ok;
    logical bWriteAccessDenied = false;
    logical bObjCheckedOut     = false;
    logical bModifiable        = false;

	if(strPropVal.size() > 0)
	{
		LMI4_ITKCALL(iRetCode, LMI4_check_privilege(tObject, bWriteAccessDenied, bObjCheckedOut, bModifiable));
		
		if(bModifiable == true && iRetCode == ITK_ok)
		{
			string strVal;

			LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, strPropName, strVal));

			if(strVal.compare(strPropVal) != 0 && iRetCode == ITK_ok)
			{
				/*  Lock the object.  */
				LMI4_ITKCALL(iRetCode, AOM_refresh(tObject, 1));
   
				/* Set the attribute string value. */                 
				LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_string(tObject, strPropName, strPropVal));

				/*  Save the object.  */
				LMI4_ITKCALL(iRetCode, AOM_save_with_extensions(tObject));

				/*  Unlock the object.  */
				LMI4_ITKCALL(iRetCode, AOM_refresh(tObject, 0));
			}		      
		}
		else
		{
			string strObjAttrVal;

			LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tObject, LMI4_ATTR_OBJECT_STRING_ATTRIBUTE, strObjAttrVal));

			TC_write_syslog("Warning: No write privileges on object '%s'.\n", strObjAttrVal);
		 }
	}

	return iRetCode;
}


/**
 * This function sets the attribute value for input TC object. This function can set value for following attribute types:
 *		- String
 *      - Integer
 *      - Float
 *      - Double
 *      - Logical
 *      - Date
 * 
 * @param[in] tObject      Tag of Object whose attribute value needs to be set
 * @param[in] strAttrName  Name of Attribute of input object
 * @param[in] strAttrValue String Value of Attribute
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_obj_aom_set_attribute_value
(
    tag_t  tObject,
    string strAttrName,
    string strAttrValue
)
{
    int               iRetCode        = ITK_ok;
    string            strValType;
    PROP_value_type_t ValueTypeOfProp = PROP_untyped;

	if(strAttrValue.size() > 0)
	{
		/* Get the type of the property value */
		LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_type(tObject, strAttrName, ValueTypeOfProp, strValType));

		/* Set the property based on its value type */
		if (iRetCode == ITK_ok)
		{
			switch(ValueTypeOfProp)
			{
				case PROP_string :
				case PROP_note   :
					{
						LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_string(tObject, strAttrName, strAttrValue));
						break;
					}
				case PROP_logical :
					{
						logical bYesNo = tc_strcmp(strAttrValue.c_str(), LMI4_CAPS_YES) == 0 ? true : false;

						LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_logical(tObject, strAttrName, bYesNo));
						break;
					}
				case PROP_int   :
				case PROP_short :
					{
						int iValue = atoi(strAttrValue.c_str());

						LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_int(tObject, strAttrName, iValue));
						break;
					}
				case PROP_float  :
				case PROP_double :
					{
						double dValue = atof(strAttrValue.c_str());

						LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_double(tObject, strAttrName, dValue));
						break;
					}
				case PROP_date :
					{
						logical  isValid = false;
						date_t   dValue  = NULLDATE;
                    
						LMI4_ITKCALL(iRetCode, LMI4_date_string_to_date_t(strAttrValue, isValid, dValue));

						if (isValid && DATE_IS_NULL(dValue) != 1 && iRetCode ==ITK_ok)
						{
							LMI4_ITKCALL(iRetCode, LMI4_aom_set_value_date(tObject, strAttrName, dValue));
						}
						break;
					}
			}
		}
	}

    return iRetCode;
}





















