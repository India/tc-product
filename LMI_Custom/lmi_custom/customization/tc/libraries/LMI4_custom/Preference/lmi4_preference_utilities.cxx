/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_preference_utilities.cxx

    Description: This File contains functions for Retrieving and Manipulating Preference. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>
#include <lmi4_constants.hxx>
#include <lmi4_errors.hxx>

/**
 * Function reads Teamcenter single valued string preference. It takes prefrence name as input argument and returns value of preference "refstrPrefValue".
 *
 * @param[in]  strPrefName     Preference Name
 * @param[out] refstrPrefValue Value of preference
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI4_pref_get_string_value
(
    string  strPrefName, 
    string& refstrPrefValue
)
{
    int iRetCode = ITK_ok;
    int iPrefCnt = 0;

    TC_preference_search_scope_t prefOldScope;
    
    LMI4_ITKCALL(iRetCode, PREF_initialize());

    LMI4_ITKCALL(iRetCode, PREF_ask_search_scope(&prefOldScope));

    LMI4_ITKCALL(iRetCode, PREF_set_search_scope(TC_preference_site));

	LMI4_ITKCALL(iRetCode, PREF_ask_value_count((strPrefName.c_str()), &iPrefCnt));

    if(iPrefCnt != 0)
    {
		LMI4_ITKCALL(iRetCode, LMI4_pref_ask_char_value((strPrefName.c_str()), 0, refstrPrefValue));
    }

    LMI4_ITKCALL(iRetCode, PREF_set_search_scope (prefOldScope));

    return iRetCode;
}

/**
 * Function reads Teamcenter multivalued string preference. It takes preference name "strPrefName" as input argument and returns list of preference values "refstrPrefVals" corresponding to
 * input preference name. 
 *
 * @param[in]  strPrefName    Preference Name
 * @param[out] refstrPrefVals List of preference values
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI4_pref_get_string_values
(
    string          strPrefName, 
    vector<string>& refstrPrefVals
)
{
    int iRetCode = ITK_ok;
    int iPrefCnt = 0;

    TC_preference_search_scope_t prefOldScope;
    
    LMI4_ITKCALL(iRetCode, PREF_initialize());

    LMI4_ITKCALL(iRetCode, PREF_ask_search_scope(&prefOldScope));

    LMI4_ITKCALL(iRetCode, PREF_set_search_scope(TC_preference_site ));

	LMI4_ITKCALL(iRetCode, PREF_ask_value_count((strPrefName.c_str()), &iPrefCnt ));

    if(iPrefCnt != 0)
    {
		LMI4_ITKCALL(iRetCode, LMI4_pref_ask_char_values((strPrefName.c_str()), refstrPrefVals));	
    }

    LMI4_ITKCALL(iRetCode, PREF_set_search_scope(prefOldScope));

    return iRetCode;
}































