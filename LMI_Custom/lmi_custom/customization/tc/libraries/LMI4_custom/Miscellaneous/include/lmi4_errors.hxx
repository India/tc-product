/*======================================================================================================================================================================
                                                               Copyright 2018  LMtec India
                                                              Unpublished - All rights reserved

========================================================================================================================================================================

File Description:

    Filename: lmi4_errors.hxx

    Description:  Header File for containing macros for custom errors

========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/

#ifndef LMI4_ERRORS_HXX
#define LMI4_ERRORS_HXX

#include <common/emh_const.h>

#define ERROR_CODE_NOT_STORED_ON_STACK                                        (EMH_USER_error_base + 1)
#define ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND                                 (EMH_USER_error_base + 3)
#define UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES                               (EMH_USER_error_base + 3)
#define INVALID_INPUT_TO_FUNCTION                                             (EMH_USER_error_base + 3)

#endif





