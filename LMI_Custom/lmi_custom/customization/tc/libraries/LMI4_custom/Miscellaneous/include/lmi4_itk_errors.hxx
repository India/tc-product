/*======================================================================================================================================================================
                                                          Copyright 2018  LMtec India
                                                         Unpublished - All rights reserved

========================================================================================================================================================================

File Description:

    Filename: lmi4_itk_errors.hxx

    Description:  Header File for containing macros for custom errors

========================================================================================================================================================================

Date                   Name                      Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Mar-2018           Megha Singh               Initial Release

========================================================================================================================================================================*/

#ifndef LMI4_ITK_ERRORS_HXX
#define LMI4_ITK_ERRORS_HXX

#include <lmi4_library.hxx>

/* We want to use our macros. If some others are defined under same name, undefine them */

#ifdef LMI4_ITKCALL
#undef LMI4_ITKCALL
#endif

/* This macro works for functions which return integer. The function return value is an input argument and return 
   value in the same time. 'f' is either ITK function, or a function which uses only ITK calls and therefore has to
   return ITK integer return code
*/
#define LMI4_ITKCALL(iErrorCode,f){\
           if(iErrorCode == ITK_ok){\
               (iErrorCode) = (f);\
               LMI4_err_start_debug(#f, iErrorCode,__FILE__, __LINE__);\
               if(iErrorCode != ITK_ok) {\
               LMI4_err_write_to_logfile(#f, iErrorCode,__FILE__, __LINE__);\
                }\
             }\
          }

#endif


