/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_itk_mem_free.hxx

    Description:  This file contains macros for memory operations.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh          Initial Release
   
========================================================================================================================================================================*/
#ifndef LMI4_ITK_MEM_FREE_HXX
#define LMI4_ITK_MEM_FREE_HXX

#include <lmi4_library.hxx>

/* We want to use our macros. If some others are defined under same name, undefine them */

#ifdef LMI4_MEM_TCFREE
#undef LMI4_MEM_TCFREE
#endif

/* Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc */

#define LMI4_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }

#ifdef LMI4_TC_STRCPY
#undef LMI4_TC_STRCPY
#endif

/* Macro to copy string from source string to destination string*/

#define LMI4_TC_STRCPY(pszDestStr, pszSourceStr){\
                    int iSourceStrLen = (int) (tc_strlen(pszSourceStr) + 1);\
                    pszDestStr = (char*)MEM_alloc((int)(iSourceStrLen * sizeof(char)));\
                    tc_strcpy(pszDestStr, pszSourceStr);\
            }

#ifdef LMI4_OBJECT_DELETE
#undef LMI4_OBJECT_DELETE
#endif

/* Macro to free object pointers using delete. */
#define LMI4_OBJECT_DELETE(pObj) {\
           if (pObj != NULL) {\
               delete pObj;\
              }\
          }

/* Macro to update a Tag Array */
#ifdef LMI4_TC_UPDATE_TAG_ARRAY
#undef LMI4_TC_UPDATE_TAG_ARRAY
#endif

/* Macro to update a tag string  */
#define LMI4_TC_UPDATE_TAG_ARRAY(ptArray, tVal, iCount){\
    if(tVal != NULLTAG){\
         iCount++;\
         if(iCount == 1){\
               ptArray = (tag_t*) MEM_alloc(iCount * (sizeof(tag_t)));\
            }\
            else {\
               ptArray = (tag_t*) MEM_realloc(ptArray, iCount * (sizeof(tag_t)));\
             }\
            ptArray[iCount-1] = tVal;\
        }\
    }


/* Macro to copy tag array to new tag array and update new tag array with the given value */
#ifdef LMI4_COPY_TAG_ARRAY_AND_UPDATE
#undef LMI4_COPY_TAG_ARRAY_AND_UPDATE
#endif

/* Macro to update a tag string  */
#define LMI4_COPY_TAG_ARRAY_AND_UPDATE(ptNewArray, ptExistArray, tNewVal, iCount, iLen){\
	for(int iDx = 0; iDx < iCount; iDx++){\
				LMI4_TC_UPDATE_TAG_ARRAY(ptNewArray, ptExistArray[iDx], iLen);{\
				}\
				LMI4_TC_UPDATE_TAG_ARRAY(ptNewArray, tNewVal, iLen);\
		}\
	}
 
   


#endif








