/*======================================================================================================================================================================
                                                               Copyright 2018  LMtec India
                                                             Unpublished - All rights reserved

========================================================================================================================================================================

File Description:

    Filename: lmi4_constants.hxx

    Description:  Header File for defining constants 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Mar-18    Megha Singh       Initial Release

========================================================================================================================================================================*/

#ifndef LMI4_CONSTANTS_HXX
#define LMI4_CONSTANTS_HXX

/* Handler */
#define LMI4_WF_AH_TEST_HANDLER                                "LMI4_Sample_Handler"
#define LMI4_WF_AH_TEST_DATASET_HANDLER                        "LMI4_Sample_Attach_Dataset"

/* Class Name */
#define LMI4_CLASS_ITEMREVISION                                "ItemRevision"
#define LM4_CLASS_FIRMWARE                                     "LM4_Firmware"
#define LM4_CLASS_CUSTOMERREVISION                             "LM4_CustomerRevision"
#define LM4_CLASS_MANUFACTURERREVISION                         "LM4_ManufacturerRevisoin"
#define LMI4_CLASS_FND0NAMEVALUESTRING                         "Fnd0NameValueString"
#define LMI4_CLASS_FND0NAMEVALUEINT                            "Fnd0NameValueInt"
#define LMI4_CLASS_FND0NAMEVALUEDOUBLE                         "Fnd0NameValueDouble"
#define LMI4_CLASS_FND0NAMEVALUEDATE                           "Fnd0NameValueDate"
#define LMI4_CLASS_FND0NAMEVALUELOGICAL                        "Fnd0NameValueLogical"

/* Attributes  */
#define LMI4_ATTR_ACTIVE_SEQ                                   "active_seq"
#define LMI4_ATTR_OBJECT_CREATION_DATE                         "creation_date"
#define LMI4_ATTR_ITEM_ID                                      "item_id"
#define LMI4_ATTR_ITEM_REVISION_ID                             "item_revision_id"
#define LMI4_ATTR_OBJECT_NAME_ATTRIBUTE                        "object_name"
#define LMI4_ATTR_OBJECT_STRING_ATTRIBUTE                      "object_string"
#define LMI4_ATTR_FND0NAME                                     "fnd0Name"
#define LMI4_ATTR_FND0VALUE                                    "fnd0Value"

/* Workflow Argument Value  */
#define LMI4_WF_ARG_VAL_TARGET                                 "TARGET"
#define LMI4_WF_ARG_VAL_REFERENCE                              "reference"

/* Separators */
#define LMI4_STRING_COMMA                                      ","
#define LMI4_STRING_ASTERISK                                   "*"
#define LMI4_STRING_SEMICOLON                                  ";"
#define LMI4_STRING_FORWARD_SLASH                              "/"
#define LMI4_STRING_BLANK                                      ""

/* Workflow Argument Name  */
#define LMI4_WF_ARG_PROPERTY                                   "property"
#define LMI4_WF_ARG_TYPE_INCLUDE_TYPE                          "include_type"
#define LMI4_WF_ARG_RELATION                                   "relation"

/* Logical String Response */
#define LMI4_CAPS_YES                                          "Y"
#define LMI4_SMALL_YES                                         "y"
#define LMI4_SMALL_NO                                          "n"
#define LMI4_CAPS_NO                                           "N"

/* Name Of Privileges  */
#define LMI4_WRITE_ACCESS                                      "WRITE"

/*  Others   */
#define LMI4_STRING_ATTR_LEN                                   256

/*  Tester Constants  */
#define LMI4_DATASET_TYPE                                     "Text"
#define LMI4_DATASET_NAME_ATTACHMNTS                          "DatasetAttchments"
#define LMI4_DATASET_FORMAT_TEXT                              "TEXT"
#define LMI4_DATASET_FILE_TYPE                                ".txt"
#define LMI4_RELATION_EXCLUDED_STATUS                         "LM4_InProgress"
#define LMI4_RELATION                                         "LM4_Derived_Info_Rel"
#define LMI4_PURCHASE_REL                                     "LM4_Purchase_Rel"
#define LMI4_IMAN_SPECIFICATION                               "IMAN_specification"
#define LMI4_DATE_FORMAT                                      "yyyy-mm-dd"



#endif


