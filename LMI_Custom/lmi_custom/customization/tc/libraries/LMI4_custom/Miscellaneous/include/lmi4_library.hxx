/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved

========================================================================================================================================================================

File Description:

    Filename: lmi4_library.hxx

    Description:  Header File containing declarations of function spread across all LMI4_<UTILITY NAME>_utilities.cxx 
                  For e.g. lmi4_dataset_utilities.cxx/lmi4_object_utilities.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Mar-18     Megha Singh    Initial Release

========================================================================================================================================================================*/
#ifndef LMI4_LIBRARY_HXX
#define LMI4_LIBRARY_HXX

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <conio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <tccore/releasestatus.h>
#include <epm/epm_task_template_itk.h>
#include <tccore/tctype.h>
#include <tc/folder.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <epm/epm.h>
#include <epm/signoff.h>
#include <sa/user.h>
#include <sa/role.h>
#include <bom/bom.h>
#include <pom/pom/pom.h>
#include <ps/ps.h>
#include <cfm/cfm.h>
#include <ctype.h>
#include <pom/enq/enq.h>
#include <me/me.h>
#include <form/form.h>
#include <tccore/project.h>
#include <tc/folder.h>
#include <tccore/tctype.h>
#include <fclasses/tc_date.h>
#include <res/res_itk.h>
#include <sa/am.h>
#include <map>
#include <string>
#include <vector>
#include <algorithm>
#include <lmi4_itk_errors.hxx>
#include <lmi4_itk_mem_free.hxx>
#include <lmi4_constants.hxx>
#include <lmi4_string_macros.hxx>
using namespace std; 


/*        Utility Functions For Error Logging        */
int LMI4_err_write_to_logfile
(
    char* pszErrorFunction,
    int   iErrorNumber,
    char* pszFileName,
    int   iLineNumber
);

int LMI4_err_start_debug
(
    char* pszCallingFunction,
    int   iErrorNumber,
    char* pszFileName,
    int   iLineNumber
);

/*        Utility Functions For Strings        */
int LMI4_str_parse_string
(
    string          strList,
    string          strSeparator,
    vector<string>& refvValueList    
);

/*        Utility Functions For Objects        */
int LMI4_obj_ask_type
(
    tag_t   tObject,
    string& refstrObjType
);

int LMI4_obj_is_type_of
(
    tag_t    tObject,
    string   strTypeName,
    logical& refbBelongsToInputClass
);

int LMI4_obj_ask_status_tag
(
    tag_t  tObject,
    string strStatusName,
    tag_t& reftStatus
);

int LMI4_obj_ask_type_display_name
(
    string  strType,
    string& refstrDisplayName
);

int LMI4_obj_save_and_unlock
(
    tag_t tObject
);

int LMI4_obj_set_str_attribute_val
(
    tag_t  tObject,
    string strPropName,
    string strPropVal
);

int LMI4_obj_set_tag_value
(
    tag_t  tObject,
    string strPropName,
    tag_t  tAttrVal
);

int LMI4_obj_is_type_of_2
(
    string   strSrcType,
    string   strDestType,
    logical& refbIsNotBOType,
    logical& refbBelongsToInputClass
);

int LMI4_check_privilege
(
    tag_t    tObject,
    logical& refbWriteAccessDenied,
    logical& refbObjCheckedOut,
    logical& refbModifiable
);

int LMI4_obj_get_obj_of_input_type
(
    vector<tag_t> vtObject,
    string        strObjType,
    tag_t&        reftCorrectObj
);

int LMI4_obj_aom_get_attribute_value
(
    tag_t   tObject,
    string  strAttributeName,
    string& refstrAttrValue
);

int LMI4_obj_is_property_type_array
(
    tag_t    tObject,
    string   strAttrName,
    logical& refbIsPropTypeArray
);

int LMI4_obj_check_and_set_str_attribute_val
(
    tag_t  tObject,
    string strPropName,
    string strPropVal
);

int LMI4_obj_aom_set_attribute_value
(
    tag_t  tObject,
    string strAttrName,
    string strAttrValue
);

/*        Utility Functions For WorkFlows     */
int LMI4_arg_get_attached_objects_of_type
(
    tag_t          tTask,
    string         strAttachedObjType,
    vector<tag_t>& refvObject
);

int LMI4_arg_get_attached_obj_of_input_type_list
(
    tag_t          tTask,
    int            iAttachmentType,
    vector<string> vstrObjTypeList,
    vector<tag_t>& refvObject
);

int LMI4_arg_get_arguments
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    string              strArgument,
    string              strSeparator,
    logical             bOptional,
    vector<string>&     refvArgValue,
    string&             refstrOriginalArgVal
);

int LMI4_arg_get_wf_argument_value
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    string              strArgument,
    logical             bOptional,
    string&             refstrArgValue
);

int LMI4_arg_get_wf_argument_info_and_value
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    string              strArgument,
    logical             bOptional,
    logical&            refbFoundArg,
    string&             refstrArgValue
);

int LMI4_arg_get_wf_attachment_type
(
    string strAttachmentType,
    int&   refiAttachmentType 
);

/*  utility Functions for Dataset */
int LMI4_create_dataset
(
    string strDatasetName,
    string strDSType,
    string strDSTool,
    string strRefFormat,
    tag_t& reftNewDataset
);

int LMI4_get_dataset_reference_and_format
(
    string  strDSType,
    string  strFileType,
    string& refstrDSReference,
    string& refstrRefFormat
);

int LMI4_get_dataset_default_tool
(
    string strDSType,
    tag_t& reftDefTool
);

/* Utility Functions for Item and Item Rev */
int LMI4_itemrev_get_previous_rev
(
    tag_t    tItemRev,
    tag_t&   reftPreviousIR,
    logical& refbIsOnlyRev,
    logical& refbIsFirstRev
);

int LMI4_itemrev_get_all_previous_rev
(
    tag_t          tItemRev,
    vector<tag_t>& refvPrevIR,
    logical&       refbIsOnlyRev,
    logical&       refbIsFirstRev
);

int LMI4_itemrev_construct_name
(
    tag_t   tItemRev,
    string& refstrName
);

/* Utility Functions for Preference  */
int LMI4_pref_get_string_value
(
    string  pszPrefName, 
    string& refstrPrefValue
);

int LMI4_pref_ask_char_values
(
	string          strPrefName,
	vector<string>& refvPrefValues
);

/*  Utility functions for Relation   */
int LMI4_grm_get_related_obj
(
    tag_t          tObject,
    string         strRelationType,
    string         strObjType,
    string         strExcludeStatus,
    logical        bGetPrimary,
    vector<tag_t>& refvValidRelatedObj
);

int LMI4_grm_create_relation
(
    tag_t  tPrimaryObj,
    tag_t  tSecondaryObj,
    string strRelationType,
    tag_t& reftRelation
);

/*  Utility Functions for BOM  */
int LMI4_bom_apply_config_rule
(
    tag_t  tBOMWindow,
    string strRule
);

int LMI4_bom_get_tag_to_view_type
(
    tag_t  tItemRev,
    string strInputViewType,
    tag_t& reftBOMViewType
);

int LMI4_bom_get_bomview_revision
(
   tag_t  tItemRev,
   string strViewType,
   tag_t& reftBOMViewRev
);

int LMI4_bom_list_structure
(
    tag_t          tBOMLine,
    int            iLevel,
    int            iMaxLevel,
    logical        bUnique,
    vector<tag_t>& refvtItem,
    vector<tag_t>& refvtItemRevs,
    vector<tag_t>& refvtBOMLines
);

int LMI4_bom_get_parent_bl_bvr
(
    tag_t  tBOMLine,
    tag_t& reftParentBL,
    tag_t& reftParentBVR
);

int LMI4_ps_get_all_note_type
(   
    vector<tag_t>&  refvNoteType,
    vector<string>& refstrNoteTypes
);

int LMI4_get_ps_occ_note_value
(
    tag_t   tBOMLine,
    tag_t   tNoteType,
    string  strNoteType,
    string& refstrNoteVal
);

int LMI4_set_ps_occ_note_value
(
    tag_t  tBOMLine,
    tag_t  tNoteType,
    string strNoteType,
    string strNoteVal
);

int LMI4_save_ps_occ_note
(
    tag_t  tParentBVR,
    tag_t  tBLOcc,
    tag_t  tNoteType,
    string strNoteVal
);

int LMI4_bom_list_structure_cpp
(
    tag_t   tBOMLine,
    int     iLevel,
    int     iMaxLevel,
    logical bUnique,
    map<tag_t, vector<tag_t>>& refmBOMInfo
);

/*  Utility functions for BOMLine  */
int LMI4_bomline_get_top_line
(
    tag_t  tItemRev,
    string strRevisionRule,
    string strViewType,
    tag_t& reftBOMWindow,
    tag_t& reftTopLine
);

int LMI4_bomline_get_parent_bomline_item_and_revision
(
    tag_t  tBOMLine,
    tag_t& reftParentBOMLine,
    tag_t& reftParentItem,
    tag_t& reftParentItemRev
);

int LMI4_bomline_get_item_and_revision_from_bomline
(
    tag_t  tBOMLine,
    tag_t& reftItem,
    tag_t& reftAttrItemRev
);

int LMI4_bomline_get_attribute_val
(
    tag_t   tBOMLine,
    string  strAttrName,
    string& refstrAttributeVal
);

int LMI4_bomline_set_attribute_val
(
	tag_t  tBOMLine,
	string strAttrName,
	string strAttributeVal
);

int LMI4_bomline_check_ir_active_seq
(
    tag_t    tBOMLine,
    tag_t&   reftItem,
    tag_t&   reftItemRev,
    logical& refbIsIRSeqCorrect
);

int LMI4_bomline_get_int_qty
(
    tag_t   tBOMLine,
    logical bDefQtyPref,
    int&    refiQuantity
);

int LMI4_bomline_get_window_top_line
( 
    tag_t    tBOMLine,
    tag_t&   reftWindowTopBL,
    logical& refbInputBLIsTopLine
);

/*   Utility Wrapper Functions For EPM (Macros)    */
int LMI4_epm_ask_attachments
(
	tag_t          tTask,
    int            iAttachmentType,
	vector<tag_t>& refvObjectList
);

/*  Utility Wrapper Functions For TCTYPE (Macros)  */
int LMI4_tctype_ask_name2
(
	tag_t   tObjectType,
	string& refstrTypeName
);

int LMI4_tctype_is_type_of
(
	tag_t    tInputObjType,
	tag_t    tType,
	logical& refbBelongsToClass
);

int LMI4_tctype_ask_display_name
(
	tag_t   tObjType,
	string& refstrTypeDisplayName
);

/*   Utility Wrapper Functions For ITK  */
int LMI4_itk_ask_argument_named_value
(
	string  strArgument,
	string& refstrSwitch,
	string& refstrValue
);

/*   Utility wrapper function for AE(dataset)  */
int LMI4_ae_ask_datasettype_file_refs
(
	tag_t           tDatasetType,
	string          strRefName,
	vector<string>& refvTemp,
	vector<string>& refvRefFormats
);

int LMI4_ae_ask_datasettype_refs
(
	tag_t           tDatasetType,
	vector<string>& refvRefList	
);

/*  Utility wrapper function for string  */
int LMI4_has_ending
(
	string   strSrc,
	string   strEnd,
	logical& refbResult
);

int LMI4_str_parse_string
(
    string          strInput,
    string          strSeparator,
    vector<string>& refvValueList
);

int LMI4_str_copy_substring
(
    string  strInput,
    int     iStrStartPosition,
    int     iStrEndPosition,
    string& refstrCopyed
);

int LMI4_contains_string_value
(
    string         strInput, 
    vector<string> vstrValues,
    logical&       refbFound
);

int LMI4_replace_sub_string
(
    string  strInput, 
    string  strSub,
    string  strReplacement,
    string& refstrNew
);

int LMI4_string_find_sub_str
(
    string   strInput,
    string   strSub,
    logical& refbSubFound
);

int LMI4_get_str_after_delimiter
(
    string     strInput,
    const char szDelim, 
    string&    refstrTrimmed
);

int LMI4_string_start_with
(
    string   strSrc,
    string   strStart,
    logical& refbIsMatch,
    logical& refbInvalidArg
);

int LMI4_string_end_with
(
    string   strSrc,
    string   strEnd,
    logical& refbIsMatch,
    logical& refbInvalidArg
);

int LMI4_get_count_of_sub_str_in_src_str
(
	string   strSrc,
	string   strSub,
    int&     refiSubCnt,
    logical& refbBadSrcOrSub
);

int LMI4_get_prefix_infix_postfix
(
    string  strSrc,
    string  strSubAtStart,
    string  strSubAtEnd,
    string& refstrPrefix,
    string& refstrInfix,
    string& refstrPostfix
);

int LMI4_get_prefix_and_postfix_from_end
(
    string  strSrc,
    string  strSub, 
    string& refstrPrefix,
    string& refstrPostfix
);

int LMI4_string_start_with_2
(
    string   strSrc,
    string   strStart,
    logical& refbIsMatch,
    string&  refstrPost,
    logical& refbInvalidArg
);

int LMI4_string_strip_blanks
(
    string   strSrc,
    string&  refstrWithoutSpaces,
    logical& refbIsEmpty
);

int LMI4_truncate_start_and_end_string
(
    string   strSrc,
    string   strStart,
    string   strEnd,
    string&  refstrTruncated,
    logical& refbFound
);

int LMI4_string_is_Integer
(
    string    strVal,
	int&      refliLongIntValue, 
    logical&  refbIsInt
);

int LMI4_string_is_Double
(
    string   strVal,
	double&  refdDoubleValue,
    logical& refbIsDouble
);

int LMI4_convert_string_is_Integer
(
    string   strVal,
    int&     refiIntVal,
    logical& refbIsInt
);

int LMI4_convert_string_is_Double
(
    string   strVal,
    double&  refdDoubleVal,
    logical& refbIsDouble
);

int LMI4_stl_copy_tag_array_to_vector
(
    int            iObjCnt,
    tag_t*         ptObject,
    vector<tag_t>& refvtObject
);

/* Utility wrapper function for Pref  */
int LMI4_pref_ask_char_value
(
	string  strPrefName,
	int     iIndex,
	string& refstrPrefValue
);

int LMI4_pref_ask_char_values
(
	string          strPrefName,
	vector<string>& refvPrefValues
);

/*  Utility wrapper function for Item  */
int LMI4_item_list_all_revs
(
	tag_t          tItem,
	vector<tag_t>& refvItemRevList
);

int LMI4_item_list_bom_views
(
	tag_t          tItem,
	vector<tag_t>& refvBOMViewsList
);

/* Utility wrapper function for AOM  */
int LMI4_aom_ask_value_string
(
	tag_t   tItemRev,
	string  strPropName,
	string& refstrItemID
);

int LMI4_aom_tag_to_string
(
	tag_t   tObject,
	string& refstrUID
);

int LMI4_aom_ask_value_type
(
	tag_t              tObject, 
	string             strPropName, 
	PROP_value_type_t& refenumValueTypeOfProp, 
	string&            refstrValType
);

int LMI4_aom_ask_prop_names
(
	tag_t           tObject,
	int&            refiPropCount,
	vector<string>& refstrPropNames
);

int LMI4_aom_append_table_rows
(
	tag_t          tObject,
	string         strTabName,
	int            iRowToAppend,
	vector<tag_t>& refvtTabRows
);


/*    Utility wrapper function for GRM   */
int LMI4_grm_list_primary_objects
(
	tag_t                   tObject, 
	tag_t                   tRelationType, 
	vector<GRM_relation_t>& refvRelatedPrimObj
);

int LMI4_grm_list_secondary_objects
(
	tag_t                   tObject,
	tag_t                   tRelationType,
	vector<GRM_relation_t>& refvRelatedSeconObj
);

/*  Utility wrapper function for PS   */
int LMI4_ps_ask_view_type_name
(
	tag_t   tViewType, 
	string& refstrViewName
);

int LMI4_item_rev_list_bom_view_revs
(
	tag_t          tItemRev,
	vector<tag_t>& refvBVRevList
);

int LMI4_ps_ask_note_type_name
(
	tag_t   tNoteTyp,
	string& strNoteName
);

/*  Utility Function for STL  */
int LMI4_vector_store_string
(
	string          strValue,
	logical         bUnique,
	vector<string>& refvFinal
);

int LMI4_vector_store_string_array
(
	int             iCount,
	char**          ppszValue,
	logical         bUnique,	
	vector<string>& refvFinal
);

int LMI4_vector_store_tag
(
	tag_t          tValue,
	logical        bUnique,
	vector<tag_t>& refvFinal
);

int LMI4_vector_store_tag_array
(
	int            iCount,
	tag_t*         ptValue,
	logical        bUnique,
	vector<tag_t>& refvFinal
);

/*  Utility wrapper function for Relstat  */
int LMI4_relstat_ask_release_status_type
(
	tag_t   tStatus,
	string& refstrRelStatType
);

/*  Utility wrapper function for BOM  */
int LMI4_bom_line_ask_child_lines
(
	tag_t          tBOMLine,
	vector<tag_t>& refvChildrenBL
);

int LMI4_bom_line_ask_attribute_string
(
	tag_t   tBOMLine,
	int     iAttributeId, 
	string& refstrAttrValue
);

/*  Utility wrapper functions for Date  */
int LMI4_date_date_to_string
(
	date_t  dDateStruct, 
	string  strDateFormat,
	string& refstrDateText
);

int LMI4_date_default_date_format
(
	string& refstrDateFormat
);

int LMI4_date_string_to_date_t
(
	string   strAttrValue,
	logical& refIsValid,
	date_t&  refdValue
);

/*  Utility wrapper functions for POM  */
int LMI4_pom_get_user
(
	string& refstrUserName, 
	tag_t&  refUser
);

typedef struct
{
    int    iPropVal;
    tag_t  tPropVal;
    short  sPropVal;
    double dPropVal;
    bool   bPropVal;
    string strPropVal;
    date_t dtPropVal;
} stProp;

/*typedef struct AttributeNameValueList
{
    char*  propertyName;               
    char** newValues;                 
    int    valueCount;
	stProp PropVal;
}AttributeNameValueList_lst;*/


int LM4_set_name_value_property
(
    tag_t               tObject,
	int                 iTablesCnt,
	string              strPropName,
    map<string, string> mSetPropVal
);

int LMI4_tctype_find_type
(
	string strTypeName,
	string strClassNam,
	tag_t& reftType
);

int LMI4_tctype_is_type_of
(
	tag_t    tInputObjType,
	tag_t    tType,
	logical& refbBelongsToInpCls
);

int LMI4_wsom_ask_release_status_list
(
	tag_t          tObject, 
	int&           refiStatCount,
	vector<tag_t>& refvtStatList
);

int LMI4_aom_set_value_string
(
	tag_t  tObject,
	string strPropName,
	string strPropVal
);

int LMI4_aom_set_value_tag
(
	tag_t  tObject,
	string strPropName,
	tag_t  tAttrVal
);

int LMI4_am_check_privilege
(
	tag_t    tObject,
	string   strPrivilege,
	logical& refbAccess
);

int LMI4_aom_ask_max_num_elements
(
	tag_t  tObject,
	string strAttrName,
	int&   refiMaxNumElements
);

int LMI4_aom_ask_table_rows
(
	tag_t          tObject,
	string         strTabPropNam,
	int&           refiTabCnt, 
	vector<tag_t>& refvtTabRow
);

int LMI4_aom_ask_value_int
(
    tag_t  tObject,
	string strPropNam,
	int&   refiValue
);

int LMI4_aom_ask_value_double
(
	tag_t   tObject, 
	string  strPropNam,
	double& refdPropVal
);

int LMI4_aom_ask_value_date
(
	tag_t   tObject,
	string  strPropNam, 
	date_s& refdtPropVal
);

int LMI4_aom_ask_value_tag
(
	tag_t  tObject,
	string strPropNam,
	tag_t& reftPropVal
);

int LMI4_aom_ask_value_logical
(
	tag_t    tObject,
	string   strPropNam,
	logical& refbPropVal
);

int LMI4_itk_date_to_string
(
	date_s  dtPropVal,
	string& refstrStringDate
);

int LMI4_pom_tag_to_string
(
	tag_t   tPropVal, 
	string& refstrConvertedVal
);

int LMI4_aom_insert_table_rows
( 
	tag_t          tObject,
	string         strTabName,
	int            iRowIndex,
	int            iRowCount, 
	vector<tag_t>& refvtTabRows
);

int LMI4_aom_set_value_int
(
	tag_t  tObject,
	string strPropNam,
	int    iPropVal
);

int LMI4_aom_set_value_double
(
	tag_t  tObject,
	string strPropNam,
	double dPropVal
);

int LMI4_itk_string_to_date
(
	string strDate,
	date_s& refstdtPropVal
);

int LMI4_aom_set_value_date
(
	tag_t  tObject,
	string strPropNam, 
	date_s dtPropVal
);

int LMI4_pom_string_to_tag
(
	string strVal,
	tag_t& reftConvertedVal
);

int LMI4_aom_set_value_logical
(
	tag_t   tObject,
	string  strPropNam,
	logical bPropVal
);

int LMI4_ask_prop_typ
(
	tag_t               tObject,
	string              strPropNam,
	PROP_value_type_t   ValueTypeOfProp,
	map<string, string> mGetPropVal
);

int LMI4_int_to_string
(
	int     iValue,
	string& refstrConverted
);

int LMI4_double_to_string
(
	double  dValue,
	string& refstrConverted
);

int LMI4_set_prop_typ
(
	tag_t  tObject,
	string strPropNam,
	string strPropVal
);

int LMI4_ask_prop_typ
(
	tag_t                tObject,
	string               strPropNam,
	map<string, string>& refmGetPropVal
);

int LMI4_logical_to_string
(
	logical bValue,
	string& refstrConverted
);

int LMI4_ask_fnd0namevalue_typ
(
	tag_t                tObject,
	map<string, string>& refmstrPropInfo
);

int LMI4_string_to_logical
(
	string   strValue,
	logical& refbConverted
);

int LMI4_aom_ask_table_row
(
	tag_t  tObject,
	string strTabPropNam,
	int    iRowIndex, 
	tag_t& reftTabRow
);

#endif







