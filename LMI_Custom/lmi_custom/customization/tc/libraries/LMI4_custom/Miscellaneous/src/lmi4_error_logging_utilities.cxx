/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved

========================================================================================================================================================================

File Description:

    Filename: lmi4_error_logging_utilities.cxx

    Description:  This File contains custom functions for logging error to facilitate error reporting and problem analysis. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>
#include <lmi4_constants.hxx>
#include <lmi4_errors.hxx>

static int     iStartDebug   = 0;
static logical bExecutedOnce = false;

/**
 * The function does error reporting to syslog. Translate error code to error string and add information to log file.
 *
 * @param[in] pszErrorFunction Function causing error.
 * @param[in] iErrorNumber     Error code.
 * @param[in] pszFileName      Name of file containing function
 * @param[in] iLineNumber      Line number on which error has occurred.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMI4_err_write_to_logfile
(
    char* pszErrorFunction,
    int   iErrorNumber,
    char* pszFileName,
    int   iLineNumber
)
{
    int  iRetCode  = ITK_ok;

    if (iErrorNumber != ITK_ok)  
    {    
        int     iEMH_severity = 0;
        char*   pszErrorMsg   = NULL;   
        logical bErrorFound   = false;

        iRetCode = EMH_ask_error_text(iErrorNumber, &pszErrorMsg);

        if(iRetCode != ITK_ok || pszErrorMsg == NULL)
        {
            char* pszErrorNumber = MEM_sprintf("%d",iErrorNumber);

            iRetCode = ERROR_CODE_NOT_STORED_ON_STACK;

            EMH_store_error_s1(EMH_severity_warning, iRetCode, pszErrorNumber);    
        }
        else
        {
            /* EMH_severity_error   */
            TC_write_syslog("ERROR: %d  ERROR MSG: %s  ERROR Severity: %d.\n", iErrorNumber, pszErrorMsg, iEMH_severity);  

            TC_write_syslog("ERROR ON FUNCTION: %s\n  FILE: %s LINE: %d\n",  pszErrorFunction, pszFileName, iLineNumber);
        }

		LMI4_MEM_TCFREE(pszErrorMsg);
    }   

	return iRetCode;
}

/**
 * This function prints the function name and information related to execution when ever Environment variable 'DEBUG' is set to 'true'.
 *
 * @param[in] pszCallingFunction Name of function invoked.
 * @param[in] iErrorNumber       Error code.
 * @param[in] pszFileName        Name of file along with full path of file containing function
 * @param[in] iLineNumber        Line number on which error has occurred.
 *
 * @retval ITK_ok 
 */
int LMI4_err_start_debug
(
    char* pszCallingFunction,
    int   iErrorNumber,
    char* pszFileName,
    int   iLineNumber
)
{
    int   iRetCode   = ITK_ok;
	char* pszEnvInfo = NULL;

    if(iStartDebug == 0 && bExecutedOnce == false)
    {
        char* pszEnvInfo = (char*)(TC_getenv("LMI4_DEBUG"));

        bExecutedOnce = true;

        if(tc_strlen(pszEnvInfo) > 0)
        {
            TC_printf("DEBUG Started: Env Info = %s\n", pszEnvInfo);
            iStartDebug = 1;
        }    
    }

    if(iStartDebug == 1)
    {
        TC_write_syslog(" %-20s:%d | %5d = %s \n", pszFileName, iLineNumber, iErrorNumber, pszCallingFunction);
    }

	LMI4_MEM_TCFREE(pszEnvInfo);
	return iRetCode;
}




