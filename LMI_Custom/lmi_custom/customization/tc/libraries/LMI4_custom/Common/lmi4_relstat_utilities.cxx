/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_relstat_utilities.cxx

    Description:  This File contains wrapper functions for OOTB API 'RELSTAT'.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-Mar-18    Megha Singh   Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function takes release status tag "tStatus" and returns release status type name of input release status.
 *
 * @param[in]  tStatus           Tag of release status object
 * @param[out] refstrRelStatType Returns type of the given release status object.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_relstat_ask_release_status_type
(
	tag_t   tStatus,
	string& refstrRelStatType
)
{
	int   iRetCode         = ITK_ok;
	char* pszRelStatusType = NULL;
	
	LMI4_ITKCALL(iRetCode, RELSTAT_ask_release_status_type(tStatus, &pszRelStatusType));

	if(iRetCode == ITK_ok && pszRelStatusType != NULL && tc_strlen(pszRelStatusType) > 0)
	{
		refstrRelStatType.assign(pszRelStatusType);
	}

	LMI4_MEM_TCFREE(pszRelStatusType);
	return iRetCode;
}























