/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_am_utilities.cxx

    Description:  This File contains wrapper functions for OOTB 'AM' API.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/
#include <lmi4_library.hxx>

 /**
 * This function returns 'true' if the user has the specified input privilege "strPrivilege" on the input object else it returns 'false'. 
 *
 * @param[in]  tObj         Tag of object.
 * @param[in]  strPrivilege Specified Privilege 
 * @param[out] refbAccess   Returns 'true' if the specified privilege is present lese it returns 'false'.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_am_check_privilege
(
	tag_t    tObject,
	string   strPrivilege,
	logical& refbAccess
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AM_check_privilege (tObject, strPrivilege.c_str(), &refbAccess));

	return iRetCode;
}





