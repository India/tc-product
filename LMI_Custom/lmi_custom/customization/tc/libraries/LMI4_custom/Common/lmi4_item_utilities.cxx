/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_item_utilities.cxx

    Description: This File contains wrapper functions for OOTB 'ITEM' API.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function retrieves all Item Revisions attached to a specified Item passed as input argument.
 *
 * @param[in]  tItem           Tag of the Input
 * @param[out] refvItemRevList List of tag of all the revisions of the Item
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_item_list_all_revs
(
	tag_t          tItem,
	vector<tag_t>& refvItemRevList
)
{
	int    iRetCode   = ITK_ok;
	int    iRevCount  = 0;
	tag_t* ptIRevList = NULL;

	LMI4_ITKCALL(iRetCode, ITEM_list_all_revs(tItem, &iRevCount, &ptIRevList));

	if(iRevCount > 0)
	{		
		LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag_array(iRevCount, ptIRevList, false, refvItemRevList));
	}
	
	LMI4_MEM_TCFREE(ptIRevList);
	return iRetCode;
}

/**
 * This function retrieves tags of all bom views related to the Item passed as input argument.
 *
 * @param[in]  tItem            Tag of the Input
 * @param[out] reftBOMViewsList List of Tag of all the bom views for the Item
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_item_list_bom_views
(
	tag_t          tItem,
	vector<tag_t>& refvBOMViewsList
)
{
	int    iRetCode   = ITK_ok;
	int    iBOMCount  = 0;
	tag_t* ptBOMViews = NULL;

	LMI4_ITKCALL(iRetCode, ITEM_list_bom_views(tItem, &iBOMCount, &ptBOMViews));

    if(iBOMCount > 0)
    {		
	    LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag_array(iBOMCount, ptBOMViews, false, refvBOMViewsList));
    }

	LMI4_MEM_TCFREE(ptBOMViews);
	return iRetCode;
}

/**
 * This function retrieves all BOMView revisions attached to the Item Revision.
 *
 * @param[in]  tItemRev       Tag of Item Revision
 * @param[out] reftBVRevsList List of BOMView revisions for the Item Revision.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_item_rev_list_bom_view_revs
(
	tag_t          tItemRev,
	vector<tag_t>& refvBVRevList
)
{
	int    iRetCode  = ITK_ok;
	int    iBOMCount = 0;
	tag_t* ptBOMVRev = NULL;

	LMI4_ITKCALL(iRetCode, ITEM_rev_list_bom_view_revs(tItemRev, &iBOMCount, &ptBOMVRev));

    if(iBOMCount > 0)
    {		
	    LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag_array(iBOMCount, ptBOMVRev, false, refvBVRevList));
    }

	LMI4_MEM_TCFREE(ptBOMVRev);
	return iRetCode;
}
















