/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_ps_utilities.cxx

    Description: This file contains wrapper functions for OOTB 'PS' API. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function takes view type tag "tViewType" and returns the name of input view type.
 *
 * @param[in]  tViewType      Tag of the view type .
 * @param[out] refstrViewName Name of the view type 
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_ps_ask_view_type_name
(
	tag_t   tViewType, 
	string& refstrViewName
)
{
	int   iRetCode    = ITK_ok;
	char* pszViewName = NULL;
	
	LMI4_ITKCALL(iRetCode, PS_ask_view_type_name(tViewType, &pszViewName));

	if(iRetCode == ITK_ok && pszViewName != NULL && tc_strlen(pszViewName) > 0)
	{
		refstrViewName.assign(pszViewName);
	}

	LMI4_MEM_TCFREE(pszViewName);
	return iRetCode;
 }

/**
 * This function takes note type tag "tNoteTyp" as input parameter and returns the name of input note type.
 *
 * @param[in]  tNoteTyp   Tag of the note type .
 * @param[out] strNotName Name of the note type
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_ps_ask_note_type_name
(
	tag_t   tNoteTyp,
	string& strNoteName
)
{
	int   iRetCode   = ITK_ok;
	char* pszNoteNam = NULL;
	
	LMI4_ITKCALL(iRetCode, PS_ask_note_type_name(tNoteTyp, &pszNoteNam));

	if(iRetCode == ITK_ok && pszNoteNam != NULL && tc_strlen(pszNoteNam))
	{
		strNoteName.assign(pszNoteNam);
	}

	LMI4_MEM_TCFREE(pszNoteNam);
	return iRetCode;
}

























