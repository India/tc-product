/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_tctype_utilities.cxx

    Description:  This File contains wrapper functions for OOTB API 'WSOM'.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/** 
 * This function returns list of release statuses attached to the input object 'tObject'.
 *
 * @param[in]  tObject       Tag of Object
 * @param[out] refiStatCount Returns no. of release statuses attached to input object.
 * @param[out] refvtStatList Returns arrays of tags of release statuses attached to input object 'tObject'.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_wsom_ask_release_status_list
(
	tag_t          tObject, 
	int&           refiStatCount,
	vector<tag_t>& refvtStatList
)
{
	int    iRetCode   = ITK_ok;
	tag_t* ptStatList = NULL;

    LMI4_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, &refiStatCount, &ptStatList));

	if(iRetCode == ITK_ok && refiStatCount > 0)
	{
		LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag_array(refiStatCount, ptStatList, false, refvtStatList));
	}

	LMI4_MEM_TCFREE(ptStatList);
	return iRetCode;
}











