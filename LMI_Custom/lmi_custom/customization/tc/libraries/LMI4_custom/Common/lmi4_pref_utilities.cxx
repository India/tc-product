/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_pref_utilities.cxx

    Description: This File contains wrapper functions for OOTB 'PS' API.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function retrieves the value of string preference.
 *
 * - The value is returned as specified at the first possible location starting at the location equal to the protection scope of the preference.
 * - The index is used to determine which value to retrieve if the preference has multiple values. The index starts at 0. If the preference is single-valued, use the value 0.
 *
 * @param[in]  strPrefName      Name of the preference 
 * @param[in]  iIndex           Index of the value to retrieve
 * @param[out] refstrPrefValue  Value of Preference
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_pref_ask_char_value
(
	string  strPrefName,
	int     iIndex,
	string& refstrPrefValue
)
{
	int   iRetCode   = ITK_ok;
	char* pszPrefVal = NULL;

	LMI4_ITKCALL(iRetCode, PREF_ask_char_value((strPrefName.c_str()), iIndex, &pszPrefVal));

	if(iRetCode == ITK_ok && pszPrefVal != NULL && tc_strlen(pszPrefVal) > 0)
	{
		refstrPrefValue.assign(pszPrefVal);
	}

	LMI4_MEM_TCFREE(pszPrefVal);
	return iRetCode;
}

/**
 * This function retrieves values of string array preference.
 *
 * @param[in]  strPrefName      Name of the preference 
 * @param[out] refstrPrefValue  List of string Preference values
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_pref_ask_char_values
(
	string          strPrefName,
	vector<string>& refvPrefValues
)
{
	int    iRetCode     = ITK_ok;
	int    iPrefValCnt  = 0;
	char** ppszPrefVals = NULL;

	LMI4_ITKCALL(iRetCode, PREF_ask_char_values((strPrefName.c_str()), &iPrefValCnt, &ppszPrefVals));

    if(iPrefValCnt > 0 && iRetCode == ITK_ok)
    {
	    LMI4_ITKCALL(iRetCode, LMI4_vector_store_string_array(iPrefValCnt, ppszPrefVals, false, refvPrefValues));
    }

	LMI4_MEM_TCFREE(ppszPrefVals);
	return iRetCode;
}













