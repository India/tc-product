/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_aom_utilities.cxx

    Description:  This File contains wrapper functions for OOTB 'AOM' API.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/
#include <lmi4_library.hxx>

/**
 * This function receives tag of object "tObj", name of property "strPropName" as input arguments and returns value of the input property 'strPropName'. 
 *
 * @param[in]  tObj            Tag of object.
 * @param[in]  strPropName     Property name 
 * @param[out] refstrValueProp Value of Property
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_ask_value_string
(
	tag_t   tObj,
	string  strPropName,
	string& refstrValueProp
)
{
	int   iRetCode   = ITK_ok;
	char* pszPropVal = NULL;
	
	LMI4_ITKCALL(iRetCode, AOM_ask_value_string(tObj, (strPropName.c_str()), &pszPropVal));

	if(iRetCode == ITK_ok && pszPropVal != NULL && tc_strlen(pszPropVal) > 0)
	{
		refstrValueProp.assign(pszPropVal);
	}

	LMI4_MEM_TCFREE(pszPropVal);
	return iRetCode;
}

/**
 * This function receives tag of object "tValue" as input parameter and retrieves UID of input object "tValue".
 *
 * @param[in]  tObject   Tag of object.
 * @param[out] refstrUID UID of input object.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_tag_to_string
(
	tag_t   tObject,
	string& refstrUID
)
{
	int   iRetCode     = ITK_ok;
	char* pszStringVal = NULL; 

	LMI4_ITKCALL(iRetCode, AOM_tag_to_string (tObject, &pszStringVal));

	if(iRetCode == ITK_ok && pszStringVal != NULL && tc_strlen(pszStringVal) > 0)
	{
		refstrUID.assign(pszStringVal);
	}

	LMI4_MEM_TCFREE(pszStringVal);
	return iRetCode;
}

/**
 * This function receives tag of object "tObject" and property name "strPropName" as input parameters. It retrieves property value type and its type name. 
 *
 * @param[in]  tObject            Tag of object.
 * @param[in]  strPropName        Property name
 * @param[out] refenumValueTypeOf Property value type
 * @param[out] refstrValType      Property value type name  
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_ask_value_type
(
	tag_t              tObject, 
	string             strPropName, 
	PROP_value_type_t& refenumValueTypeOfProp, 
	string&            refstrValType
)
{
	int   iRetCode   = ITK_ok;
	char* pszValType = NULL;
	
	LMI4_ITKCALL(iRetCode, AOM_ask_value_type(tObject, (strPropName.c_str()), &refenumValueTypeOfProp, &pszValType));

	if(iRetCode == ITK_ok && tc_strlen(pszValType) > 0  && pszValType != NULL )
	{		
		refstrValType.assign(pszValType);
	}

	LMI4_MEM_TCFREE(pszValType);
	return iRetCode;
}

/**
 * This function sets the input string value 'strPropVal' on input property 'strPropName' present on the input object 'tObject'.
 *
 * @param[in]  tObject     Tag of object.
 * @param[in]  strPropName String Property name on which value is to be set.
 * @param[in]  strPropVal  String value to be set on property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_set_value_string
(
	tag_t  tObject,
	string strPropName,
	string strPropVal
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AOM_set_value_string(tObject, (strPropName.c_str()), (strPropVal.c_str())));

	return iRetCode;
}

/**
 * This function sets the input tag value 'strPropVal' on input property 'strPropName' present on the input object 'tObject'.
 *
 * @param[in]  tObject     Tag of object.
 * @param[in]  strPropName Property name on which value is to be set.
 * @param[in]  tPropVal    Tag value to be set on property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_set_value_tag
(
	tag_t  tObject,
	string strPropName,
	tag_t  tAttrVal
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AOM_set_value_tag(tObject, (strPropName.c_str()), tAttrVal));
    
	return iRetCode;
}

/**
 * This function gets maximum number of elements present on input attribute 'strAttrName'.
 *
 * @param[in]  tObject            Tag of object.
 * @param[in]  strAttrName        Property name.
 * @param[out] refiMaxNumElements Count of Max. num of elements present on attribute 'strAttrName'.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_ask_max_num_elements
(
	tag_t  tObject,
	string strAttrName,
	int&   refiMaxNumElements
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AOM_ask_max_num_elements(tObject, (strAttrName.c_str()), &refiMaxNumElements));
	
	return iRetCode;
}

/**
 * This function returns number of tables and tags of the table row objecs present on the input property "strTabpropNam".
 *
 * @param[in]  tObject       Tag of object.
 * @param[in]  strTabPropNam Property name on which value is to be set.
 * @param[out] refiTabCnt    Count of tables present on table property.
 * @param[out] refvtTabRow   Tags of table row objects found on input table property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_ask_table_rows
(
	tag_t          tObject,
	string         strTabPropNam,
	int&           refiTabCnt, 
	vector<tag_t>& refvtTabRow
)
{
	int    iRetCode = ITK_ok;
	tag_t* ptTabRow = NULL; 

	LMI4_ITKCALL(iRetCode, AOM_ask_table_rows(tObject, strTabPropNam.c_str(), &refiTabCnt, &ptTabRow));

	if(iRetCode == ITK_ok && refiTabCnt > 0)
	{
		LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag_array(refiTabCnt, ptTabRow, false, refvtTabRow));		
	}

	LMI4_MEM_TCFREE(ptTabRow);
	return iRetCode;
}

/**
 * This function returns tags of the table row object of input index 'iRowIndex' present on the input property "strTabPropNam".
 *
 * @param[in]  tObject       Tag of object.
 * @param[in]  strTabPropNam Property name on which value is to be set.
 * @param[in]  iRowIndex     Row index of table property.
 * @param[out] refvtTabRow   Tags of table row objects found on input table property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_ask_table_row
(
	tag_t  tObject,
	string strTabPropNam,
	int    iRowIndex, 
	tag_t& reftTabRow
)
{
	int    iRetCode = ITK_ok;
	
	LMI4_ITKCALL(iRetCode, AOM_ask_table_row(tObject, strTabPropNam.c_str(), iRowIndex, &reftTabRow));

	return iRetCode;
}

/**
 * This function returns integer value of input property "strPropNam".
 *
 * @param[in]  tObject    Tag of object.
 * @param[in]  strPropNam Property name whose value is to be extracted.
 * @param[out] refiValue  Integer value of property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_ask_value_int
(
    tag_t  tObject,
	string strPropNam,
	int&   refiValue
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AOM_ask_value_int(tObject, strPropNam.c_str(), &refiValue));

	return iRetCode;
}

/**
 * This function returns double value of input property "strPropNam".
 *
 * @param[in]  tObject    Tag of object.
 * @param[in]  strPropNam Property name whose value is to be extracted.
 * @param[out] refdValue  Double value of property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_ask_value_double
(
	tag_t   tObject, 
	string  strPropNam,
	double& refdPropVal
)
{

	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AOM_ask_value_double(tObject, strPropNam.c_str(), &refdPropVal));

	return iRetCode;
}

/**
 * This function returns value of input date property "strPropNam".
 *
 * @param[in]  tObject      Tag of object.
 * @param[in]  strPropNam   Property name whose value is to be extracted.
 * @param[out] refdtPropVal Value of input date property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_ask_value_date
(
	tag_t  tObject,
	string strPropNam, 
	date_s& refdtPropVal
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AOM_ask_value_date(tObject, strPropNam.c_str(), &refdtPropVal));

	return iRetCode;
}

/**
 * This function returns tag value of input property "strPropNam".
 *
 * @param[in]  tObject     Tag of object.
 * @param[in]  strPropNam  Property name whose value is to be extracted.
 * @param[out] reftPropVal Tag value of input property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_ask_value_tag
(
	tag_t  tObject,
	string strPropNam,
	tag_t& reftPropVal
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AOM_ask_value_tag(tObject, strPropNam.c_str(), &(reftPropVal)));

	return iRetCode;
}

/**
 * This function returns logical value of input property "strPropNam".
 *
 * @param[in]  tObject     Tag of object.
 * @param[in]  strPropNam  Property name whose value is to be extracted.
 * @param[out] refbPropVal Logical value of input property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_ask_value_logical
(
	tag_t    tObject,
	string   strPropNam,
	logical& refbPropVal
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AOM_ask_value_logical(tObject, strPropNam.c_str(), &refbPropVal));

	return iRetCode;
}

/**
 * This function inserts table rows in input table property "strPropNam".
 *
 * @param[in]  tObject      Tag of object.
 * @param[in]  strTabName   Property name whose value is to be extracted.
 * @param[in]  iRowIndex    Index at which row is to nbe inserted.
 * @param[in]  iRowCount    Number of rows present in input table property.
 * @param[out] refvtTabRows Array of inserted table row objects.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_insert_table_rows
( 
	tag_t          tObject,
	string         strTabName,
	int            iRowIndex,
	int            iRowCount, 
	vector<tag_t>& refvtTabRows
)
{
	int    iRetCode  = ITK_ok;
	tag_t* ptTabRows = NULL;

	LMI4_ITKCALL(iRetCode, AOM_insert_table_rows(tObject,  strTabName.c_str(), iRowIndex, iRowCount, &ptTabRows));
	
	if(iRetCode == ITK_ok && iRowCount > 0)
	{
		LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag_array(iRowCount, ptTabRows, false, refvtTabRows));
	}

	LMI4_MEM_TCFREE(ptTabRows);
	return iRetCode;
}

/**
 * This function appends table rows in input table property "strPropNam".
 *
 * @param[in] tObject       Tag of object.
 * @param[in] strTabName    Table Property name 
 * @param[in] iRowToAppend  No. of rows to append
 * @param[out] refvtTabRows List of table row objects appended.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_append_table_rows
(
	tag_t          tObject,
	string         strTabName,
	int            iRowToAppend,
	vector<tag_t>& refvtTabRows
)
{
	int    iRetCode  = ITK_ok;
	int    iRowIndex = 0;
	tag_t* ptTabRows = NULL;

	LMI4_ITKCALL(iRetCode, AOM_append_table_rows(tObject, strTabName.c_str(), iRowToAppend, &iRowIndex, &ptTabRows));

	if(iRetCode == ITK_ok && ptTabRows != NULL)
	{
		LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag_array(iRowToAppend, ptTabRows, false, refvtTabRows));
	}

	return iRetCode;
}
 
/**
 * This function sets integer value to input property "strPropNam".
 *
 * @param[in] tObject    Tag of object.
 * @param[in] strTabName Property name whose value is to be set.
 * @param[in] iPropNal   Integer value to be set to input property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_set_value_int
(
	tag_t  tObject,
	string strPropNam,
	int    iPropVal
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AOM_set_value_int(tObject, strPropNam.c_str(), iPropVal));

	return iRetCode;
}

/**
 * This function sets double value to input property 'strpropNam'.
 *
 * @param[in]  tObject     Tag of object.
 * @param[in]  strPropNam  Property name whose value is to be set.
 * @param[in]  dPropVal    Double value to be set in input property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_set_value_double
(
	tag_t  tObject,
	string strPropNam,
	double dPropVal
)
{
	int iRetCode = ITK_ok; 

	LMI4_ITKCALL(iRetCode, AOM_set_value_double(tObject, strPropNam.c_str(), dPropVal));

	return iRetCode;
}

/**
 * This function sets date value to input property 'strPropNam'.
 *
 * @param[in]  tObject     Tag of object.
 * @param[in]  strPropNam  Property name whose value is to be set.
 * @param[in]  dtPropVal   Date value to be set in input property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_set_value_date
(
	tag_t  tObject,
	string strPropNam, 
	date_s dtPropVal
)
{
	int iRetCode = ITK_ok; 

	LMI4_ITKCALL(iRetCode, AOM_set_value_date(tObject, strPropNam.c_str(), dtPropVal));

	return iRetCode;
}

/**
 * This function sets double value to input property 'strpropNam'.
 *
 * @param[in]  tObject     Tag of object.
 * @param[in]  strPropNam  Property name whose value is to be set.
 * @param[in]  bPropVal    Logical value to be set in input property.
 *
 * @retval ITK_ok is always returned.
 */
int LMI4_aom_set_value_logical
(
	tag_t   tObject,
	string  strPropNam,
	logical bPropVal
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, AOM_set_value_logical(tObject, strPropNam.c_str(), bPropVal));

	return iRetCode;
}



















































		



