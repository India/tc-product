/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_date_utilities.cxx

    Description:  This File contains wrapper functions for OOTB 'DATE' API.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/
#include <lmi4_library.hxx>

/**
 * This function retrieves string representation of input date structure "dDateStructure" in format "strDateFormat" passed as input argument.
 *
 * @param[in]  dDateStruct    Supplied DateStructure
 * @param[in]  strDateFormat  Expected date Format
 * @param[out] refstrDateText String representation of Date
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_date_date_to_string
(
	date_t  dDateStruct, 
	string  strDateFormat,
	string& refstrDateText
)
{
	int   iRetCode     = ITK_ok;
	char* pszAttrValue = NULL;
	
	LMI4_ITKCALL(iRetCode, DATE_date_to_string(dDateStruct, strDateFormat.c_str(), &pszAttrValue));

	if(iRetCode == ITK_ok && pszAttrValue != NULL && tc_strlen(pszAttrValue) > 0)
	{
		refstrDateText.append(pszAttrValue);
	}

	LMI4_MEM_TCFREE(pszAttrValue);
	return iRetCode;
}


/**
 * This function retrieves localized default date format, as specified by the entry "DefaultDateFormat" in the localization file.
 *
 * @param[out] refstrDateText String representation of Date
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_date_default_date_format
(
	string& refstrDateFormat
)
{
	int   iRetCode      = ITK_ok;
	char* pszDateFormat = NULL;

    LMI4_ITKCALL(iRetCode, DATE_default_date_format(&pszDateFormat));	

	if(iRetCode == ITK_ok && pszDateFormat != NULL && tc_strlen(pszDateFormat) > 0)
	{
		refstrDateFormat.assign(pszDateFormat);
	}

	LMI4_MEM_TCFREE(pszDateFormat);
	return iRetCode;
}

int LMI4_date_string_to_date_t
(
	string   strAttrValue,
	logical& refIsValid,
	date_t&  refdValue
)
{
	int   iRetCode = ITK_ok;
		
	LMI4_ITKCALL(iRetCode, DATE_string_to_date_t((char*)(strAttrValue.c_str()), &refIsValid, &refdValue));

	return iRetCode;
}

















