/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_epm_utilities.cxx

    Description:  This File contains wrapper functions for OOTB 'EPM' API.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function gets tag of task and attachment type as input arguments. It returns tags of all objects attached to the given input task "tTask".
 *
 * @param[in]  tTask           Tag of the task on which the attachments are done
 * @param[in]  iAttachmentType Specific attachment type like EPM_target_attachment, EPM_reference_attachment 
 * @param[out] refvObjectList  List of tags of all attached objects.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_epm_ask_attachments
(
	tag_t          tTask,
    int            iAttachmentType,
	vector<tag_t>& refvObjectList
)	
{
	int    iRetCode     = ITK_ok;
	int    iTargetCount = 0;
	tag_t* ptTargets    = NULL;
	
	LMI4_ITKCALL(iRetCode, EPM_ask_attachments(tTask, iAttachmentType, &iTargetCount, &ptTargets));
	
	/* Validate input */
    if(iTargetCount == 0)
    {
        TC_write_syslog( "ERROR: Missing input data: List passed or seprator is null\n");
    }
    else
    {		
		LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag_array(iTargetCount, ptTargets, false, refvObjectList));
	}

	LMI4_MEM_TCFREE(ptTargets);
	return iRetCode;
}























