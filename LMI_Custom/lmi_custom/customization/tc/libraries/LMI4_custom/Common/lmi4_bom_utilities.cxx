/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_bom_utilities.cxx

    Description:  This File contains wrapper functions for OTB 'BOM' API.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
21-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function retrieves Lists the visible ImanItemLines below the specified parent line 'tBOMLine' passed as an input parameter.
 *
 * @param[in]  tBOMLine        Tag of Parent BOMLine whosechildren are required
 * @param[out] refvChildrenBL  Vector of Child BOMLines.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_bom_line_ask_child_lines
(
	tag_t          tBOMLine,
	vector<tag_t>& refvChildrenBL
)
{
	int    iRetCode     = ITK_ok;
	int    iBLCount     = 0;
	tag_t* ptChildrenBL = NULL;

	LMI4_ITKCALL(iRetCode, BOM_line_ask_child_lines(tBOMLine, &iBLCount, &ptChildrenBL));

	for(int iDx = 0; iDx < iBLCount && iRetCode == ITK_ok; iDx++)
	{
		LMI4_ITKCALL(iRetCode, LMI4_vector_pushback_tag(ptChildrenBL[iDx], false, refvChildrenBL));
	}

	LMI4_MEM_TCFREE(ptChildrenBL);
	return iRetCode;
}

/**
 * This function retrieves the display value of BOM line attribute.
 *
 * @param[in]  tBOMLine      Tag of BOMLine 
 * @param[in]  iAttributeId  ID of enquired attribute
 * @param[out] refvAttrValue Value of attribute
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_bom_line_ask_attribute_string
(
	tag_t   tBOMLine,
	int     iAttributeId, 
	string& refstrAttrValue
)
{
	int   iRetCode = ITK_ok;
	char* pszName  = NULL;

	LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_string(tBOMLine, iAttributeId, &pszName));

	if(iRetCode == ITK_ok && pszName != NULL && tc_strlen(pszName) > 0)
	{
		refstrAttrValue.assign(pszName);
	}

	LMI4_MEM_TCFREE(pszName);
	return iRetCode;
}





	



