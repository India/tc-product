/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_pom_utilities.cxx

    Description:  This File contains wrapper functions for OOTB 'POM' API.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function retrieves the name of the logged-in user "refstrUserName" and the tag of the User "reftUser".
 *
 * @param[out] refstrUserName Name of User
 * @param[out] reftUser       Tag of User
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_pom_get_user
(
	string& refstrUserName, 
	tag_t&  reftUser
)
{
	int   iRetCode    = ITK_ok;
	char* pszUserName = NULL;
	tag_t tUser       = NULLTAG;

     LMI4_ITKCALL(iRetCode, POM_get_user(&pszUserName, &tUser));

	 if(iRetCode == ITK_ok)
	 {
		 if(pszUserName != NULL && tc_strlen(pszUserName) > 0)
		 {
			 refstrUserName.assign(pszUserName);
		 }

		 if(tUser != NULLTAG)
		 {
			 reftUser = tUser;
		 }
	 }

	 LMI4_MEM_TCFREE(pszUserName);
	 return iRetCode;
}

/**
 * This function returns the string representation of the input tag "tPropVal".
 *
 * @param[in]  tPropVal           Input tag property 
 * @param[out] refstrConvertedVal Return string representation of input tag property. 
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_pom_tag_to_string
(
	tag_t   tPropVal, 
	string& refstrConvertedVal
)
{
	int   iRetCode        = ITK_ok;
	char* pszConvertedVal = NULL;

	LMI4_ITKCALL(iRetCode, POM_tag_to_string(tPropVal, &pszConvertedVal));

	if(iRetCode == ITK_ok && tc_strlen(pszConvertedVal) > 0)
	{
		refstrConvertedVal.assign(pszConvertedVal);
	}

	return iRetCode;
}

/**
 * This function returns the corresponding tag of the input string representation "strVal".
 *
 * @param[in]  strVal             Input string representation 
 * @param[out] refstrConvertedVal Return tag corresponding to input string 'strVal'. 
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_pom_string_to_tag
(
	string strVal,
	tag_t& reftConvertedVal
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, POM_string_to_tag(strVal.c_str(), &reftConvertedVal));

	return iRetCode;
}




















