/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_ae_utilities.cxx

    Description:  This file contains wrapper functions for OOTB API 'AE'.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-Mar-18     Megha Singh         Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function receives tag of Dataset type, Reference name as input parameters and retrieves list of template and format specificatons for Reference name "strRefName" passed as input parameter.
 *
 * @param[in]  tDatasetType    Tag of the Dataset Type
 * @param[in]  strRefName      Name of Reference
 * @param[out] refvTemp        List of template specifications for reference name (strRefName)
 * @param[out] refvRefFormats  List of format specifications for reference name(strRefName).
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_ae_ask_datasettype_file_refs
(
	tag_t           tDatasetType,
	string          strRefName,
	vector<string>& refvTemp,
	vector<string>& refvRefFormats
)
{
	int    iRetCode       = ITK_ok;
	int    iCount         = 0;
    char** ppszTemplate   = NULL;
    char** ppszRefFormats = NULL;
		
	LMI4_ITKCALL(iRetCode, AE_ask_datasettype_file_refs(tDatasetType, (strRefName.c_str()), &iCount, &ppszTemplate, &ppszRefFormats));

	LMI4_ITKCALL(iRetCode, LMI4_vector_store_string_array(iCount, ppszTemplate, false, refvTemp));
		
	LMI4_ITKCALL(iRetCode, LMI4_vector_store_string_array(iCount, ppszRefFormats, false, refvRefFormats));
		
	LMI4_MEM_TCFREE(ppszTemplate);
	LMI4_MEM_TCFREE(ppszRefFormats);	
	return iRetCode;
}

/**
 * This function receives tag of dataset type as input argument and retrieves list of valid names of Named References for input dataset type.
 *
 * @param[in]  tDatasetType Tag of the DatasetType
 * @param[out] refvRefList  List of names of Named References
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_ae_ask_datasettype_refs
(
	tag_t           tDatasetType,
	vector<string>& refvRefList	
)
{
	int    iRetCode     = ITK_ok;
	int    iRefCnt      = 0;
    char** ppszRefList  = NULL;	

	LMI4_ITKCALL(iRetCode, AE_ask_datasettype_refs(tDatasetType, &iRefCnt, &ppszRefList));

	for(int iDx = 0; iDx < iRefCnt && iRetCode == ITK_ok; iDx++)
	{
		LMI4_ITKCALL(iRetCode, LMI4_vector_store_string_array(iRefCnt, ppszRefList, false, refvRefList));
	}
	
	LMI4_MEM_TCFREE(ppszRefList);
	return iRetCode;
}



















