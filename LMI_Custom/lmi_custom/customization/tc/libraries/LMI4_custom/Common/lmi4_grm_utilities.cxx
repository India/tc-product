/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_grm_utilities.cxx

    Description:  This File contains wrapper functions for OOTB 'GRM' API.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function retrieves list of all primary objects with a specified relation type to the specified secondary object passed as input argument.
 *
 * @param[in]  tObject            Tag of secondary object
 * @param[in]  iRelationType      Tag of relation type 
 * @param[out] refvRelatedPrimObj List of structure "GRM_relation_t" (Structure GRM_relation_t contains datamemebers - primary_object, secondary_object, relation_type, user_data)
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_grm_list_primary_objects
(
	tag_t                   tObject, 
	tag_t                   tRelationType, 
	vector<GRM_relation_t>& refvRelatedPrimObj
)
{
	int             iRetCode        = ITK_ok;
	int             iRelatedObjCnt  = 0;
	GRM_relation_t* pstruRelatedObj = NULL;

	LMI4_ITKCALL(iRetCode, GRM_list_primary_objects(tObject, tRelationType, &iRelatedObjCnt, &pstruRelatedObj));

	for(int iDx = 0; iDx < iRelatedObjCnt && iRetCode == ITK_ok ; iDx++)
	{
		refvRelatedPrimObj.push_back(pstruRelatedObj[iDx]);
	}	

	LMI4_MEM_TCFREE(pstruRelatedObj);
	return iRetCode;
}

/**
 * This function retrieves list of all secondary objects with a specified relation type to the specified primary object passed as input argument.
 *
 * @param[in]  tObject            Tag of primary object
 * @param[in]  iRelationType      Tag of relation type 
 * @param[out] refvRelatedPrimObj List of structure "GRM_relation_t" (Structure GRM_relation_t contains datamemebers - primary_object, secondary_object, relation_type, user_data)
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_grm_list_secondary_objects
(
	tag_t                   tObject,
	tag_t                   tRelationType,
	vector<GRM_relation_t>& refvRelatedSeconObj
)
{
	int             iRetCode        = ITK_ok;
	int             iRelatedObjCnt  = 0;
	GRM_relation_t* pstruRelatedObj = NULL;

	LMI4_ITKCALL(iRetCode, GRM_list_secondary_objects(tObject, tRelationType, &iRelatedObjCnt, &pstruRelatedObj));

	for(int iDx = 0; iDx < iRelatedObjCnt && iRetCode == ITK_ok; iDx++)
	{
		refvRelatedSeconObj.push_back(pstruRelatedObj[iDx]);
	}

	LMI4_MEM_TCFREE(pstruRelatedObj);
	return iRetCode;
}
















