/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_tctype_utilities.cxx

    Description:  This File contains wrapper functions for OOTB API 'TCTYPE'.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function returns type name of input Object type tag "tObjectType".
 *
 * @param[in]  tObjectType      Tag of Object type
 * @param[out] refstrObjectName Object type name 
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_tctype_ask_name2
(
	tag_t   tObjectType,
	string& refstrTypeName
)
{
	int   iRetCode   = ITK_ok;
	char* pszObjType = NULL;
		
	LMI4_ITKCALL(iRetCode, TCTYPE_ask_name2(tObjectType, &pszObjType));

	if(iRetCode == ITK_ok && pszObjType != NULL && tc_strlen(pszObjType) > 0)
	{
		refstrTypeName.assign(pszObjType);
	}

	LMI4_MEM_TCFREE(pszObjType);
	return iRetCode;
}

/**
 * This function returns display name of input Object type tag "tObjType".
 *
 * @param[in]  tObjType              Tag of Object type
 * @param[out] refstrTypeDisplayName Display name of Object type.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_tctype_ask_display_name
(
	tag_t   tObjType,
	string& refstrTypeDisplayName
)
{
	int   iRetCode       = ITK_ok;
	char* pszTypDispName = NULL;
 
	LMI4_ITKCALL(iRetCode, TCTYPE_ask_display_name(tObjType, &pszTypDispName));

	if(iRetCode == ITK_ok && pszTypDispName != NULL && tc_strlen(pszTypDispName) > 0)
	{
		refstrTypeDisplayName.assign(pszTypDispName);
	}

	LMI4_MEM_TCFREE(pszTypDispName);
	return iRetCode;
}

/**
 * This function returns tag of type corresponding to input type name "strTypeName" and class name "strClassNam".
 *
 * @param[in]  strTypeName Type name
 * @param[in]  strClassNam Class name.
 * @param[out] reftType    Returns tag of type corresponding to input type and class name.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_tctype_find_type
(
	string strTypeName,
	string strClassNam,
	tag_t& reftType
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, TCTYPE_find_type(strTypeName.c_str(), strClassNam.c_str(), &reftType));

	return iRetCode;
}

/**
 * This function returns 'true' if the given input type 'tType' is the same as or a subtype of the input parent type 'tInputObjType'.
 *
 * @param[in]  tInputObjType       Tag of input parent type
 * @param[in]  tType               Tag of type.
 * @param[out] refbBelongsToInpCls Returns 'true' if input type 'tType' is same or sub type of 'tInputObjType'.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_tctype_is_type_of
(
	tag_t    tInputObjType,
	tag_t    tType,
	logical& refbBelongsToInpCls
)
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, TCTYPE_is_type_of(tInputObjType, tType, &refbBelongsToInpCls));

	return iRetCode;
}

































