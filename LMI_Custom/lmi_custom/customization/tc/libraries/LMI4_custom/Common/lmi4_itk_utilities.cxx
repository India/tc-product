/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_itk_utilities.cxx

    Description:  This File contains wrapper functions for OOTB 'ITK' API.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function retrieves the switch string and the corresponding value string from the argument string passed as input.
 *
 * - The format of the switch-value pair must conform to the following pattern: [^=]*=[^=]*.
 * - The switch and the value can be any alpha-numeric string that does not contain the equal (=) character.
 * - The equal (=) character is used to delimit the switch and the value.
 *   
 * @param[in]  strNextArgument String representing argument of function
 * @param[out] refstrSwitch    Switch string
 * @param[out] refstrArgValue  Value string    
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_itk_ask_argument_named_value
(
	string  strArgument,
	string& refstrSwitch,
	string& refstrArgValue
)
{
	int   iRetCode  = ITK_ok;
	char* pszSwitch = NULL;
    char* pszValue  = NULL;

	LMI4_ITKCALL(iRetCode, ITK_ask_argument_named_value((strArgument.c_str()), &pszSwitch, &pszValue));

	if(iRetCode == ITK_ok && pszValue != NULL && tc_strlen(pszValue) > 0 && pszSwitch != NULL && tc_strlen(pszSwitch) > 0)
	{
		refstrArgValue.assign(pszValue);
		
		refstrSwitch.assign(pszSwitch);		
	}

	LMI4_MEM_TCFREE(pszSwitch);
	LMI4_MEM_TCFREE(pszValue);
	return iRetCode;
}

/**
 * This function returns the given input date "dtPropVal" in string format "refstrStringDate".
 *
 * @param[in]  dtPropVal        Input date 
 * @param[out] refstrStringDate Return date in string format
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_itk_date_to_string
(
	date_s  dtPropVal,
	string& refstrStringDate
)
{
	int   iRetCode      = ITK_ok;
	char* pszDateString = NULL;

	LMI4_ITKCALL(iRetCode, ITK_date_to_string(dtPropVal, &pszDateString));

	if(iRetCode == ITK_ok && tc_strlen(pszDateString) > 0)
	{
		refstrStringDate.assign(pszDateString);
	}

	return iRetCode;
}

/**
 * This function returns the input string date "strDate" in date structure .
 *
 * @param[in]  strDate        Input date in string format.
 * @param[out] refstdtPropVal Return date in structure.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_itk_string_to_date
(
	string strDate,
	date_s& refstdtPropVal
) 
{
	int iRetCode = ITK_ok;

	LMI4_ITKCALL(iRetCode, ITK_string_to_date(strDate.c_str(), &refstdtPropVal));

	return iRetCode;
}


















