/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_vector_utilities.cxx

    Description: This File contains wrapper functions for pushing values in Vector.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>

/**
 * This function stores input string to vector "refvFinal". If input argument "bUnique" is 'true' than value "strValue" is directly stored in vector "refvFinal" otherwise uniqueness check is
 * performed and only if the value is not found in vector "refvFinal" than it is stored in the vector.  
 * 
 * @param[in]  strValue  Value to be added to vector.
 * @param[in]  bUnique   If 'true' than value is checked and if found unique than added to vector
 * @param[out] refvFinal List of values.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_vector_store_string
(
	string          strValue,
	logical         bUnique,
	vector<string>& refvFinal
)
{
	int iRetCode = ITK_ok;

	if(bUnique == false)
	{
		refvFinal.push_back(strValue);
	}
	else
	{
		vector<string>::iterator itervFinal;

		itervFinal = find(refvFinal.begin(), refvFinal.end(), strValue);

		if(itervFinal == refvFinal.end())
		{
			refvFinal.push_back(strValue);
		}
	}	

	return iRetCode;
}

/**
 * This function loops input array of strings "ppszValue" and add string from input string array to vector "refvFinal". If input argument "bUnique" is 'true' than string from input array is directly stored 
 * in vector "refvFinal" otherwise uniqueness check is performed and only if the value is not found in vector "refvFinal" than it is stored in the vector. 
 * 
 * @param[in]  iCount    Size of the string array "ppszValue"
 * @param[in]  ppszValue Array containing strings
 * @param[in]  bUnique   If "bUnique" is 'true' than value is checked for uniqueness and if found unique than value is pushed into vector.
 * @param[out] refvFinal List of values.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_vector_store_string_array
(
	int             iCount,
	char**          ppszValue,
	logical         bUnique,	
	vector<string>& refvFinal
)
{
	int iRetCode = ITK_ok;

	for(int iDx = 0; iDx < iCount; iDx++)
	{
        if(ppszValue[iDx] != NULL && tc_strlen(ppszValue[iDx]) > 0)
        {
		    if(bUnique == false)
		    {
			    refvFinal.push_back(ppszValue[iDx]);
		    }
		    else
		    {
			    vector<string>::iterator itervFinal;

			    itervFinal = find(refvFinal.begin(), refvFinal.end(), ppszValue[iDx]);

			    if(itervFinal == refvFinal.end())
			    {
				    refvFinal.push_back(ppszValue[iDx]);
			    }
		    }
        }
	}    

	return iRetCode;
}

/**
 * This function stores input tag "tValue" to vector "refvFinal". If value of input argument 'bUnique' is 'true' than input tag is stored directly to vector "refvFinal" otherwise a uniqueness check 
 * is performed and only if the value is not found in vector "refvFinal" than it is stored to vector. 
 *
 * @param[in]  tValue    Value to be pushed in vector
 * @param[in]  bUnique   If 'true' than input value "tValue" is checked and if found unique than value is pushed into vector
 * @param[out] refvFinal List of tags.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_vector_store_tag
(
	tag_t          tValue,
	logical        bUnique,
	vector<tag_t>& refvFinal
)
{
	int iRetCode = ITK_ok;

	if(bUnique == false)
	{
		refvFinal.push_back(tValue);
	}
	else
	{
		vector<tag_t>::iterator itervFinal;

		itervFinal = find(refvFinal.begin(), refvFinal.end(), tValue);

		if(itervFinal == refvFinal.end())
		{
			refvFinal.push_back(tValue);
		}
	}
	
	return iRetCode;
}


/** 
 * This function loops input array of tags "ptValue" and add the tag from input tag array to vector "refvFinal". If the value of input argument 'bUnique' is 'true' than input tag is added directly to 
 * vector "refvFinal" otherwise a uniqueness check is performed and only if the value is not found in vector "refvFinal" than it is added to vector. 
 *
 * @param[in]  iCount    Size of array of tags
 * @param[in]  ptValue   Value to be pushed in vector
 * @param[in]  bUnique   If 'true' the input value is checked and if found unique than pushed into vector
 * @param[out] refvFinal List of tags.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_vector_store_tag_array
(
	int            iCount,
	tag_t*         ptValue,
	logical        bUnique,
	vector<tag_t>& refvFinal
)
{
	int iRetCode = ITK_ok;

	for(int iDx = 0; iDx < iCount; iDx++)
	{
        if(ptValue[iDx] != NULLTAG)
        {
		    if(bUnique == false)
		    {
			    refvFinal.push_back(ptValue[iDx]);
		    }
		    else
		    {
			    vector<tag_t>::iterator itervFinal;

			    itervFinal = find(refvFinal.begin(), refvFinal.end(), ptValue[iDx]);

			    if(itervFinal == refvFinal.end())
			    {
				    refvFinal.push_back(ptValue[iDx]);
			    }
		    }
        }
	}

	return iRetCode;
}































