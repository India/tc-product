/*======================================================================================================================================================================
                                                               Copyright 2018  LMtec India
                                                            Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_workflow_handler_impl.hxx

    Description:  Header File for lm4_workflow_handler_impl.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/

#ifndef LMI4_WORKFLOW_HANDLER_IMPL_HXX
#define LMI4_WORKFLOW_HANDLER_IMPL_HXX

int LMI4_Sample_Handler
(
    EPM_action_message_t msg
);

int LMI4_Sample_Attach_Dataset
(
    EPM_action_message_t msg
);

int LMI4_init_struct_value
(
    stProp* pstProp
);

/*int LMI4_init_nam_val_struct
(
	AttributeNameValueList_lst* pstAttrNamValList
);*/



#endif

