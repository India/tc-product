/*======================================================================================================================================================================
                                                              Copyright 2018  LMtec India
                                                             Unpublished - All rights reserved

========================================================================================================================================================================

File Description:

    Filename: lmi4_workflow_handler_impl.cxx

    Description: This file contains function.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Mar-18    Megha Singh           Initial Release

========================================================================================================================================================================*/
#include <lmi4_library.hxx>
#include <lmi4_constants.hxx>
#include <lmi4_errors.hxx>
#include <lmi4_workflow_handler_impl.hxx>
#include <lmi4_get_nam_val_prop.hxx>
#include <lmi4_property_containers.hxx>
#include <lmi4_get_table_prop.hxx>

int LMI4_Sample_Handler
(
    EPM_action_message_t msg
)
{
	int     iRetCode    = ITK_ok;
	int     iAttchType  = 0;
	logical bFoundArg   = false;
	string  strArgValue;	
    string  strArgInfoValue;
	
	
	vector<string> vstrObjTypeList;
	vector<tag_t>  vtInpTypObject;
	vector<tag_t>  vtTypObject;
	vector<string> vstrArguments;
	
	vstrObjTypeList.push_back(LM4_CLASS_FIRMWARE);
	vstrObjTypeList.push_back(LM4_CLASS_CUSTOMERREVISION);
	vstrObjTypeList.push_back(LM4_CLASS_MANUFACTURERREVISION);

	LMI4_ITKCALL(iRetCode, LMI4_arg_get_attached_obj_of_input_type_list(msg.task, EPM_target_attachment, vstrObjTypeList, vtInpTypObject));

	LMI4_ITKCALL(iRetCode, LMI4_arg_get_attached_objects_of_type(msg.task, LM4_CLASS_MANUFACTURERREVISION, vtTypObject));
    
    LMI4_ITKCALL(iRetCode, LMI4_arg_get_arguments(msg.arguments, msg.task, LMI4_WF_ARG_PROPERTY, LMI4_STRING_COMMA, false, vstrArguments, strArgValue));

	LMI4_ITKCALL(iRetCode, LMI4_arg_get_wf_argument_value(msg.arguments, msg.task,  LMI4_WF_ARG_TYPE_INCLUDE_TYPE, false, strArgValue));

 	LMI4_ITKCALL(iRetCode, LMI4_arg_get_wf_argument_info_and_value(msg.arguments, msg.task, LMI4_WF_ARG_RELATION, false, bFoundArg, strArgInfoValue));  

	LMI4_ITKCALL(iRetCode, LMI4_arg_get_wf_attachment_type(LMI4_WF_ARG_VAL_TARGET, iAttchType));	

    return iRetCode;
}

int LMI4_Sample_Attach_Dataset
(
    EPM_action_message_t msg
)
{
	int    iRetCode      = ITK_ok;
	int    iTabCount     = 0;
	int    iTablesCnt    = 0;
	string strOriginalArgVal;
	tag_t  tTabRowObjNew = NULLTAG;
    
	vector<string> vstrArgValue;
	vector<tag_t>  vtObjectList;
	vector<tag_t>::iterator itervObjList;

	vector<string>  vstrPropName;
	vector<tag_t>   vtTabRow;	
	vector<string>  vstrPropVal;
	vector<string>  vstrTabPropVal;

	vstrPropName.push_back("lm4_boys");
	vstrPropName.push_back("lm4_class_rooms");
	vstrPropName.push_back("lm4_girls");
	vstrPropName.push_back("lm4_teachers");

	vstrTabPropVal.push_back("Mohit");
	vstrTabPropVal.push_back("Science");
	vstrTabPropVal.push_back("Megha");
	vstrTabPropVal.push_back("Sarika");

	stProp stGetPropVal;
	
	map <string, string> getPropVal;
	map <string, string> setPropVal;
	map<Base*, PROP_value_type_e> mstrPropInfo;
	map<Base*, PROP_value_type_e> mstrSpecPropInfo;
	map<Base*, PROP_value_type_e> mstrAllTabPropInfo;
	map<Base*, PROP_value_type_e> mstrSpecTabPropInfo; 
	map <string, LMI4_NameValuePropSet*> mSetNamVal;
	
	LMI4_ITKCALL(iRetCode, LMI4_arg_get_attached_objects_of_type(msg.task, "LM4_FirmwareRevision", vtObjectList));

	LMI4_ITKCALL(iRetCode, LMI4_arg_get_arguments(msg.arguments, msg.task, "property", ",", false, vstrArgValue, strOriginalArgVal));

	for (itervObjList = vtObjectList.begin(); itervObjList < vtObjectList.end() && iRetCode == ITK_ok; itervObjList++)
	{
		TableProp  *getTableProp  = new TableProp;
		NamValProp *getNamValProp = new NamValProp;

		LMI4_ITKCALL(iRetCode, LMI4_init_struct_value(&stGetPropVal));

		LMI4_ITKCALL(iRetCode, getTableProp->LMI4_get_all_table_attr((*itervObjList), vstrArgValue.at(0), vstrPropName, iTabCount, mstrAllTabPropInfo));

		LMI4_ITKCALL(iRetCode, getTableProp->LMI4_get_specific_table_attr((*itervObjList), vstrArgValue.at(0), vstrPropName, 2, mstrSpecTabPropInfo));
				
		if(iRetCode == ITK_ok)
		{
			setPropVal.insert(pair <string, string> ("lm4_boys", "SHIVAM"));
			setPropVal.insert(pair <string, string> ("lm4_class_rooms", "MANY"));
			setPropVal.insert(pair <string, string> ("lm4_girls", ""));
			setPropVal.insert(pair <string, string> ("lm4_teachers", "SHIVANI"));			
		}

		LMI4_ITKCALL(iRetCode, getTableProp->LMI4_set_table_attr_val((*itervObjList), "lm4_new_tab", setPropVal));

		/*LMI4_ITKCALL(iRetCode, getNamValProp->LMI4_get_all_nam_val_prop((*itervObjList), "lm4_courses_colleges", iTablesCnt, mstrPropInfo));
				
		LMI4_ITKCALL(iRetCode, getNamValProp->LMI4_get_specific_nam_val_prop((*itervObjList), "lm4_courses_colleges", 1, mstrSpecPropInfo));
				
		if(iRetCode == ITK_ok)
		{
			LMI4_NameValuePropSet *SetFirstNameVal = new LMI4_NameValuePropSet();
			SetFirstNameVal->strPropValue = "Shivanand";
			SetFirstNameVal->type         = PROP_string;
		
			mSetNamVal.insert(pair <string, LMI4_NameValuePropSet*> ("Name", SetFirstNameVal));

			LMI4_NameValuePropSet *SetSecNameVal = new LMI4_NameValuePropSet();
			SetSecNameVal->strPropValue = "24-Aug-1991 09:09:56";
			SetSecNameVal->type         = PROP_date;

			mSetNamVal.insert(pair <string, LMI4_NameValuePropSet*> ("Age", SetSecNameVal));						
		
			LMI4_ITKCALL(iRetCode, getNamValProp->LMI4_set_nam_val_prop((*itervObjList), "lm4_courses_colleges", mSetNamVal));
		}*/
		
		LMI4_OBJECT_DELETE(getTableProp);
		LMI4_OBJECT_DELETE(getNamValProp);
	}
	
    return iRetCode;
}

int LMI4_init_struct_value
(
    stProp* pstProp
)
{   
    int iRetCode = ITK_ok;
    int iLen     = 0;

    /*  Initializing Structure Variables    */
    if(pstProp != NULL)
    {
        pstProp->strPropVal;                                          
        pstProp->dPropVal    = 0;
        pstProp->dtPropVal   = NULLDATE;
        pstProp->sPropVal    = 0;
        pstProp->bPropVal    = false;
        pstProp->iPropVal    = 0;
        pstProp->tPropVal    = NULLTAG;
    }

    return iRetCode;
}































