/*======================================================================================================================================================================
                                                                Copyright 2018  LMtec India
                                                              Unpublished - All rights reserved

========================================================================================================================================================================

File Description:

    Filename: lmi4_action_handlers.cxx

    Description: This file contains function that registers all Action Handlers for custom DLL

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Mar-18    Megha Singh           Initial Release

========================================================================================================================================================================*/
#include <lmi4_library.hxx>
#include <lmi4_action_handlers.hxx>
#include <lmi4_constants.hxx>
#include <lmi4_workflow_handler_impl.hxx>

/**
 * Functon to register the action handlers to be used in the workflows.
 *
 * @return Status of registration of action handlers. 
 */
int LMI4_register_action_handlers
(
    void
)
{
    int iRetCode = ITK_ok;
    
    TC_write_syslog("In Rehau Custom Register Action Handlers Operation\n");

    LMI4_ITKCALL(iRetCode, EPM_register_action_handler(LMI4_WF_AH_TEST_HANDLER, "Handler For Testing purpose", LMI4_Sample_Handler));

	LMI4_ITKCALL(iRetCode, EPM_register_action_handler(LMI4_WF_AH_TEST_DATASET_HANDLER, "Handler For Attaching Dataset", LMI4_Sample_Attach_Dataset));
       
    return iRetCode;
}






























