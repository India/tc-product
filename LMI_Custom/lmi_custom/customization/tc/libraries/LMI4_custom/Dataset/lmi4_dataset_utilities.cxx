/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_dataset_utilities.cxx

    Description:  This file contains custom functions for dataset to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-Mar-18   Megha Singh         Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>
#include <lmi4_constants.hxx>

/**
 * This function takes following input arguments - name of dataset "strData", its type "strDSType", tool name "strDSTool" and reference format name "strRefFormat". It returns the tag of
 * created dataset "reftDataset". If "strDSTool" argument is empty then default tool is used to create dataset.
 *
 * @param[in]  strDataName  Name of dataset.
 * @param[in]  strDSType    Dataset type.
 * @param[in]  strDSTool    Dataset tool name.
 * @param[in]  strRefFormat Dataset reference format name
 * @param[out] reftDataset  Newly created dataset tag.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMI4_create_dataset
(
    string strDatasetName,
    string strDSType,
    string strDSTool,
    string strRefFormat,
    tag_t& reftNewDataset
)
{
    int   iRetCode     = ITK_ok;
    tag_t tDatasetType = NULLTAG;
    tag_t tTool        = NULLTAG;
   	tag_t tDataset     = NULLTAG;

    LMI4_ITKCALL(iRetCode, AE_find_datasettype2((strDSType.c_str()), &tDatasetType));
    
	LMI4_ITKCALL(iRetCode, AE_create_dataset_with_id(tDatasetType, (strDatasetName.c_str()), (strDatasetName.c_str()), 0, 0, &tDataset));
    
	if(strDSTool.size() > 0)
    {
		LMI4_ITKCALL(iRetCode, AE_find_tool2((strDSTool.c_str()), &tTool));
    }
    else
    {
		LMI4_ITKCALL(iRetCode, LMI4_get_dataset_default_tool((strDSType.c_str()), tTool));
    }

    LMI4_ITKCALL(iRetCode, AE_set_dataset_tool(tDataset, tTool));

	LMI4_ITKCALL(iRetCode, AE_set_dataset_format2(tDataset, (strRefFormat.c_str())));

    LMI4_ITKCALL(iRetCode,  AOM_save_with_extensions(tDataset));

    LMI4_ITKCALL(iRetCode, AOM_refresh(tDataset, false));

	if(iRetCode == ITK_ok && tDataset != NULLTAG)
	{
		reftNewDataset = tDataset;
	}

    return iRetCode;
}

/**
 * This function accepts dataset type "strDSType" and file type "strFileType" as input arguments and returns refrence type name "refstrDSReference" and refrence format for input dataset type.
 *
 * @param[in]  strDSType         Dataset Type.
 * @param[in]  strFileType       File type or file extension.
 * @param[out] refstrDSReference Dataset reference type name.
 * @param[out] refstrRefFormat   Dataset reference format.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMI4_get_dataset_reference_and_format
(
    string  strDSType,
    string  strFileType,
    string& refstrDSReference,
    string& refstrRefFormat
)
{
    int     iRetCode     = ITK_ok;
    tag_t   tDatasetType = NULLTAG;
    logical bFoundRef    = false;

	vector<string> vstrRefList;
	vector<string>::iterator itervRefList;
	
	LMI4_ITKCALL(iRetCode, AE_find_datasettype2((strDSType.c_str()), &tDatasetType));

	LMI4_ITKCALL(iRetCode, LMI4_ae_ask_datasettype_refs(tDatasetType, vstrRefList));	
	    
	for(itervRefList = vstrRefList.begin(); itervRefList!= vstrRefList.end() && iRetCode == ITK_ok && bFoundRef == false; itervRefList++)
    {
        logical bResult = false;
		vector<string> vstrFileType;
		vector<string> vstrRefFormats;
				
		LMI4_ITKCALL(iRetCode, LMI4_ae_ask_datasettype_file_refs(tDatasetType, (*itervRefList), vstrFileType, vstrRefFormats));

		if(iRetCode == ITK_ok && (vstrFileType.size() > 0)|| (vstrRefFormats.size() > 0))
		{
			int iFndIdx = 0;
			vector<string>::iterator itervFileTyp;

			for (itervFileTyp = vstrFileType.begin(); itervFileTyp != vstrFileType.end(); itervFileTyp++)
			{
				string strRefFor = vstrRefFormats[iFndIdx];

				iFndIdx++;
							
				LMI4_ITKCALL(iRetCode, LMI4_has_ending((*itervFileTyp), strFileType, bResult));			

				if(iRetCode == ITK_ok)
				{
					if(bResult  == true || strcmp((*itervFileTyp).c_str(), LMI4_STRING_ASTERISK) == 0)
					{
						refstrDSReference.assign(*itervFileTyp);
						refstrRefFormat.assign(strRefFor);
						bFoundRef = true;
					}
				}
			}
		}
	}
    	   
	return iRetCode;
}

/**
 * This function gets dataset type "strDSType" as input argument and returns tag of default dataset tool "reftDefTool" for the input dataset type.
 *
 * @param[in]  strDSType   Dataset Type.
 * @param[out] reftDefTool Tag of tool which is the first tool in the list.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMI4_get_dataset_default_tool
(
    string strDSType,
    tag_t& reftDefTool
)
{
    int   iRetCode     = ITK_ok;
    tag_t tDatasetType = NULLTAG;
	tag_t tDefTool     = NULLTAG;
    	
	LMI4_ITKCALL(iRetCode, AE_find_datasettype2((strDSType.c_str()), &tDatasetType));

    LMI4_ITKCALL(iRetCode, AE_ask_datasettype_def_tool(tDatasetType, &tDefTool));

	if(iRetCode == ITK_ok && tDefTool != NULLTAG)
	{
		reftDefTool = tDefTool; 
	}

    return iRetCode;
}



















