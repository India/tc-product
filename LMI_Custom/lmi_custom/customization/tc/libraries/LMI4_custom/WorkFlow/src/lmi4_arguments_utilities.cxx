/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_arguments_utilities.cxx

    Description:  This File contains functions for string parsing operations and functions for retrieving arguments passed in workflow action or rule handlers
                  These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>
#include <lmi4_errors.hxx>

/**
 * This function takes tag of task "tTask", attachment type "strAttachedObjType" as input arguments and returns list of objects "refvObject" from the input task.
 *
 * e.g. Suppose 'PartRevision' is of type 'ItemRevision'. We can pass 'strAttachedObjType' as 'PartRevision' then function will specifically return all the attachments of type 'PartRevision' only.
 *
 * @param[in]  tTask              Tag of task on which action is triggered
 * @param[in]  strAttachedObjType Type of Object.
 * @param[out] refvObject         List of tags of attached objects.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_arg_get_attached_objects_of_type
(
    tag_t          tTask,
    string         strAttachedObjType,
    vector<tag_t>& refvObject
)
{
    int   iRetCode  = ITK_ok;
    tag_t tRootTask = NULLTAG;
            
    /* Validate input */
	if((strAttachedObjType.size()) > 0)
    {
		vector<tag_t> vtObjectList;
		vector<tag_t>::iterator itervObjList;

        LMI4_ITKCALL(iRetCode, EPM_ask_root_task(tTask, &tRootTask));
          
        /* Get the target objects. */
        LMI4_ITKCALL(iRetCode, LMI4_epm_ask_attachments(tRootTask, EPM_target_attachment, vtObjectList));
		        
        /* For each target, get its type and compare it with the input type. Check if it is a WorkspaceObject and then add it to returned list of attached object tags */
		for (itervObjList = vtObjectList.begin(); itervObjList < vtObjectList.end() && iRetCode == ITK_ok; itervObjList++)
        {
            string strObjType;
            
			LMI4_ITKCALL(iRetCode, LMI4_obj_ask_type((*itervObjList), strObjType));

            if(iRetCode == ITK_ok)
            {            
                if (strAttachedObjType.compare(strObjType) == 0) 
                { 
                    int     iActiveSeq = 0;
                    logical bIsWSObj   = false;

                    LMI4_ITKCALL(iRetCode, LMI4_obj_is_type_of((*itervObjList), LMI4_CLASS_ITEMREVISION, bIsWSObj));

                    if(iRetCode == ITK_ok)
                    {
                        if(bIsWSObj == true)
                        {
						    LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_int((*itervObjList), LMI4_ATTR_ACTIVE_SEQ, iActiveSeq));
                        }
                        else
                        {
                            iActiveSeq = 1;
                        }
                    }

                    /* This check indicates that Object with Active sequence will be considered */
                    if(iActiveSeq != 0 && iRetCode == ITK_ok)
                    {
					    LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag((*itervObjList), true, refvObject));					
				    }
                }
            }
        }
    }
	   
    return iRetCode;
}

/**
 * This function returns valid list of WF task attachments(target or reference) "refvObject" based on input argument "iAttachmentType". Function will filter valid WF task attachment types based on 
 * input object type list "vstrObjTypeList".
 * 
 * @param[in]  tTask           Tag of WF task
 * @param[in]  iAttachmentType Determines the valid objects needed to be retrieved from target or Reference of input workflow task. Valid values of this parameter are 'EPM_reference_attachment'
 *                             or 'EPM_target_attachment'
 * @param[in]  vstrObjTypeList List of valid WF attachment types.
 * @param[out] refvObject      List of valid WF attachment tags.
 * 
 * @retval ITK_ok is always returned.
 */
int LMI4_arg_get_attached_obj_of_input_type_list
(
    tag_t          tTask,
    int            iAttachmentType,
    vector<string> vstrObjTypeList,
    vector<tag_t>& refvObject
)
{
    int iRetCode = ITK_ok;
       
	/* Validate input */
	if((vstrObjTypeList.size()) > 0)
    {
		tag_t tRootTask = NULLTAG;
		vector<tag_t> vtObjectList;	
	    vector<tag_t>::iterator itervObjList;
		
        LMI4_ITKCALL(iRetCode, EPM_ask_root_task(tTask, &tRootTask));
          
        /* Get the target objects. */
		LMI4_ITKCALL(iRetCode, LMI4_epm_ask_attachments(tRootTask, iAttachmentType, vtObjectList));
        
        /* Get the type of attached object and compare it from the given list. Also check if it is an WorkspaceObject and add it to returned list of attached objects. */
        
		for (itervObjList = vtObjectList.begin(); itervObjList != vtObjectList.end() && iRetCode == ITK_ok; itervObjList++)
        {
			string strObjType;
			vector<string>::iterator itervObjType;
								
            LMI4_ITKCALL(iRetCode, LMI4_obj_ask_type((*itervObjList), strObjType));

			for (itervObjType = vstrObjTypeList.begin(); itervObjType != vstrObjTypeList.end() && iRetCode == ITK_ok; itervObjType++)
			{
				if((*itervObjType).compare(strObjType) == 0)                           
                { 
                    int     iActiveSeq = 0;
                    logical bIsWSObj   = false;

                    LMI4_ITKCALL(iRetCode, LMI4_obj_is_type_of((*itervObjList), LMI4_CLASS_ITEMREVISION, bIsWSObj));

                    if(iRetCode == ITK_ok)
                    {
                        if(bIsWSObj == true)
                        {
                            LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_int((*itervObjList), LMI4_ATTR_ACTIVE_SEQ, iActiveSeq));
                        }
                        else
                        {
                            iActiveSeq = 1;
                        }
                    }

                    /* This check indicates that Object with Active sequence will be considered */
                    if(iActiveSeq != 0 && iRetCode == ITK_ok)
                    {					
						LMI4_ITKCALL(iRetCode, LMI4_vector_store_tag((*itervObjList), true, refvObject));
					}
                }				                
            }   
        }       
    }
	       
    return iRetCode;
}

/**
 * This function returns parsed list of values "refvArgValue" corresponding to input WF argument "strArgument". The values are parsed based on input separator "strSeparator". It also 
 * returns original value (with separator) "refstrOriginalArgVal" corresponding to input argument "strArgument".
 *
 * @note Example: -type=item;document \n
 *       where -type is the switch and \n
 *       item and document are the two values which appear in the array.
 *
 * @note The separator for multiple values can be semi-colon (;) or coma(,). This is generic function which takes input as character separator for parsing WF argument value.
 *       
 * @param[in]  pArgList             Argument list provided by the handler msg structure
 * @param[in]  tTask                Tag of the workflow task
 * @param[in]  strArgument          Name of the argument
 * @param[in]  strSeparator         Separator
 * @param[in]  bOptional            If FALSE the argument is required, else optional
 * @param[out] refvArgValue         List of WF argument values returned
 * @param[out] refstrOriginalArgVal Value of WF argument with separator 
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI4_arg_get_arguments
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    string              strArgument,
    string              strSeparator,
    logical             bOptional,
    vector<string>&     refvArgValue,
    string&             refstrOriginalArgVal
)
{
    /* Use TC_init_argument_list and TC_number_of_arguments to get the argument list find the right argument using ITK_ask_argument_named_value if found, split the value to an array of values using
	   the separator
    */
    int iRetCode  = ITK_ok;
    int iArgCount = 0;
    		        
    /* Validate input */
	if(strArgument.size() == 0 || strSeparator.size() == 0)
    {
        TC_write_syslog( "ERROR: Missing input data:Name of the argument or seprator is null \n");
    }
    else
    {
		logical bFoundArg = false;

        TC_init_argument_list(pArgList);

        iArgCount = TC_number_of_arguments(pArgList);

        for(int iDx = 0; iDx < iArgCount && iRetCode == ITK_ok; iDx++)
        {            
            string strNextArgument;
			string strValue;
			string strSwitch;

            strNextArgument = TC_next_argument(pArgList);

			LMI4_ITKCALL(iRetCode, LMI4_itk_ask_argument_named_value((strNextArgument.c_str()), strSwitch, strValue));  

            if(iRetCode == ITK_ok)
            {            
			    if(strSwitch.compare(strArgument) == 0)
                {				
				    refstrOriginalArgVal.assign(strValue);

                    LMI4_ITKCALL(iRetCode, LMI4_str_parse_string(strValue, strSeparator, refvArgValue));

                    if(iRetCode != ITK_ok)
                    {
                        /*Store error into error stack */
                        iRetCode = UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES;
                        EMH_store_error(EMH_severity_error, iRetCode);
                        break;
                    }
                    else
                    {
                        bFoundArg = true;
                        break;
                    }
                }
            }
        }

        /*
         * When the requested argument is not found: If bOptional flag is set to TRUE, Do Nothing. Else If bOptional flag is set to FALSE. Throw an Error.
         */
        if(bOptional == false && bFoundArg == false)
        {
            iRetCode = ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND;
            EMH_store_error(EMH_severity_error, iRetCode);
        }
    }

    return iRetCode;
}

/**
 * This function loops through the arguments given in a handler definition list and searches value of input argument. It gives back original value "refvArgValue" corresponding to the input argument
 * name "strArgument". If the value of input argument "bOptional" is "false" then argument must be present in the input argument list "pArgList" of handler otherwise the function will throw custom
 * error.
 *       
 * @param[in]  pArgList       Argument list 
 * @param[in]  tTask          Tag of the workflow task
 * @param[in]  strArgument    Name of the argument 
 * @param[in]  bOptional      If FALSE the argument is required, else optional
 * @param[out] refstrArgValue Value of input argument  
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI4_arg_get_wf_argument_value
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    string              strArgument,
    logical             bOptional,
    string&             refstrArgValue
)
{
    /* Use TC_init_argument_list and TC_number_of_arguments to get the argument list find the right argument using ITK_ask_argument_named_value if found, split the value to an array of values using
	  the separator.
    */
    int     iRetCode  = ITK_ok;
    int     iArgCount = 0;
    logical bFoundArg = false;
	 
    TC_init_argument_list(pArgList);

    iArgCount = TC_number_of_arguments(pArgList);

    for(int iDx = 0; iDx < iArgCount && iRetCode == ITK_ok; iDx++)
    {
        string strSwitch;
        string strValue;
        string strNextArg;

        strNextArg = TC_next_argument(pArgList);

		LMI4_ITKCALL(iRetCode, LMI4_itk_ask_argument_named_value((strNextArg.c_str()), strSwitch, strValue));

        if(iRetCode == ITK_ok)
        {        
		    if(strSwitch.compare(strArgument) == 0)
            {
			    bFoundArg = true;

                refstrArgValue.assign(strValue);			
            
                break;
            }
        }
    }

    /* When the requested argument is not found: If bOptional flag is set to TRUE, Do Nothing. Else If lOptional flag is set to FALSE. Throw an Error.  */
    if(bOptional == false && bFoundArg == false)
    {
        iRetCode = ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND;
        EMH_store_error(EMH_severity_error, iRetCode);
    }

	return iRetCode;
}

/**
 * This function returns original value "refstrArgValue" of input argument "strArgument". It also returns 'true' as value of "refbFoundArg" if input argument is found in argument
 * list "pArgList". If the value of input argument "bOptional" is "false" then argument must be present in the input argument list "pArgList" of handler otherwise the function will throw custom
 * error.
 *
 * @note: This function returns a logical argument that let the calling function know if argument is supplied or not. It could be possible that argument is supplied without any value and this
 *        information is needed by calling function
 *       
 * @param[in]  pArgList       Argument list provided by the handler msg structure
 * @param[in]  tTask          Task tag of the workflow task
 * @param[in]  strArgument    Name of the argument 
 * @param[in]  bOptional      If FALSE the argument is required, else optional
 * @param[out] refbFoundArg   TRUE if argument is found in the list, else FALSE
 * @param[out] refstrArgValue Value of input argument 
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI4_arg_get_wf_argument_info_and_value
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    string              strArgument,
    logical             bOptional,
    logical&            refbFoundArg,
    string&             refstrArgValue
)
{
    /* Use TC_init_argument_list and TC_number_of_arguments to get the argument list find the right argument using ITK_ask_argument_named_value if found, split the value to an array of values using 
	   the separator.
    */
    int iRetCode  = ITK_ok;
    int iArgCount = 0;
	    
    TC_init_argument_list(pArgList);

    iArgCount = TC_number_of_arguments(pArgList);

    for(int iDx = 0; iDx < iArgCount && iRetCode == ITK_ok; iDx++)
    {
        string strSwitch;
        string strValue;
        string strNextArgument;

        strNextArgument = TC_next_argument(pArgList);

		LMI4_ITKCALL(iRetCode, LMI4_itk_ask_argument_named_value((strNextArgument.c_str()), strSwitch, strValue));

        if(iRetCode == ITK_ok)
        {
		    if(strSwitch.compare(strArgument) == 0)
            {
			    refbFoundArg = true;

                refstrArgValue.assign(strValue);
            
                break;
            }
        }
    }

    /* When the requested argument is not found: If bOptional flag is set to TRUE, Do Nothing. Else If bOptional flag is set to FALSE. Throw an Error. */
    if(bOptional == false && refbFoundArg == false)
    {
        iRetCode = ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND;
        EMH_store_error(EMH_severity_error, iRetCode);
    }

    return iRetCode;
}

/**
 * This function checks input string "strAttachmentType" for value "TARGET" or "REFERENCE" and sets the return argument "refiAttachmentType" to value "EPM_target_attachment" or 
 * "EPM_reference_attachment". If input string "strAttachmentType" has any other value apart from "TARGET" or "REFERENCE" then default value of "refiAttachmentType" will be "EPM_target_attachment".
 *       
 * @param[in]  strAttachmentType  Attachment type 
 * @param[out] refiAttachmentType Attachment type integer macro as defined in TC ITK API.
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI4_arg_get_wf_attachment_type
(
    string strAttachmentType,
    int&   refiAttachmentType  
)
{
    int iRetCode = ITK_ok;

	if(tc_strcasecmp((strAttachmentType.c_str()), LMI4_WF_ARG_VAL_TARGET) == 0)
    {
        refiAttachmentType = EPM_target_attachment;
    }
    else
    {
		if(tc_strcasecmp((strAttachmentType.c_str()), LMI4_WF_ARG_VAL_REFERENCE) == 0)
        {
            refiAttachmentType = EPM_reference_attachment;
        }
        else
        {
            refiAttachmentType = EPM_target_attachment;
        }
    }

    return iRetCode;
}





























