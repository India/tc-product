/*======================================================================================================================================================================
                                                    Copyright 2017  LMtec 	India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_bomline_utilities.cxx

    Description: This file contains function that provides various information about BOM Line. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
21-Mar-18    Megha Singh   Initial Release

========================================================================================================================================================================*/

#include <lmi4_constants.hxx>
#include <lmi4_errors.hxx>
#include <lmi4_library.hxx>

/**
 * This function creates a new BOM window and applies input Revision Rule to the window. All Items in the BOM will automatically be reconfigured using the new Rule. 
 * Function also sets the top line of the BOM window to contain the input Item Revision.
 *
 * @note The function uses error protect mark to protects the current state of the error store. BOM commands clears the error stack which result in loss of previous errors.
*        In order to avoid loss of error function uses protect mark commands.
 *
 * @param[in]  tItemRev        Tag of input Item revision.
 * @param[in]  pszRevisionRule Revision rule used to configure the
 * @param[in]  pszViewType     Type of "BOM View"
 * @param[out] reftBOMwindow   Tag of the newly created window.
 * @param[out] reftTopLine     Tag of the top line of the BOM window to contain the input Item Revision
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_bomline_get_top_line
(
    tag_t  tItemRev,
    string strRevisionRule,
    string strViewType,
    tag_t& reftBOMWindow,
    tag_t& reftTopLine
)
{
    int   iRetCode     = ITK_ok;
    int   iMark        = 0;
    tag_t tTopLine     = NULLTAG;
    tag_t tBOMViewType = NULLTAG;
	tag_t tBOMWind     = NULLTAG;
	
    /* Set error protect mark to protects the current state of the error store. BOM commands clears the error
       stack which result in loss of previous errors. In order to avoid loss of error use protect mark commands.
    */
    EMH_set_protect_mark(&iMark);

    LMI4_ITKCALL(iRetCode, LMI4_bom_get_tag_to_view_type(tItemRev, (strViewType.c_str()), tBOMViewType));
    
    if(tBOMViewType != NULLTAG)
    {
        LMI4_ITKCALL(iRetCode, BOM_create_window(&tBOMWind));
       
        LMI4_ITKCALL(iRetCode, LMI4_bom_apply_config_rule(tBOMWind, strRevisionRule));
        
        LMI4_ITKCALL(iRetCode, BOM_set_window_top_line(tBOMWind, NULLTAG, tItemRev, tBOMViewType, &tTopLine));
       
        /* Do a refresh of the BOM window. The content of the structure might have been changed by a different process. */
        LMI4_ITKCALL(iRetCode, BOM_refresh_window(tBOMWind));
    }
    else
    {
        string strObjStr = "";

        LMI4_ITKCALL(iRetCode, LMI4_aom_ask_value_string(tItemRev, LMI4_ATTR_OBJECT_STRING_ATTRIBUTE, strObjStr));

        TC_write_syslog("View of type %s is not attached to the %s.\n", strViewType, strObjStr);       
    }

	if(iRetCode == ITK_ok)
	{
		if(tBOMWind != NULL)
		{
			reftBOMWindow = tBOMWind;
		}

		if(tTopLine != NULL)
		{
			reftTopLine = tTopLine; 
		}
	}

    EMH_clear_protect_mark(iMark);

    return iRetCode;
}

/**
 * This function retrives the Parent Item and Item Revision from the current BOMLine.
 *
 * @param[in]  tBOMLine          Current BOMLine
 * @param[out] reftParentBOMLine Tag of parent BOM Line
 * @param[out] reftParentItem    Tag of parent item
 * @param[out] reftParentItemRev Tag of parent item reivsion
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_bomline_get_parent_bomline_item_and_revision
(
    tag_t  tBOMLine,
    tag_t& reftParentBOMLine,
    tag_t& reftParentItem,
    tag_t& reftParentItemRev
)
{
    int    iRetCode     = ITK_ok;
    int   iAttrRevId    = 0;
	tag_t tParentBMLine = NULLTAG;
	tag_t tParItemRev   = NULLTAG;
	tag_t tParItem      = NULLTAG;

    /* This function returns the Id of the property named "Parent" which is representing the immediate parent BOMLine.
     * Macro "bomAttr_lineParentTag" represents string for the Parent BOMLine.
     */
    LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineParentTag, &iAttrRevId));

    /* We will get the tag of the Immediate Parent BOMLine */
    LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBOMLine, iAttrRevId, &tParentBMLine));
    /* Reset the variable. */
    iAttrRevId = 0;
    LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId));
     
    LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tParentBMLine, iAttrRevId, &tParItemRev));
     
    LMI4_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tParItemRev, &tParItem));

	if(iRetCode == ITK_ok)
	{
		if(tParItemRev != NULLTAG)
		{
			reftParentItemRev = tParItemRev;
		}

		if(tParItem != NULLTAG)
		{
			reftParentItem = tParItem;
		}
	}
     
    return iRetCode;
}

/**
 * This function retrieves Item and Item Revision tags  from the current BOM Line.
 *
 * @param[in]  tBOMLine    Current BOM Line
 * @param[out] reftItem    Tag of Item of current BOM Line
 * @param[out] reftItemRev Tag of Item Revision at current BOM Line
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_bomline_get_item_and_revision_from_bomline
(
    tag_t  tBOMLine,
    tag_t& reftItem,
    tag_t& reftAttrItemRev
)
{
    int   iRetCode   = ITK_ok;
    int   iAttrRevId = 0;
	tag_t tItemRev   = NULLTAG;
	tag_t tItem      = NULLTAG; 
   
    LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId));

    LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBOMLine, iAttrRevId, &tItemRev));

    LMI4_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));
    
	if(iRetCode == ITK_ok)
	{
		if(tItemRev != NULLTAG)
		{
			reftAttrItemRev = tItemRev;
		}

		if(tItem != NULLTAG)
		{
			reftItem = tItem;
		}
	}

    return iRetCode;
}

/**
 * This function retrieves the attribute value of the input attribute name from input BOM Line.
 *
 * @param[in]  tBOMLine           BOM line tag.
 * @param[in]  strAttrName        Name of  BOM line attribute whose value is to be retrieved.
 * @param[out] refstrAttributeVal Value of input attribute as string.
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_bomline_get_attribute_val
(
    tag_t   tBOMLine,
    string  strAttrName,
    string& refstrAttributeVal
)
{
    int    iRetCode     = ITK_ok;
    int	   iMode        = 0;
    int    iAttributeId = 0;
	string strName      = "";
    
	LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute((strAttrName.c_str()), &iAttributeId));
   
    LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_mode(iAttributeId, &iMode));

    switch(iMode)
    {		
        case BOM_attribute_mode_string :
        {
            LMI4_ITKCALL(iRetCode, LMI4_bom_line_ask_attribute_string(tBOMLine, iAttributeId, strName));

			refstrAttributeVal.append(MEM_sprintf("%s", (strName.c_str) == NULL ? "" : (strName.c_str)));
            break;
        }
        case BOM_attribute_mode_logical :
        {
            logical bYesNo;

            //refstrAttributeVal = (char*) MEM_alloc(sizeof (char) * LMI4_STRING_ATTR_LEN);

            LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_logical(tBOMLine, iAttributeId, &bYesNo));

			bYesNo ? sprintf((refstrAttributeVal.c_str), "%s", LMI4_SMALL_YES) : sprintf((refstrAttributeVal.c_str), "%s", LMI4_SMALL_NO);
            break;
        }
        case BOM_attribute_mode_int :
        {
            int iValue = 0;

            LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_int(tBOMLine, iAttributeId, &iValue));

            refstrAttributeVal = MEM_sprintf("%d", iValue);
            break;
        }
        case BOM_attribute_mode_tag :
        {
            tag_t  tValue       = NULLTAG ;
            string strStringVal = "";

            LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBOMLine, iAttributeId, &tValue));

            if(tValue != NULLTAG)
			
				string strStringVal = "";
				
				iRetCode = LMI4_aom_tag_to_string(tValue, strStringVal); 

			refstrAttributeVal = MEM_sprintf("%s", (strStringVal.c_str()) == NULL ? "" : (strStringVal.c_str()));

            break;
        }
        case BOM_attribute_mode_double :
        {
            double dValue = 0.0;

            LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_double(tBOMLine, iAttributeId, &dValue));

            refstrAttributeVal = MEM_sprintf("%f", dValue);
            break;
        }
        default :
        {
            refstrAttributeVal = MEM_sprintf ("%s", "");
        }
    }
	    
    return iRetCode;
}

/**
 * This function sets the value of input BOM Line attribute 'pszAttrName'.
 *
 * @param[in] tBOMLine        BOM line tag.
 * @param[in] strAttrName     Name of  BOM line attribute whose value is to be set.
 * @param[in] strAttributeVal Value of input attribute as string to be set.
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_bomline_set_attribute_val
(
	tag_t  tBOMLine,
	string strAttrName,
	string strAttributeVal
)
{
    int iRetCode     = ITK_ok;
    int	iMode        = 0;
    int iAttributeId = 0;

    LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute((strAttrName.c_str()), &iAttributeId));

    LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_mode(iAttributeId, &iMode));

    switch( iMode )
    {
        case BOM_attribute_mode_string :
        {
			LMI4_ITKCALL(iRetCode, BOM_line_set_attribute_string(tBOMLine, iAttributeId, (strAttributeVal.c_str())));
            break;
        }
        case BOM_attribute_mode_logical :
        {
            logical bYesNo = false;

			if (tc_strcasecmp((strAttributeVal.c_str()), LMI4_SMALL_YES) == 0 )
            {
                bYesNo = true;
            }

            LMI4_ITKCALL(iRetCode, BOM_line_set_attribute_logical(tBOMLine, iAttributeId, bYesNo));
            break;
        }
        case BOM_attribute_mode_int :
        {
            int iValue = 0;

			sscanf((strAttributeVal.c_str()), "%d", &iValue);

            LMI4_ITKCALL(iRetCode, BOM_line_set_attribute_int(tBOMLine, iAttributeId, iValue));
            break;
        }
        case BOM_attribute_mode_double :
        {
            float dValue = 0;

			sscanf((strAttributeVal.c_str()), "%f", &dValue);

            LMI4_ITKCALL(iRetCode, BOM_line_set_attribute_double(tBOMLine, iAttributeId, dValue));
            break;
        }
    }

    return iRetCode;
}
/**
 * This function check the active sequence of BOM Line Item Revision.
 *
 * @param[in]  tBOMLine           Input BOM Line
 * @param[out] reftItem           Tag of Item of Input BOM Line
 * @param[out] reftItemRev        Tag of Item Revision of Input BOM Line
 * @param[out] refbIsIRSeqCorrect 'true' if BL Item revision has valid active sequence.
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_bomline_check_ir_active_seq
(
    tag_t    tBOMLine,
    tag_t&   reftItem,
    tag_t&   reftItemRev,
    logical& refbIsIRSeqCorrect
)
{
    int   iRetCode   = ITK_ok;
    int   iActiveSeq = 0;
    int   iAttrRevId = 0;
	tag_t tItemRev   = NULLTAG; 
	tag_t tItem      = NULLTAG;
   
    LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId));
       
    LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBOMLine, iAttrRevId, &tItemRev));

    LMI4_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));
       
    LMI4_ITKCALL(iRetCode, AOM_ask_value_int(tItemRev, LMI4_ATTR_ACTIVE_SEQ, &iActiveSeq));

    if(iActiveSeq != 0 && iRetCode == ITK_ok)
    {
        refbIsIRSeqCorrect = true;
    }
    else
    {
        refbIsIRSeqCorrect = false;
    }

	if(iRetCode == ITK_ok)
	{
		if(tItem != NULLTAG)
		{
			reftItem = tItem;
		}

		if(tItemRev != NULLTAG)
		{
			reftItemRev = tItemRev;
		}
	}
     
    return iRetCode;
}

/**
 * This function gets the BOM Line quantity information.
 *
 * @param[in]  tBOMLine     Input BOM Line
 * @param[in]  bDefQtyPref  If value of Preference 'TC_NX_occ_default_qty_prevent_from_blank' is set to 'true'
 * @param[out] refiQuantity Quantity information of input BOM Line
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_bomline_get_int_qty
(
    tag_t   tBOMLine,
    logical bDefQtyPref,
    int&    refiQuantity
)
{
    int    iRetCode   = ITK_ok;
    int    iAttrQtyID = 0;
    int    iQtyVal    = 0;
    string strQty     = "";

    LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_occQty, &iAttrQtyID));

    LMI4_ITKCALL(iRetCode, LMI4_bom_line_ask_attribute_string(tBOMLine, iAttrQtyID, strQty));

    if(iRetCode == ITK_ok)
    {
        iQtyVal = atoi((strQty.c_str()));

        if(bDefQtyPref == true && iQtyVal == 0)
        {
            refiQuantity = 1;
        }
        else
        {
            refiQuantity = iQtyVal;
        }
    }
    
    return iRetCode;
}

/**
 * This function retrieves tag to the Top Line of configured BOM Window from the input BOM Line. If input BOM Line is the Top Line function returns value 
 * of 'pbInputLineIsTopLine' as 'true' else 'false'.
 *
 * @param[in]  tBOMLine              Tag to BOM Line through which Top Line is to be found.
 * @param[out] reftWindowTopBL       Tag to Window Top BOM Line
 * @param[out] refbInputBLIsTopLine 'true' if Input BOM Line is Top Line else 'false'
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMI4_bomline_get_window_top_line
( 
    tag_t    tBOMLine,
    tag_t&   reftWindowTopBL,
    logical& refbInputBLIsTopLine
)
{
    int iRetCode = ITK_ok;
	
    if(tBOMLine != NULLTAG)
    {    
        int   iAttrRevId1 = 0;
        int   iAttrRevId2 = 0;
		tag_t tWindow     = NULLTAG;
		tag_t tTopBOMLine = NULLTAG;
        tag_t tTopLineIR  = NULLTAG; 
        tag_t tBLItemRev  = NULLTAG;

        LMI4_ITKCALL(iRetCode, BOM_line_ask_window(tBOMLine, &tWindow));

        LMI4_ITKCALL(iRetCode, BOM_ask_window_top_line(tWindow, &tTopBOMLine));

        LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId1));     
        /* Get Window Top Line Item Revision */
        LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tTopBOMLine, iAttrRevId1, &tTopLineIR));

        LMI4_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId2));
        /* Get Input BOM Line Item Revision */
        LMI4_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBOMLine, iAttrRevId2, &tBLItemRev));

        if(tBLItemRev == tTopLineIR)
        {
            refbInputBLIsTopLine = true;
        }
        else
        {
            refbInputBLIsTopLine = false;
        }

		if(iRetCode == ITK_ok && tTopBOMLine != NULLTAG)
		{
			reftWindowTopBL  = tTopBOMLine;
		}
    }

    return iRetCode;
}







