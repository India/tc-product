/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_string_utilities.cxx

    Description:  This File contains functions for string parsing operations and functions for retrieving arguments passed in workflow action or rule handlers
                  These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh    Initial Release

========================================================================================================================================================================*/

#include <lmi4_library.hxx>
#include <lmi4_errors.hxx>

/**
 * This function takes source string "strVal" and end sub string "strEnd" as input parameters and returns "true" if input sub string is found at end of input source string else it returns "false".
 *
 * @param[in]  strSrc     Source string
 * @param[in]  strEnd     Sub string
 * @param[out] refbResult 'true' if sub string found at end of input source string.
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI4_has_ending
(
	string   strSrc,
	string   strEnd,
	logical& refbResult
)
{
	int iRetCode = ITK_ok;
	int iSrcLen  = 0;
	int iEndLen  = 0;
	string strCheck;

	iSrcLen = (int)strSrc.length();
	iEndLen = (int)strEnd.length();

	if(iSrcLen > 0 && iEndLen > 0)
	{
		if (iSrcLen >= iEndLen) 
		{
			int iStartIndex = iSrcLen - iEndLen;
		
			strCheck = strSrc.substr(iStartIndex, iSrcLen);
		
			if(strCheck.compare(strEnd) == 0)
			{			
				refbResult = true;
			}
			else
			{
				refbResult = false;
			}
		}
	}

	return iRetCode;
}

/**
 * This function takes string "strInput" and separator "strSeparator" as input arguments .It parses string 'strInput' by separator 'strSeparator' and returns a vector containing parsed values. 
 *
 * @param[in]  strInput      String to be parsed
 * @param[in]  strSeparator  Separator 
 * @param[out] refvValuelist List of parsed values
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI4_str_parse_string
(
    string          strInput,
    string          strSeparator,
    vector<string>& refvValueList
)
{
    int    iRetCode    = ITK_ok;
	
     /* Validate input */
    if(strInput.size() == 0 || strSeparator.size() == 0)
    {
        TC_write_syslog( "ERROR: Missing input data: List passed or seprator is null\n");
    }
    else
    {
		string strToken;
		size_t pos = 0;
    
		while((pos = strInput.find(strSeparator)) != string::npos) 
		{
			strToken = strInput.substr(0, pos);
        
			LMI4_ITKCALL(iRetCode, LMI4_vector_store_string(strToken, false, refvValueList));

			strInput.erase(0, pos + strSeparator.length());
		}

		LMI4_ITKCALL(iRetCode, LMI4_vector_store_string(strInput, false, refvValueList));
	}
		
	return iRetCode;
}


/**
 * This function returns the substring when provided with start and end position on the the input string. It takes actual string "strInputString", position of starting "iStrStartPosition" and ending
 * "iStrEndPosition" character for substring as input parameters and returns the substring "refstrCopyedStr". 
 *
 * For example If 'pszInputString' = 'Application'
 *                'iStrStartPosition' = 4
 *                'iStrEndPosition'   = 9
 *             then Output Will be: 'licati'
 *
 * Function will copy the characters starting 4th character to the 9th character of input string.
 * 
 * @param[in]  strInputString    Input string
 * @param[in]  iStrStartPosition Position of starting character for the substring.
 * @param[in]  iStrEndPosition   Position of last character for the substring.
 * @param[out] refstrCopyedStr   Copied string.
 *
 * @retval INVALID_INPUT_TO_FUNCTION if input not correct
 * @retval ITK_ok on successful execution
 */
int LMI4_str_copy_substring
(
    string  strInput,
    int     iStrStartPosition,
    int     iStrEndPosition,
    string& refstrCopyedStr
)
{
    int    iRetCode     = ITK_ok;
    int    iStrLen      = 0;
	string strObtString;
	    
	iStrLen = (int)(strInput.length());
    
	if (strInput.size() > 0 && iStrStartPosition >= 0 && iStrEndPosition <=  iStrLen && iStrStartPosition <= iStrEndPosition)
    {
		strObtString = strInput.substr(iStrStartPosition, iStrEndPosition); 

		if(strObtString.size() > 0)
		{
			refstrCopyedStr.assign(strObtString);
		}
		else
		{
			refstrCopyedStr = "";
		}            
    }    
    else
    {
        iRetCode = INVALID_INPUT_TO_FUNCTION;
        EMH_store_error(EMH_severity_error, iRetCode);
    }

    return iRetCode;
}


/**
 * This function gets string to looked in list "strVal", list of strings "vstrValue" as input arguments and  the output argument "refbFound" returns "true" if input string is found in list otherwise it 
 * it returns "false".
 *
 * @param[in]  strVal     Input string 
 * @param[in]  vstrVals   List of strings 
 * @param[out] refbFound  'true' if input string 'pszStrVal' exist in list of strings 
 *
 * @return ITK_ok if string is found in the array else return ITK_ok
 */
int LMI4_contains_string_value
(
    string         strInput, 
    vector<string> vstrValues,
    logical&       refbFound
)
{
    int iRetCode = ITK_ok;

	refbFound = (find(vstrValues.begin(), vstrValues.end(), strInput) != vstrValues.end());
	        
    return iRetCode;
}

/**
 * This function gets input arguments "strInput" the source string, "strSub" the string to be searched  in source string  and "strReplacement" the string to replace substring in source string and returns
 * "refstrNew" source string containing replaced substring.
 *
 * @note: if sub string does not exist as part of input string then new string will have value same as input string
 *
 * For e.g.       'pszInputStr'       = 'Function Example'
 *                'pszSubstring'      = 'ion'
 *                'pszReplacementStr' = 'x1x'
 *                Output Will be: 'Functx1x Example'
 * 
 *                'pszInputStr'       = 'Fun'
 *                'pszSubstring'      = 'IN'
 *                'pszReplacementStr' = 'x1x'
 *                Output Will be: 'Fun'
 *
 *                'pszInputStr'       = 'Fun'
 *                'pszSubstring'      = 'Fun'
 *                'pszReplacementStr' = 'x1x'
 *                Output Will be: 'x1x'
 *
 * @param[in]  strInput       Source string 
 * @param[in]  strSub         Sub String (Part of source string)
 * @param[in]  strReplacement Replacement string 
 * @param[out] refstrNew      New string.
 *
 * @return ITK_ok if string is found in the array else return ITK_ok
 */
int LMI4_replace_sub_string
(
    string  strInput, 
    string  strSub,
    string  strReplacement,
    string& refstrNew
)
{
    int iRetCode = ITK_ok;
    
    /* Validate input */
	if (strInput.size() == 0 || strSub.size() == 0 || strReplacement.size() == 0)
    {
        iRetCode = INVALID_INPUT_TO_FUNCTION;
        EMH_store_error(EMH_severity_error, iRetCode);
    }
    else
    {
        if(strInput.length() > strSub.length())
        {
			size_t nPos = strInput.find(strSub, 0); // first occurrence

			if(nPos != 0)
			{           
				while(nPos != string::npos)
				{				
					strInput.replace(nPos, strSub.length(), strReplacement);

					nPos = strInput.find(strSub, nPos + 1);
				}

				refstrNew.assign(strInput);
			}
			else if(nPos = 0 && strInput.compare(strSub) != 0 )
			{
				refstrNew.assign(strInput);
			}
		}
		else 
		{
			if(strInput.compare(strSub) == 0)
			{
				refstrNew.assign(strReplacement);
			}
			else
			{
				refstrNew.assign(strInput);
			}

		}
	}	

	return iRetCode;
}		
   
/**
 * This function takes source string "strInput" and substring "strSub" to be searched in source string as input arguments. It returns "true" as the value of "refbSubFound" if source string contains
 * sub-string otherwise it returns "false".
 *
 * @param[in]  strInput     Source string
 * @param[in]  strSub       Sub String
 * @param[out] refbSubFound 'true' if input string contains Sub String else 'false'
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_string_find_sub_str
(
    string   strInput,
    string   strSub,
    logical& refbSubFound
)
{
    int    iRetCode = ITK_ok;
    size_t Found    = 0;
	
	if(strInput.size() > strSub.size())
	{
		Found = strInput.find(strSub);
		
		if (Found != string::npos)
		{
			refbSubFound = true;
		}        
		else
		{
			refbSubFound = false;        
		}
	}

    return iRetCode;
}

/**
 * This function returns the remaining string "refstrTrimmed" after splitting the input string "strInput" based on very first occurrence of input Delimiter character "szDelim". This function will return. 
 * For e.g. 
 * If input string is "ENT_01-Test_Data-1" and then the output string will be "Test_Data-1"
 *
 * @param[in]  strInput      String to be checked for Delimiter occurrence
 * @param[in]  szDelim       Delimiter
 * @param[out] refstrTrimmed Trimmed String
 *
 * @return 
 */
int LMI4_get_str_after_delimiter
(
    string     strInput,
    const char szDelim, 
    string&    refstrTrimmed
)
{
    int    iRetCode   = ITK_ok;
    string strTrimStr;
	
	strTrimStr = strInput.substr(strInput.find(szDelim) + 1);
   
    if(strTrimStr.size() > 0)
    {   
        refstrTrimmed.assign(strTrimStr);
    }

    return iRetCode;
}

/**
 * This function checks if input source string "strSrc" starts with input start string "strStart" and returns "true" as value of "refbIsMatch" is input source string starts with start strings otherwise it 
 * it returns "false". It also returns "true" as the value of "refbInvalidArg" if input source string is NULL otherwise it returns "false".
 *
 * @param[in]  strSrc         Source string
 * @param[in]  strStart       String to match
 * @param[out] refbIsMatch    'true' if input string starts with given start string else 'False'
 * @param[out] refbInvalidArg 'true' if input string arguments are NULL
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_string_start_with
(
    string   strSrc,
    string   strStart,
    logical& refbIsMatch,
    logical& refbInvalidArg
)
{
    int    iRetCode = ITK_ok;
	string strCheck;
		
	if (strSrc.length() >= strStart.length()) 
	{				
		strCheck = strSrc.substr(0, strStart.length());
		
		if(strCheck.compare(strStart) == 0)
		{			
			refbIsMatch = true;
		}
		else
		{
			refbIsMatch = false;
		}
	}   
    else
    {
        refbInvalidArg = true;
    }

    return iRetCode;
}

/**
 * This function checks if input source string "strSrc" ends with input end string 'strEndStr' and returns "true" as the value of "pbIsMatch" if input source string ends with input end string
 * otherwise it retuns "false". It also returns "true" as the value of "pbInvalidArg" if the input source string is NULL else it returns "false".
 *
 * @param[in]  strSrc       Source string
 * @param[in]  strEnd       String to match from the end of the string
 * @param[out] pbIsMatch    'true' if input string ends with input end string "pszEndStr" else 'false'
 * @param[out] pbInvalidArg 'true' if input string arguments are NULL
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_string_end_with
(
    string   strSrc,
    string   strEnd,
    logical& refbIsMatch,
    logical& refbInvalidArg
)
{
	int    iRetCode = ITK_ok;
	string strCheck;
		
	if (strSrc.length() >= strEnd.length()) 
	{
		int iStartIndex = (int)(strSrc.length() - strEnd.length());

		/*size_t nPos = strSrcStr.find(strEndStr);

		strCheck = strSrcStr.substr(nPos, strEndStr.length()); */
		
		strCheck = strSrc.substr(iStartIndex, strEnd.length());
		
		if(strCheck.compare(strEnd) == 0)
		{			
			refbIsMatch = true;
		}
		else
		{
			refbIsMatch = false;
		}
	}   
    else
    {
        refbInvalidArg = true;
    }

	return iRetCode;
}

/**
 * This function takes source string "strSrc" and substring "strSub" as input arguments and returns count of occurrences "refiSubCnt" of sub-string in source string. It also returns "true"
 * as the value of "refbBadSrcOrSub" if input are NULL or invalid.
 *
 * @param[in]  strSrc          Source string
 * @param[in]  strSub          Sub-String to match
 * @param[out] refiSubCnt      Count of occurrences of sub-string in source string
 * @param[out] refbBadSrcOrSub 'true' if input string "pszSrcStr" or "pszSubStr" are NULL or length of sub string is more than source string
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_get_count_of_sub_str_in_src_str
(
	string   strSrc,
	string   strSub,
    int&     refiSubCnt,
    logical& refbBadSrcOrSub
)
{
    int iRetCode = ITK_ok;
		    
	if(strSrc.length() > 0 && strSub.length() > 0)
    {
		if(strSrc.length() > strSub.length())
        {
			size_t nPos = strSrc.find(strSub, 0); // first occurrence
           
			while(nPos != string::npos)
			{
				refiSubCnt++;

				nPos = strSrc.find(strSub, nPos + 1);
			}
		}
		else
		{
			refbBadSrcOrSub = true;
		}
	}
    else
    {
        refbBadSrcOrSub = true;        
    }

    return iRetCode;
}

/**
 * This function gets source string "strSrc", start sub string "strSubAtStart" and end sub string "strSubAtEnd" as input parameters. It returns "refstrPrefix" a prefix string present before input start sub
 * string, returns "refstrInfx" string present in between input first start sub string and input last end sub string. It also returns "refstrPostfix" a postfix string present after input end sub string
 * strSubAtEnd.
 *
 * @param[in]  strSrc        Input string
 * @param[in]  strSubAtStart Sub String at start of main string
 * @param[in]  strSubAtEnd   Sub String at end of main string
 * @param[out] refstrPrefix  String which is present in the main string before the occurrence of start sub string "pszSubStrAtStart"
 * @param[out] refstrInfix   String which is present in the main string between the occurrence of first start sub string "pszSubStrAtStart" and last end sub string "pszSubStrAtEnd".
 * @param[out] refstrPostfix String which is present in the main string after the occurrence of last end sub string "pszSubStrAtEnd" if "pszSubStrAtEnd" is not NULL or string after sub string
 *                           "pszSubStrAtStart" if "pszSubStrAtEnd" is not NULL.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_get_prefix_infix_postfix
(
    string  strSrc,
    string  strSubAtStart,
    string  strSubAtEnd,
    string& refstrPrefix,
    string& refstrInfix,
    string& refstrPostfix
)
{
    int iRetCode   = ITK_ok;
    int iSrcStrLen = 0;

	if(strSrc.size() > 0)
    {       
		if(strSubAtStart.size() > 0)
        {
			size_t nStartPos = strSrc.find(strSubAtStart, 0); 

			size_t nEndPos = strSrc.rfind(strSubAtEnd);
						
			if((int)nStartPos != -1 && (int)nEndPos != -1 && nStartPos != 0 && nEndPos != 0)
			{
				refstrPrefix.assign(strSrc.substr(0, nStartPos));

				refstrPostfix.assign(strSrc.substr((nEndPos + strSubAtEnd.length()), (strSrc.length())));

				refstrInfix.assign(strSrc.substr((nStartPos + strSubAtStart.length()), (nEndPos - (refstrPrefix.length() + strSubAtStart.length()))));
			}
			else if(nStartPos == -1 && (int)nEndPos == -1)
			{
				refstrPostfix.assign(strSrc);

				refstrPrefix.assign(strSrc);

				refstrInfix.assign(strSrc);
			}
			else if(nStartPos == 0)
			{
				if((int)nEndPos != -1 && nEndPos != (strSrc.length() - strSubAtEnd.length()))
				{
					refstrPrefix.assign("");

					refstrInfix.assign(strSrc.substr(nStartPos + strSubAtStart.length(), ((strSrc.length() - strSubAtEnd.length()))));

					refstrPostfix.assign(strSrc.substr(nEndPos + strSubAtEnd.length(), strSrc.length()));
				}
				else if((int)nEndPos == -1)
				{
					refstrPrefix.assign("");

					refstrInfix.assign(strSrc.substr(nStartPos + strSubAtStart.length(), (strSrc.length() - strSubAtStart.length())));

					refstrPostfix.assign("");
				}
				else
				{
					refstrPrefix.assign("");

					refstrInfix.assign(strSrc.substr(nStartPos + strSubAtStart.length(), ((strSrc.length() - (strSubAtEnd.length() + strSubAtStart.length())))));

					refstrPostfix.assign("");
				}
			}
			else if((int)nStartPos == -1 && (int)nEndPos != -1)
			{
				if(nEndPos != (strSrc.length() - strSubAtEnd.length()))
				{
					refstrPrefix.assign(strSrc.substr(0, nEndPos));

					refstrInfix.assign(strSrc.substr(0, nEndPos));

					refstrPostfix.assign(strSrc.substr(nEndPos + strSubAtEnd.length(), strSrc.length()));
				}
				else
				{
					refstrPrefix.assign(strSrc.substr(0, nEndPos));

					refstrInfix.assign(strSrc.substr(0, nEndPos));

					refstrPostfix.assign("");
				}
			}
			else if ((int)nEndPos == -1 && (int)nStartPos != -1)
			{
				if(nStartPos == 0)
				{
					refstrPrefix.assign("");

					refstrInfix.assign(strSrc.substr(0, strSrc.length()));

					refstrPostfix.assign(strSrc.substr(0, strSrc.length()));
				}
				else
				{
					refstrPrefix.assign(strSrc.substr(0, nStartPos));

					refstrInfix.assign(strSrc.substr(nStartPos + strSubAtStart.length(), (strSrc.length() - strSubAtEnd.length())));

					refstrPostfix.assign(strSrc.substr(nStartPos + strSubAtStart.length(), (strSrc.length() - strSubAtEnd.length())));
				}
			}
		}
	}		  
    
    return iRetCode;
}

/**
 * This function has source string "strSrc" and sub string "strSub" as input parameters. It returns "refstrPrefix" as prefix string and "refstrPostfix" as postfix strings at the end of input source string.
 *
 * @param[in]  strSrc        Source string
 * @param[in]  strSub        Sub string 
 * @param[out] refstrPrefix  Prefix string before appearance of sub-string "pszSubStr" at the end of the string
 * @param[out] refstrPostfix Postfix string after appearance of sub-string "pszSubStr" at the end of the string
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI4_get_prefix_and_postfix_from_end
(
    string  strSrc,
    string  strSub, 
    string& refstrPrefix,
    string& refstrPostfix
)
{
    int   iRetCode   = ITK_ok;
    
	if(strSrc.length() > 0 && strSub.length() > 0 && strSrc.length() > strSub.length())
    {
		size_t nPos = strSrc.rfind(strSub);

		if((int)nPos != -1 && nPos != 0)
		{
			if(nPos != (strSrc.length() - strSub.length()))
			{
				refstrPrefix.assign(strSrc.substr(0, nPos));

				refstrPostfix.assign(strSrc.substr((nPos + strSub.length()), (strSrc.length() - (nPos + strSub.length())))); 
			}
			else
			{
				refstrPrefix.assign(strSrc.substr(0, nPos));
			}
		}
		else if(nPos == 0)
		{						
			refstrPostfix.assign(strSrc.substr(strSub.length(), strSrc.length()));				
		}		
		else
		{
			refstrPrefix.assign(strSrc.substr(0, strSrc.length()));

			refstrPostfix.assign(strSrc.substr(0, strSrc.length()));
		}
	}
	
    return iRetCode;
}

/**
 * This function takes source string "strSrc", start string "strSart" as input parameters. The output parameter "refbIsMatch" returns 'true' if input source string starts with input start string
 * otherwise it returns 'false'. The outparameter "refstrPost" returns postfix string in input source string. Also "refbInvalidArg" returns "true" if input arguments are NULL.
 *
 * @param[in]  strSrc         Source string
 * @param[in]  strStart       String to match
 * @param[out] refbbIsMatch   'true' if input string starts with given start string else 'False'
 * @param[out] refstrPost     Remaining string of source string succeeding 'pszStartString'.
 * @param[out] refbInvalidArg 'true' if input string arguments are NULL
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_string_start_with_2
(
    string   strSrc,
    string   strStart,
    logical& refbIsMatch,
    string&  refstrPost,
    logical& refbInvalidArg
)
{
    int iRetCode = ITK_ok;
	
	if (strSrc.length() > 0 && strStart.length() > 0)
    {        
		if(strSrc.length() > strStart.length())
        {
			logical bIsStartMatch      = false;
			logical bIsStartInvalidArg = false;
			
			LMI4_ITKCALL(iRetCode, LMI4_string_start_with(strSrc, strStart, bIsStartMatch, bIsStartInvalidArg));

			if(bIsStartMatch == true && bIsStartInvalidArg == false)
			{
				refbIsMatch = true;

				strSrc.erase(0, strStart.length());

				refstrPost.assign(strSrc);
			}
			else
			{
				refbInvalidArg = true;        
			}
		}
		else
		{
			refbInvalidArg = true;
		}
	}
	else
	{
		refbInvalidArg = true;
	}
	
	return iRetCode;
}

/**
 * This function takes source string "strSrc" as input argument and returns copy of input source string "refstrWithoutSpaces" without any spaces.
 *
 * @param[in]  strSrc              Source string
 * @param[out] refstrWithoutSpaces Copy of source string without any spaces.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_string_strip_blanks
(
    string   strSrc,
    string&  refstrWithoutSpaces,
    logical& refbIsEmpty
)
{
    int iRetCode = ITK_ok;
    
	if(strSrc.length() > 0)
    {
         strSrc.erase(remove(strSrc.begin(), strSrc.end(), ' '), strSrc.end());
       
		 refstrWithoutSpaces.assign(strSrc);
    }
    else
    {
        refbIsEmpty = true;
    }

    return iRetCode;
}

/**
 * This function takes source string "strSrc", start string "strStart" and end string "strEnd" as input parameters. It return input source string truncated with start and end string "refstrTruncated"
 * and logical value "refbFound" containing value "true" if input source string contains start and end strings otherwise it returns "false".
 *
 * @param[in]  strSrc          Source string
 * @param[in]  strStart        Start string
 * @param[in]  strEnd          End string
 * @param[out] refstrTruncated String constructed after truncating start and end string
 * @param[out] refbFound       "true" if source string contains start and end string else "false"  
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMI4_truncate_start_and_end_string
(
    string   strSrc,
    string   strStart,
    string   strEnd,
    string&  refstrTruncated,
    logical& refbFound
)
{
    int iRetCode     = ITK_ok;
    
	if(strSrc.length() > 0 && strSrc.length() > (strStart.length() + strEnd.length()))
    {
		logical bIsStartMatch      = false;
		logical bIsStartInvalidArg = false;
		logical bIsEndMatch        = false;
		logical bIsEndInvalidArg   = false;
	
		LMI4_ITKCALL(iRetCode, LMI4_string_start_with(strSrc, strStart, bIsStartMatch, bIsStartInvalidArg));
				
		LMI4_ITKCALL(iRetCode, LMI4_string_end_with(strSrc, strEnd, bIsEndMatch, bIsEndInvalidArg));

		if(bIsStartMatch == true && bIsEndMatch == true && bIsStartInvalidArg == false && bIsEndInvalidArg == false)
		{
			refbFound = true;

			strSrc.erase(0, strStart.length());

			strSrc.erase((strSrc.length() - strEnd.length()), strSrc.length()); 

			refstrTruncated.assign(strSrc);
		}
        else
        {
            refbFound = false;
        }        
    }
	else
	{
		refbFound = false;
	}

    return iRetCode;
}

/**
 * This function takes source string "strVal" as input parameter and returns integer converted input source string. It also returns "true" as value of "refbIsInt" if input string is a potential integer
 * else it returns "false".
 *
 * @param[in]  strVal       Source string
 * @param[out] refiIntValue Converted Integer value
 * @param[out] refbIsInt    'true' if the input string is a potential Integer
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI4_string_is_Integer
(
    string   strVal,
	int&     refiIntValue, 
    logical& refbIsInt
)
{
    int iRetCode = ITK_ok;
   
	if(strVal.length() == 0 )
	{
		refbIsInt = false ;
	}
	else
	{
		size_t pszEnd = 0;
	  
		int iValue = stoi(strVal, &pszEnd) ;

	   if (iValue == 0)
	   {
			refbIsInt = false;

			refiIntValue = iValue;
		}
	   else
	   {
		   refbIsInt = true;
	   }
    }

    return iRetCode;
}

/**
 * This function takes source string as input parameter and return "true" as value of "refbIsDouble" if input source string is a potential string else it returns "false".
 *
 * @param[in]  strVal           Source string 
 * @param[out] refdDoubleValue  Converted Double value
 * @param[out] refbIsDouble    'true' if the input string is a potential Double value
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI4_string_is_Double
(
    string   strVal,
	double&  refdDoubleValue,
    logical& refbIsDouble
)
{
    int    iRetCode = ITK_ok;
    char*  pszEnd   = NULL;
	double dValue   = 0;
	
	if(strVal.length() > 0)
    {
		dValue = strtod(strVal.c_str(), &pszEnd);

		if(pszEnd == strVal.c_str() || *pszEnd != NULL)
		{
			refbIsDouble = true;

			refdDoubleValue = dValue;
		}
		else
		{
			refbIsDouble = false;
		}            
    }

    return iRetCode;
}

/**
 * This function takes source string "strVal" as input parameter. It returns input source string converted to integer "refiIntVal" and also returns "true" as value of "refbIsInteger" if input source
 * can be converted to an Integer value else it returns "false". 
 *
 * @param[in]  strVal      Source string
 * @param[out] refiIntVal  Converted integer value
 * @param[out] refbIsInt   'true' if the input string is a potential Integer

 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI4_convert_string_is_Integer
(
    string   strVal,
    int&     refiIntVal,
    logical& refbIsInt
)
{
    int iRetCode = ITK_ok;
	
	LMI4_ITKCALL(iRetCode, LMI4_string_is_Integer(strVal, refiIntVal, refbIsInt));

    return iRetCode;
}

/**
 * This function takes source string "strVal" as input parameter and returns input source string converted to double . It also returns "true" if it can be converted to an Double value else it returns 
 * "false".
 *
 * @param[in]  strVal     Source string
 * @param[out] refdDoubleVal Converted Double value
 * @param[out] refbIsDouble  'true' if the input string is a potential Double value

 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LMI4_convert_string_is_Double
(
    string   strVal,
    double&  refdDoubleVal,
    logical& refbIsDouble
)
{
    int iRetCode = ITK_ok;
    
	LMI4_ITKCALL(iRetCode, LMI4_string_is_Double(strVal, refdDoubleVal, refbIsDouble));

    return iRetCode;
}

/**
 * This function takes obejct count "iObjCnt" and object tags array "ptObject" as input parameters. It returns list of unique object tags "refvtObject".
 *
 * @param[in]  iObjCnt     Object count
 * @param[in]  ptObject    Array of object tags
 * @param[out] refvtObject Vector containing unique list of tags
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI4_stl_copy_tag_array_to_vector
(
    int            iObjCnt,
    tag_t*         ptObject,
    vector<tag_t>& refvtObject
)
{
    int iRetCode = ITK_ok;
	vector<tag_t>::iterator iterInfo;

    for(int iDx = 0; iDx < iObjCnt; iDx++)
    {
        iterInfo = find(refvtObject.begin(), refvtObject.end(), (ptObject[iDx]));
        
        if(iterInfo == refvtObject.end())
        {
            refvtObject.push_back(ptObject[iDx]);
        }
    }

    return iRetCode;
}

/**
 * This function converts the input integer value 'iValue' to its corresponding string value 'refstrConverted'.
 *
 * @param[in]  iValue         Input integer value
 * @param[out] refvtConverted Returns corresponding string of input integer value.
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI4_int_to_string
(
	int     iValue,
	string& refstrConverted
)
{
	int iRetCode = ITK_ok;

	if(iValue > 0)
	{
		refstrConverted.assign(to_string(iValue));
	}

	return iRetCode;
}

/**
 * This function converts the input double value 'dValue' to its corresponding string value 'refstrConverted'.
 *
 * @param[in]  dValue          Input double value
 * @param[out] refstrConverted Returns corresponding string of input integer value.
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI4_double_to_string
(
	double  dValue,
	string& refstrConverted
)
{
	int iRetCode = ITK_ok;

	if(dValue > 0)
	{
		refstrConverted.assign(to_string(dValue));
	}

	return iRetCode;
}

/**
 * This function converts the input logical value 'bValue' to its corresponding string value 'refstrConverted'.
 *
 * @param[in]  bValue          Input logical value
 * @param[out] refstrConverted Returns corresponding string of input integer value.
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI4_logical_to_string
(
	logical bValue,
	string& refstrConverted
)
{
	int iRetCode = ITK_ok;

	if(bValue == false)
	{
		refstrConverted.assign("false");
	}
	else
	{
		refstrConverted.assign("true");
	}

	return iRetCode;
}

/**
 * This function converts the input string Value 'strValue' to its corresponding logical value 'refbConverted'.
 *
 * @param[in]  bValue          Input logical value
 * @param[out] refstrConverted Returns corresponding string of input integer value.
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LMI4_string_to_logical
(
	string   strValue,
	logical& refbConverted
)
{
	int iRetCode = ITK_ok;

	if(strcmp (strValue.c_str(), "false") == 0)
	{
		refbConverted = false;
	}
	else
	{
		refbConverted = true;
	}

	return iRetCode;
}

























