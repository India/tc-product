/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmi4_string_macros.hxx

    Description:  This file contains macros for string's basic operation such as copy, concatination etc.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Mar-18    Megha Singh          Initial Release

========================================================================================================================================================================*/
#ifndef LMI4_STRING_MACROS_HXX
#define LMI4_STRING_MACROS_HXX

#include <lmi4_library.hxx>

#ifdef LMI4_STRCPY
#undef LMI4_STRCPY
#endif
#define LMI4_STRCPY(pszDestStr, pszSourceStr){\
                    int iSourceStrLen = (int) (tc_strlen(pszSourceStr)+1);\
                    pszDestStr = (char*)MEM_alloc((int)(iSourceStrLen * sizeof(char)));\
                    tc_strcpy(pszDestStr, pszSourceStr);\
}

#ifdef LMI4_STRCAT
#undef LMI4_STRCAT
#endif
#define LMI4_STRCAT(pszDestStr,pszSourceStr){\
                    int iSourceStrLen = (int) tc_strlen(pszSourceStr);\
                    int iDestStrLen = 0;\
                    if(pszDestStr != NULL){\
                        iDestStrLen = (int) tc_strlen (pszDestStr);\
                        pszDestStr = (char*)MEM_realloc(pszDestStr, (int)((iSourceStrLen + iDestStrLen + 1)* sizeof(char)));\
                    }\
                    else{\
                        pszDestStr = (char*)MEM_alloc((int)(sizeof(char) * (iSourceStrLen + 1)));\
                    }\
                    if(iDestStrLen == 0){\
                       tc_strcpy(pszDestStr, pszSourceStr);\
                    }\
                    else{\
                       tc_strcat(pszDestStr, pszSourceStr);\
                       tc_strcat(pszDestStr, '\0');\
                    }\
}

/**
 * This function adds input new string to the String Array. This also increases the value of the variable holding the 
 * original size of array
 *  
 */
#ifdef LMI4_UPDATE_STRING_ARRAY
#undef LMI4_UPDATE_STRING_ARRAY
#endif
#define LMI4_UPDATE_STRING_ARRAY(iCurrentArrayLen, ppszStrArray, pszNewValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ppszStrArray = (char**) MEM_alloc(iCurrentArrayLen * sizeof(char*));\
            }\
            else{\
                ppszStrArray = (char**) MEM_realloc(ppszStrArray, iCurrentArrayLen * sizeof(char*));\
            }\
            ppszStrArray[iCurrentArrayLen - 1] = (char*) MEM_alloc((int)((tc_strlen(pszNewValue) + 1)* sizeof(char)));\
            tc_strcpy(ppszStrArray[iCurrentArrayLen - 1], pszNewValue);\
}

#endif //A4_STRING_MACROS_HXX





