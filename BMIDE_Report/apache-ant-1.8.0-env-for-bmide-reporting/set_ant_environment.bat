@echo off
echo WARNING: do not run this script from an existing shell, run it directly
pause

rem Set ANT_HOME environment variable, make sure that the folder Utilities is mapped correctly
set ANT_HOME=Z:\Utilities\apache-ant-1.8.0

if not exist %ANT_HOME% (
    echo ERROR: the directory %ANT_HOME% does not exist
    goto finish
)
rem set system environment variable
setx ANT_HOME %ANT_HOME% /m

echo updating path variable
echo.

rem old path, e.g. C:\ProgramData\Oracle\Java\javapath;%SystemRoot%\system32;%SystemRoot%;%SystemRoot%\System32\Wbem;%SYSTEMROOT%\System32\WindowsPowerShell\v1.0\;C:\Program Files (x86)\Microsoft SQL Server\110\Tools\Binn\;C:\Program Files\Microsoft SQL Server\110\Tools\Binn\;C:\Program Files\Microsoft SQL Server\110\DTS\Binn\;C:\Program Files (x86)\Microsoft SQL Server\110\Tools\Binn\ManagementStudio\;C:\Program Files (x86)\Microsoft SQL Server\110\DTS\Binn\;C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\PrivateAssemblies\
echo retrieving old path: %path%
echo.

set NEW_PATH=%path%;%ANT_HOME%\bin
echo setting new path: %NEW_PATH%

setx path "%NEW_PATH%" /m

echo.
echo INFO: Environment variables successfully set.
pause
goto :eof

:finish
echo.
pause