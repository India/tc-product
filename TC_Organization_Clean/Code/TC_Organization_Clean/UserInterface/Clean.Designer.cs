﻿namespace TC_Organization_Clean.UserInterface
{
    partial class Clean
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btSubmit = new System.Windows.Forms.Button();
            this.lbHeader = new System.Windows.Forms.Label();
            this.lbOutFile = new System.Windows.Forms.Label();
            this.tbOutFilePath = new System.Windows.Forms.TextBox();
            this.btnOutBrowse = new System.Windows.Forms.Button();
            this.lbInpFilePath = new System.Windows.Forms.Label();
            this.tbInpFile = new System.Windows.Forms.TextBox();
            this.btnInpBrowse = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.lbOutpurFileName = new System.Windows.Forms.Label();
            this.tbOutFileName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btSubmit
            // 
            this.btSubmit.Location = new System.Drawing.Point(345, 243);
            this.btSubmit.Name = "btSubmit";
            this.btSubmit.Size = new System.Drawing.Size(75, 29);
            this.btSubmit.TabIndex = 5;
            this.btSubmit.Text = "Submit";
            this.btSubmit.UseVisualStyleBackColor = true;
            this.btSubmit.Click += new System.EventHandler(this.btSubmit_Click);
            // 
            // lbHeader
            // 
            this.lbHeader.AutoSize = true;
            this.lbHeader.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.lbHeader.Location = new System.Drawing.Point(31, 33);
            this.lbHeader.Name = "lbHeader";
            this.lbHeader.Size = new System.Drawing.Size(151, 17);
            this.lbHeader.TabIndex = 9;
            this.lbHeader.Text = "TC Organization Clean";
            // 
            // lbOutFile
            // 
            this.lbOutFile.AutoSize = true;
            this.lbOutFile.Location = new System.Drawing.Point(31, 136);
            this.lbOutFile.Name = "lbOutFile";
            this.lbOutFile.Size = new System.Drawing.Size(110, 17);
            this.lbOutFile.TabIndex = 39;
            this.lbOutFile.Text = "Output File Path";
            // 
            // tbOutFilePath
            // 
            this.tbOutFilePath.Location = new System.Drawing.Point(172, 137);
            this.tbOutFilePath.Name = "tbOutFilePath";
            this.tbOutFilePath.Size = new System.Drawing.Size(153, 22);
            this.tbOutFilePath.TabIndex = 3;
            // 
            // btnOutBrowse
            // 
            this.btnOutBrowse.Location = new System.Drawing.Point(345, 136);
            this.btnOutBrowse.Name = "btnOutBrowse";
            this.btnOutBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnOutBrowse.TabIndex = 4;
            this.btnOutBrowse.Text = "Browse";
            this.btnOutBrowse.UseVisualStyleBackColor = true;
            this.btnOutBrowse.Click += new System.EventHandler(this.btnOutBrowse_Click);
            // 
            // lbInpFilePath
            // 
            this.lbInpFilePath.AutoSize = true;
            this.lbInpFilePath.Location = new System.Drawing.Point(31, 67);
            this.lbInpFilePath.Name = "lbInpFilePath";
            this.lbInpFilePath.Size = new System.Drawing.Size(98, 17);
            this.lbInpFilePath.TabIndex = 42;
            this.lbInpFilePath.Text = "Input File Path";
            // 
            // tbInpFile
            // 
            this.tbInpFile.Location = new System.Drawing.Point(172, 70);
            this.tbInpFile.Name = "tbInpFile";
            this.tbInpFile.Size = new System.Drawing.Size(153, 22);
            this.tbInpFile.TabIndex = 1;
            // 
            // btnInpBrowse
            // 
            this.btnInpBrowse.Location = new System.Drawing.Point(345, 67);
            this.btnInpBrowse.Name = "btnInpBrowse";
            this.btnInpBrowse.Size = new System.Drawing.Size(75, 25);
            this.btnInpBrowse.TabIndex = 2;
            this.btnInpBrowse.Text = "Browse";
            this.btnInpBrowse.UseVisualStyleBackColor = true;
            this.btnInpBrowse.Click += new System.EventHandler(this.btnInpBrowse_Click);
            // 
            // btCancel
            // 
            this.btCancel.Location = new System.Drawing.Point(239, 240);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 34);
            this.btCancel.TabIndex = 6;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // lbOutpurFileName
            // 
            this.lbOutpurFileName.AutoSize = true;
            this.lbOutpurFileName.Location = new System.Drawing.Point(34, 192);
            this.lbOutpurFileName.Name = "lbOutpurFileName";
            this.lbOutpurFileName.Size = new System.Drawing.Size(118, 17);
            this.lbOutpurFileName.TabIndex = 43;
            this.lbOutpurFileName.Text = "Output File Name";
            // 
            // tbOutFileName
            // 
            this.tbOutFileName.Location = new System.Drawing.Point(172, 192);
            this.tbOutFileName.Name = "tbOutFileName";
            this.tbOutFileName.Size = new System.Drawing.Size(153, 22);
            this.tbOutFileName.TabIndex = 44;
            // 
            // Clean
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 299);
            this.Controls.Add(this.tbOutFileName);
            this.Controls.Add(this.lbOutpurFileName);
            this.Controls.Add(this.btnInpBrowse);
            this.Controls.Add(this.tbInpFile);
            this.Controls.Add(this.lbInpFilePath);
            this.Controls.Add(this.btnOutBrowse);
            this.Controls.Add(this.tbOutFilePath);
            this.Controls.Add(this.lbOutFile);
            this.Controls.Add(this.lbHeader);
            this.Controls.Add(this.btSubmit);
            this.Controls.Add(this.btCancel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Clean";
            this.Text = "LMtec India Consulting Services Pvt. Ltd.";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSubmit;
        private System.Windows.Forms.Label lbHeader;
        private System.Windows.Forms.Label lbOutFile;
        private System.Windows.Forms.TextBox tbOutFilePath;
        private System.Windows.Forms.Button btnOutBrowse;
        private System.Windows.Forms.Label lbInpFilePath;
        private System.Windows.Forms.TextBox tbInpFile;
        private System.Windows.Forms.Button btnInpBrowse;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Label lbOutpurFileName;
        private System.Windows.Forms.TextBox tbOutFileName;
    }
}

