﻿namespace TC_Organization_Clean.UserInterface
{
    partial class Export
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbHeader = new System.Windows.Forms.Label();
            this.lbTCRoot = new System.Windows.Forms.Label();
            this.lbUsername = new System.Windows.Forms.Label();
            this.lbPassword = new System.Windows.Forms.Label();
            this.lbClass = new System.Windows.Forms.Label();
            this.lbOutFile = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.tbOutFilePath = new System.Windows.Forms.TextBox();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.tbTCRoot = new System.Windows.Forms.TextBox();
            this.btnTCRoot = new System.Windows.Forms.Button();
            this.btnOutBrowse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbTCData = new System.Windows.Forms.TextBox();
            this.btbBrowseTCData = new System.Windows.Forms.Button();
            this.tbClass = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbHeader
            // 
            this.lbHeader.AutoSize = true;
            this.lbHeader.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.lbHeader.Location = new System.Drawing.Point(30, 36);
            this.lbHeader.Name = "lbHeader";
            this.lbHeader.Size = new System.Drawing.Size(213, 17);
            this.lbHeader.TabIndex = 11;
            this.lbHeader.Text = "Export Teamcenter Organisation";
            // 
            // lbTCRoot
            // 
            this.lbTCRoot.AutoSize = true;
            this.lbTCRoot.Location = new System.Drawing.Point(30, 78);
            this.lbTCRoot.Name = "lbTCRoot";
            this.lbTCRoot.Size = new System.Drawing.Size(93, 17);
            this.lbTCRoot.TabIndex = 12;
            this.lbTCRoot.Text = "TC Root Path";
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Location = new System.Drawing.Point(33, 169);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(73, 17);
            this.lbUsername.TabIndex = 13;
            this.lbUsername.Text = "Username";
            // 
            // lbPassword
            // 
            this.lbPassword.AutoSize = true;
            this.lbPassword.Location = new System.Drawing.Point(33, 228);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(76, 17);
            this.lbPassword.TabIndex = 14;
            this.lbPassword.Text = "Passsword";
            // 
            // lbClass
            // 
            this.lbClass.AutoSize = true;
            this.lbClass.Location = new System.Drawing.Point(30, 290);
            this.lbClass.Name = "lbClass";
            this.lbClass.Size = new System.Drawing.Size(42, 17);
            this.lbClass.TabIndex = 15;
            this.lbClass.Text = "Class";
            // 
            // lbOutFile
            // 
            this.lbOutFile.AutoSize = true;
            this.lbOutFile.Location = new System.Drawing.Point(30, 370);
            this.lbOutFile.Name = "lbOutFile";
            this.lbOutFile.Size = new System.Drawing.Size(110, 17);
            this.lbOutFile.TabIndex = 16;
            this.lbOutFile.Text = "Output File Path";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(243, 433);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 35);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(351, 433);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 35);
            this.btnSubmit.TabIndex = 10;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(161, 225);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(153, 22);
            this.tbPassword.TabIndex = 6;
            // 
            // tbOutFilePath
            // 
            this.tbOutFilePath.Location = new System.Drawing.Point(165, 365);
            this.tbOutFilePath.Name = "tbOutFilePath";
            this.tbOutFilePath.Size = new System.Drawing.Size(153, 22);
            this.tbOutFilePath.TabIndex = 8;
            // 
            // tbUsername
            // 
            this.tbUsername.Location = new System.Drawing.Point(161, 166);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(153, 22);
            this.tbUsername.TabIndex = 5;
            // 
            // tbTCRoot
            // 
            this.tbTCRoot.Location = new System.Drawing.Point(161, 75);
            this.tbTCRoot.Name = "tbTCRoot";
            this.tbTCRoot.Size = new System.Drawing.Size(153, 22);
            this.tbTCRoot.TabIndex = 1;
            // 
            // btnTCRoot
            // 
            this.btnTCRoot.Location = new System.Drawing.Point(351, 72);
            this.btnTCRoot.Name = "btnTCRoot";
            this.btnTCRoot.Size = new System.Drawing.Size(75, 23);
            this.btnTCRoot.TabIndex = 2;
            this.btnTCRoot.Text = "Browse";
            this.btnTCRoot.UseVisualStyleBackColor = true;
            this.btnTCRoot.Click += new System.EventHandler(this.btnTCRoot_Click);
            // 
            // btnOutBrowse
            // 
            this.btnOutBrowse.Location = new System.Drawing.Point(351, 364);
            this.btnOutBrowse.Name = "btnOutBrowse";
            this.btnOutBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnOutBrowse.TabIndex = 9;
            this.btnOutBrowse.Text = "Browse";
            this.btnOutBrowse.UseVisualStyleBackColor = true;
            this.btnOutBrowse.Click += new System.EventHandler(this.btnOutBrowse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 17);
            this.label1.TabIndex = 28;
            this.label1.Text = "TC Data Path";
            // 
            // tbTCData
            // 
            this.tbTCData.Location = new System.Drawing.Point(161, 118);
            this.tbTCData.Name = "tbTCData";
            this.tbTCData.Size = new System.Drawing.Size(153, 22);
            this.tbTCData.TabIndex = 3;
            // 
            // btbBrowseTCData
            // 
            this.btbBrowseTCData.Location = new System.Drawing.Point(351, 117);
            this.btbBrowseTCData.Name = "btbBrowseTCData";
            this.btbBrowseTCData.Size = new System.Drawing.Size(75, 23);
            this.btbBrowseTCData.TabIndex = 4;
            this.btbBrowseTCData.Text = "Browse";
            this.btbBrowseTCData.UseVisualStyleBackColor = true;
            this.btbBrowseTCData.Click += new System.EventHandler(this.btbBrowseTCData_Click);
            // 
            // tbClass
            // 
            this.tbClass.Location = new System.Drawing.Point(161, 290);
            this.tbClass.Name = "tbClass";
            this.tbClass.Size = new System.Drawing.Size(153, 22);
            this.tbClass.TabIndex = 7;
            this.tbClass.Text = "Group";
            // 
            // Export
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 497);
            this.Controls.Add(this.tbClass);
            this.Controls.Add(this.btbBrowseTCData);
            this.Controls.Add(this.tbTCData);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOutBrowse);
            this.Controls.Add(this.btnTCRoot);
            this.Controls.Add(this.tbTCRoot);
            this.Controls.Add(this.tbUsername);
            this.Controls.Add(this.tbOutFilePath);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lbOutFile);
            this.Controls.Add(this.lbClass);
            this.Controls.Add(this.lbPassword);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.lbTCRoot);
            this.Controls.Add(this.lbHeader);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Export";
            this.Text = "LMtec India Consulting Services Pvt. Ltd.";
            this.Load += new System.EventHandler(this.Export_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbHeader;
        private System.Windows.Forms.Label lbTCRoot;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.Label lbClass;
        private System.Windows.Forms.Label lbOutFile;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox tbOutFilePath;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.TextBox tbTCRoot;
        private System.Windows.Forms.Button btnTCRoot;
        private System.Windows.Forms.Button btnOutBrowse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbTCData;
        private System.Windows.Forms.Button btbBrowseTCData;
        private System.Windows.Forms.TextBox tbClass;
    }
}