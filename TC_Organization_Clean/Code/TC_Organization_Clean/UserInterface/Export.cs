﻿/*======================================================================================================================================================================
                Copyright 2018  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: Export.cs

    Description: This file is contains UI handlers for export dialog.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-August-2018      Singh	Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using TC_Organization_Clean.ProcessingEngine;
using TC_Organization_Clean.Beans;
using TC_Organization_Clean.Requests;
using TC_Organization_Clean.Utilities;

namespace TC_Organization_Clean.UserInterface
{
    public partial class Export : Form
    {
        public static Export stExport = null;
        FileNameUtils objFnameUtil = new FileNameUtils();

        public Export()
        {
            InitializeComponent();
        }

        /*  This function is called when form is loaded */
        private void Export_Load(object sender, EventArgs e)
        {
            stExport = this;
            
            populateTCRootPath();
            
            populateTCDataPath();
        }

        /*  Cancel Button handler */
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        /*  Submit button Action */
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            vaildateDataAndProceed();
        }

        /*  This function is called when user click on TC Root Browse button */
        private void btnTCRoot_Click(object sender, EventArgs e)
        {
            string strPath = "";
            FolderBrowserDialog fbDialog = new FolderBrowserDialog();

            /*  Removing make new folder button from the directory browse button */
            fbDialog.ShowNewFolderButton = false;

            try
            {
                /*  Getting the output file location and setting it to Text Box */
                if (fbDialog.ShowDialog() == DialogResult.OK)
                {

                    strPath = fbDialog.SelectedPath;

                    if (strPath != null && strPath.Length > 0)
                    {
                        tbTCRoot.Clear();

                        tbTCRoot.Text = strPath;
                    }
                }
            }
            catch (Exception exception)
            {
            }
        }

       /*  This function sets the value in TC Root Path Text box, it checks for the value in TC_ROOT environent variable,
        *  If TC_ROOT environment variable is set then it prepopulates the value in TC_ROOT text box.
        */
        public void populateTCRootPath()
        {
            string strPath = "";

            EnvironmentUtils eUtils = new EnvironmentUtils();

            try
            {
                strPath = eUtils.getEnvVariable(Constants.TC_ROOT);
            }
            catch
            {
            }

            /*  Checking if there is something which exists in TC_ROOT text box, if yes then it does not poulates the value in text box */
            if (tbTCRoot.Text != null && tbTCRoot.Text.Length <= 0)
            {
                tbTCRoot.Text = strPath;
            }
        }

        /*  This Function is called when user clicks browse button of TC_Data */
        private void btbBrowseTCData_Click(object sender, EventArgs e)
        {
            string strPath = "";
            FolderBrowserDialog fbDialog = new FolderBrowserDialog();

            /*  Removing make new folder button from the directory browse button */
            fbDialog.ShowNewFolderButton = false;

            try
            {
                /*  Getting the output file location and setting it to Text Box */
                if (fbDialog.ShowDialog() == DialogResult.OK)
                {

                    strPath = fbDialog.SelectedPath;

                    if (strPath != null && strPath.Length > 0)
                    {
                        tbTCData.Clear();

                        tbTCData.Text = strPath;
                    }
                }
            }
            catch (Exception exception)
            {
            }

        }

       /*  This function sets the value in TC Root Path Text box, it checks for the value in TC_ROOT environent variable,
        *  If TC_ROOT environment variable is set then it prepopulates the value in TC_ROOT text box.
        */
        public void populateTCDataPath()
        {
            string strPath = "";

            EnvironmentUtils eUtils = new EnvironmentUtils();

            try
            {
                strPath = eUtils.getEnvVariable(Constants.TC_DATA);
            }
            catch
            {
            }

            /*  Checking if there is something which exists in TC_ROOT text box, if yes then it does not poulates the value in text box */
            if (tbTCData.Text != null && tbTCData.Text.Length <= 0)
            {
                tbTCData.Text = strPath;
            }
        }

        /*  This function is called when User Clicks in Browse button of Output file path */
        private void btnOutBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbDialog = new FolderBrowserDialog();

            string strPath = "";

            try
            {
                /*  Getting the output file location and setting it to Text Box */
                if (fbDialog.ShowDialog() == DialogResult.OK)
                {
                    tbOutFilePath.Clear();

                    strPath = fbDialog.SelectedPath;

                    if (strPath != null && strPath.Length > 0)
                    {
                        tbOutFilePath.Text = strPath;
                    }

                 }
            }
            catch (Exception exception)
            {
            }
        }
        /*  This function validates Input Data and proceeds for export operation */
        public void vaildateDataAndProceed()
        {
            Boolean isValid = true;

            if (tbTCRoot.Text == null || tbTCRoot.Text.Length <= 0 ||
               tbTCData.Text == null || tbTCData.Text.Length <= 0 ||
               tbUsername.Text == null || tbUsername.Text.Length <= 0 ||
               tbPassword.Text == null || tbPassword.Text.Length <= 0 ||
               tbClass.Text == null || tbClass.Text.Length <= 0 ||
               tbOutFilePath.Text == null || tbOutFilePath.Text.Length <= 0)
            {
                MessageBox.Show(Constants.INP_PARAMETER, Constants.INFO);
            }
            else
            {
                try
                {
                    ExportBean expBean = new ExportBean();

                    /*  Filling Data in Bean */
                    expBean.strTCRoot = tbTCRoot.Text;
                    expBean.strTCData = tbTCData.Text;
                    expBean.strUsername = tbUsername.Text;
                    expBean.strPassword = tbPassword.Text;
                    expBean.strClass = tbClass.Text;
                    expBean.strOutFilePath = tbOutFilePath.Text;

                    /*  Starting Progress bar for performing operation background */
                    ProgressBar pBar = new ProgressBar(expBean, Constants.REQ_EXPORT);

                    pBar.ShowDialog();
                }
                catch (Exception exception)
                {
                    if (exception.Message != null && exception.Message.Length > 0)
                    {
                        MessageBox.Show(exception.Message, Constants.ERROR);
                    }
                }
            }
        }

        /*  Handling response */
        public void handleResponseEvent(int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_EXPORT:
                        handleExportRespEvent();
                        break;
                 }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void handleExportRespEvent()
        {
            stExport.Dispose();

            if (AppDelegate.strOutputString != null && AppDelegate.strOutputString.Length > 0)
            {
                string strFilePath = AppDelegate.strOutPutLogFileDir;
                
                if (AppDelegate.strOutPutLogFileDir != null && AppDelegate.strOutPutLogFileDir.Length > 0)
                {

                    /*  Creating Output Log File */
                    strFilePath = strFilePath + objFnameUtil.constructGenericFileNames(Constants.LOG_FILE_NAME, Constants.FEX_LOG);
                    
                    File.Create(strFilePath).Dispose();

                    TextWriter tw = new StreamWriter(strFilePath);

                    tw.WriteLine(AppDelegate.strOutputString);
                    
                    tw.Close();
                }

                /* Showing result in App Delegate */
                MessageBox.Show(AppDelegate.strOutputString + Constants.LOG_FILE_LOCATION + strFilePath, Constants.INFO);
            }
        }
    }
}






