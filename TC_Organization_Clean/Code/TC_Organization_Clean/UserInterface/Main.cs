﻿/*======================================================================================================================================================================
                Copyright 2018  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: Main.cs

    Description: This file is entry point to entire application.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-August-2018      Singh	Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TC_Organization_Clean.ProcessingEngine;
using TC_Organization_Clean.Beans;
using TC_Organization_Clean.ValidationEngine;

namespace TC_Organization_Clean.UserInterface
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.CenterScreen;
        }

        /*  Cancel button action */
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        /*  Submit Button Action */
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string strInpText = "";
            strInpText = cbExport.Text;

            MainBean mBean = new MainBean();

            mBean.strOperationType = strInpText;

            try
            {
                ValidateMainForm validateMain = new ValidateMainForm();

                validateMain.validateMainForm(mBean);

                showOperationDialog(strInpText);

            }
            catch (Exception exception)
            {
                if (exception.Message != null && exception.Message.Length > 0)
                {
                    MessageBox.Show(exception.Message, Constants.INFO);
                }
            }
        }

        public void showOperationDialog(String strOperation)
        {

            if (strOperation.CompareTo(Constants.OP_IMPORT) == 0)
            {
                /*  Importing Organisation */
                Import imp = new Import();

                imp.StartPosition = FormStartPosition.CenterScreen;

                imp.ShowDialog();
            }
            else if (strOperation.CompareTo(Constants.OP_CLEAN) == 0)
            {
                /* Opening Clean Dialog */
                Clean clean = new Clean();

                clean.StartPosition = FormStartPosition.CenterScreen;

                clean.ShowDialog();
            }
            else if (strOperation.CompareTo(Constants.OP_EXPORT) == 0)
            {
                /* Opening Export Dialog */
                Export exp = new Export();

                exp.StartPosition = FormStartPosition.CenterScreen;

                exp.ShowDialog();
            }
        }
    }
}



