﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using TC_Organization_Clean.ProcessingEngine;
using TC_Organization_Clean.Beans;
using TC_Organization_Clean.Utilities;

namespace TC_Organization_Clean.UserInterface
{
    public partial class Import : Form
    {
        public static Import stImport = null;
        FileNameUtils objFnameUtil = new FileNameUtils();

        public Import()
        {
            InitializeComponent();
        }
        
        /*  This function is called when form is loaded */
        private void Import_Load(object sender, EventArgs e)
        {
            stImport = this;
            
            populateTCRootPath();

            populateTCDataPath();
        }

        /*  This function sets the value in TC Root Path Text box, it checks for the value in TC_ROOT environent variable,
        *  If TC_ROOT environment variable is set then it prepopulates the value in TC_ROOT text box.
        */
        public void populateTCRootPath()
        {
            string strPath = "";

            EnvironmentUtils eUtils = new EnvironmentUtils();

            try
            {
                strPath = eUtils.getEnvVariable(Constants.TC_ROOT);

                /*  Checking if there is something which exists in TC_ROOT text box, if yes then it does not poulates the value in text box */
                if (tbTCRoot.Text != null && tbTCRoot.Text.Length <= 0)
                {
                    tbTCRoot.Text = strPath;
                }
            }
            catch
            {
            }
        }

        /*  TC Root button browse click */
        private void btnTCRoot_Click(object sender, EventArgs e)
        {
            string strPath = "";
            FolderBrowserDialog fbDialog = new FolderBrowserDialog();

            /*  Removing make new folder button from the directory browse button */
            fbDialog.ShowNewFolderButton = false;

            try
            {
                /*  Getting the output file location and setting it to Text Box */
                if (fbDialog.ShowDialog() == DialogResult.OK)
                {

                    strPath = fbDialog.SelectedPath;

                    if (strPath != null && strPath.Length > 0)
                    {
                        tbTCRoot.Clear();

                        tbTCRoot.Text = strPath;
                    }
                }
            }
            catch (Exception exception)
            {
            }

        }

        /*  This function sets the value in TC Root Path Text box, it checks for the value in TC_ROOT environent variable,
        *  If TC_ROOT environment variable is set then it prepopulates the value in TC_ROOT text box.
        */
        public void populateTCDataPath()
        {
            string strPath = "";

            EnvironmentUtils eUtils = new EnvironmentUtils();

            try
            {
                strPath = eUtils.getEnvVariable(Constants.TC_DATA);
            }
            catch
            {
            }

            /*  Checking if there is something which exists in TC_ROOT text box, if yes then it does not poulates the value in text box */
            if (tbTCData.Text != null && tbTCData.Text.Length <= 0)
            {
                tbTCData.Text = strPath;
            }
        }

        /*  TC_Data Browse button click */
        private void btbBrowseTCData_Click(object sender, EventArgs e)
        {
            string strPath = "";
            FolderBrowserDialog fbDialog = new FolderBrowserDialog();

            /*  Removing make new folder button from the directory browse button */
            fbDialog.ShowNewFolderButton = false;

            try
            {
                /*  Getting the output file location and setting it to Text Box */
                if (fbDialog.ShowDialog() == DialogResult.OK)
                {

                    strPath = fbDialog.SelectedPath;

                    if (strPath != null && strPath.Length > 0)
                    {
                        tbTCData.Clear();

                        tbTCData.Text = strPath;
                    }
                }
            }
            catch (Exception exception)
            {
            }

        }

        /*  This function is called when user clicks on browse button for Organisation */
        private void btnInpBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog opFileDiag = new OpenFileDialog();

                opFileDiag.DefaultExt = Constants.DIAG_FEX_XML;

                /*  Dialog Filter for opening only XML files */
                opFileDiag.Filter = Constants.DIAG_XML_FILTER;

                opFileDiag.ShowDialog();

                string strFileName = opFileDiag.FileName;

                /*  Extracting the complete file path with file name and extracting it to Dialog Box */
                if (strFileName != null && strFileName.Length > 0)
                {
                    tbInpFilePath.Text = strFileName;
                }
            }
            catch (Exception exception)
            {
            }
        }

        /*  Cancel Button Action */
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        /*  Submit Button Action */
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            vaildateDataAndProceed();
        }

        public void vaildateDataAndProceed()
        {
            Boolean isValid = true;

            if (tbTCRoot.Text == null || tbTCRoot.Text.Length <= 0 ||
               tbTCData.Text == null || tbTCData.Text.Length <= 0 ||
               tbUsername.Text == null || tbUsername.Text.Length <= 0 ||
               tbPassword.Text == null || tbPassword.Text.Length <= 0 ||
               tbInpFilePath.Text == null || tbInpFilePath.Text.Length <= 0)
            {
                MessageBox.Show(Constants.INP_PARAMETER, Constants.INFO);
            }
            else
            {
                ProcessImport pImport = new ProcessImport();
                try
                {
                    ImportBean impBean = new ImportBean();
                    
                    /*  Filling data in Bean */
                    impBean.strTCRoot = tbTCRoot.Text;
                    impBean.strTCData = tbTCData.Text;
                    impBean.strUsername = tbUsername.Text;
                    impBean.strPassword = tbPassword.Text;
                    impBean.strInpFilePath = tbInpFilePath.Text;

                    if (cbCleanInpFile.Checked == true)
                    {
                        impBean.isCleanRequired = true;

                        impBean.strOutFileName = objFnameUtil.constructGenericFileNames(Constants.OUT_FILE_NAME, Constants.FEX_XML);
                    }

                    ProgressBar pBar = new ProgressBar(impBean, Constants.REQ_IMPORT);

                    pBar.ShowDialog();
                    
                }
                catch (Exception exception)
                {
                    if (exception.Message != null && exception.Message.Length > 0)
                    {
                        MessageBox.Show(exception.Message, Constants.ERROR);
                    }
                }
            }
        }
        
        /*  Handle Event */
        public void handleResponseEvent(int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_IMPORT:
                        handleImportRespEvent();
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Handle Import Event */
        public void handleImportRespEvent()
        {
            stImport.Dispose();

            if (AppDelegate.strOutputString != null && AppDelegate.strOutputString.Length > 0)
            {
                string strFilePath = AppDelegate.strOutPutLogFileDir;
                
                if (AppDelegate.strOutPutLogFileDir != null && AppDelegate.strOutPutLogFileDir.Length > 0)
                {

                    strFilePath = strFilePath + objFnameUtil.constructGenericFileNames(Constants.LOG_FILE_NAME, Constants.FEX_LOG);

                    File.Create(strFilePath).Dispose();

                    TextWriter tw = new StreamWriter(strFilePath);

                    tw.WriteLine(AppDelegate.strOutputString);

                    tw.Close();
                }

                MessageBox.Show(AppDelegate.strOutputString + Constants.LOG_FILE_LOCATION + strFilePath, Constants.INFO);
             
            }
        }
    }
}



