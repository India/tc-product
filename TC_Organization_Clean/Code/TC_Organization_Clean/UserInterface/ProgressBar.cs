﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TC_Organization_Clean.Requests;


namespace TC_Organization_Clean.UserInterface
{
    public partial class ProgressBar : Form
    {
        Object inpObj  = new Object();
        int iEventType = Constants.REQ_EMPTY;
        
        /*  Default Constructor */
        public ProgressBar()
        {
            InitializeComponent();
        }

        /*  Parametrized Constructor */
        public ProgressBar(Object inpObj, int iEventType)
        {
            this.inpObj = inpObj;
            
            this.iEventType = iEventType;

            InitializeComponent();
        }

        /*  This function is called when progress bar form is loaded */
        private void ProgressBar_Load(object sender, EventArgs e)
        {
            try
            {
                /*  Starting Progress */
                startProgress();
                
                /*  If thread is not busy and thread is not cancelled, then starting background worker thread */
                if (!bgwProcessing.IsBusy && !bgwProcessing.CancellationPending)
                {
                    bgwProcessing.RunWorkerAsync();
                }

            }
            catch (Exception exception)
            {

            }
        }

        /*  Progress Bar Start */
        public void startProgress()
        {
            pBar.Style = ProgressBarStyle.Marquee;
            pBar.MarqueeAnimationSpeed = 30;
            pBar.Show();
        }

        /*  Progress Bar End */
        public void endProgress()
        {
            this.Dispose();
        }

        /*  Do Work Method of Background worker thread */
        private void bgwProcessing_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ReqAndRespMain reqAndResp = new ReqAndRespMain();

                reqAndResp.processReqEvent(inpObj, iEventType);
            }
            catch (Exception exception)
            {
            }

        }

        /*  Work Completed */
        /*  Called when processing it completed */
        private void bgwProcessing_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                endProgress();
                ReqAndRespMain reqAndResp = new ReqAndRespMain();

                /* This is the case if any Exception has Occured during the background operation */
                if (e.Error != null)
                {
                    string strErrorString = null;

                    if (e.Error.InnerException != null && e.Error.InnerException.ToString().Length > 0)
                    {
                        strErrorString = e.Error.InnerException.ToString() + "\n";
                    }

                    if (e.Error.Message != null && e.Error.Message.ToString().Length > 0)
                    {
                        strErrorString = strErrorString + e.Error.Message;
                    }

                    throw new Exception(strErrorString);
                }
                else if (e.Cancelled == true)
                {
                    reqAndResp.handleResponseEvent(sender, Constants.REQ_CANCELLED);
                }
                else
                {
                     reqAndResp.handleResponseEvent(sender, iEventType);
                }
            }
            catch (Exception exException)
            {
                /*  Any Exceptions that have occurred in background thread, are caught here, so no need to further throw exception from here,
                 *  Just displaying them in dialog.
                 **/

                endProgress();

                MessageBox.Show(exException.Message, Constants.ERROR);
            }
        }

    }
}
