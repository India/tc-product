﻿namespace TC_Organization_Clean.UserInterface
{
    partial class Import
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbHeader = new System.Windows.Forms.Label();
            this.lbTCRoot = new System.Windows.Forms.Label();
            this.tbTCRoot = new System.Windows.Forms.TextBox();
            this.btnTCRoot = new System.Windows.Forms.Button();
            this.lbTCData = new System.Windows.Forms.Label();
            this.tbTCData = new System.Windows.Forms.TextBox();
            this.btbBrowseTCData = new System.Windows.Forms.Button();
            this.lbUsername = new System.Windows.Forms.Label();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.lbPassword = new System.Windows.Forms.Label();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.lbInpFilePath = new System.Windows.Forms.Label();
            this.tbInpFilePath = new System.Windows.Forms.TextBox();
            this.btnInpBrowse = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.cbCleanInpFile = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lbHeader
            // 
            this.lbHeader.AutoSize = true;
            this.lbHeader.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.lbHeader.Location = new System.Drawing.Point(26, 34);
            this.lbHeader.Name = "lbHeader";
            this.lbHeader.Size = new System.Drawing.Size(212, 17);
            this.lbHeader.TabIndex = 12;
            this.lbHeader.Text = "Import Teamcenter Organisation";
            // 
            // lbTCRoot
            // 
            this.lbTCRoot.AutoSize = true;
            this.lbTCRoot.Location = new System.Drawing.Point(26, 79);
            this.lbTCRoot.Name = "lbTCRoot";
            this.lbTCRoot.Size = new System.Drawing.Size(93, 17);
            this.lbTCRoot.TabIndex = 13;
            this.lbTCRoot.Text = "TC Root Path";
            // 
            // tbTCRoot
            // 
            this.tbTCRoot.Location = new System.Drawing.Point(135, 79);
            this.tbTCRoot.Name = "tbTCRoot";
            this.tbTCRoot.Size = new System.Drawing.Size(153, 22);
            this.tbTCRoot.TabIndex = 1;
            // 
            // btnTCRoot
            // 
            this.btnTCRoot.Location = new System.Drawing.Point(316, 79);
            this.btnTCRoot.Name = "btnTCRoot";
            this.btnTCRoot.Size = new System.Drawing.Size(75, 23);
            this.btnTCRoot.TabIndex = 2;
            this.btnTCRoot.Text = "Browse";
            this.btnTCRoot.UseVisualStyleBackColor = true;
            this.btnTCRoot.Click += new System.EventHandler(this.btnTCRoot_Click);
            // 
            // lbTCData
            // 
            this.lbTCData.AutoSize = true;
            this.lbTCData.Location = new System.Drawing.Point(31, 131);
            this.lbTCData.Name = "lbTCData";
            this.lbTCData.Size = new System.Drawing.Size(93, 17);
            this.lbTCData.TabIndex = 29;
            this.lbTCData.Text = "TC Data Path";
            // 
            // tbTCData
            // 
            this.tbTCData.Location = new System.Drawing.Point(135, 131);
            this.tbTCData.Name = "tbTCData";
            this.tbTCData.Size = new System.Drawing.Size(153, 22);
            this.tbTCData.TabIndex = 3;
            // 
            // btbBrowseTCData
            // 
            this.btbBrowseTCData.Location = new System.Drawing.Point(316, 131);
            this.btbBrowseTCData.Name = "btbBrowseTCData";
            this.btbBrowseTCData.Size = new System.Drawing.Size(75, 23);
            this.btbBrowseTCData.TabIndex = 4;
            this.btbBrowseTCData.Text = "Browse";
            this.btbBrowseTCData.UseVisualStyleBackColor = true;
            this.btbBrowseTCData.Click += new System.EventHandler(this.btbBrowseTCData_Click);
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Location = new System.Drawing.Point(31, 184);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(73, 17);
            this.lbUsername.TabIndex = 32;
            this.lbUsername.Text = "Username";
            // 
            // tbUsername
            // 
            this.tbUsername.Location = new System.Drawing.Point(135, 179);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(153, 22);
            this.tbUsername.TabIndex = 5;
            // 
            // lbPassword
            // 
            this.lbPassword.AutoSize = true;
            this.lbPassword.Location = new System.Drawing.Point(31, 236);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(76, 17);
            this.lbPassword.TabIndex = 34;
            this.lbPassword.Text = "Passsword";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(135, 236);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(153, 22);
            this.tbPassword.TabIndex = 6;
            // 
            // lbInpFilePath
            // 
            this.lbInpFilePath.AutoSize = true;
            this.lbInpFilePath.Location = new System.Drawing.Point(31, 291);
            this.lbInpFilePath.Name = "lbInpFilePath";
            this.lbInpFilePath.Size = new System.Drawing.Size(98, 17);
            this.lbInpFilePath.TabIndex = 43;
            this.lbInpFilePath.Text = "Input File Path";
            // 
            // tbInpFilePath
            // 
            this.tbInpFilePath.Location = new System.Drawing.Point(135, 291);
            this.tbInpFilePath.Name = "tbInpFilePath";
            this.tbInpFilePath.Size = new System.Drawing.Size(153, 22);
            this.tbInpFilePath.TabIndex = 7;
            // 
            // btnInpBrowse
            // 
            this.btnInpBrowse.Location = new System.Drawing.Point(316, 290);
            this.btnInpBrowse.Name = "btnInpBrowse";
            this.btnInpBrowse.Size = new System.Drawing.Size(75, 25);
            this.btnInpBrowse.TabIndex = 8;
            this.btnInpBrowse.Text = "Browse";
            this.btnInpBrowse.UseVisualStyleBackColor = true;
            this.btnInpBrowse.Click += new System.EventHandler(this.btnInpBrowse_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(213, 363);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 35);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(316, 363);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 35);
            this.btnSubmit.TabIndex = 9;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // cbCleanInpFile
            // 
            this.cbCleanInpFile.AutoSize = true;
            this.cbCleanInpFile.Checked = true;
            this.cbCleanInpFile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCleanInpFile.Location = new System.Drawing.Point(34, 338);
            this.cbCleanInpFile.Name = "cbCleanInpFile";
            this.cbCleanInpFile.Size = new System.Drawing.Size(127, 21);
            this.cbCleanInpFile.TabIndex = 44;
            this.cbCleanInpFile.Text = "Clean Input File";
            this.cbCleanInpFile.UseVisualStyleBackColor = true;
             // 
            // Import
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 424);
            this.Controls.Add(this.cbCleanInpFile);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnInpBrowse);
            this.Controls.Add(this.tbInpFilePath);
            this.Controls.Add(this.lbInpFilePath);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.lbPassword);
            this.Controls.Add(this.tbUsername);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.btbBrowseTCData);
            this.Controls.Add(this.tbTCData);
            this.Controls.Add(this.lbTCData);
            this.Controls.Add(this.btnTCRoot);
            this.Controls.Add(this.tbTCRoot);
            this.Controls.Add(this.lbTCRoot);
            this.Controls.Add(this.lbHeader);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Import";
            this.Text = "LMtec India Consulting Services Pvt. Ltd.";
            this.Load += new System.EventHandler(this.Import_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbHeader;
        private System.Windows.Forms.Label lbTCRoot;
        private System.Windows.Forms.TextBox tbTCRoot;
        private System.Windows.Forms.Button btnTCRoot;
        private System.Windows.Forms.Label lbTCData;
        private System.Windows.Forms.TextBox tbTCData;
        private System.Windows.Forms.Button btbBrowseTCData;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label lbInpFilePath;
        private System.Windows.Forms.TextBox tbInpFilePath;
        private System.Windows.Forms.Button btnInpBrowse;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.CheckBox cbCleanInpFile;
    }
}