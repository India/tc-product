﻿/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec India
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: Clean.cs

    Description: This file contains UI handlers for clean dialog. 

========================================================================================================================================================================

Date            Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
28- July-18     Singh                  Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using TC_Organization_Clean.Beans;
using TC_Organization_Clean.ProcessingEngine;
using TC_Organization_Clean.Utilities;

namespace TC_Organization_Clean.UserInterface
{
    public partial class Clean : Form
    {
        public static Clean stClean = null;
        
        /*  Default Contructor */
        public Clean()
        {
            InitializeComponent();
        }

        /*  This function is invoked when the dialog is loaded */
         private void Main_Load(object sender, EventArgs e)
        {
            stClean = this;
        }
       
        /*  This function is called when user click on browse button of input file */
        private void btnInpBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog();
        }

        /*  Function to open file dialog */
        public void openFileDialog()
        {
            try
            {
                OpenFileDialog opFileDiag = new OpenFileDialog();

                opFileDiag.DefaultExt = Constants.DIAG_FEX_XML;

                /*  Dialog Filter for opening only XML files */
                opFileDiag.Filter = Constants.DIAG_XML_FILTER;

                opFileDiag.ShowDialog();

                string strFileName = opFileDiag.FileName;

                /*  Extracting the complete file path with file name and extracting it to Dialog Box */
                if (strFileName != null && strFileName.Length > 0)
                {
                    tbInpFile.Text = strFileName;
                }
            }
            catch (Exception exception)
            {
            }
        }

        /*  This function is called when user click in browse button of output file location */
        private void btnOutBrowse_Click(object sender, EventArgs e)
        {
            openFolderDialog();
        }

        /*  Function to Open Output file location dialog box */
        public void openFolderDialog()
        {
            FolderBrowserDialog fbDialog = new FolderBrowserDialog();

            string strPath = "";

            try
            {
                /*  Getting the output file location and setting it to Text Box */
                if (fbDialog.ShowDialog() == DialogResult.OK)
                {
                    strPath = fbDialog.SelectedPath;

                    if (strPath != null && strPath.Length > 0)
                    {
                        tbOutFilePath.Text = strPath;
                    }
                }
            }
            catch (Exception exception)
            {
            }
        }

        /*  Cancel Button Click Handler */
        private void btCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        /*  Submit Button Click Handler */
        private void btSubmit_Click(object sender, EventArgs e)
        {
            validateAndProceed();
        }

        public void validateAndProceed()
        {
            if (tbInpFile.Text == null || tbInpFile.Text.Length <= 0 ||
               tbOutFilePath.Text == null || tbOutFilePath.Text.Length <= 0)
            {
                MessageBox.Show(Constants.INP_PARAMETER, Constants.INFO);
            }
            else
            {
                ProcessClean pClean = new ProcessClean();

                try
                {

                    CleanBean cBean = new CleanBean();
                    
                    cBean.strInpFilePath = tbInpFile.Text;

                    cBean.strOutFilePath = tbOutFilePath.Text;

                    /*  File name Construction */
                    if (tbOutFileName.Text.Length > 0)
                    {
                        cBean.strOutFileName = "\\" + tbOutFileName.Text + Constants.FEX_XML;
                    }
                    else
                    {

                        FileNameUtils objFnameUtil = new FileNameUtils();

                        cBean.strOutFileName = objFnameUtil.constructGenericFileNames(Constants.OUT_FILE_NAME, Constants.FEX_XML);
                        
                    }

                    /*  Check if the directory, If same File exists with that name then Displaying message to user, OK option by user overriders the current existing file */
                     if (File.Exists(cBean.strOutFilePath + cBean.strOutFileName))
                     {
                        if (MessageBox.Show(Constants.FILE_ALREADY_EXISTS, Constants.INFO, MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            ProgressBar pBar = new ProgressBar(cBean, Constants.REQ_CLEAN);

                            pBar.ShowDialog();
                        }
                    }
                    else
                    {
                        ProgressBar pBar = new ProgressBar(cBean, Constants.REQ_CLEAN);

                        pBar.ShowDialog();
                    }

                }
                catch (Exception exception)
                {
                    if (exception.Message != null && exception.Message.Length > 0)
                    {
                        MessageBox.Show(exception.Message, Constants.ERROR);
                    }
                }
            }
        }
        public void handleResponseEvent(int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_CLEAN:
                        handleCleanRespEvent();
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        public void handleCleanRespEvent()
        {
            stClean.Dispose();

            if (AppDelegate.strOutputString != null && AppDelegate.strOutputString.Length > 0)
            {
                MessageBox.Show(AppDelegate.strOutputString, Constants.INFO);
            }
        }
     }       
}



