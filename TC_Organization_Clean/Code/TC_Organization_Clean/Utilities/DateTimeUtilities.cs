﻿/*======================================================================================================================================================================
                Copyright 2018  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: DateTimeUtils.cs

    Description: This function genrates feneric File names.

========================================================================================================================================================================

Date				Name                Description of Change
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-August-2018      Singh	            Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TC_Organization_Clean.Utilities
{
    class DateTimeUtilities
    {
        /*  This Function return current epoch time, Epoch time is current time in seconds from 1st January 1970 */
        public int getEpochTime()
        {
            int iEpochTime = -1;

            /*  Generating current epoch time */
            iEpochTime = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;

            return iEpochTime;
        }

    }
}
