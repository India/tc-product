﻿/*======================================================================================================================================================================
                Copyright 2018  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: ExportBean.cs

    Description: Class container for Export request.

========================================================================================================================================================================

Date				Name                Description of Change
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-August-2018      Singh	Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TC_Organization_Clean.Beans
{
    class ExportBean
    {
        public string strTCRoot      = "";
        public string strTCData      = "";
        public string strUsername    = "";
        public string strPassword    = "";
        public string strClass       = "";
        public string strOutFilePath = "";
    }
}
