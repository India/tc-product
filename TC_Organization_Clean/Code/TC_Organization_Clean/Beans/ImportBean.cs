﻿/*======================================================================================================================================================================
                Copyright 2018  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: ImportBean.cs

    Description: Class container for Import request.

========================================================================================================================================================================

Date				Name                Description of Change
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-August-2018      Singh	Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TC_Organization_Clean.Beans
{
    class ImportBean
    {
        public string strTCRoot = "";
        public string strTCData = "";
        public string strUsername = "";
        public string strPassword = "";
        public string strInpFilePath = "";
        public string strOutFileName = "";
        public Boolean isCleanRequired = false;
    }
}
