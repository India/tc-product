﻿/*======================================================================================================================================================================
                Copyright 2018  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: ProcessExport.cs

    Description: This file is contains logics for exporting Organisation.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-August-2018      Singh	Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;

namespace TC_Organization_Clean.ProcessingEngine
{
    class ProcessExport
    {
        /*  This function is entry point to process export opertaion,This function gets triggered when user selects export operation at start */
        public void processExportOperation(string strTcRoot, string strTCData, string strUsername, String strPassword, string strParameter, string strOutFilePath)
        {
            try
            {
                AppDelegate.strOutputString = "";

                AppDelegate.strOutPutLogFileDir = "";
                
                /*  Creating instance of new process, This is done because the export operations are run on command line , for this we have to launch command promt on shell
                 *  For launchinf command promt on shell we have to first create a process and then we assign cmd.exe to that process  
                 */
                Process process = new Process();
                
                /*  Assigning cmd.exe to process */
                process.StartInfo.FileName = Constants.CMD_EXE;

                /*  Process Constant to run process in background i.e BLACK window is not shown to user */
                process.StartInfo.CreateNoWindow = true;

                /*  Process Constant for provding inputs to shell */
                process.StartInfo.RedirectStandardInput = true;

                /*  Process Constant for collecting standard Output */
                process.StartInfo.RedirectStandardOutput = true;
                
                process.StartInfo.UseShellExecute = false;

                /*  Starting Process */
                process.Start();
                
                /*  TC_Data Environment variable */
                process.StandardInput.WriteLine(Constants.SET_TC_ROOT + strTcRoot);
                
                /*  TC_ROOT Environement variable */
                process.StandardInput.WriteLine(Constants.SET_TC_DATA + strTCData);
                
                /*  TC_Profile vars */
                process.StandardInput.WriteLine(Constants.TC_DATA_TC_PROFILEVARS);
                
                /* Executing Command */
                process.StandardInput.WriteLine("dsa_util -u=" + strUsername + " -p=" + strPassword + " -g=dba -class=Group -f=export  -filename=" + strOutFilePath+Constants.TC_ORG_NAME);
                
                /*  Flushing process Buffer */
                process.StandardInput.Flush();
                
                process.StandardInput.Close();

                /*  Waiting for process to exit */
                process.WaitForExit();

                if (process.StandardOutput != null)
                {
                    AppDelegate.strOutputString = process.StandardOutput.ReadToEnd();

                    AppDelegate.strOutPutLogFileDir = strOutFilePath;
                }
              
            }         
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}






