﻿/*======================================================================================================================================================================
                Copyright 2018  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: EnvironmentUtils.cs

    Description: This contains functions related to Envrimonment variables..

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-August-2018      Singh	Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TC_Organization_Clean
{
    class EnvironmentUtils
    {
        /*  This utility function returns value of Enviroment variables */
        public string getEnvVariable(String strEnvironVar)
        {
            string strPath = null;

            try
            {
                /* Getting value of Environment variable */
                strPath = Environment.GetEnvironmentVariable(strEnvironVar);
            }
            catch (Exception exException)
            {
                throw exException;
            }
            return strPath;
        }
    }
}



