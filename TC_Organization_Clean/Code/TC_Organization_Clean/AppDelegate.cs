﻿/*======================================================================================================================================================================
                Copyright 2018  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: AppDelegate.cs

    Description: This contains Application Level Static Variables.

========================================================================================================================================================================

Date				Name                Description of Change
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-August-2018      Singh	Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TC_Organization_Clean
{
    class AppDelegate
    {
        public static string strOutputString = "";
        public static string strOutPutLogFileDir = "";
    }
}
