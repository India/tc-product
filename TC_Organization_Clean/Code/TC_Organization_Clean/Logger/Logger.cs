﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TC_Organization_Clean.logger
{
    class Logger
    {

        static int iDefaultLogLevel = Constants.LOG_ERROR;
        static string strLogFilePath = "";


            /*  Diffrent Log Levels */
            int VERBOSE = 0;
            int DEBUG = 1;
            int INFO = 2;
            int WARNING = 3;
            int ERROR = 4;
            static Logger log = null;

            /*  Default Constructor */
            public Logger()
            {
            }

            /*  This function gives static instance of logger   */
            public static Logger getInstance()
            {
                if (log == null)
                {
                    log = new Logger();
                }
                return log;
            }

            /*  This function writes basic details of system initially at start of Log File */
            public void init(int iLogLevel, string strPath)
            {
                iDefaultLogLevel = iLogLevel;

                strLogFilePath = strPath;
            }

            /*  Function that creates log file and writes log to it */
            public void LogWrite(string logMessage)
            {
                try
                {
                    using (StreamWriter w = File.AppendText(strLogFilePath))
                    {
                        Log(logMessage, w);
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            /*  Function writes log in log file */
            public void Log(string logMessage, TextWriter txtWriter)
            {
                try
                {
                    txtWriter.Write("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                    txtWriter.WriteLine("\t\t" + logMessage);
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            /*  For writing Verbose log   */
            public void verbose(String szLog)
            {
                if (szLog == null)
                    return;
                if (iDefaultLogLevel <= VERBOSE)
                {
                    try
                    {
                        LogWrite("Verbose  : " + szLog);
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                }
            }

            /*  For writing debug log   */
            public void debug(String szLog)
            {
                if (szLog == null)
                    return;
                if (iDefaultLogLevel <= DEBUG)
                {
                    try
                    {
                        LogWrite("Debug    : " + szLog);
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                }
            }

            /*  For writing info log   */
            public void info(String szLog)
            {
                if (szLog == null)
                    return;
                if (iDefaultLogLevel <= VERBOSE)
                {
                    try
                    {
                        LogWrite("Info     : " + szLog);
                    }
                    catch (Exception exception)
                    {
                        throw;
                    }
                }
            }

            /*  For writing warning log   */
            public void warning(String szLog)
            {
                if (szLog == null)
                    return;
                if (iDefaultLogLevel <= VERBOSE)
                {
                    try
                    {
                        LogWrite("Warning  : " + szLog);

                    }
                    catch (Exception exception)
                    {
                        throw;
                    }
                }
            }

            /*  For writing warning log   */
            public void error(String szLog)
            {
                if (szLog == null)
                    return;
                if (iDefaultLogLevel <= VERBOSE)
                {
                    try
                    {
                        LogWrite("Error    : " + szLog);
                    }
                    catch (Exception exception)
                    {
                        throw;
                    }
                }
            }
        }
    }
