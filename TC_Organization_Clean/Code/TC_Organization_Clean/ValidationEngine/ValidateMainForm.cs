﻿/*======================================================================================================================================================================
                Copyright 2018  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: ValidateMainForm.cs

    Description: This file is for performing validation for Main Dialog

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-August-2018      Singh	Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TC_Organization_Clean.Beans;

namespace TC_Organization_Clean.ValidationEngine
{
    class ValidateMainForm
    {
        /*  Validate Main Form */
        public void validateMainForm(MainBean mBean)
        {
            if (mBean.strOperationType == null || mBean.strOperationType.Length <= 0)
            {
                throw new Exception(Constants.SELECT_OPERTAION_TYPE);
            }
        }
    }
}
