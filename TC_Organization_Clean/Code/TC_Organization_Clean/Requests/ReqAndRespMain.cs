﻿/*======================================================================================================================================================================
                Copyright 2018  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: ReqAndRespMain.cs

    Description: This file is main class of requests and responses.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-August-2018      Singh	Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TC_Organization_Clean.ProcessingEngine;
using TC_Organization_Clean.Beans;
using System.Windows.Forms;
using TC_Organization_Clean.UserInterface;


namespace TC_Organization_Clean.Requests
{
    class ReqAndRespMain
    {
        /*  Default Constructor */
        public ReqAndRespMain()
        {
        }

         /*  Adding event, These events are added on Background Worker thread, Do not perform any operations related to user interface here, the system will crash, processing is going not on UI Thread */
        public void processReqEvent(Object objInp, int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_EXPORT:
                        req_export_operation(objInp);
                        break;
                    case Constants.REQ_IMPORT:
                        req_import_operation(objInp);
                        break;
                    case Constants.REQ_CLEAN:
                        req_clean_operation(objInp);
                        break;
                }
            }
            catch (Exception exception)
            {
            }
        }

        /* Handle Requests Method */
        public void handleResponseEvent(Object outObj, int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                   case Constants.REQ_CANCELLED:
                        break;
                   case Constants.REQ_EXPORT:
                        Export.stExport.handleResponseEvent(iEventType);
                        break; 
                   case Constants.REQ_IMPORT:
                        Import.stImport.handleResponseEvent(iEventType);
                        break;
                   case Constants.REQ_CLEAN:
                        Clean.stClean.handleResponseEvent(iEventType);
                        break;
                }
            }
            catch (Exception exception)
            {
                /*  No need to do anything here, because this has been called by Custom Progress bar form, and upto now that form has already been disposed,
                 *  so that exception will become unhandled, leaving it the way it is and it will remain the thing as it is. 
                 */
            }
        }

        /*  Export Operation Request */
        public void req_export_operation(Object inpObj)
        {
            try
            {
                ProcessExport pExport = new ProcessExport();
                ExportBean    expBean = new ExportBean();

                /*  Type casting the input Object based on export bean as the request if of export */
                expBean = (ExportBean)inpObj;

                /*  Proceeding export operation */
                pExport.processExportOperation(expBean.strTCRoot, expBean.strTCData, expBean.strUsername,expBean.strPassword, expBean.strClass, expBean.strOutFilePath);
                
            }
            catch (Exception exception)
            {
            }
        }

        /*  Import Operation Request */
        public void req_import_operation(Object objInp)
        {
            try
            {
                ProcessImport pExport = new ProcessImport();
                ImportBean impBean    = new ImportBean();

                /*  Type casting the input Object based on export bean as the request if of export */
                impBean = (ImportBean)objInp;

                /*  Proceeding export operation */
                pExport.processImportOperation(impBean.strTCRoot, impBean.strTCData, impBean.strUsername, impBean.strPassword, impBean.strInpFilePath, impBean.isCleanRequired, impBean.strOutFileName);

            }
            catch (Exception exception)
            {
            }
        }

        /*  Clean Operation request */
        public void req_clean_operation(Object objInp)
        {
            try
            {
                ProcessClean pClean = new ProcessClean();
                CleanBean cBean     = new CleanBean();

                /*  Type casting the input Object based on export bean as the request if of export */
                cBean = (CleanBean)objInp;

                /*  Proceeding export operation */
                pClean.processCleanOperation(cBean.strInpFilePath, cBean.strOutFilePath, cBean.strOutFileName);

            }
            catch (Exception exception)
            {
            }
        }
    }
}



