﻿/*======================================================================================================================================================================
                Copyright 2018  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: Constants.cs

    Description: This contains Constant Variables.

========================================================================================================================================================================

Date				Name                Description of Change
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-August-2018      Singh	Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TC_Organization_Clean
{
    class Constants
    {
        /*  Environment Variables */
        public static string TC_ROOT = "TC_ROOT";
        public static string TC_DATA = "TC_DATA";

        /*  Operation Type */
        public static string OP_EXPORT = "Export";
        public static string OP_IMPORT = "Import";
        public static string OP_CLEAN  = "Clean";

        /*  Message Box Header Strings */
        public static string INFO  = "Information";
        public static string ERROR = "Error";

        /*  Message Box Message strings */
        public static string INP_PARAMETER = "Please Provide all Input Parameters";
        public static string VALID_OP      = "Please select valid Operation";
        public static string FILE_ALREADY_EXISTS = "File with same name already exists in output File path Directory. \n Do you want to overwrite it ?";
        public static string SELECT_OPERTAION_TYPE = "Please Select Operation Type.";

        /*  Process name */
        public static string CMD_EXE = "cmd.exe";

        /*  CMD.exe eniviroment variables */
        public static string SET_TC_ROOT           = "SET TC_ROOT=";
        public static string SET_TC_DATA           = "SET TC_DATA=";
        public static string TC_DATA_TC_PROFILEVARS="%TC_DATA%\\tc_profilevars";

        /*  Other Constants */
        public static string TC_ORG_NAME = "\\tc_organisaton.xml";
        public static string SUCCESS = "Operation Successfully completed. Output file and log file is stored at location ";
        public static string DIAG_FEX_XML = "xml";
        public static string DIAG_XML_FILTER = "XML File (*.xml) | *.xml";
        public static string LOG = "Log_Clean_";
        public static string LOG_FILE_LOCATION = "\n\n\nLog File Location: ";

        /*  Requests Event Types */
        public const int REQ_CANCELLED        = 999;
        public const int REQ_EMPTY            = 1000;
        public const int REQ_IMPORT           = 1001;
        public const int REQ_EXPORT           = 1002;
        public const int REQ_CLEAN            = 1003;
        public const int REQ_CLEAN_AND_EXPORT = 1004;

        /*  XML File Tag constants */
        public static string TAG_USER_DATA = "/xmln:PLMXML/xmln:UserData";
        public static string TAG_PERSONS = "/xmln:PLMXML/xmln:Person";
        public static string TAG_ORG_MEM = "/xmln:PLMXML/xmln:OrganisationMember";
        public static string TAG_USER = "/xmln:PLMXML/xmln:User";
        public static string TAG_HEADER = "/xmln:PLMXML/xmln:Header";
        public static string TAG_PLMXML = "/xmln:PLMXML";

        /*  XML Attributes Constants */
        public static string ATTR_TRAVERSE_ROOT_REFS = "traverseRootRefs";
        public static string ATTR_TRANSFER_CONTEXT = "transferContext";
        public static string ATTR_TIME = "time";
        public static string ATTR_SCHEMA_VERSION = "schemaVersion";
        public static string ATTR_AUTHOR = "author";
        public static string ATTR_DATE = "date";
       
        /*  Output File name */
        public static string OUT_FILE_NAME ="/tc_organization_";
        public static string LOG_FILE_NAME = "\\Log_";
        public static string OUT_IMP_CLEAN_FNAME = "\\tc_organization_new.xml";

        /* XML Name space */
        public static string XML_NS = "http://www.plmxml.org/Schemas/PLMXMLSchema";
        public static string XML_NS_NAME = "xmln";

        /*  File Extensions */
        public static string FEX_XML = ".xml";
        public static string FEX_LOG = ".log";
        public static string FEX_TXT = ".txt";

        /*  Logger Constants    */
        public static int LOG_VERBOSE = 0;
        public static int LOG_DEBUG = 1;
        public static int LOG_INFO = 2;
        public static int LOG_WARNING = 3;
        public static int LOG_ERROR = 4;

        /*  Separators */
        public static string SEP_BACK_SLASH = "//";
    }
}
