@echo off

REM # ==================================================================== #
REM # SCRIPT TO RUN catdrawing2PDF TRANSLATOR
REM # Parameter:
REM # %1: Input-File
REM # %2: Output-File
REM # %3: Input-Directory.
REM # %4: Output-Directory.
REM # %5: Item-ID
REM # %6: Item-Rev
REM # %7: Dataset-Name
REM # Example: <Translator>.bat test.in test.out 100070782 00 100070782
REM # ==================================================================== #
REM Add the translator name in service.properties, activate the translator definition in the translator.xml

set OLDPATH=%PATH%

setlocal

REM Environment variables required for this translation, Adjust these variables as per Teamcenter Configuration
set TC_ROOT=D:\plm\TC12
set TC_DATA=D:\plmshare\tc_data
set SPLM_LICENSE_SERVER=28000@Teamcenter
set UPG_TRANS=-u=dcproxy -p=dcproxy -g=dba
set TC_TMP_DIR=D:\temp\tc


@set PARAM1=%1

if x%PARAM1%== x goto help
if %1== -help goto help

goto setenv

:help
echo 	Usage:
echo    Translator executable name: %0
echo  	Arguments are:
echo    Param 1) Input  file name to translate with complete path.
echo    Param 2) Output file name to translate with complete path.
echo    Param 3) Full path to the input directory.
echo    Param 4) Full path to the output directory.
echo    Custom Param 5) Item ID from translation request.
echo    Custom Param 6) Item revision ID from translation request.
echo    Custom Param 7) Source Dataset name from translation request.
EXIT /B 1

:setenv
REM Check environment
if x"%TC_ROOT%"==x (
  echo set TC_ROOT
  EXIT /B 2
 )

if x"%TC_DATA%"==x (
  echo set TC_DATA
  EXIT /B 3
 )

if x"%SPLM_LICENSE_SERVER%"==x (
  echo set SPLM_LICENSE_SERVER value
  EXIT /B 4
 )

if x"%SPLM_SHR_DIR%"==x (
  echo set SPLM_SHR_DIR Path
  EXIT /B 5
 )

REM Copy the arguments passed on to translator.
set SRCDIRPATH=%1
set DSTDIRPATH=%2
set SRCDIR=%3
set DSTDIR=%4
set ITEM_ID=%5
set ITEM_REV_ID=%6
set SRC_DATASET_NAME=%7

echo SRCDIRPATH %SRCDIRPATH%
echo DSTDIRPATH %DSTDIRPATH%
echo SRCDIR %SRCDIR%
echo DSTDIR %DSTDIR%
echo ITEM_ID %ITEM_ID%
echo ITEM_REV_ID %ITEM_REV_ID%
echo SRC_DATASET_NAME %SRC_DATASET_NAME%
echo UGII_BASE_DIR %UGII_BASE_DIR%
echo.

REM Settings required by the Translator

REM Prepare the translation, This is temporary location in which dataset is copied, converted to pdf and then it is copied to satging location, once translation is done this location is deleted
set TRANSLATION_TEMPDIR=%TC_TMP_DIR%\PDF_%ITEM_ID%_%ITEM_REV_ID%


if exist "%TRANSLATION_TEMPDIR%" rmdir /S /Q "%TRANSLATION_TEMPDIR%" > NUL 2>&1
echo New created temporary translation directory = "%TRANSLATION_TEMPDIR%"
if not exist "%TRANSLATION_TEMPDIR%" mkdir "%TRANSLATION_TEMPDIR%"
cd /d "%TRANSLATION_TEMPDIR%"
echo Copy the file "%SRCDIRPATH%" to "%TRANSLATION_TEMPDIR%"
copy /Y %SRCDIRPATH% "%TRANSLATION_TEMPDIR%"
dir /b "%TRANSLATION_TEMPDIR%\*.CATDrawing" > %~dp0_outfilename.txt
REM !! Change the extention to initial one
set /p PRT_OUTPUT_FILE=<%~dp0_outfilename.txt
if exist %~dp0_outfilename.txt del /F %~dp0_outfilename.txt

REM This copied file will be used as input file for translation
echo Input file for translation   = %PRT_OUTPUT_FILE%

REM Extract only the file name, This will help us to contrut PDF name as PDF will also be construted with same name, so we will append .pdf to this name after removing CAT Drawing
REM !! Keep this only if required
set extract_file_name=%PRT_OUTPUT_FILE%
for /f "tokens=1 delims=." %%a in ("%extract_file_name%") do (set file_name=%%a)
echo %file_name%

REM These parameters are used by VB Script
SET ExportDir=%TRANSLATION_TEMPDIR%\
SET ImportDir=%TRANSLATION_TEMPDIR%\

REM Final PDF NAME, appending .pdf to it
SET DatasetName=%file_name%.pdf
echo %DatasetName%
SET FileName=%PRT_OUTPUT_FILE%

REM Calling CATIA VB Script for PDF Conversion, Adjust these paths as per catia configuration
echo D:\opt\ds\catia__v5_6R2018\win_b64\code\bin\CATSTART.exe  -run "CNEXT.exe -object -batch -macro D:\plm\Dispatcher\Module\Translators\catdrawing2pdf\catdrawing2pdf.catvbs" -env Teamcenter_Integration_for_CATIAV5 -direnv "D:\plm\catia_config"

CALL D:\opt\ds\catia__v5_6R2018\win_b64\code\bin\CATSTART.exe  -run "CNEXT.exe -object -batch -macro D:\plm\Dispatcher\Module\Translators\catdrawing2pdf\catdrawing2pdf.catvbs" -env Teamcenter_Integration_for_CATIAV5 -direnv "D:\plm\catia_config"
REM End vbs script here

REM Copy the converted PDF File to STAGING Location
if not exist "%TRANSLATION_TEMPDIR%\*.pdf" (
	echo "ERROR: There is no PDF file generated from catvbs, Unable to Find PDF"
	goto :noresult
)

echo %TRANSLATION_TEMPDIR%\%DatasetName% %DSTDIRPATH%
copy /y %TRANSLATION_TEMPDIR%\%DatasetName% %DSTDIRPATH%

REM if exist "%TRANSLATION_TEMPDIR%" rmdir /S /Q "%TRANSLATION_TEMPDIR%" > NUL 2>&1

REM Before the import exe could be called source the Teamcenter environment.
call "%TC_DATA%\tc_profilevars.bat"

REM Import result file
echo Import the result file

echo %TC_ROOT%\bin\import_file %UPG_TRANS% -f="%DSTDIRPATH%" -d=%ITEM_ID%-%ITEM_REV_ID% -item=%ITEM_ID% -revision=%ITEM_REV_ID% -type=PDF -ref=PDF_Reference -de=r -relationType=IMAN_Rendering -use_ds_attached_to_rev_only -ie=y
%TC_ROOT%\bin\import_file %UPG_TRANS% -f="%DSTDIRPATH%" -d=%SRC_DATASET_NAME% -item=%ITEM_ID% -revision=%ITEM_REV_ID% -type=PDF -ref=PDF_Reference -de=r -relationType=IMAN_Rendering -use_ds_attached_to_rev_only -ie=y

echo %ERRORLEVEL%
echo [%DATE% - %TIME%] The translation process and the result file import process have been finished.
goto :endext

:noresult

echo ERROR - ERROR - ERROR
echo Any PDF file has not been created, NO Pdf file has been generated by catia macro.
endlocal
EXIT /B 9

:endext
endlocal
set EXITVALUE=%ERRORLEVEL%
set PATH=%OLDPATH%
set OLDPATH=
cd /d %CURRDIR%
EXIT /B %EXITVALUE%