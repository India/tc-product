/*
 * ---------------------------------------------------------------------------------------------------------
 *               Copyright 2018 Lmtec GmbH
 * 			    Unpublished - All rights reserved
 * ----------------------------------------------------------------------------------------------------------
 * Filename: DCConstants.java
 * File Description:Constant File
 *-------------------------------------------------------------------------------------------------------------
 * Date            Name                 Description of Change
 * 21-Nov-2018     Akshay Singh           First draft
 * ------------------------------------------------------------------------------------------------------------*/
package com.teamcenter.dc.roedersdc.utils;

public class DCConstants
{
    public static final String PROP_NAME_ITEM_ID                  = "item_id";
    public static final String PROP_NAME_ITEM_REVISION_ID         = "item_revision_id";
    public static final String PROP_NAME_OBJECT_NAME              = "object_name";
    public static final String PROP_ITEM_REV_OBJ_STRING           = "object_string";
}
