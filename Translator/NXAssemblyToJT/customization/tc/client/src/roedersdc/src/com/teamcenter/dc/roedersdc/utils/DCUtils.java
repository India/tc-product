/*
 * ---------------------------------------------------------------------------------------------------------
 *  		    Copyright 2018 Lmtec GmbH
 * 			    Unpublished - All rights reserved
 * ----------------------------------------------------------------------------------------------------------
 * Filename: DCUtils.java
 * File Description:
 *-------------------------------------------------------------------------------------------------------------
 * Date            Name                 Description of Change
 * 21-Nov-2018     Akshay Singh           First draft
 * ------------------------------------------------------------------------------------------------------------*/

package com.teamcenter.dc.roedersdc.utils;

import com.teamcenter.dc.roedersdc.utils.DCUtils;
import com.teamcenter.ets.soa.ConnectionManager;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.strong.core.DataManagementService;
import com.teamcenter.soa.client.model.ModelObject;
import com.teamcenter.soa.client.model.ServiceData;
import com.teamcenter.translationservice.task.Option;
import com.teamcenter.translationservice.task.TranslatorOptions;
import com.teamcenter.tstk.util.log.ITaskLogger;

public class DCUtils
{

    /* Gets the Data Management Service from current connection */
    public static DataManagementService getDataManagementService() throws Exception
    {
        return DataManagementService.getService(ConnectionManager.getActiveConnection());
    }

    /* Writes the service data errors in task logger */
    public static void printErrorUsingTaskLogger(ITaskLogger taskLogger, String functionTraceName, ServiceData serviceData) throws ServiceException
    {
        if (serviceData.sizeOfPartialErrors() > 0)
        {
            int partialErrorSize = serviceData.sizeOfPartialErrors();

            for (int i = 0; i < partialErrorSize; i++)
            {
                String errorStack[] = serviceData.getPartialError(i).getMessages();
                for (int j = 0; j < errorStack.length; j++)
                {
                    taskLogger.error(errorStack[j]);
                }
            }
            throw new ServiceException(functionTraceName + "returned a partial error.");
        }
    }

    /* Adds the translation option with the given key and value */
    public static void addTranslationOption(ITaskLogger taskLogger, TranslatorOptions translatorOptions, String optionKey, String optionValue)
    {
        taskLogger.info("Adding option with key \"" + optionKey + "\" and value \"" + optionValue + "\"");

        if (translatorOptions != null)
        {
            Option option = new Option();
            option.setName(optionKey);
            option.setValue(optionValue);
            translatorOptions.addOption(option);

            taskLogger.info("Added option with key \"" + optionKey + "\" and value \"" + optionValue + "\"");
        }
    }

    /* Refresh the given Model object */
    public static void refreshObject(ITaskLogger taskLogger, ModelObject obj) throws Exception
    {
        ServiceData serviceData = getDataManagementService().refreshObjects(new ModelObject[] {obj});
        DCUtils.printErrorUsingTaskLogger(taskLogger, "DCUtils.refreshObject", serviceData);
    }
}
