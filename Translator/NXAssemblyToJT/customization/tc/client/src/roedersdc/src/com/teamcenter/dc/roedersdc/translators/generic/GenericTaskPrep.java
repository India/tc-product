/*
 * ---------------------------------------------------------------------------------------------------------
 *  		    Copyright 2018 Lmtec GmbH
 * 			    Unpublished - All rights reserved
 * ----------------------------------------------------------------------------------------------------------
 * Filename: GenericTaskPrep.java
 * File Description:This file implements the extract functionality for generic translator i.e intended to
 *                  be used as common taskprep class. It will extract item id, revision id, source dataset name
 *                  and add them as translation request arguments which can be utilized by the module.    
 *-------------------------------------------------------------------------------------------------------------
 * Date            Name                 Description of Change
 * 21-Nov-2018     Akshay Singh           First draft
 * ------------------------------------------------------------------------------------------------------------*/

package com.teamcenter.dc.roedersdc.translators.generic;

import java.io.File;
import java.util.ArrayList;

import com.teamcenter.dc.roedersdc.utils.*;
import com.teamcenter.ets.translator.ugs.basic.TaskPrep;
import com.teamcenter.translationservice.task.TranslationTask;
//import com.teamcenter.ets.util.Registry;
import com.teamcenter.translationservice.task.TranslatorOptions;
import com.teamcenter.soa.client.model.ModelObject;
import com.teamcenter.ets.request.TranslationRequest;
import com.teamcenter.soa.client.model.strong.ItemRevision;
import com.teamcenter.soa.client.model.strong.Dataset;
import com.teamcenter.soa.client.model.ServiceData;

/* Generic Task preparation class for all pxc translators. (this is how R2 was implemented */

public class GenericTaskPrep extends TaskPrep
{
    // private static Registry registry =
    // Registry.getRegistry("com.teamcenter.dc.pxcdc.translators.generic.generic");

    public static final String OPT_KEY_ITEM_ID          = "item_id";
    public static final String OPT_KEY_ITEM_REVISION_ID = "item_revision_id";
    public static final String OPT_KEY_SRC_DATASET_NAME = "src_dataset_name";
    public static final String OPT_KEY_OBJECT_STRING    = "item_rev_object_string";

    protected TranslationTask  m_zTransTask;
    private TranslatorOptions  m_translatorOptions;

    protected ItemRevision     m_itemRevision;
    protected Dataset          m_dataset;

    @Override
    public TranslationTask prepareTask() throws Exception
    {  
        m_zTransTask = super.prepareTask();

        // get primary objects and secondary objects
        ModelObject primary_objs[] = request.getPropertyObject(TranslationRequest.PRIMARY_OBJS).getModelObjectArrayValue();
        ModelObject secondary_objs[] = request.getPropertyObject(TranslationRequest.SECONDARY_OBJS).getModelObjectArrayValue();

        if ((primary_objs != null) && (secondary_objs != null))
        {
            m_dataset = (Dataset) primary_objs[0];
            m_itemRevision = (ItemRevision) secondary_objs[0];
        }

        addArgumentsToTranslationRequest();

        return m_zTransTask;
    }

    private void addArgumentsToTranslationRequest() throws Exception
    {
        if (m_zTransTask != null)
        {
            if (null == m_translatorOptions)
            {
                /* Get current translation options */
                m_translatorOptions = m_zTransTask.getTranslatorOptions();
                if (null == m_translatorOptions)
                {
                    m_translatorOptions = new TranslatorOptions();
                    m_zTransTask.setTranslatorOptions(m_translatorOptions);
                }
            }
        }

        ArrayList<String> listKeys = new ArrayList<String>();
        ArrayList<String> listValues = new ArrayList<String>();

        ServiceData serviceData = DCUtils.getDataManagementService().getProperties(new ModelObject[] { m_itemRevision },
                new String[] { DCConstants.PROP_NAME_ITEM_ID, DCConstants.PROP_NAME_ITEM_REVISION_ID, DCConstants.PROP_ITEM_REV_OBJ_STRING });
        DCUtils.printErrorUsingTaskLogger(m_zTaskLogger, "DCUtils.addArgumentsToTranslationRequest", serviceData);
        DCUtils.refreshObject(m_zTaskLogger, m_itemRevision);

        String itemID = m_itemRevision.get_item_id();
        String itemRevisionID = m_itemRevision.get_item_revision_id();
        String itemRevObjString = m_itemRevision.get_object_string();

        serviceData = DCUtils.getDataManagementService().getProperties(new ModelObject[] { m_dataset }, new String[] { DCConstants.PROP_NAME_OBJECT_NAME });
        DCUtils.printErrorUsingTaskLogger(m_zTaskLogger, "DCUtils.addArgumentsToTranslationRequest", serviceData);
        DCUtils.refreshObject(m_zTaskLogger, m_dataset);

        String srcDatasetName = m_dataset.get_object_name();

        listKeys.add(OPT_KEY_ITEM_ID);
        listKeys.add(OPT_KEY_ITEM_REVISION_ID);
        listKeys.add(OPT_KEY_SRC_DATASET_NAME);
        listKeys.add(OPT_KEY_OBJECT_STRING);

        listValues.add(itemID);
        listValues.add(itemRevisionID);
        listValues.add(srcDatasetName);
        listValues.add(itemRevObjString);

        for (int i = 0; i < listKeys.size(); i++)
        {
            System.out.println("Adding arguments");
            m_zTaskLogger.info("\n" + listKeys.get(i));
            m_zTaskLogger.info("\n" + listValues.get(i));

            if (listValues.get(i) == null || listValues.get(i).trim().equals(""))
            {
                /*
                 * Add a fixed string such as "DUMMY" when the option value is not properly
                 * filled
                 */
                
                DCUtils.addTranslationOption(m_zTaskLogger, m_translatorOptions, listKeys.get(i).trim().toString(), "DUMMY");             
            }
            else
            {
                /*
                 * Before adding the key and value trim them for any leading or
                 * trailing spaces for best results
                 */
                
                DCUtils.addTranslationOption(m_zTaskLogger, m_translatorOptions, listKeys.get(i).trim().toString(), listValues.get(i).trim().toString());
            }

        }
    }
}
