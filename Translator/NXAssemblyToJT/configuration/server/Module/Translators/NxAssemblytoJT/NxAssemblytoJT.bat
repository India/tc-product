@echo off

REM # ==================================================================== #
REM # SCRIPT TO RUN NXTOSTEP TRANSLATOR
REM # Parameter:
REM # %1: Input-File
REM # %2: Output-File
REM # %3: Input-Directory.
REM # %4: Output-Directory.
REM # %5: Item-ID
REM # %6: Item-Rev
REM # %7: Dataset-Name
REM # Example: <Translator>.bat test.in test.out 100070782 00 100070782
REM # ==================================================================== #
REM Add the translator name in service.properties, activate the translator definition in the translator.xml

set OLDPATH=%PATH%

setlocal

REM Environment variables required for this translation.

rem # Set TC_ROOT to the directory where Teamcenter is installed.
rem # Example set TC_ROOT=D:\Tceng2007
set TC_ROOT=D:\plm\tc11

rem # Set the teamcenter data location
rem # Example set TC_DATA=D:\Tc_Data
set TC_DATA=D:\plmshare\tcdata

rem # Set the FMS location
rem # Example set FMS_HOME=D:\Tceng2007\fcc
set FMS_HOME=D:\plm\tc11\tccs


rem # Set PERL_DIR to the directory where perl is installed.
rem # Example set PERL_DIR=C:\perl\bin
set PERL_DIR=%TC_ROOT%\perl
set SPLM_LICENSE_SERVER=28000@appsedvWS16.roeders.local
set UGS_LICENSE_SERVER=28000@appsedvWS16.roeders.local
REM !! Change to correct license server
set UGII_BASE_DIR=D:\plm\nx12
REM !! Change to correct NX path
set UPG_TRANS=-u=dcproxy -p=dcproxy -g=dba
REM !! Give ACL for the respective users definition
set MODULEBASE=D:\plm\dispatcher\Module
set UGII_CONFIG_FILE=%MODULEBASE%\Translators\NxAssemblytoJT\monolithic.config

@set PARAM1=%1

if x%PARAM1%== x goto help
if %1== -help goto help

goto setenv

:help
echo 	Usage:
echo    Translator executable name: %0
echo  	Arguments are:
echo    Param 1) Input  file name to translate with complete path.
echo    Param 2) Output file name to translate with complete path.
echo    Param 3) Full path to the input directory.
echo    Param 4) Full path to the output directory.
echo    Custom Param 5) Item ID from translation request.
echo    Custom Param 6) Item revision ID from translation request.
echo    Custom Param 7) Source Dataset name from translation request.
EXIT /B 1

:setenv
REM Check environment
if x"%TC_ROOT%"==x (
  echo set TC_ROOT
  EXIT /B 2
 )

if x"%TC_DATA%"==x (
  echo set TC_DATA
  EXIT /B 3
 )

if x"%SPLM_LICENSE_SERVER%"==x (
  echo set SPLM_LICENSE_SERVER value
  EXIT /B 4
 )
 
if x"%UGS_LICENSE_SERVER%"==x (
  echo set UGS_LICENSE_SERVER value
  EXIT /B 5
 )
 
if x"%UGII_BASE_DIR%"==x (
  echo set UGII_BASE_DIR Path
  EXIT /B 6
 )

if x"%SPLM_SHR_DIR%"==x (
  echo set SPLM_SHR_DIR Path
  EXIT /B 7
 )



REM Copy the arguments passed on to translator.
set SRCDIRPATH=%1
set DSTDIRPATH=%2
set SRCDIR=%3
set DSTDIR=%4
set ITEM_ID=%5
set ITEM_REV_ID=%6
set SRC_DATASET_NAME=%7

echo SRCDIRPATH %SRCDIRPATH%
echo DSTDIRPATH %DSTDIRPATH%
echo SRCDIR %SRCDIR%
echo DSTDIR %DSTDIR%
echo ITEM_ID %ITEM_ID%
echo ITEM_REV_ID %ITEM_REV_ID%
echo SRC_DATASET_NAME %SRC_DATASET_NAME%
echo UGII_BASE_DIR %UGII_BASE_DIR%
echo.

REM Settings required by the Translator
call "%TC_DATA%\tc_profilevars"
echo Start Translation
REM Call the translator to translate the source dataset
  echo Start Translation
  echo call %UGII_BASE_DIR%\PVTRANS\run_ugtopv.bat -pim=yes "@DB/%ITEM_ID%/%ITEM_REV_ID%" -honour_structure %UPG_TRANS% -noupdate -config="%UGII_CONFIG_FILE%"
  call %UGII_BASE_DIR%\PVTRANS\run_ugtopv.bat -pim=yes "@DB/%ITEM_ID%/%ITEM_REV_ID%" -honour_structure %UPG_TRANS% -noupdate -config="%UGII_CONFIG_FILE%"
  

REM Extract the jt file
echo
if not exist "%SRCDIR%\*.jt" echo "there is no jt file generated"
copy /y "%SRCDIR%\*.jt" %DSTDIR%

goto :endext

:noresult

echo ERROR - ERROR - ERROR

endlocal
EXIT /B 9

:endext
endlocal
set EXITVALUE=%ERRORLEVEL%
set PATH=%OLDPATH%
set OLDPATH=
cd /d %CURRDIR%
EXIT /B %EXITVALUE%